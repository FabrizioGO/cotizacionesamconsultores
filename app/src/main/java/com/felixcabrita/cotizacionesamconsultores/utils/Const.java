package com.felixcabrita.cotizacionesamconsultores.utils;

/**
 * Created by Joseris Hostienda on 11/01/2018.
 */

public class Const {
    //Desarrollo: http://ventas.novatecpe.com:8080/novatec_desarrollo/api/v1/
    //Pre-producción (Test): http://ventas.novatecpe.com:8080/novatec_test/api/v1/
    //Producción: http://ventas.novatecpe.com:8080/novatec/api/v1/
    public static final String URL_V1 = "http://ventas.novatecpe.com:8080/novatec/api/v1/";
    public static final String BASE_URL_PRODUCION="http://ventas.novatecpe.com:8080/novatec/api/v1/clientes/";
}

