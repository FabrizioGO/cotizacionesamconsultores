package com.felixcabrita.cotizacionesamconsultores.utils;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by Joseris on 16/01/2018.
 */

public class Preferences {

    private static SharedPreferences mSharedPref;
    private static String TAG_ruc="Tag_ruc";
    private static String TAG_key="Tag_key";
    private static String TAG_PREFERENCE="My_Preferences";
    public static void init(Context context)
    {
        if(mSharedPref == null){
            mSharedPref = context.getSharedPreferences(TAG_PREFERENCE, Activity.MODE_PRIVATE);
        }
    }
    public static void setRucClient(String value){
        SharedPreferences.Editor editor = mSharedPref.edit();
        editor.putString(TAG_ruc, value);
        editor.commit();
    }

    public static String getRucClient()    {
        return mSharedPref.getString(TAG_ruc, "");
    }

    public static void setKeyUser(String value){
        SharedPreferences.Editor editor = mSharedPref.edit();
        editor.putString(TAG_ruc, value);
        editor.apply();
    }

    public static String getkeyUser()    {
        return mSharedPref.getString(TAG_ruc, "");
    }

    public static void saveDireccionContacto(String direccion, String TAG)
    {
        SharedPreferences.Editor editor = mSharedPref.edit();
        editor.putString(TAG, direccion);
        editor.commit();
    }

    public static String loadDireccionContacto(String TAG)
    {
        String direccion;
        direccion = mSharedPref.getString(TAG,"");
        return direccion;
    }
    public static void saveDireccionLugarContacto(String direccion, String TAG)
    {
        SharedPreferences.Editor editor = mSharedPref.edit();
        editor.putString(TAG, direccion);
        editor.commit();
    }

    public static String loadDireccionLugarContacto(String TAG)
    {
        String direccion;
        direccion = mSharedPref.getString(TAG,"");
        return direccion;
    }
    public static void saveNombreContacto(String nombre, String TAG)
    {
        SharedPreferences.Editor editor = mSharedPref.edit();
        editor.putString(TAG, nombre);
        editor.commit();
    }

    public static String loadNombreContacto(String TAG)
    {
        String direccion;
        direccion = mSharedPref.getString(TAG,"");
        return direccion;
    }
    public static void saveCorreoContacto(String correo, String TAG)
    {
        SharedPreferences.Editor editor = mSharedPref.edit();
        editor.putString(TAG, correo);
        editor.commit();
    }

    public static String loadCorreoContacto(String TAG)
    {
        String correo;
        correo = mSharedPref.getString(TAG,"");
        return correo;
    }
    public static void saveNumeroFijoContacto(String numero, String TAG)
    {
        SharedPreferences.Editor editor = mSharedPref.edit();
        editor.putString(TAG, numero);
        editor.commit();
    }

    public static String loadNumeroFijoContacto(String TAG)
    {
        String numero;
        numero = mSharedPref.getString(TAG,"");
        return numero;
    }
    public static void saveNumeroMovilContacto(String numero, String TAG)
    {
        SharedPreferences.Editor editor = mSharedPref.edit();
        editor.putString(TAG, numero);
        editor.commit();
    }

    public static String loadNumeroMovilContacto(String TAG)
    {
        String numero;
        numero = mSharedPref.getString(TAG,"");
        return numero;
    }


    public static void saveCodPagoContacto(int numero, String TAG)
    {
        SharedPreferences.Editor editor = mSharedPref.edit();
        editor.putInt(TAG, numero);
        editor.commit();
    }

    public static int loadCodPagoContacto(String TAG)
    {
        int numero;
        numero = mSharedPref.getInt(TAG,0);
        return numero;
    }

    public static void saveCodVenta(int numero, String TAG)
    {
        SharedPreferences.Editor editor = mSharedPref.edit();
        editor.putInt(TAG, numero);
        editor.commit();
    }

    public static int loadCodVenta(String TAG)
    {
        int numero;
        numero = mSharedPref.getInt(TAG,0);
        return numero;
    }
    public static void savePosicion(int numero, String TAG)
    {
        SharedPreferences.Editor editor = mSharedPref.edit();
        editor.putInt(TAG, numero);
        editor.commit();
    }

    public static int loadPosiscion(String TAG)
    {
        int numero;
        numero = mSharedPref.getInt(TAG,0);
        return numero;
    }

    public static void saveCode(int numero, String TAG)
    {
        SharedPreferences.Editor editor = mSharedPref.edit();
        editor.putInt(TAG, numero);
        editor.commit();
    }

    public static int loadCode(String TAG)
    {
        int numero;
        numero = mSharedPref.getInt(TAG,0);
        return numero;
    }
}
