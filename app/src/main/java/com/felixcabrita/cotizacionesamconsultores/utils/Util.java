package com.felixcabrita.cotizacionesamconsultores.utils;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.support.v7.app.AlertDialog;
import android.util.Log;

import com.felixcabrita.cotizacionesamconsultores.R;

/**
 * Created by lreque on 23/08/17.
 */

public class Util {

    public static boolean isNetworkAvailable(Context context) {
        ConnectivityManager connectivityManager
                = (ConnectivityManager)  context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

    public static void mostrarMensaje(Activity actividad, String mensaje){
        Log.i("mensaje", mensaje);
        AlertDialog alertDialog = new AlertDialog.Builder(actividad).create();
        alertDialog.setTitle(actividad.getString(R.string.app_name));
        alertDialog.setMessage(mensaje);
        alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "OK",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
        alertDialog.show();
    }

    public static AlertDialog mostrarMensaje(final Activity actividad, String mensaje, String buttonText){
        Log.i("mensaje", mensaje);
        AlertDialog alertDialog = new AlertDialog.Builder(actividad).create();
        alertDialog.setTitle(actividad.getString(R.string.app_name));
        alertDialog.setMessage(mensaje);
        alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, buttonText,
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        actividad.finish();
                    }
                });
        return alertDialog;
    }
}
