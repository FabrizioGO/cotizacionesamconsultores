package com.felixcabrita.cotizacionesamconsultores;

import android.content.Context;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.preference.PreferenceManager;

import com.felixcabrita.cotizacionesamconsultores.utils.Const;

import org.json.JSONArray;
import org.json.JSONException;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

class SyncClientes extends AsyncTask<Void,Void,Void> {


    boolean internet = true;

    private BaseDatosSqlite cnn;
    private String urls = Const.URL_V1 + "clientes";
    private Context c;
    private SharedPreferences pref ;
    public SyncClientes(Context c) {
        cnn = new BaseDatosSqlite(c);
        pref = PreferenceManager.getDefaultSharedPreferences(c);
        this.c = c;
    }


    @Override
    protected void onPreExecute() {
        super.onPreExecute();

    }

    @Override
    protected Void doInBackground(Void... voids) {while (internet){
            ConnectivityManager connMgr = (ConnectivityManager)
                    c.getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();
            if (networkInfo != null && networkInfo.isConnected()) {
                if(cnn.DataSync("clientes")<1) {
                    try {
                        URL cliente = new URL(urls);
                        HttpURLConnection conCliente = (HttpURLConnection) cliente.openConnection();
                        conCliente.setRequestProperty("x-api-key", pref.getString("key", ""));
                        if (conCliente.getResponseCode() == HttpURLConnection.HTTP_OK) {
                            InputStream inputStreamResponse = conCliente.getInputStream();
                            String linea;
                            StringBuilder respuestaCadena = new StringBuilder();
                            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStreamResponse, "UTF-8"));
                            while ((linea = bufferedReader.readLine()) != null) {
                                respuestaCadena.append(linea);
                            }
                            // JSONObject datos = new JSONObject(respuestaCadena.toString());
                            JSONArray jsonArray = new JSONArray(respuestaCadena.toString());
                            cnn.CapturaDatosClientesJSONV2(jsonArray);
                            System.out.println("ACTUALIZANDO CLIENTES..");
                        }
                    } catch (IOException e) {
                        System.out.println("Error1: " + e.toString());
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
                System.out.println("Sync Clientes Activa..");
            } else {
                internet = false;
            }
        }
        return  null;
    }

    @Override
    protected void onPostExecute(Void aVoid) {
        super.onPostExecute(aVoid);
    }
}