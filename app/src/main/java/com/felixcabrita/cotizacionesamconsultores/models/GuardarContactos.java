package com.felixcabrita.cotizacionesamconsultores.models;

import android.os.AsyncTask;

import com.felixcabrita.cotizacionesamconsultores.BaseDatosSqlite;
import com.felixcabrita.cotizacionesamconsultores.net.model.responses.AllContacts;

import java.util.ArrayList;

public class GuardarContactos extends AsyncTask<Void, Void, Integer> {

    private BaseDatosSqlite control;
    private ArrayList<AllContacts> list;
    private String ruc;
    private int estado;

    public GuardarContactos(BaseDatosSqlite control, ArrayList<AllContacts> list, String ruc, int estado){
        this.control = control;
        this.list = list;
        this.ruc = ruc;
        this.estado = estado;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
    }

    @Override
    protected void onPostExecute(Integer integer) {
        super.onPostExecute(integer);
    }

    @Override
    protected Integer doInBackground(Void... voids) {
        control.deleteContactoClientes(ruc);
        control.saveClienteContact(ruc,list,estado);
        return 0;
    }
}