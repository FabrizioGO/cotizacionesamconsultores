package com.felixcabrita.cotizacionesamconsultores.models;

/**
 * Creado por Felix Cabrita en 27/6/2017.
 */

public class Tb_Cotizaciones{
    public static String nombreTabla = "cotizaciones";
    public static String codigo = "coti_codigo";
    public static String fecha = "coti_fecha";
    public static String fecha_registro ="coti_fecha_registo";
    public static String nombre = "coti_nombre";
    public static String cliente = "coti_cliente";
    public static String vendedor ="coti_vendedor";
    public static String cond_pago = "coti_cond_venta";
    public static String cond_venta = "coti_cond_pago";
    public static String flete = "coti_flete";
    public static String tiempo_validez  = "coti_tiempo_validez";
    public static String lugar_cotizacion  ="coti_ligar";
    public static String longitud  = "coti_logitud";
    public static String latirud = "coti_latitud";
    public static String moneda = "coti_moneda";
    public static String subtotal = "coti_subtotal";
    public static String monto_igv = "coti_monto";
    public static String monto_total = "coti_monto_total";
    public static String observaciones = "coti_observ";
    public static String icotem = "coti_icoten";
    public static String lugar = "coti_lugar";
    public static String por_revision = "coti_por_revicion";
    public static String estado  = "coti_estado";
    public static String estadoSynco = "sync";
    public static String porcentaje = "coti_porcentaje";
    public static String items  = "coti_items";
    public static String lugar_direccion = "lugar_direcion";

}
