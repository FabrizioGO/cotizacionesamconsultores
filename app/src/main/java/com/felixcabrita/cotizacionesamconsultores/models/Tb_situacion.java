package com.felixcabrita.cotizacionesamconsultores.models;

/**
 * Created by Fabrizio Gagliardi on 19/2/2018.
 */

public class Tb_situacion {
    public static String nombreTabla = "situaciones";

    public static String fechaDoc = "Fecha_Docmto";
    public static String docmto = "Docmto";
    public static String vcSerie = "vc_serie";
    public static String vcNroDoc = "vc_nrodocumento";
    public static String codVta = "CODVTA";
    public static String fechaVenc = "Fecha_Vecnto";
    public static String ubicacion = "Ubicacion";
    public static String situacion = "situacion";
    public static String mnd = "Mnd";
    public static String totalDeuda = "Total_Deuda";
    public static String amortizado = "Amortizado";
    public static String saldo = "Saldo";
    public static String vendedor = "VENDEDOR";
    public static String clave = "CLAVE";
    public static String ccbCordCom = "CCB_CORDCOM";
    public static String TD = "TD";
    public static String Documento = "DOCUMENTO";
    public static String vcDescripcion = "VC_Descrip";
    public static String vcRuc = "VC_Ruc";
}
