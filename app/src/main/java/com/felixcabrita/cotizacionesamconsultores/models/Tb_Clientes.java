package com.felixcabrita.cotizacionesamconsultores.models;

/**
 * Creado por Felix Cabrita en 27/6/2017.
 */

public class Tb_Clientes {

    public static String nombreTabla = "clientes";
    public static String razon_social = "clie_razonsocial";
    public static String nombre_comercial = "clie_nombrecomercial";
    public static String nombres = "clie_nombres";
    public static String apellidos = "clie_apellidos";
    public static String ruc = "clie_ruc";
    public static String current_ruc = "clie_current_ruc";
    public static String contacto = "clie_contacto";
    public static String direccion = "clie_direccion";
    public static String vendedor  = "clie_vendedor";
    public static String telefono_movil = "clie_telemovil";
    public static String telefono_fijo = "clie_telefijo";
    public static String tipoExtaNa = "clie_tipoExtraNa";
    public static String correo = "clie_correo";
    public static String zona = "clie_zona";
    public static String cond_pago = "clie_pago";
    public static String cond_venta = "clie_venta";
    public static String perceptor = "clie_perceptor";
    public static String por_perceptor = "clie_porperceptor";
    public static String retenedor = "clie_retenedor";
    public static String calificacion = "clie_calificacion";
    public static String visita_cotizacion = "clie_visita";
    public static String estado = "clie_estado";
    public static String rubro = "clie_rubro";
    public static String sector = "clie_sector";
    public static String tiponegocio = "clie_tipo_negocio";
    public static String code = "code";
}

