package com.felixcabrita.cotizacionesamconsultores.models;

/**
 * Created by Fabrizio Gagliardi on 18/2/2018.
 */

public class Tb_pedidos {

    public static final String nombreTabla = "pedidos";

    public static final String pedido = "pedido";
    public static final String fecha = "fecha";
    public static final String tipoPedido = "tipo_pedido";
    public static final String tipoPago = "tipo_pago";
    public static final String cliente = "cliente";
    public static final String ruc = "ruc";
    public static final String direccion = "direccion";
    public static final String montoNeto = "monto_neto";
    public static final String montoIgv = "monto_igv";
    public static final String montoTotal = "monto_total";
    public static final String tipoMoneda = "tipo_moneda";
    public static final String percepcion = "percepcion";
}
