package com.felixcabrita.cotizacionesamconsultores.models;

/**
 * Creado por Felix Cabrita en 27/6/2017.
 */

public class Tb_Productos {

    public static String nombreTabla = "productos";
    public static String codigo = "prod_codigo";
    public static String nombre = "prod_nombre";
    public static String moneda = "prod_moneda";
    public static String tamanio = "prod_tamanio";
    public static String peso = "prod_peso";
    public static String volumen = "prod_volumen";
    public static String stock = "prod_stock";
    public static String comprometida = "prod_comprometida";
    public static String disponible = "prod_disponible";
    public static String material = "prod_material";
    public static String colores = "prod_colores";
    public static String precio = "prod_precio";
    public static String igv = "prod_igv";
    public static String total = "prod_total";
    public static String descripcion = "prod_descripcion";
    public static String ficha_tecnica = "prod_ficha_tecnica";
    public static String foto = "prod_foto";
    public static String cunio = "prod_cunio";
    public static String rango_menor = "prod_rango_menor";

}


