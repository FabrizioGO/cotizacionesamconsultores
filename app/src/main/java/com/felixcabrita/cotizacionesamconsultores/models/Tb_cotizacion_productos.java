package com.felixcabrita.cotizacionesamconsultores.models;

/**
 * Creado por Felix Cabrita en 27/6/2017.
 */

public class Tb_cotizacion_productos {

    public static String nombreTabla = "cotizacion_productos";
    public static String cotizacion  = "copr_cotizacion";
    public static String producto= "copr_producto";
    public static String producto_nombre= "copr_producto_nombre";
    public static String idproduc= "copr_id";
    public static String cantidad = "copr_cantidad";
    public static String precio = "copr_precio";
    public static String subtotal = "copr_subtotal";
    public static String igv = "copr_igv";
    public static String action = "copr_action";
    public static String total = "copr_total";
    public static String cunio = "copr_cunio";
    public static String produccion = "copr_produccion";
    public static String requerido = "copr_requerido";
    public static String material = "copr_material";
    public static String color = "copr_color";
}

