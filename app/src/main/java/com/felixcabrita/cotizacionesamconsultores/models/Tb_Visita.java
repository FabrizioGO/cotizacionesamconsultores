package com.felixcabrita.cotizacionesamconsultores.models;

/**
 * Creado por Felix Cabrita en 14/7/2017.
 */

public class Tb_Visita {

    public static String nombreTabla = "visitas";
    public static String cliente = "visi_cliente";
    public static String codigo = "visi_codigo";
    public static String fecha_paraEnviar ="visi_paraEnviar";
    public static String fecha_inicio = "visi_fechaI";
    public static String fecha_proxima = "visi_fechaP";
    public static String fecha_final = "visi_fechaf";
    public static String latitud_inicio = "visi_latitudI";
    public static String longitud_inicio = "visi_longitudI";
    public static String latitud_final = "visi_latitudF";
    public static String longitud_final = "visi_longitudF";
    public static String motivo  = "visi_motivo";
    public static String pendiente = "visi_pendiente";
    public static String direccion = "visi_direccion";
    public static String estado = "visi_estado";
}

