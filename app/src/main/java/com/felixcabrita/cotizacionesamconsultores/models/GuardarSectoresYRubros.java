package com.felixcabrita.cotizacionesamconsultores.models;

import android.content.ContentValues;
import android.os.AsyncTask;

import com.felixcabrita.cotizacionesamconsultores.BaseDatosSqlite;

import java.util.ArrayList;

/*
 * Created by Fabrizio Gagliardi on 24/4/2018.
 */

public class GuardarSectoresYRubros extends AsyncTask<Void, Void, Integer> {

    private BaseDatosSqlite control;
    private ArrayList<ContentValues> ListSectores;
    private ArrayList<ContentValues> ListRubros;

    public GuardarSectoresYRubros(BaseDatosSqlite control, ArrayList<ContentValues> listSectores, ArrayList<ContentValues> listRubros){
        this.control = control;
        this.ListSectores = listSectores;
        this.ListRubros = listRubros;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
    }

    @Override
    protected void onPostExecute(Integer integer) {
        super.onPostExecute(integer);
    }

    @Override
    protected Integer doInBackground(Void... voids) {

        control.vaciarSectoresYRubros();

        for (ContentValues value : ListRubros) {
            control.insertRubros(value);
        }

        for (ContentValues value : ListSectores) {
            control.insertSectores(value);
        }

        return 0;
    }
}
