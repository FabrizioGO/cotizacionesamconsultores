package com.felixcabrita.cotizacionesamconsultores.models;

/***
 * Created by Fabrizio Gagliardi on 17/2/2018.
 */

public class Tb_contactos {

    public static String nombreTabla = "clientes_contacto";
    public static String cliente = "cliente";
    public static String tipo = "tipo";
    public static String descripcion = "descripcion";
    public static String contacto = "contacto";
    public static String direccion = "direccion";
    public static String correo = "correo";
    public static String telefonoFijo = "telefono_fijo";
    public static String telefonoMovil = "telefono_movil";
    public static String zona = "zona";
    public static String vendedor = "vendedor";
    public static String condVenta = "cond_venta";
    public static String condPago = "cond_pago";
    public static String estado = "estado";
}
