package com.felixcabrita.cotizacionesamconsultores.models;

/**
 * Creado por Felix Cabrita en 3/7/2017.
 */

public class Tb_Usuarios {

    public static String tb_nombreTabla ="usuarios";
    public static String tb_codigo = "usua_codigo";
    public static String tb_nombre = "usua_nombre";
    public static String tb_apellidos = "usua_apellidos";
    public static String tb_tipoUsuario = "usua_tipo";
    public static String tb_correo = "usua_correo";
    public static String tb_telefijo = "usua_telefijo";
    public static String tb_telemovil = "usua_telemovil";
    public static String tb_pw = "usua_pw";
    public static String tb_estado = "usua_estado";

}
