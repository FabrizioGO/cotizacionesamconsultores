package com.felixcabrita.cotizacionesamconsultores.models;

/**
 * Creado por Felix Cabrita en 20/7/2017.
 */

public class Tb_EstadoCotizacion {
    public static String nombreTabla = "estadoCotizcion";
    public static String codigo = "estcot_codigo";
    public static String estado  = "estcot_estado";
    public static String estadoSynco = "sync";
}
