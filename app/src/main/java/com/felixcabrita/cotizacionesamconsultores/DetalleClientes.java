package com.felixcabrita.cotizacionesamconsultores;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.app.TimePickerDialog;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.database.sqlite.SQLiteDatabase;
import android.location.Location;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.NavigationView;
import android.support.v4.app.ActivityCompat;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.InputFilter;
import android.text.InputType;
import android.text.Spanned;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.felixcabrita.cotizacionesamconsultores.activity.ActivityHistorialPedidos;
import com.felixcabrita.cotizacionesamconsultores.activity.ActivityListContactos;
import com.felixcabrita.cotizacionesamconsultores.activity.ActivitySituacionActual;
import com.felixcabrita.cotizacionesamconsultores.models.GuardarContactos;
import com.felixcabrita.cotizacionesamconsultores.models.GuardarSectoresYRubros;
import com.felixcabrita.cotizacionesamconsultores.net.RetrofitAdapter;
import com.felixcabrita.cotizacionesamconsultores.net.ServicesContact;
import com.felixcabrita.cotizacionesamconsultores.net.model.responses.AllContacts;
import com.felixcabrita.cotizacionesamconsultores.net.model.responses.Visita;
import com.felixcabrita.cotizacionesamconsultores.utils.Const;
import com.felixcabrita.cotizacionesamconsultores.utils.Preferences;
import com.felixcabrita.cotizacionesamconsultores.utils.Util;
import com.felixcabrita.cotizacionesamconsultores.models.Tb_Clientes;
import com.felixcabrita.cotizacionesamconsultores.models.Tb_Visita;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationServices;
import com.google.gson.Gson;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.regex.Pattern;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;

public class DetalleClientes extends AppCompatActivity implements View.OnClickListener, NavigationView.OnNavigationItemSelectedListener, GoogleApiClient.OnConnectionFailedListener, GoogleApiClient.ConnectionCallbacks {

    ServicesContact api;
    Call<List<AllContacts>> call;
    ArrayList<String> ListContactos = new ArrayList<>();
    ArrayList<ContentValues> ListSectores = new ArrayList<>();
    ArrayList<ContentValues> ListRubros = new ArrayList<>();
    ArrayList<AllContacts> ListContactosDestalle = new ArrayList<>();
    public ArrayList<ContentValues> condiVenta;
    ProgressDialog progressDoalog;
    String idVenta, idPago;

    @BindView(R.id.spinner_contactos)
    Spinner spinner;
    @BindView(R.id.calificacion)
    Spinner calificacion;
    @BindView(R.id.condi_venta)
    Spinner condi_venta;
    @BindView(R.id.zona)
    Spinner zona;
    @BindView(R.id.rubro)
    Spinner rubro11;

    @BindView(R.id.nombres)
    EditText nombre;
    @BindView(R.id.ruc)
    EditText ruc;
    @BindView(R.id.contacto)
    EditText contacto;
    @BindView(R.id.direccion)
    EditText direccion;
    @BindView(R.id.fijo)
    EditText telfijo;
    @BindView(R.id.movil)
    EditText telmovil;
    @BindView(R.id.por_per)
    EditText por;
    @BindView(R.id.nombreComercial)
    EditText nombreComercial;
    @BindView(R.id.correo)
    EditText correo;

    @BindView(R.id.condi_pago)
    TextView condPago;
    @BindView(R.id.sinMovil)
    TextView ntMovil;
    @BindView(R.id.sinFijo)
    TextView ntFijo;
    @BindView(R.id.sinCorreo)
    TextView ntCorreo;
    @BindView(R.id.text_ircontacto)
    TextView text_ircontacto;
    @BindView(R.id.sector)
    TextView sector;

    //@BindView(R.id.tipo)
    //RadioGroup nacionalidad;
    @BindView(R.id.tipoEmpresa)
    RadioGroup tipoEmpresa;
    @BindView(R.id.juridica)
    RadioButton juridica;
    @BindView(R.id.natural)
    RadioButton natural;
    @BindView(R.id.extrangero)
    RadioButton Extranjero;
    @BindView(R.id.nacional)
    RadioButton nacional;

    @BindView(R.id.perceptor)
    CheckBox perceptor;
    @BindView(R.id.retenedor)
    CheckBox retenedor;

    @BindView(R.id.view_per)
    LinearLayout view_por;
    @BindView(R.id.nacionalL)
    LinearLayout nacionalLa;

    BaseDatosSqlite control;
    CondicionVentaAdapter adapterVentas;
    
    int creacion = 0;
    String rucS = "";
    ContentValues cliente;
    //ArrayAdapter pagoAdapter,pagoAdapter1;
    int statuspantalla = 0, condipago = 0, condiventa = 0;
    int r = 0, p = 0, v = 0, t = 0, Idsector;
    private String Latitudi = "0";
    private String Longitudi = "0";
    private String Latitudf = "0";
    private String Longitudf = "0";
    private int codeContacto = 0;
    GoogleApiClient apiClient;

    Boolean estado = false;
    SharedPreferences pref;

    private SharedPreferences.Editor editor;
    int ntM = 0, ntF = 0, ntC = 0;
    Menu menee;
    boolean proxima;
    private boolean isInitCV;

    int maxLenRuc = 11;
    String current_ruc ="",idid ="";

    int successCount;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_principal_detalles_clientes);
        ButterKnife.bind(this);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);

        progressDoalog =  new ProgressDialog(this);
        progressDoalog.setIndeterminate(true);
        progressDoalog.setMessage("Cargando Datos");
        progressDoalog.setCancelable(true);

        successCount = 0;

        control = new BaseDatosSqlite(this);

        text_ircontacto.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), ActivityListContactos.class);
                intent.putExtra("ruc",current_ruc);
                startActivity(intent);
            }
        });
        creacion = getIntent().getExtras().getInt("creacion");
        try{
            estado =  getIntent().getExtras().getBoolean("estado");
        }catch (Exception e){
            estado = true;
        }

        if (estado &&  !getIntent().getExtras().getString(Tb_Clientes.ruc).equalsIgnoreCase("")){
            Util.mostrarMensaje(this,"Este cliente no esta sincronizado. No aparecerán los contactos de este cliente hasta que sincronice con la web");
        }

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        if (creacion == 1) {
            toolbar.setTitle("Crear cliente");
        }
        apiClient = new GoogleApiClient.Builder(this)
                .enableAutoManage(this, this)
                .addConnectionCallbacks(this)
                .addApi(LocationServices.API)
                .build();
        setSupportActionBar(toolbar);

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        pref = PreferenceManager.getDefaultSharedPreferences(this);
        editor = pref.edit();
        navigationView.setNavigationItemSelectedListener(this);
        SharedPreferences pref = PreferenceManager.getDefaultSharedPreferences(this);
        View headerView = navigationView.getHeaderView(0);
        TextView tvHeaderName = (TextView) headerView.findViewById(R.id.usuario);
        tvHeaderName.setText(pref.getString("nombre", "usuario"));

        perceptor.setChecked(true);
        retenedor.setChecked(false);
        view_por.setVisibility(View.VISIBLE);

        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                codeContacto = ListContactosDestalle.get(position).getItem();
                Log.d("spinner","-->"+position);
                Log.d("spininer","--> " + codeContacto);
                actualizarInterfz(position);

            }

            @Override
            public void onNothingSelected(AdapterView<?> parentView) {
                // your code here
            }

        });

        final LocationManager manager = (LocationManager) getSystemService( Context.LOCATION_SERVICE );
        if ( !manager.isProviderEnabled( LocationManager.GPS_PROVIDER ) ) {
            AlertNoGps();
        }

        Extranjero.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ruc.setBackgroundResource(R.drawable.edittex);
                ruc.setEnabled(true);
                maxLenRuc = 14;
                ruc.setFilters(new InputFilter[]{filter,new InputFilter.LengthFilter(maxLenRuc)});
                ruc.setInputType(InputType.TYPE_CLASS_TEXT);
                nacionalLa.setVisibility(View.GONE);

            }
        });
        nacional.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ruc.setBackgroundResource(R.drawable.edittex);
                ruc.setEnabled(true);
                ruc.setInputType(InputType.TYPE_CLASS_NUMBER);
                if(juridica.isChecked()){
                    maxLenRuc = 11;
                    ruc.setFilters(new InputFilter[]{filter,new InputFilter.LengthFilter(maxLenRuc)});
                }else{
                    ruc.setFilters(new InputFilter[]{filter,new InputFilter.LengthFilter(maxLenRuc)});
                    maxLenRuc = 8;
                }
                nacionalLa.setVisibility(View.VISIBLE);
            }
        });

        perceptor.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                p = perceptor.isChecked() ? 1 : 0;

                if (r == 1) {
                    retenedor.setChecked(true);
                } else {
                    retenedor.setChecked(false);
                }
                if (p == 1) {
                    perceptor.setChecked(true);
                } else {
                    perceptor.setChecked(false);
                }
                if (r == 0 && p == 1) {
                    view_por.setVisibility(View.VISIBLE);
                    por.setText("0.5");
                }

                if (r == 0 && p == 0) {
                    view_por.setVisibility(View.VISIBLE);
                    por.setText("2");
                }
                if (r == 1 && p == 0) {
                    view_por.setVisibility(View.GONE);
                    por.setText("0");
                }

                if (r == 1 && p == 1) {
                    view_por.setVisibility(View.GONE);
                    por.setText("0");
                }
            }
        });



        retenedor.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                r = retenedor.isChecked() ? 1 : 0;

                if (r == 1) {
                    retenedor.setChecked(true);
                } else {
                    retenedor.setChecked(false);
                }

                if (p == 1) {
                    perceptor.setChecked(true);
                } else {
                    perceptor.setChecked(false);
                }

                if (r == 0 && p == 1) {
                    view_por.setVisibility(View.VISIBLE);
                    por.setText("0.5");
                }

                if (r == 0 && p == 0) {
                    view_por.setVisibility(View.VISIBLE);
                    por.setText("2");
                }
                if (r == 1 && p == 0) {
                    view_por.setVisibility(View.GONE);
                    por.setText("0");
                }

                if (r == 1 && p == 1) {
                    view_por.setVisibility(View.GONE);
                    por.setText("0");
                }
            }
        });

        /*natural.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                ruc.setInputType(InputType.TYPE_CLASS_NUMBER);
                if (b){
                    ruc.setBackgroundResource(R.drawable.edittex);
                    ruc.setEnabled(true);
                    retenedor.setChecked(false);
                    perceptor.setChecked(true);
                    retenedor.setEnabled(false);
                    perceptor.setEnabled(false);
                    r = 0;
                    p = 1;
                    view_por.setVisibility(View.VISIBLE);
                    por.setText("2");

                    ruc.setFilters(new InputFilter[]{filter,new InputFilter.LengthFilter(maxLenRuc)});
                    maxLenRuc = 8;
                } else {
                    retenedor.setChecked(false);
                    perceptor.setChecked(false);
                    por.setText("2");
                    perceptor.setEnabled(true);
                    retenedor.setEnabled(true);
                }
            }
        });*/
        natural.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ruc.setInputType(InputType.TYPE_CLASS_NUMBER);
                boolean b = natural.isChecked();
                if (b) {
                    ruc.setBackgroundResource(R.drawable.edittex);
                    ruc.setEnabled(true);
                    //ruc.setM
                    retenedor.setChecked(!b);
                    perceptor.setChecked(b);
                    retenedor.setEnabled(false);
                    perceptor.setEnabled(false);
                    r = 0;
                    p = 1;
                    view_por.setVisibility(View.VISIBLE);
                    por.setText("2");
                } else {
                    retenedor.setChecked(b);
                    perceptor.setChecked(b);
                    por.setText("2");
                    perceptor.setEnabled(true);
                    retenedor.setEnabled(true);
                }
                if(juridica.isChecked()){
                    maxLenRuc = 11;
                    ruc.setFilters(new InputFilter[]{filter,new InputFilter.LengthFilter(maxLenRuc)});
                }else{
                    ruc.setFilters(new InputFilter[]{filter,new InputFilter.LengthFilter(maxLenRuc)});
                    maxLenRuc = 8;
                }
            }
        });

        juridica.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ruc.setInputType(InputType.TYPE_CLASS_NUMBER);
                boolean b = natural.isChecked();
                retenedor.setChecked(b);
                perceptor.setChecked(b);
                ruc.setBackgroundResource(R.drawable.edittex);
                ruc.setEnabled(true);

                por.setText("2");
                perceptor.setEnabled(true);
                retenedor.setEnabled(true);
                if(juridica.isChecked()){
                    maxLenRuc = 11;
                    ruc.setFilters(new InputFilter[]{filter,new InputFilter.LengthFilter(maxLenRuc)});
                }else{
                    ruc.setFilters(new InputFilter[]{filter,new InputFilter.LengthFilter(maxLenRuc)});
                    maxLenRuc = 8;
                }
            }
        });

        condi_venta.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int position, long l) {
                if (!isInitCV) {
                    if (condi_venta.getSelectedItem().toString().equalsIgnoreCase("Contado")) {
                        Log.e("pago_text_cv_lis"," "+adapterView.getItemAtPosition(position));
                        condPago.setText((String) adapterView.getItemAtPosition(position));
                        idPago = "18";
                    } else {
                        Log.e("pago_text_cv_cont","FACTURA 15 DIAS");
                        condPago.setText("FACTURA A 15 DIAS");
                        idPago = "2";
                    }
                }
                isInitCV = false;
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
            }
        });

        condPago.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String cv = condi_venta.getSelectedItem().toString();
                if(cv.compareToIgnoreCase("Crédito")==0 || cv.compareToIgnoreCase("Credito")==0){
                    ListadoCondicionPago();
                }else{
                    idPago = "18";
                }
            }
        });

        ntMovil.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Mov(true);

            }
        });
        ntFijo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Fij(true);
            }
        });
        ntCorreo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Corr(true);
            }
        });

        statuspantalla = creacion;
        if (creacion == 0){
            rucS = getIntent().getExtras().getString(Tb_Clientes.ruc);
            current_ruc = rucS;
        }else{
            idid = String.valueOf(control.GetIdCliente() +1);
            if(nacional.isChecked()){
                if(natural.isChecked()){
                    maxLenRuc = 8;
                }else{
                    maxLenRuc = 11;
                }
                ruc.setFilters(new InputFilter[]{filter,new InputFilter.LengthFilter(maxLenRuc)});
                ruc.setInputType(InputType.TYPE_CLASS_NUMBER);
            }else{
                maxLenRuc = 14;
                ruc.setFilters(new InputFilter[]{filter,new InputFilter.LengthFilter(maxLenRuc)});
                ruc.setInputType(InputType.TYPE_CLASS_TEXT);
            }

            spinner.setVisibility(View.GONE);
            contacto.setVisibility(View.VISIBLE);
        }

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {

            if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                    Manifest.permission.ACCESS_FINE_LOCATION)) {
                // Aquí muestras confirmación explicativa al usuario
                // por si rechazó los permisos anteriormente
            } else {
                ActivityCompat.requestPermissions(
                        this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                        1);
            }
        }
    }

    private void setDatosCliente(){
        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                if (creacion == 0) {
                    try{
                        nombre.setBackgroundResource(R.drawable.edittex_enable);
                        ruc.setBackgroundResource(R.drawable.edittex_enable);

                        nombreComercial.setBackgroundResource(R.drawable.edittex_enable);
                        nombre.setEnabled(false);
                        ruc.setEnabled(false);
                        nombreComercial.setEnabled(false);
                        cliente = control.GetCliente(rucS);
                        codeContacto = Integer.parseInt(cliente.get(Tb_Clientes.code).toString());
                        idid = cliente.get("id").toString();
                        nombre.setText(cliente.get(Tb_Clientes.razon_social).toString());
                        ruc.setText(cliente.get(Tb_Clientes.ruc).toString());
                        current_ruc = ruc.getText().toString();
                        contacto.setText(cliente.get(Tb_Clientes.contacto).toString());
                        telfijo.setText(cliente.get(Tb_Clientes.telefono_fijo).toString());
                        direccion.setText(cliente.get(Tb_Clientes.direccion).toString());
                        telmovil.setText(cliente.get(Tb_Clientes.telefono_movil).toString());
                        int ExttipoNa =getNumberI(cliente.get(Tb_Clientes.tipoExtaNa).toString(),1);
                        Log.e("Sector ID",control.GetSector(cliente.getAsString(Tb_Clientes.sector)));
                        sector.setText(control.GetSector(cliente.getAsString(Tb_Clientes.sector)));
                        Idsector =cliente.getAsInteger(Tb_Clientes.sector);
                        rubro11.setSelection(cliente.getAsInteger(Tb_Clientes.rubro));

                        if(ExttipoNa==1){
                            nacional.setChecked(true);
                            nacionalLa.setVisibility(View.VISIBLE);
                            maxLenRuc = 14;
                            ruc.setFilters(new InputFilter[]{filter,new InputFilter.LengthFilter(maxLenRuc)});
                        }else{
                            nacional.setChecked(false);
                            Extranjero.setChecked(true);
                            nacionalLa.setVisibility(View.GONE);
                        }

                        r = getNumberI(cliente.get(Tb_Clientes.retenedor).toString(),0);
                        p = getNumberI(cliente.get(Tb_Clientes.perceptor).toString(),0);
                        t = getNumberI(cliente.get(Tb_Clientes.tiponegocio).toString(),0);
                        //   Toast.makeText(this,"Juridico(1)-Natural(2): " + t + "\nPerceptor: " + p + "\nReceptor: " + r,Toast.LENGTH_SHORT ).show();

                        try {
                            condiventa = Integer.parseInt(cliente.get(Tb_Clientes.cond_venta).toString()) - 1;
                            if (condiventa >= 0 && condiventa < 2) {
                                isInitCV = true;
                                condi_venta.setSelection(condiventa);
                            }
                            System.out.println("Condicion Venta: " + condiventa);
                        } catch (ArrayIndexOutOfBoundsException e) {
                            v = 0;
                            condiventa = 0;
                            System.out.println("Err Pago: " + condiventa + " Condicion Venta: " + v);
                        }

                        try {
                            //condipago = Integer.parseInt(cliente.get(Tb_Clientes.cond_pago).toString()) - 1;
                            idPago = cliente.get(Tb_Clientes.cond_pago).toString();
                            System.out.println("Condicion Pago: " + idPago);
                            if (idPago.trim().equalsIgnoreCase("18")) {
                                Log.e("pago_text_create","Contado");
                                condPago.setText("Contado");
                            }else {
                                Log.e("pago_text_getCondPago"," "+control.GetCondicionPago(idPago));
                                String condPagoText = control.GetCondicionPago(idPago);
                                condPago.setText(condPagoText);
                            }

                        } catch (ArrayIndexOutOfBoundsException e) {
                            v = 0;
                            condipago = 0;
                            System.out.println("Err Pago: " + condipago + " Condicion Venta: " + v);
                        }

                        try {
                            v = Integer.parseInt(cliente.get(Tb_Clientes.zona).toString()) - 1;
                            zona.setSelection(v);
                            System.out.println("Condicion Pago: " + condipago + " Zona: " + v);
                        } catch (Exception e) {
                            v = 0;
                            System.out.println("Err Pago: " + condipago + " Condicion Venta: " + v);
                        }
                        int calif = Integer.parseInt(cliente.get(Tb_Clientes.calificacion).toString());
                        nombreComercial.setText(cliente.get(Tb_Clientes.nombre_comercial).toString());
                        calificacion.setSelection(calif);
                        correo.setText(cliente.get(Tb_Clientes.correo).toString());

                        ntC = correo.getText().toString().isEmpty()?0:1;
                        ntF = telfijo.getText().toString().isEmpty()?0:1;
                        ntM = telmovil.getText().toString().isEmpty()?0:1;
                        Mov(false);
                        Fij(false);
                        Corr(false);
                        if (r == 1) {
                            retenedor.setChecked(true);
                        }
                        if (p == 1) {
                            perceptor.setChecked(true);
                        }

                        if (t == 2) {
                            natural.setChecked(true);
                            if (r == 0 && p == 1) {
                                view_por.setVisibility(View.VISIBLE);
                                por.setText("2");
                            }
                            if (natural.isChecked()) {
                                maxLenRuc = 8;
                                ruc.setFilters(new InputFilter[]{filter,new InputFilter.LengthFilter(maxLenRuc)});
                                retenedor.setChecked(!natural.isChecked());
                                perceptor.setChecked(natural.isChecked());
                                retenedor.setEnabled(false);
                                perceptor.setEnabled(false);
                                r = 0;
                                p = 1;
                            } else {
                                retenedor.setEnabled(true);
                            }
                        } else {
                            retenedor.setEnabled(true);
                            perceptor.setEnabled(true);
                            if (r == 0 && p == 1) {
                                retenedor.setChecked(false);
                                perceptor.setChecked(true);
                                view_por.setVisibility(View.VISIBLE);
                                por.setText("0.5");
                            }

                            if (r == 0 && p == 0) {
                                retenedor.setChecked(false);
                                perceptor.setChecked(false);
                                view_por.setVisibility(View.VISIBLE);
                                por.setText("2");
                            }
                            if (r == 1 && p == 0) {
                                view_por.setVisibility(View.GONE);
                                por.setText("0");
                                retenedor.setChecked(true);
                                perceptor.setChecked(false);
                            }

                            if (r == 1 && p == 1) {
                                retenedor.setChecked(true);
                                perceptor.setChecked(true);
                                view_por.setVisibility(View.GONE);
                                por.setText("0");
                            }
                            juridica.setChecked(true);
                            maxLenRuc = 11;
                            ruc.setFilters(new InputFilter[]{filter,new InputFilter.LengthFilter(maxLenRuc)});

                        }
                        int position = 0;
                        for (int i = 0; i < ListContactosDestalle.size(); i++) {
                            if (ListContactosDestalle.get(i).getItem() == codeContacto){
                                position = i;
                            }
                        }
                        actualizarInterfz(position);
                    }catch (Exception e){
                        Toast.makeText(DetalleClientes.this,"El cliente que quiere escoger tiene errores de datos",Toast.LENGTH_SHORT).show();
                        Intent i2 = new Intent(DetalleClientes.this, ListaClientes.class);
                        startActivity(i2);
                        finish();
                    }
                }
            }
        },800);
    }

    @Override
    protected void onResume() {
        super.onResume();
        // Create an ArrayAdapter using the string array and a default spinner layout
        initProvidedNetwork();
    }

    @Override
    protected void onPause() {
        super.onPause();
        successCount = 0;
    }

    private void initProvidedNetwork() {
        RetrofitAdapter.mBaseUrl = Const.URL_V1;
        api = RetrofitAdapter.getApiService();
        requestContacts();
        requestRubrosYSectores();
    }

    private void requestContacts() {
        if (Util.isNetworkAvailable(this)) {
            progressDoalog.show();
            call = api.allContacts(current_ruc);
            Log.e("TestURLContacto",call.request().url().toString());
            call.enqueue(new Callback<List<AllContacts>>() {

                @Override
                public void onResponse(Call<List<AllContacts>> call, retrofit2.Response<List<AllContacts>> response) {
                    Log.d("TestContact", "Response: " + response.code() + " call: " + call + " message" + response.raw().message());
                    progressDoalog.dismiss();
                    switch (response.code()) {
                        case 200:
                            if (!response.body().isEmpty()) {
                                //  progressDoalog.dismiss();

                                Log.d("TestContact", "-->isEmpty()" + call.request().url());
                                ListContactos = new ArrayList<>();
                                ListContactosDestalle = new ArrayList<>();
                                ListContactosDestalle.addAll(response.body());
                                for (AllContacts contacts: ListContactosDestalle) {
                                    ListContactos.add(contacts.getContacto());
                                }
                                new GuardarContactos(control,ListContactosDestalle,current_ruc,1).execute();
                            }
                            ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<>(DetalleClientes.this, android.R.layout.simple_spinner_item, ListContactos);
                            spinnerArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item); // The drop down view
                            Log.e("ContactAdapterOn","set" + spinnerArrayAdapter.getCount());
                            spinner.setAdapter(spinnerArrayAdapter);
                            successCount++;
                            Log.e("ContactCount","is "+successCount);
                            if (successCount>=2){
                                setDatosCliente();
                            }
                            break;
                        default:
                            // progressDoalog.dismiss();

                            Log.d("TestContact", "onResponse: El codigo no es 200, ha ocurrido un error");
                    }

                }

                @Override
                public void onFailure(Call<List<AllContacts>> call, Throwable t) {
                    Log.d("TestContact", "Throwable: " + t.getCause() + " call: " + call.toString());
                    Toast.makeText(DetalleClientes.this, "Hubo un error al obtener los datos. Compruebe su conexion a internet", Toast.LENGTH_LONG).show();
                    progressDoalog.dismiss();
                }
            });
        }else{
            new GetContactos(control,current_ruc).execute();
        }
    }

    private void requestRubrosYSectores() {
        if (Util.isNetworkAvailable(this)) {

            RequestQueue queue = Volley.newRequestQueue(this);
            String url = Const.URL_V1 + "cotizaciones/sectoresrubros";
            StringRequest postRequest = new StringRequest(Request.Method.GET, url,
                    new Response.Listener<String>()
                    {
                        @Override
                        public void onResponse(String response) {
                            Log.d("TestSectores", "Response: " + response);
                            try {
                                JSONObject jsonResponse = new JSONObject(response);
                                //  progressDoalog.dismiss();
                                Log.d("TestSectores", "-->isEmpty()" + call.request().url());
                                ListSectores = new ArrayList<>();
                                ListRubros = new ArrayList<>();

                                JSONArray sectores = jsonResponse.getJSONArray("sectores");
                                for (int i = 0; i < sectores.length(); i++) {
                                    JSONObject sector = sectores.getJSONObject(i);
                                    ContentValues sectoresValues = new ContentValues();

                                    sectoresValues.put("id", sector.getInt("id"));
                                    sectoresValues.put("nombre", sector.getString("nombre"));

                                    ListSectores.add(sectoresValues);
                                }

                                JSONArray rubros = jsonResponse.getJSONArray("rubros");
                                for (int i = 0; i < rubros.length(); i++) {
                                    JSONObject rubro = rubros.getJSONObject(i);
                                    ContentValues rubrosValues = new ContentValues();

                                    rubrosValues.put("id", rubro.getInt("id"));
                                    rubrosValues.put("nombre", rubro.getString("nombre"));
                                    rubrosValues.put("cod", rubro.getString("cod_equivalencia"));

                                    ListRubros.add(rubrosValues);
                                }

                                setRubrosAdapter();

                                new GuardarSectoresYRubros(control, ListSectores, ListRubros).execute();
                            }catch (JSONException e){
                                e.printStackTrace();
                            }
                            successCount++;
                            Log.e("ContactCount","is "+successCount);
                            if (successCount>=2){
                                setDatosCliente();
                            }
                        }
                    },
                    new Response.ErrorListener()
                    {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            Log.i("LOGIN1", "error");
                            Toast.makeText(DetalleClientes.this, "Hubo un error al obtener los datos. Compruebe su conexion a internet", Toast.LENGTH_LONG).show();
                            progressDoalog.dismiss();
                        }
                    }
            ) {

                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    Map<String, String>  params = new HashMap<>();
                    params.put("x-api-key", pref.getString("key", ""));
                    return params;
                }
            };
            queue.add(postRequest);
        }else{
            new GetSectoresYRubros(control).execute();
        }
    }

    @Override
    public void onBackPressed() {

    }

    ContentValues datos(String op) {
        ContentValues d = new ContentValues();
        d.put(Tb_Clientes.razon_social, nombre.getText().toString().trim());
        d.put(Tb_Clientes.ruc, ruc.getText().toString().trim());
        d.put(Tb_Clientes.nombre_comercial, nombreComercial.getText().toString().trim());
        d.put(Tb_Clientes.contacto, contacto.getText().toString().trim());
        d.put(Tb_Clientes.code, codeContacto);
        d.put(Tb_Clientes.telefono_fijo, telfijo.getText().toString().trim());
        d.put(Tb_Clientes.telefono_movil, telmovil.getText().toString().trim());
        d.put(Tb_Clientes.direccion, direccion.getText().toString().trim());
        d.put(Tb_Clientes.retenedor, (retenedor.isChecked()) ? 1 : 0);
        d.put(Tb_Clientes.perceptor, (perceptor.isChecked()) ? 1 : 0);
        d.put(Tb_Clientes.por_perceptor, por.getText().toString());
        d.put(Tb_Clientes.correo, correo.getText().toString());
        d.put(Tb_Clientes.calificacion, calificacion.getSelectedItemPosition());
        d.put(Tb_Clientes.cond_venta, condi_venta.getSelectedItemPosition() + 1);
        d.put(Tb_Clientes.cond_pago,idPago);
        d.put(Tb_Clientes.zona, zona.getSelectedItemPosition() + 1);
        d.put(Tb_Clientes.tiponegocio, (juridica.isChecked()) ? 1 : 2);
        d.put(Tb_Clientes.tipoExtaNa, (Extranjero.isChecked()) ? 2 : 1);
        d.put(Tb_Clientes.rubro, rubro11.getSelectedItemPosition());
        d.put(Tb_Clientes.sector,Idsector);
        d.put(Tb_Clientes.current_ruc,op.equalsIgnoreCase("2")?rucS:"");
        d.put("id",idid);
        d.put(Tb_Clientes.estado, op);
        return d;
    }



    int getNumberI(String d, int i){
        try{
            return Integer.parseInt(d);
        }catch (Exception e){
            return i;
        }
    }

    /*float getNumberF(String d,float i){
        try{
            return Float.parseFloat(d);
        }catch (Exception e){
            return i;
        }
    }*/

    void Mov(Boolean c){
        if (ntM == 0) {
            telmovil.setHint("no tiene telefono móvil");
            telmovil.setEnabled(false);
            telmovil.setBackgroundResource(R.drawable.edittex_enable);
            ntM = 1;
            telmovil.setText("");
        } else  if(c && ntM == 1){
            telmovil.setText("");
            telmovil.setHint("");
            telmovil.setEnabled(true);
            telmovil.setBackgroundResource(R.drawable.edittex);
            ntM = 0;
        }
    }
    void Fij(Boolean c){
        if(ntF==0){
            telfijo.setHint("no tiene telefono fijo");
            telfijo.setEnabled(false);
            telfijo.setBackgroundResource(R.drawable.edittex_enable);
            telfijo.setText("");
            ntF=1;
        }else if(c && ntF == 1){
            telfijo.setText("");
            telfijo.setHint("");
            telfijo.setEnabled(true);
            telfijo.setBackgroundResource(R.drawable.edittex);
            ntF=0;
        }
    }
    void Corr(Boolean c){
        if(ntC==0){
            correo.setHint("no tiene correo");
            correo.setText("");
            correo.setEnabled(false);
            correo.setBackgroundResource(R.drawable.edittex_enable);
            ntC=1;
        }else if(c && ntC == 1){
            correo.setText("");
            correo.setHint("");
            correo.setEnabled(true);
            correo.setBackgroundResource(R.drawable.edittex);
            ntC=0;
        }
    }

    boolean validacion() {
        String r = ruc.getText().toString().trim();
        String tf = telfijo.getText().toString().trim();
        String tm = telmovil.getText().toString().trim();
        boolean sucess = true;
        if (!Extranjero.isChecked()) {
            if(!natural.isChecked()){
                if(r.length() !=11){
                    Toast.makeText(DetalleClientes.this, "RUC incorrecto", Toast.LENGTH_LONG).show();
                    ruc.setFocusable(true);
                    sucess = false;
                }
            }else if(r.length() != 8){
                Toast.makeText(DetalleClientes.this,"DNI incorrecto", Toast.LENGTH_LONG).show();
                ruc.setFocusable(true);
                sucess = false;
            }

        } else if(Extranjero.isChecked() && (r.length() > 14|| r.length()<8)){
            Toast.makeText(DetalleClientes.this, "Cod. Extr. incorrecto", Toast.LENGTH_LONG).show();
            ruc.setFocusable(true);
            sucess = false;
        }else if (tf.length() != 7 && ntF == 0) {
            Toast.makeText(DetalleClientes.this, "Numero de telefono fijo incorrecto", Toast.LENGTH_LONG).show();
            telfijo.setFocusable(true);
            sucess = false;
        } else if (tm.length() != 9 && ntM == 0) {
            Toast.makeText(DetalleClientes.this, "Numero de telefono movil incorrecto", Toast.LENGTH_LONG).show();
            telmovil.setFocusable(true);
            sucess = false;
        }else if (correo.getText().toString().isEmpty() && ntC == 0) {
            Toast.makeText(DetalleClientes.this, "Debe colocar un correo", Toast.LENGTH_LONG).show();
            telmovil.setFocusable(true);
            sucess = false;
        }
        return sucess;
    }


    /*private static boolean isNumeric(String cadena){
        try {
            Integer.parseInt(cadena);
            return true;
        } catch (NumberFormatException nfe){
            return false;
        }
    }*/

    InputFilter filter = new InputFilter() {
        @Override
        public CharSequence filter(CharSequence source, int start, int end, Spanned dest, int dstart, int dend) {
            for (int i = start; i < end; ++i)
            {
                if (!Pattern.compile("[ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz1234567890]*").matcher(String.valueOf(source.charAt(i))).matches())
                {
                    return "";
                }
            }

            return null;
        }
    };
    @Override
    public void onClick(View view) {
        int id = view.getId();

        switch (id) {
            case R.id.guardarCliente:
                if (!nombre.getText().toString().trim().isEmpty() && !nombreComercial.getText().toString().trim().isEmpty() && /*!contacto.getText().toString().trim().isEmpty() &&*/ !direccion.getText().toString().trim().isEmpty() && !ruc.getText().toString().trim().isEmpty() && Idsector>0 && rubro11.getSelectedItemPosition()>0) {
                    if (validacion()) {

                        Guardar();

                    }
                } else {
                    Toast.makeText(DetalleClientes.this, "Deben llenarse los campos obligatorios(*)" +
                            "", Toast.LENGTH_SHORT).show();
                }
                break;
            case R.id.cancelarCliente:
                if (!nombre.getText().toString().trim().isEmpty() || !nombreComercial.getText().toString().trim().isEmpty() || !ruc.getText().toString().trim().isEmpty()) {
                    Cancelar();
                } else {
                    Intent i = new Intent(DetalleClientes.this, ListaClientes.class);
                    startActivity(i);
                    finish();
                }
                break;
            case R.id.limpiarCliente:
                nombre.setText("");
                ruc.setText("");
                contacto.setText("");
                correo.setText("");
                nombreComercial.setText("");
                telfijo.setText("");
                direccion.setText("");
                telmovil.setText("");
                perceptor.setChecked(false);
                retenedor.setChecked(false);
                view_por.setVisibility(View.VISIBLE);
                por.setText("2");
                nombre.setBackgroundResource(R.drawable.edittex);
                ruc.setBackgroundResource(R.drawable.edittex);
                nombreComercial.setBackgroundResource(R.drawable.edittex);
                nombre.setEnabled(true);
                ruc.setEnabled(true);
                nombreComercial.setEnabled(true);
                creacion = 1;
                break;
            case R.id.sector:
                ListadoSectores();
                break;
        }
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        int id = item.getItemId();

        switch (id) {
            case R.id.inicio:
                Intent i33 = new Intent(DetalleClientes.this, Menu_Principal.class);
                startActivity(i33);
                finish();
                break;
            case R.id.usuari:
                Intent i = new Intent(DetalleClientes.this, DatosUsuario.class);
                startActivity(i);
                finish();
                break;
            case R.id.clientes:
                Intent i2 = new Intent(DetalleClientes.this, ListaClientes.class);
                startActivity(i2);
                finish();
                break;
            case R.id.cotizar:
                Intent i1 = new Intent(DetalleClientes.this, ActividadPrincipal.class);
                i1.putExtra("edit",0);
                startActivity(i1);
                finish();
                break;
            case R.id.cerrar:
                Cerrar();
                break;

            case R.id.histo:
                Intent i3 = new Intent(DetalleClientes.this, Historial_cotizacion.class);
                startActivity(i3);
                finish();
                break;
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    Dialog customDialog;

    public void Cerrar() {
        customDialog = new Dialog(this);
        customDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        customDialog.setCancelable(false);
        customDialog.setContentView(R.layout.notificacion_01);
        (customDialog.findViewById(R.id.si)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                customDialog.dismiss();
                Intent intent = new Intent(DetalleClientes.this, Inicio.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
                finish();
            }
        });
        (customDialog.findViewById(R.id.no)).setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                customDialog.dismiss();

            }
        });

        assert customDialog != null;
        customDialog.show();

    }

    public void Guardar() {
        customDialog = new Dialog(this);
        customDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        customDialog.setCancelable(false);
        customDialog.setContentView(R.layout.notificacion_01);

        TextView titulo = (TextView) customDialog.findViewById(R.id.mensaje);
        titulo.setText("¿Desea guardar los datos del cliente?");
        TextView s = (TextView) customDialog.findViewById(R.id.si);
        s.setText("Si");
        TextView n = (TextView) customDialog.findViewById(R.id.no);
        n.setText("No");
        (customDialog.findViewById(R.id.si)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String msj = "Cliente ";
                if (creacion == 1) {
                    control.InsertClientes(datos("0"));
                    msj = msj + " guardado";
                } else {
                    control.UpdateCliente(datos("2"), rucS,idid);
                    msj = msj + " modificado";
                }
                Log.i("DetalleCliente",msj);
                Intent i = new Intent(DetalleClientes.this, ListaClientes.class);
                startActivity(i);

                finish();
            }
        });
        (customDialog.findViewById(R.id.no)).setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                customDialog.dismiss();

            }
        });

        assert customDialog != null;
        customDialog.show();

    }

    public void Cancelar() {
        customDialog = new Dialog(this);
        customDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        customDialog.setCancelable(false);
        customDialog.setContentView(R.layout.notificacion_01);

        TextView titulo = (TextView) customDialog.findViewById(R.id.mensaje);
        titulo.setText("¿Desea cancelar?");
        (customDialog.findViewById(R.id.si)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(DetalleClientes.this, ListaClientes.class);
                startActivity(i);
                finish();
            }
        });
        (customDialog.findViewById(R.id.no)).setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                customDialog.dismiss();

            }
        });

        assert customDialog != null;
        customDialog.show();

    }


    public void AlertNoGps() {
        customDialog = new Dialog(this);
        customDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        customDialog.setCancelable(false);
        customDialog.setContentView(R.layout.notificacion_01_gps);

        TextView titulo = (TextView) customDialog.findViewById(R.id.mensaje);
        TextView si = (TextView) customDialog.findViewById(R.id.si);
        si.setText("Aceptar");
        titulo.setText("El sistema GPS esta desactivado, ¿Desea activarlo?\nEsto puede ocasionar problemas con las coordenadas");
        (customDialog.findViewById(R.id.si)).setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view)
            {
                customDialog.dismiss();
                startActivity(new Intent(android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS));

            }
        });
        assert customDialog != null;
        customDialog.show();

    }

    SimpleDateFormat FechaFormat;
    SimpleDateFormat Hora;
    SimpleDateFormat Fecha1;
    SimpleDateFormat Hora1;
    String datecrear;
    String time;
    DatePickerDialog datePickerDialog;
    TimePickerDialog mTimePicker;
    ContentValues datosPuros;
    int i = 0, ii = 0,pe = 0;
    String echaConve = "",horaeca = "";
    String fechaha ="",idcodigo ="";
    String[] datatime;
    String Fechahoy;
    String horahoy,timeme;
    String horaBorrador;
    String fechaProximaSeleccionada;
    String horaProximaSeleccionada;
    boolean editarCompleto = true;
    public void Visitas(final Visita visita, final int estado) {

        customDialog = new Dialog(this);
        customDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        customDialog.setCancelable(true);
        customDialog.setContentView(R.layout.notificacion_07);
        ii =i= pe = 0;
        datosPuros = control.GetVisita(cliente.getAsString(Tb_Clientes.ruc));
        final TextView nombre1 = (TextView) customDialog.findViewById(R.id.nombre);
        nombre1.setText("Cliente: "+nombre.getText().toString());
        final TextView fechaI = (TextView) customDialog.findViewById(R.id.fechaI);
        final TextView horaI = (TextView) customDialog.findViewById(R.id.horaI);
        final TextView fechaF = (TextView) customDialog.findViewById(R.id.fechaF);
        final TextView date = (TextView) customDialog.findViewById(R.id.fechaP);
        final TextView time1 = (TextView) customDialog.findViewById(R.id.horaP);
        final TextView horaF = (TextView) customDialog.findViewById(R.id.horaF);
        final Spinner spinnerDialog = (Spinner) customDialog.findViewById(R.id.spinner_contactos);
        final EditText descripcionI = (EditText) customDialog.findViewById(R.id.descripcionI);
        final LinearLayout linearI = (LinearLayout) customDialog.findViewById(R.id.linearI);
        final LinearLayout linearF = (LinearLayout) customDialog.findViewById(R.id.linearF);
        final LinearLayout linearAF = (LinearLayout) customDialog.findViewById(R.id.linearAF);
        final LinearLayout proximamma = (LinearLayout) customDialog.findViewById(R.id.proximamma);

        final LinearLayout linearP = (LinearLayout) customDialog.findViewById(R.id.linearP);
        final ContentValues datos = new ContentValues();
        datos.put(Tb_Visita.cliente, cliente.getAsString(Tb_Clientes.ruc));
        int valor = validaPreferencias(nombre.getText().toString());
        date.setText("Seleccione fecha");
        time1.setText("Seleccione hora");
        proxima = false;

        ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<>(DetalleClientes.this,   android.R.layout.simple_spinner_item, ListContactos);
        spinnerArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item); // The drop down view
        spinnerDialog.setAdapter(spinnerArrayAdapter);

        FechaFormat = new SimpleDateFormat("dd/MM/yyyy",Locale.getDefault());
        Hora = new SimpleDateFormat("hh:mm:ss a",Locale.getDefault());
        Fecha1 = new SimpleDateFormat("yyyy-MM-dd",Locale.getDefault());
        Hora1 = new SimpleDateFormat("HH:mm:ss",Locale.getDefault());
        datecrear = FechaFormat.format(new Date());
        time = Hora.format(new Date());

        if(datosPuros!=null){
            idcodigo = datosPuros.getAsString(Tb_Visita.codigo);
            fechaha = datosPuros.getAsString(Tb_Visita.fecha_proxima);
            proxima = true;
            linearP.setVisibility(View.VISIBLE);
            String[] datatime = fechaha.split(" ");
            date.setText(datatime[0]);
            time1.setText(datatime[1]);
            String[] datatime11 = datatime[1].split(":");
            timeme = datatime11[0];
//            Toast.makeText(DetalleClientes.this,
            //                  "FechaFormat: "+datatime[0] + " Hora: " +datatime[1],Toast.LENGTH_SHORT).show();
            pe=1;
        }



        if (visita!=null){
            editarCompleto = false;
            String fecha = null;
            if (estado == 0){
                fecha = visita.getFechaInicio().replace(".000","");
            }else if (estado == 2){
                fecha = visita.getProximaVisita().replace(".000","");
            }
            SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault());

            if (fecha != null) {
                Date dateSelected;
                Date dateDevide;
                try {
                    dateSelected = formatter.parse(fecha);

                    Calendar calendar = Calendar.getInstance();

                    int year = calendar.get(Calendar.YEAR);
                    int month = calendar.get(Calendar.MONTH) + 1;
                    int day = calendar.get(Calendar.DAY_OF_MONTH);

                    int hour = calendar.get(Calendar.HOUR_OF_DAY);
                    int minute = calendar.get(Calendar.MINUTE);

                    String mf = "" + year + "-" + month + "-" + day + " " + hour + ":" + minute + ":00";
                    Log.e("FechaProxima",mf);
                    dateDevide = formatter.parse(mf);

                    editarCompleto = dateSelected.compareTo(dateDevide) <= 0;

                } catch (ParseException e) {
                    e.printStackTrace();
                }
            }
        }else{
            editarCompleto = true;
        }

        (customDialog.findViewById(R.id.inicio)).setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                if (editarCompleto) {
                    if (i == 0) {

                        if (fechaha.isEmpty()) {
                            proximamma.setVisibility(View.GONE);
                            fechaha = Fecha1.format(new Date()) + " " + Hora1.format(new Date());
                            Fechahoy = Fecha1.format(new Date());
                            horahoy = Hora1.format(new Date());
                        } else {
                            Fechahoy = FechaFormat.format(new Date());
                            horahoy = Hora1.format(new Date());
                        }
                        datatime = fechaha.split(" ");


                        //if (datatime[0].compareTo(Fechahoy) == 0) {
                            horaeca = Hora.format(new Date());
                            horaBorrador = Hora.format(new Date());
                            time1.setText(horaeca);
                            linearI.setVisibility(View.VISIBLE);
                            linearAF.setVisibility(View.VISIBLE);
                            fechaI.setText(datecrear);
                            horaI.setText(Hora.format(new Date()));
                            datos.put(Tb_Visita.fecha_inicio, Fecha1.format(new Date()) + " " + horahoy);
                            if (ActivityCompat.checkSelfPermission(DetalleClientes.this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(DetalleClientes.this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                                // TODO: Consider calling
                                //    ActivityCompat#requestPermissions
                                // here to request the missing permissions, and then overriding
                                //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                                //                                          int[] grantResults)
                                // to handle the case where the user grants the permission. See the documentation
                                // for ActivityCompat#requestPermissions for more details.

                            }
                            Location lastLocation =
                                    LocationServices.FusedLocationApi.getLastLocation(apiClient);
                            updateUI(lastLocation);
                            i = 1;
                        //} else {
                            //Toast.makeText(DetalleClientes.this, "La fecha actual no coincide con la fecha programada para esta visita, debe actualizarla para continuar", Toast.LENGTH_SHORT).show();
                        //}
                    }
                }else{
                    Toast.makeText(DetalleClientes.this, "La fecha actual no coincide con la fecha programada para esta visita, debe actualizarla para continuar", Toast.LENGTH_SHORT).show();
                }
            }
        });

        (customDialog.findViewById(R.id.fin)).setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view)
            {
                if (editarCompleto) {
                    if (i == 1) {
                        if (ii == 0) {
                            if (!descripcionI.getText().toString().isEmpty()) {
                                fechaF.setText(datecrear);
                                datos.put(Tb_Visita.fecha_final, Fecha1.format(new Date()) + " " + Hora1.format(new Date()));
                                horaF.setText(Hora.format(new Date()));
                                linearF.setVisibility(View.VISIBLE);
                                if (ActivityCompat.checkSelfPermission(DetalleClientes.this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(DetalleClientes.this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                                    // TODO: Consider calling
                                    //    ActivityCompat#requestPermissions
                                    // here to request the missing permissions, and then overriding
                                    //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                                    //                                          int[] grantResults)
                                    // to handle the case where the user grants the permission. See the documentation
                                    // for ActivityCompat#requestPermissions for more details.

                                }
                                Location lastLocation =
                                        LocationServices.FusedLocationApi.getLastLocation(apiClient);
                                updateUIF(lastLocation);

                                ii = 1;
                            } else {

                                Toast.makeText(DetalleClientes.this, "Debe colocar un motivo" +
                                        "", Toast.LENGTH_SHORT).show();
                            }

                        }
                    }
                }else{
                    Toast.makeText(DetalleClientes.this, "La fecha actual no coincide con la fecha programada para esta visita, debe actualizarla para continuar", Toast.LENGTH_SHORT).show();
                }
            }
        });

        (customDialog.findViewById(R.id.proxima)).setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view)
            {
                pe=1;
                linearP.setVisibility(View.VISIBLE);
            }
        });

        date.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // calender class's instance and get current date , month and year from calender
                final Calendar c = Calendar.getInstance();
                final int mYear = c.get(Calendar.YEAR); // current year
                final int mMonth = c.get(Calendar.MONTH); // current month
                final int mDay = c.get(Calendar.DAY_OF_MONTH); // current day
                // date picker dialog


                datePickerDialog = new DatePickerDialog(DetalleClientes.this,R.style.DialogTheme,
                        new DatePickerDialog.OnDateSetListener() {
                            @Override
                            public void onDateSet(DatePicker view, int year,
                                                  int monthOfYear, int dayOfMonth) {
                                // set day of month , month and year value in the edit text
                                String fe = (num(dayOfMonth) + "/"+ num(monthOfYear +1) + "/" + year);
                                echaConve = year  + "-"+ num(monthOfYear +1) + "-" +(num(dayOfMonth));
                                // c.set;
                                if((dayOfMonth<mDay && (monthOfYear)==mMonth) || (monthOfYear)<mMonth || year<mYear){
                                    Toast.makeText(DetalleClientes.this, "La fecha debe ser mayor al dia de hoy", Toast.LENGTH_SHORT).show();
                                }else{
                                    date.setText(fe);
                                    fechaProximaSeleccionada = fe;
                                }
                            }
                        }, mYear, mMonth, mDay);
                datePickerDialog.show();
            }
        });
        time1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // calender class's instance and get current date , month and year from calender
                Calendar mcurrentTime = Calendar.getInstance();
                int hour = mcurrentTime.get(Calendar.HOUR_OF_DAY);
                int minute = mcurrentTime.get(Calendar.MINUTE);

                mTimePicker = new TimePickerDialog(DetalleClientes.this,R.style.DialogTheme, new TimePickerDialog.OnTimeSetListener() {
                    @Override
                    public void onTimeSet(TimePicker timePicker, int selectedHour, int selectedMinute) {
                        time1.setText( selectedHour + ":" + num(selectedMinute) + ":00");
                        horaeca = selectedHour + ":" + num(selectedMinute) + ":00";
                    }

                }, hour, minute, true);//Yes 24 hour time

                mTimePicker.show();
            }
        });
        (customDialog.findViewById(R.id.si)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view)
            {
                String fechaProxima = date.getText().toString();
                if (!fechaProxima.isEmpty()) {
                    Date date;
                    try {
                        date = FechaFormat.parse(fechaProxima);
                        fechaProxima = Fecha1.format(date);
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                }
                fechaha = fechaProxima + " " + time1.getText().toString();
                if (ii == 1 && i == 1) {
                    if (fechaha.trim().isEmpty() && pe == 1 && date.getText().toString().compareTo("Seleccione fecha") != 0 && time1.getText().toString().compareTo("Seleccione hora") != 0) {
                        fechaha = date.getText().toString() + " " + time1.getText().toString();
                    } else {
                        echaConve = Fecha1.format(new Date());
                        horaeca = Hora1.format(new Date());
                    }
                    if (datosPuros != null) {
                        String selectQuery = "DELETE FROM " + Tb_Visita.nombreTabla + " WHERE " + Tb_Visita.codigo + " = '" + datosPuros.getAsString(Tb_Visita.codigo) + "' AND " + Tb_Visita.cliente + " = '" + current_ruc + "'";
                        SQLiteDatabase database = control.getWritableDatabase();
                        database.execSQL(selectQuery);
                    }
                    horaeca = horahoy;
                    datos.put(Tb_Visita.motivo, descripcionI.getText().toString());
                    datos.put(Tb_Visita.latitud_inicio, Latitudi);
                    datos.put(Tb_Visita.longitud_inicio, Longitudi);
                    datos.put(Tb_Visita.latitud_final, Latitudf);
                    datos.put(Tb_Visita.longitud_final, Longitudf);
                    datos.put(Tb_Visita.estado, visita != null ? "2" : "0");

                    datos.put(Tb_Visita.codigo, datosPuros == null ? "" : datosPuros.getAsString(Tb_Visita.codigo));
                    datos.put(Tb_Visita.fecha_proxima, fechaha);
                    datos.put(Tb_Visita.fecha_paraEnviar, echaConve + " " + horaeca);
                    datos.put(Tb_Visita.pendiente, datosPuros == null ? "1" : "3");
                    datos.put(Tb_Visita.direccion, ListContactosDestalle.get(spinnerDialog.getSelectedItemPosition()).getItem());

                    if (visita!=null) {
                        datos.put(Tb_Visita.codigo, visita.getId());
                        datos.put(Tb_Visita.pendiente, "3");
                        datos.put(Tb_Visita.direccion, visita.getDireccion());
                    }else{
                        datos.put(Tb_Visita.codigo, datosPuros == null ? "" : datosPuros.getAsString(Tb_Visita.codigo));
                    }

                    if (Util.isNetworkAvailable(DetalleClientes.this)){
                        enviarVisita(datos, visita!=null ? "3" : "1", String.valueOf(ListContactosDestalle.get(spinnerDialog.getSelectedItemPosition()).getItem()));
                    }else{
                        control.InsertVisitas(datos);
                    }
                    i = ii = 0;
                    //new DatosSynco(DetalleClientes.this).execute();
                    //VerificarVisita("Visita enviada");
                    editor.putString("fecha", "");
                    editor.apply();
                    editor.putString("hora", "");
                    editor.apply();
                    editor.putString("fechaI", "");
                    editor.apply();
                    editor.putString("horaI", "");
                    editor.apply();
                    editor.putString("cliente", "");
                    editor.apply();
                    editor.putString("longitud", "");
                    editor.apply();
                    editor.putString("latitud", "");
                    editor.apply();
                    editor.putString("motivo", "");
                    editor.apply();
                    customDialog.dismiss();
                } else if (i == 1 && ii == 0) {
                    editor.putString("fecha", datecrear);
                    editor.apply();
                    editor.putString("hora", horaBorrador);
                    editor.apply();
                    editor.putString("fechaI", Fecha1.format(new Date()));
                    editor.apply();
                    editor.putString("horaI", horahoy);
                    editor.apply();
                    editor.putString("cliente", nombre.getText().toString());
                    editor.apply();
                    editor.putString("longitud", Longitudi);
                    editor.apply();
                    editor.putString("latitud", Latitudi);
                    editor.apply();
                    editor.putString("motivo", descripcionI.getText().toString());
                    editor.apply();
                    customDialog.dismiss();
                    Toast.makeText(DetalleClientes.this, "Guardado en borrador" +
                            "", Toast.LENGTH_SHORT).show();
                } else {
                    if (visita!=null){
                        datos.put(Tb_Visita.motivo, descripcionI.getText().toString());
                        datos.put(Tb_Visita.latitud_inicio, Latitudi);
                        datos.put(Tb_Visita.longitud_inicio, Longitudi);
                        datos.put(Tb_Visita.latitud_final, Latitudf);
                        datos.put(Tb_Visita.longitud_final, Longitudf);
                        datos.put(Tb_Visita.estado, "2");
                        datos.put(Tb_Visita.codigo, visita.getId());
                        datos.put(Tb_Visita.codigo, datosPuros == null ? "" : datosPuros.getAsString(Tb_Visita.codigo));
                        datos.put(Tb_Visita.fecha_proxima, fechaha);
                        datos.put(Tb_Visita.fecha_paraEnviar, echaConve + " " + horaeca);
                        //datos.put(Tb_Visita.pendiente, datosPuros == null ? "1" : "3");
                        datos.put(Tb_Visita.direccion, ListContactosDestalle.get(spinnerDialog.getSelectedItemPosition()).getItem());
                        datos.put(Tb_Visita.codigo, visita.getId());
                        datos.put(Tb_Visita.pendiente, "3");
                        datos.put(Tb_Visita.direccion, visita.getDireccion());
                        if (Util.isNetworkAvailable(DetalleClientes.this)){
                            enviarVisita(datos, "3", String.valueOf(ListContactosDestalle.get(spinnerDialog.getSelectedItemPosition()).getItem()));
                        }
                        customDialog.dismiss();
                    }else if (pe == 1 && date.getText().toString().compareTo("Seleccione fecha") != 0 && time1.getText().toString().compareTo("Seleccione hora") != 0) {
                        if (datosPuros != null) {
                            String selectQuery = "DELETE FROM " + Tb_Visita.nombreTabla + " WHERE " + Tb_Visita.fecha_proxima + " = '" + datosPuros.getAsString(Tb_Visita.fecha_proxima) + "' AND " + Tb_Visita.cliente + " = '" + current_ruc + "'";
                            SQLiteDatabase database = control.getWritableDatabase();
                            database.execSQL(selectQuery);
                        }

                        datos.put(Tb_Visita.motivo, descripcionI.getText().toString());
                        datos.put(Tb_Visita.latitud_inicio, Latitudi);
                        datos.put(Tb_Visita.longitud_inicio, Longitudi);
                        datos.put(Tb_Visita.latitud_final, Latitudf);
                        datos.put(Tb_Visita.longitud_final, Longitudf);
                        datos.put(Tb_Visita.estado, "2");
                        datos.put(Tb_Visita.codigo, datosPuros == null ? "" : datosPuros.getAsString(Tb_Visita.codigo));
                        datos.put(Tb_Visita.fecha_proxima, fechaha);
                        datos.put(Tb_Visita.fecha_paraEnviar, echaConve + " " + horaeca);
                        //datos.put(Tb_Visita.pendiente, datosPuros == null ? "1" : "3");
                        datos.put(Tb_Visita.direccion, ListContactosDestalle.get(spinnerDialog.getSelectedItemPosition()).getItem());
                        datos.put(Tb_Visita.pendiente, "3");
                        if (Util.isNetworkAvailable(DetalleClientes.this)){
                            enviarVisita(datos, "3", String.valueOf(ListContactosDestalle.get(spinnerDialog.getSelectedItemPosition()).getItem()));
                        }
                        customDialog.dismiss();
                        /*
                        datos.put(Tb_Visita.fecha_inicio, "");
                        datos.put(Tb_Visita.fecha_final, "");
                        datos.put(Tb_Visita.motivo, "");
                        datos.put(Tb_Visita.latitud_inicio, "");
                        datos.put(Tb_Visita.longitud_inicio, "");
                        datos.put(Tb_Visita.latitud_final, "");
                        datos.put(Tb_Visita.longitud_final, "");
                        datos.put(Tb_Visita.estado, "0");
                        datos.put(Tb_Visita.codigo, datosPuros == null ? "" : datosPuros.getAsString(Tb_Visita.codigo));
                        datos.put(Tb_Visita.fecha_proxima, fechaProxima + " " + time1.getText().toString());
                        datos.put(Tb_Visita.fecha_paraEnviar, echaConve + " " + horaeca);
                        datos.put(Tb_Visita.pendiente, datosPuros == null ? "2" : "3");
                        datos.put(Tb_Visita.direccion, "");
                        control.InsertVisitas(datos);*/
                        //new DatosSynco(DetalleClientes.this).execute();
                        //VerificarVisita("Proxima visita enviada");
                        customDialog.dismiss();

                    } else {
                        customDialog.dismiss();
                        Toast.makeText(DetalleClientes.this, "Debe colocar fecha y hora" +
                                "", Toast.LENGTH_SHORT).show();
                    }

                }
            }
        });
        assert customDialog != null;
        if (visita!=null){

            String[] datatimeInicio;
            String[] datatimeFinal;
            String[] datatimeProximo;

            if (visita.getFechaInicio() != null){
                linearI.setVisibility(View.VISIBLE);
                datatimeInicio = visita.getFechaInicio().split(" ");
                String fi = datatimeInicio[0];
                try{
                    Date date1 = Fecha1.parse(datatimeInicio[0]);
                    fi = FechaFormat.format(date1);
                }catch (ParseException e){
                    e.printStackTrace();
                }
                fechaI.setText(fi);
                horaI.setText(datatimeInicio[1]);
                i = 1;
            }

            if (visita.getFechaFin() != null){
                linearF.setVisibility(View.VISIBLE);
                datatimeFinal = visita.getFechaFin().split(" ");
                String ff = datatimeFinal[0];
                try{
                    Date date1 = Fecha1.parse(datatimeFinal[0]);
                    ff = FechaFormat.format(date1);
                }catch (ParseException e){
                    e.printStackTrace();
                }
                fechaF.setText(ff);
                horaF.setText(datatimeFinal[1]);
                ii = 1;
            }

            if (visita.getProximaVisita() != null){
                linearP.setVisibility(View.VISIBLE);
                datatimeProximo = visita.getProximaVisita().split(" ");
                String fp = datatimeProximo[0];
                try{
                    Date fpd = Fecha1.parse(datatimeProximo[0]);
                    fp = FechaFormat.format(fpd);
                }catch (ParseException e){
                    e.printStackTrace();
                }
                date.setText(fp);
                time1.setText(datatimeProximo[1]);
            }
            
            linearAF.setVisibility(View.VISIBLE);

            descripcionI.setText(visita.getMotivo());

            idcodigo = String.valueOf(visita.getId());
            fechaha = visita.getProximaVisita();
            proxima = true;

            Latitudf = visita.getLatitudFin();
            Latitudi = visita.getLatitudInicio();
            Longitudf = visita.getLongitudFin();
            Longitudi = visita.getLongitudInicio();

        } else {
            if (valor == 0) {
                linearI.setVisibility(View.GONE);
                linearAF.setVisibility(View.GONE);
                linearF.setVisibility(View.GONE);
            } else if (valor == 2) {

                if (!pref.getString("fecha", "").isEmpty()) {
                    i = 1;
                    linearI.setVisibility(View.VISIBLE);
                    linearI.setVisibility(View.VISIBLE);
                    linearAF.setVisibility(View.VISIBLE);
                    fechaI.setText(pref.getString("fecha", ""));
                    horaI.setText(pref.getString("hora", ""));
                    descripcionI.setText(pref.getString("motivo", ""));
                    Longitudi = pref.getString("longitud", "0");
                    Latitudi = pref.getString("latitud", "0");
                    datos.put(Tb_Visita.fecha_inicio, pref.getString("fechaI", "") + " " + pref.getString("horaI", ""));
                }

            }//else if(valor ==1){
            //customDialog.dismiss();
            //}
        }
        customDialog.show();



    }

    boolean success = true;
    @SuppressLint({"DefaultLocale", "StaticFieldLeak"})
    private void enviarVisita(final ContentValues datos, final String action, final String direccion){
        final String urlsV = Const.URL_V1 + "visitas";
        new AsyncTask<Void,Void,Integer>(){
            @Override
            protected void onPreExecute() {
                super.onPreExecute();
            }

            @Override
            protected void onPostExecute(Integer integer) {
                super.onPostExecute(integer);
                Toast.makeText(DetalleClientes.this, "Visita enviada", Toast.LENGTH_SHORT).show();
            }

            @Override
            protected Integer doInBackground(Void... voids) {
                try {
                    HttpClient httpclient = new DefaultHttpClient();
                    HttpPost httppost = new HttpPost(urlsV);
                    httppost.setHeader("x-api-key", Preferences.getkeyUser());
                    List<NameValuePair> params = new ArrayList<>();
                    String id = String.valueOf(datos.getAsString(Tb_Visita.codigo));
                    params.add(new BasicNameValuePair("cliente", datos.getAsString(Tb_Visita.cliente)));
                    params.add(new BasicNameValuePair("fecha_inicio", datos.getAsString(Tb_Visita.fecha_inicio)));
                    params.add(new BasicNameValuePair("fecha_fin", datos.getAsString(Tb_Visita.fecha_final)));
                    params.add(new BasicNameValuePair("latitud_inicio", datos.getAsString(Tb_Visita.latitud_inicio)));
                    params.add(new BasicNameValuePair("longitud_inicio", datos.getAsString(Tb_Visita.longitud_inicio)));
                    params.add(new BasicNameValuePair("latitud_fin", datos.getAsString(Tb_Visita.latitud_final)));
                    params.add(new BasicNameValuePair("longitud_fin", datos.getAsString(Tb_Visita.longitud_final)));
                    params.add(new BasicNameValuePair("motivo", datos.getAsString(Tb_Visita.motivo)));
                    String fechaProxima = datos.getAsString(Tb_Visita.fecha_proxima).replace("/","-").replace(".000","");
                    params.add(new BasicNameValuePair("proxima_visita", fechaProxima));
                    Log.e("VisitaParams",fechaProxima);
                    params.add(new BasicNameValuePair("action", action));
                    params.add(new BasicNameValuePair("id", id));         //Se cambia 12 por 9
                    params.add(new BasicNameValuePair("direccion", direccion));

                    httppost.setEntity(new UrlEncodedFormEntity(params));
                    HttpResponse resp = httpclient.execute(httppost);
                    HttpEntity ent = resp.getEntity();/*y obtenemos una respuesta*/
                    if (HttpStatus.SC_CREATED == resp.getStatusLine().getStatusCode()) {
                        success = true;
                        String text = EntityUtils.toString(ent);
                        JSONObject json = new JSONObject(text);
                        String iddd = action.compareTo("1") == 0 ? "" : json.getString("id");
                        control.UpdateEstadoVisita(id, iddd);
                    } else {
                        success = false;
                        String text = EntityUtils.toString(ent);


                    }
                } catch (Exception e) {
                    System.out.println("Error Sync Subieda 0: " + e.toString());
                }
                return null;
            }
        }.execute();

    }

    String num(int i){
        return (i<10)?"0"+i:""+i;
    }

    private void updateUI(Location loc) {
        if (loc != null) {
            Latitudi = String.valueOf(loc.getLatitude());
            Longitudi =String.valueOf(loc.getLongitude());
        } else {
            Latitudi = "0";
            Longitudi = "0";
        }
    }

    private void updateUIF(Location loc) {
        if (loc != null) {
            Latitudf = String.valueOf(loc.getLatitude());
            Longitudf =String.valueOf(loc.getLongitude());
        } else {
            Latitudf = "0";
            Longitudf = "0";
        }
    }

    int validaPreferencias(String c){
        //String fecha = pref.getString("fecha","");
        //String hora = pref.getString("hora","");
        String cliente = pref.getString("cliente","");
        Boolean detec = cliente.compareTo(c)==0;
        int sucess;
        if ((!cliente.isEmpty() &&!detec) ) {
            Toast.makeText(DetalleClientes.this, "Tienes una visita pendiente con el cliente: " + cliente, Toast.LENGTH_LONG).show();
            sucess = 1;
        } else if(detec) {
            sucess = 2;
        }else{
            sucess = 0;
        }
        return sucess;
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {

    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    public void actualizarInterfz(int posi) {
        AllContacts contacts = ListContactosDestalle.get(posi);
        direccion.setText(""+contacts.getDireccion());
        correo.setText(""+contacts.getCorreo());
        telfijo.setText(""+contacts.getTelefonoFijo());
        telmovil.setText(""+contacts.getTelefonoMovil());

        idPago = String.valueOf(contacts.getCondPago());
        String condPagoText;
        condPagoText = idPago.equalsIgnoreCase("18") ? "Contado" : control.GetCondicionPago(idPago);
        Log.e("CondicionPago", "id = " + idPago + " TEXT = " + condPagoText);
        condPago.setText(condPagoText);

        int condVentaPosition = contacts.getCondVenta() - 1;
        condi_venta.setSelection(condVentaPosition);


    }

    public void ListadoSectores(){
        customDialog = new Dialog(this);
        customDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        customDialog.setCancelable(false);
        customDialog.setContentView(R.layout.notificacion_03_cargalista);
        TextView titulo = (TextView) customDialog.findViewById(R.id.titulo);
        titulo.setText("Seleccione un sector");
        EditText busqueda = (EditText) customDialog.findViewById(R.id.buscar);
        busqueda.setHint("Buscar sector");
        final RecyclerView lista1 = (RecyclerView) customDialog.findViewById(R.id.clientes);
        lista1.setHasFixedSize(true);

        adapterVentas = new CondicionVentaAdapter(this, ListSectores);
        lista1.setAdapter(adapterVentas);

        busqueda.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void onTextChanged(CharSequence s, int i, int i1, int i2) {
                ListSectores = control.GetFullSectoresBusqueda(s.toString());
                adapterVentas = new CondicionVentaAdapter(DetalleClientes.this, ListSectores);
                lista1.setAdapter(adapterVentas);
            }

            @Override
            public void afterTextChanged(Editable editable) {
            }
        });

        adapterVentas.setOnItemClickListener(new CondicionVentaAdapter.ClickListener() {
            @Override
            public void onItemClick(int position, View v) {
                ContentValues dato = adapterVentas.lista.get(position);
                Idsector = dato.getAsInteger("id");
                sector.setText(dato.get("nombre").toString());
                customDialog.dismiss();
            }
        });
        lista1.setLayoutManager(new LinearLayoutManager(this,LinearLayoutManager.VERTICAL,false));
        (customDialog.findViewById(R.id.no)).setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view)
            {
                customDialog.dismiss();
            }
        });

        assert customDialog != null;
        customDialog.show();
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.cliente_detalle_menu, menu);
        menee = menu;
        MenuItem iiimenu = menee.findItem(R.id.visita);
        iiimenu.setVisible(!estado);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.visita:
                VerificarVisita("");
                return true;
            case R.id.historial_pedidos:
                Intent i = new Intent(DetalleClientes.this, ActivityHistorialPedidos.class);
                i.putExtra("ruc",current_ruc); //20508737221
                startActivity(i);
               // overridePendingTransition(R.anim.zoom_back_in,R.anim.zoom_back_out);
                return true;
            case R.id.situacion_actual:
                Intent in = new Intent(DetalleClientes.this, ActivitySituacionActual.class);
                in.putExtra("ruc",current_ruc);
                startActivity(in);
             //   overridePendingTransition(R.anim.zoom_back_in,R.anim.zoom_back_out);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    boolean dispVisita;
    public void VerificarVisita(final String cadenaenvio){
        final ProgressDialog dialog = new ProgressDialog(this);
        dialog.setTitle("Cargando...");
        dialog.show();
        RequestQueue queue = Volley.newRequestQueue(this);
        String url = Const.URL_V1 + "visitas/check?ruc=" + current_ruc;
        StringRequest postRequest = new StringRequest(Request.Method.GET, url,
                new Response.Listener<String>()
                {
                    @Override
                    public void onResponse(String response) {
                        // response
                        Log.d("Response", response);
                        boolean hasVisit = true;
                        JSONObject visitaObject = null;

                        try {
                            JSONObject visitaResponse = new JSONObject(response);
                            visitaObject = visitaResponse.getJSONObject("visita");
                            hasVisit = visitaObject != null;
                        }catch (JSONException e){
                            e.printStackTrace();
                        }

                        dialog.dismiss();
                        int estado = 1;
                        if (hasVisit && visitaObject != null) {
                            Log.e("visitaResponse",visitaObject.toString());
                            Visita visita = new Gson().fromJson(visitaObject.toString(), Visita.class);

                            ContentValues datos = new ContentValues();
                            datos.put(Tb_Visita.cliente, visita.getRazonSocial());
                            datos.put(Tb_Visita.motivo, visita.getMotivo());
                            datos.put(Tb_Visita.latitud_inicio, visita.getLatitudInicio());
                            datos.put(Tb_Visita.longitud_inicio, visita.getLongitudInicio());
                            datos.put(Tb_Visita.latitud_final, visita.getLatitudFin());
                            datos.put(Tb_Visita.longitud_final, visita.getLongitudFin());
                            estado = visita.getEstado();
                            datos.put(Tb_Visita.estado,estado );
                            datos.put(Tb_Visita.codigo, visita.getId());
                            datos.put(Tb_Visita.fecha_proxima, visita.getProximaVisita());
                            datos.put(Tb_Visita.fecha_inicio, visita.getFechaInicio());
                            datos.put(Tb_Visita.fecha_final, visita.getFechaFin());
                            datos.put(Tb_Visita.pendiente, "3");
                            datos.put(Tb_Visita.direccion, visita.getDireccion());

                            //control.InsertVisitas(datos);
                            Visitas(visita,estado);
                        }else{
                            Visitas(null,estado);
                        }
                        /*try {
                            JSONObject dato = new JSONObject(response);
                            dispVisita = dato.getBoolean("status");
                            if(dispVisita){
                                //Visitas();
                                new DatosSynco(DetalleClientes.this).execute();
                                Toast.makeText(DetalleClientes.this,cadenaenvio +
                                        "",Toast.LENGTH_SHORT).show();
                            }else{
                                if(proxima){
                                    new DatosSynco(DetalleClientes.this).execute();
                                    Toast.makeText(DetalleClientes.this,cadenaenvio +
                                            "",Toast.LENGTH_SHORT).show();
                                }else{
                                    Toast.makeText(DetalleClientes.this,"Usted ya tiene un visita programada o pendiente por terminar con este cliente",Toast.LENGTH_SHORT).show();
                                }

                            }
                            //msj = "Estatus: " + resp.getStatusLine().getStatusCode() + " key" + dato.get("key");
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        dialog.dismiss();*/
                    }
                },
                new Response.ErrorListener()
                {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        dispVisita = false;
                        Toast.makeText(DetalleClientes.this,"Hubo un error al obtener la informacion",Toast.LENGTH_SHORT).show();
                        dialog.dismiss();
                    }
                }
        ) {
            /*@Override
            protected Map<String, String> getParams()
            {
                Map<String, String> params = new HashMap<String, String>();
                params.put("cliente", cliente.get(Tb_Clientes.ruc).toString());

                return params;
            }*/

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String>  params = new HashMap<>();
                params.put("x-api-key", pref.getString("key", ""));
                return params;
            }
        };
        queue.add(postRequest);
    }

    public void ListadoCondicionPago(){
        customDialog = new Dialog(this);
        customDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        customDialog.setCancelable(false);
        customDialog.setContentView(R.layout.notificacion_03_cargalista);
        condiVenta = new ArrayList<>();
        condiVenta = control.GetFullVenta();
        TextView titulo = (TextView) customDialog.findViewById(R.id.titulo);
        titulo.setText("Condicion de pago");
        EditText busqueda = (EditText) customDialog.findViewById(R.id.buscar);
        busqueda.setHint("Buscar condición de pago");
        final  RecyclerView lista1 = (RecyclerView) customDialog.findViewById(R.id.clientes);
        lista1.setHasFixedSize(true);

        adapterVentas = new CondicionVentaAdapter(this, condiVenta);
        lista1.setAdapter(adapterVentas);

        busqueda.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence s, int i, int i1, int i2) {
                condiVenta = control.GetFullVentaBusqueda(s.toString());
                adapterVentas = new CondicionVentaAdapter(DetalleClientes.this, condiVenta);
                lista1.setAdapter(adapterVentas);
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        adapterVentas.setOnItemClickListener(new CondicionVentaAdapter.ClickListener() {
            @Override
            public void onItemClick(int position, View v) {
                ContentValues dato = adapterVentas.lista.get(position);
                idPago = dato.get("id").toString();
                Log.e("pago_text_listapago"," "+dato.get("nombre").toString());
                condPago.setText(dato.get("nombre").toString());
                customDialog.dismiss();
            }
        });
        lista1.setLayoutManager(new LinearLayoutManager(this,LinearLayoutManager.VERTICAL,false));
        (customDialog.findViewById(R.id.no)).setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view)
            {
                customDialog.dismiss();
            }
        });

        assert customDialog != null;
        customDialog.show();
    }

    private class GetContactos extends AsyncTask<Void, Void, Integer> {

        private BaseDatosSqlite control;
        private String ruc;
        private ArrayAdapter<String> adapter;

        GetContactos(BaseDatosSqlite control,String ruc){
            this.control = control;
            this.ruc = ruc;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected void onPostExecute(Integer integer) {
            super.onPostExecute(integer);
            adapter = new ArrayAdapter<>(DetalleClientes.this, android.R.layout.simple_spinner_item, ListContactos);
            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item); // The drop down view
            Log.e("ContactAdapterOff","set" + adapter.getCount());
            spinner.setAdapter(adapter);
            successCount++;
            Log.e("ContactCount","is "+successCount);
            if (successCount>=2){
                setDatosCliente();
            }
        }

        @Override
        protected Integer doInBackground(Void... voids) {
            ListContactos = new ArrayList<>();
            ListContactosDestalle = control.getContactsByRUC(ruc);
            for (AllContacts contacts: ListContactosDestalle) {
                ListContactos.add(contacts.getContacto());
            }
            return 0;
        }
    }

    private class GetSectoresYRubros extends AsyncTask<Void, Void, Integer> {

        private BaseDatosSqlite control;

        GetSectoresYRubros(BaseDatosSqlite control){
            this.control = control;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected void onPostExecute(Integer integer) {
            super.onPostExecute(integer);
            setRubrosAdapter();
            successCount++;
            Log.e("ContactCount","is "+successCount);
            if (successCount>=2){
                setDatosCliente();
            }
        }

        @Override
        protected Integer doInBackground(Void... voids) {
            ListSectores = new ArrayList<>();
            ListRubros = new ArrayList<>();

            System.out.print("Getting list of Rubros");
            ListSectores = control.GetFullSectores();
            ListRubros = control.GetFullRubros();

            return 0;
        }


    }

    private void setRubrosAdapter(){
        ArrayList<String> list = new ArrayList<>();
        list.add("Selecciona un rubro");
        for (ContentValues values :
                ListRubros) {
            list.add(values.getAsString("nombre"));
        }
        ArrayAdapter<String> adapter = new ArrayAdapter<>(DetalleClientes.this,android.R.layout.simple_list_item_1,list);
        rubro11.setAdapter(adapter);
    }

}

