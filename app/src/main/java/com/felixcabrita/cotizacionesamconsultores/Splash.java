package com.felixcabrita.cotizacionesamconsultores;

import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import com.crashlytics.android.Crashlytics;
import io.fabric.sdk.android.Fabric;

public class Splash extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Fabric.with(this, new Crashlytics());
        setContentView(R.layout.activity_splash);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

        Thread Splas = new Thread(){
            public void run(){
                try{
                    int tiempo =0;
                    while((tiempo<2000)){
                        sleep(100);
                        tiempo +=100;
                    }
                }catch (Exception e) {
                    System.out.println("Splas-Thread: " + e.toString());
                }finally {
                    finish();
                    startActivity(new Intent(getApplicationContext(), Inicio.class));
                }
            }
        };
        Splas.start();
    }
}
