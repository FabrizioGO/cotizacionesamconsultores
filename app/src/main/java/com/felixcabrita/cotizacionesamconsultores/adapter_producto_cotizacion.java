package com.felixcabrita.cotizacionesamconsultores;

/**
 * Creado por Felix Cabrita en 29/6/2017.
*/
import android.content.ContentValues;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.ArrayList;
import java.util.Locale;

import com.felixcabrita.cotizacionesamconsultores.models.Tb_cotizacion_productos;

public class adapter_producto_cotizacion extends BaseAdapter {

    private DecimalFormat convertidor = new DecimalFormat("#,##0.00", DecimalFormatSymbols.getInstance(Locale.ITALIAN));   // CONVERTIDOR FLOAT \\
    ArrayList<ContentValues> lista = new ArrayList<>();
    private Context propio;
    int moneda = 0;
    adapter_producto_cotizacion(Context c, ArrayList<ContentValues> l,int _moneda){
        this.propio = c;
        this.lista = l;
        DecimalFormatSymbols dfs = new DecimalFormatSymbols();
        dfs.setDecimalSeparator('.');
        dfs.setGroupingSeparator(',');
        convertidor.setDecimalFormatSymbols(dfs);
        moneda = _moneda;
    }

    @Override
    public int getCount() {
        return lista.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        TextView nombre;
        TextView cantidad;
        TextView precio;
        TextView cunio;
        TextView produccion;
        TextView subtotal;
        TextView igv;
        TextView total;
        TextView por;
        LayoutInflater inf = (LayoutInflater) propio.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        if (null == convertView) {
            convertView = inf.inflate(R.layout.adapter_item_cotizacion, parent, false);
        }
        nombre   = (TextView) convertView.findViewById(R.id.nombre);
        cantidad         = (TextView) convertView.findViewById(R.id.cantidad);
        precio           = (TextView) convertView.findViewById(R.id.precio);
        cunio           = (TextView) convertView.findViewById(R.id.cunio);
        subtotal            = (TextView) convertView.findViewById(R.id.subtotal);
        produccion            = (TextView) convertView.findViewById(R.id.produccion);
        por            = (TextView) convertView.findViewById(R.id.por);

        nombre.setText(lista.get(position).get(Tb_cotizacion_productos.producto_nombre).toString());
        cantidad.setText(lista.get(position).get(Tb_cotizacion_productos.cantidad).toString());
        precio.setText(((moneda==2)?"US$ ":"S/. ") +convertidor.format(Float.parseFloat(lista.get(position).get(Tb_cotizacion_productos.precio).toString())));
        cunio.setText(lista.get(position).get(Tb_cotizacion_productos.cunio +"print").toString());
        por.setText(lista.get(position).get("por").toString());
        subtotal.setText(((moneda==2)?"US$ ":"S/. ") +convertidor.format(Float.parseFloat(lista.get(position).get(Tb_cotizacion_productos.subtotal).toString())));
        produccion.setText(lista.get(position).get(Tb_cotizacion_productos.produccion +"print").toString());
        System.out.println("IGV " + lista.get(position).get(Tb_cotizacion_productos.igv).toString());
        return convertView;
    }
}



