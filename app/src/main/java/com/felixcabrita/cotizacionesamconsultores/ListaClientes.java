package com.felixcabrita.cotizacionesamconsultores;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.felixcabrita.cotizacionesamconsultores.utils.Const;
import com.felixcabrita.cotizacionesamconsultores.models.Tb_Clientes;
import com.felixcabrita.cotizacionesamconsultores.models.Tb_Visita;
import com.felixcabrita.cotizacionesamconsultores.utils.Preferences;
import com.felixcabrita.cotizacionesamconsultores.utils.Util;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

public class ListaClientes extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener,View.OnClickListener {

    public ArrayList<ContentValues> clientes;
    BaseDatosSqlite control;

    private ListClienteAdapter adapter;
    private RecyclerView recView;
    SharedPreferences pref;
    int Status = 1;
    ProgressBar progreso;
    View view;
    private SharedPreferences.Editor editor;
    Handler escribirenUI;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lista_cliente);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        clientes = new ArrayList<>();
        control = new BaseDatosSqlite(this);
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        view = findViewById(R.id.view);
        progreso = (ProgressBar) findViewById(R.id.progreso);
        pref = PreferenceManager.getDefaultSharedPreferences(this);
        editor = pref.edit();
        editor.putInt("syncC",0);
        editor.apply();
        editor.putInt("syncP",0);
        editor.apply();
        editor.putInt("syncCo",0);
        editor.apply();
        Status = 1;
        escribirenUI = new Handler();
        toggle.syncState();
        EditText busqueda = (EditText) findViewById(R.id.buscar);
        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        SharedPreferences pref = PreferenceManager.getDefaultSharedPreferences(this);
        View headerView = navigationView.getHeaderView(0);
        TextView tvHeaderName= (TextView) headerView.findViewById(R.id.usuario);
        tvHeaderName.setText(pref.getString("nombre","usuario"));

        recView = (RecyclerView) findViewById(R.id.visitados);
        recView.setHasFixedSize(true);

        /*recView1 = (RecyclerView) findViewById(R.id.novisitados);
        recView1.setHasFixedSize(true);




        recView1.setAdapter(adapter);
        recView1.setLayoutManager(new LinearLayoutManager(this,LinearLayoutManager.VERTICAL,false));*/

        adapter = new ListClienteAdapter(this, clientes);
        adapter.setOnItemClickListener(new ListClienteAdapter.ClickListener() {
            @Override
            public void onItemClick(int position, View v) {
                Status = 0;
                ContentValues dato = adapter.lista.get(position);
                Log.i("CLIENTE", dato.toString());
                String codigo = dato.get(Tb_Clientes.ruc).toString();

                Intent i = new Intent(ListaClientes.this, DetalleClientes.class);
                i.putExtra("creacion",0);
                Log.e("cliente_estado",dato.get(Tb_Clientes.estado).toString());
                i.putExtra("estado", dato.getAsInteger(Tb_Clientes.estado)==0);
                i.putExtra(Tb_Clientes.ruc, codigo);
                startActivity(i);
                finish();
            }
        });
        busqueda.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                clientes = control.GetFullClientesBusqueda(s.toString());
                adapter = new ListClienteAdapter(ListaClientes.this, clientes);
                recView.setAdapter(adapter);
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }

        });

        //new DatosSynco(this).execute();
        recView.setAdapter(adapter);
        recView.setLayoutManager(new LinearLayoutManager(this,LinearLayoutManager.VERTICAL,false));
    }

    @Override
    protected void onResume() {
        super.onResume();
        new Datos(this).execute();
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        int id = item.getItemId();

        switch (id){
            case R.id.inicio:
                Intent i33 = new Intent(ListaClientes.this, Menu_Principal.class);
                startActivity(i33);
                finish();
                break;
            case R.id.usuari:
                Intent i = new Intent(ListaClientes.this,DatosUsuario.class);
                startActivity(i);
                Status = 0;
                finish();
                break;
            case R.id.clientes:
                Intent i22 = new Intent(ListaClientes.this,ListaClientes.class);
                startActivity(i22);
                Status = 0;
                finish();
                break;
            case R.id.cotizar:
                Intent i1 = new Intent(ListaClientes.this,ActividadPrincipal.class);
                i1.putExtra("edit",0);
                startActivity(i1);
                Status = 0;
                finish();
                break;
            case R.id.cerrar:
                Cerrar();
                break;
            case R.id.histo:
                Intent i3 = new Intent(ListaClientes.this,Historial_cotizacion.class);
                i3.putExtra("volver","1");
                startActivity(i3);
                Status = 0;
                finish();
                break;
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.addcliente:
                Status = 0;
                Intent i = new Intent(ListaClientes.this, DetalleClientes.class);
                i.putExtra("creacion",1);
                i.putExtra("estado", true);
                i.putExtra(Tb_Clientes.ruc, "");
                startActivity(i);
                finish();
                break;
        }
    }


    private class Datos extends AsyncTask<Void,Void,Integer> {


        ProgressDialog pd;
        Context cont;

        public Datos(Context c){
            try{
                cont = c;
                pd =  new ProgressDialog(c);
                pd.setIndeterminate(true);
                pd.setMessage("Cargando Datos");
                pd.setCancelable(false);
                pd.show();
            }catch (Exception e){
                System.out.println("Error1: "+e.toString());
            }

        }


        @Override
        protected void onPreExecute() {
            super.onPreExecute();

        }

        @Override
        protected Integer doInBackground(Void... voids) {
            clientes = control.GetFullClientes();
            adapter = new ListClienteAdapter(cont, clientes);
            return 0;
        }

        @Override
        protected void onPostExecute(Integer integer) {
            super.onPostExecute(integer);
            recView.setAdapter(adapter);
            new DatosSynco(ListaClientes.this).execute();
            pd.dismiss();
        }

    }


    private class DatosSynco extends AsyncTask<Void,Void,Integer> {

        private String response;
        boolean internet = true;
        Context cont;
        String urls =  Const.URL_V1 + "clientes";
        String urlsV = Const.URL_V1 + "visitas";
        String msj ="";
        String mensj = "";
        public DatosSynco(Context c){
            try{
                cont = c;
            }catch (Exception e){
                System.out.println("Error1: "+e.toString());
            }

        }

        String centa(String i,String o){
            return "El Ruc/DNI/Cod. Ext. " + i + " del cliente " +o+ " ya existe.";
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progreso.setVisibility(View.VISIBLE);
            view.setVisibility(View.GONE);
            Log.e("DatosSynco","onPreExecute");
        }

        @Override
        protected Integer doInBackground(Void... voids) {
                Log.e("DatosSynco","onInBackgroundInicio");
                if (Util.isNetworkAvailable(ListaClientes.this)) {
                    internet = true;
                    if (control.CheckDataSync()) {
                        try {
                            String selectQuery = "SELECT " + Tb_Clientes.razon_social + "," + Tb_Clientes.ruc + "," + Tb_Clientes.contacto + "," +Tb_Clientes.code + "," +
                                    Tb_Clientes.direccion + "," + Tb_Clientes.telefono_fijo + "," + Tb_Clientes.telefono_movil + "," + Tb_Clientes.perceptor
                                    + "," + Tb_Clientes.retenedor + "," + Tb_Clientes.por_perceptor + "," + Tb_Clientes.calificacion
                                    + "," + Tb_Clientes.cond_pago + "," + Tb_Clientes.cond_venta+"," + Tb_Clientes.nombre_comercial + "," + Tb_Clientes.correo + "," + Tb_Clientes.estado
                                    + "," + Tb_Clientes.zona+ "," + Tb_Clientes.tiponegocio + "," + Tb_Clientes.tipoExtaNa +"," + Tb_Clientes.rubro +
                                    "," + Tb_Clientes.sector +"," + Tb_Clientes.current_ruc +" FROM " + Tb_Clientes.nombreTabla + " WHERE " + Tb_Clientes.estado + "= 0 OR " + Tb_Clientes.estado + "= 2";
                            SQLiteDatabase database = control.getWritableDatabase();
                            Cursor cursor = database.rawQuery(selectQuery, null);
                            if (cursor.moveToFirst()) {
                                do {
                                    try {
                                        HttpClient httpclient = new DefaultHttpClient();
                                        HttpPost httppost = new HttpPost(urls);
                                        httppost.setHeader("x-api-key", Preferences.getkeyUser());
                                        List<NameValuePair> params = new ArrayList<>();
                                        params.add(new BasicNameValuePair("razon_social", cursor.getString(0).trim()));
                                        params.add(new BasicNameValuePair("ruc", cursor.getString(1).trim()));
                                        params.add(new BasicNameValuePair("contacto", ""+cursor.getString(2).trim()));
                                        params.add(new BasicNameValuePair("code", cursor.getString(3).trim()));
                                        params.add(new BasicNameValuePair("direccion", cursor.getString(4).trim()));
                                        params.add(new BasicNameValuePair("telefono_fijo", cursor.getString(5).trim()));
                                        params.add(new BasicNameValuePair("telefono_movil", cursor.getString(6).trim()));
                                        params.add(new BasicNameValuePair("perceptor", cursor.getString(7)));
                                        params.add(new BasicNameValuePair("retenedor", cursor.getString(8)));
                                        params.add(new BasicNameValuePair("porcentaje", cursor.getString(9)));
                                        params.add(new BasicNameValuePair("calificacion", cursor.getString(10).trim()));
                                        params.add(new BasicNameValuePair("cond_pago", cursor.getString(11).trim()));
                                        params.add(new BasicNameValuePair("cond_venta",cursor.getString(12).trim()));
                                        params.add(new BasicNameValuePair("nombre_comercial", cursor.getString(13).trim()));
                                        params.add(new BasicNameValuePair("correo", cursor.getString(14).trim()));
                                        params.add(new BasicNameValuePair("zona", cursor.getString(16).trim()));
                                        params.add(new BasicNameValuePair("tipo_cliente", cursor.getString(17).trim()));
                                        params.add(new BasicNameValuePair("tipo", cursor.getString(18).trim()));
                                        params.add(new BasicNameValuePair("rubro", cursor.getString(19)));
                                        params.add(new BasicNameValuePair("sector", cursor.getString(20)));
                                        params.add(new BasicNameValuePair("current_ruc", ""+cursor.getString(21)));

                                        Log.e("razon_social", cursor.getString(0).trim());
                                        Log.e("ruc", cursor.getString(1).trim());
                                        Log.e("contacto", ""+cursor.getString(2).trim());
                                        Log.e("code", cursor.getString(3).trim());
                                        Log.e("direccion", cursor.getString(4).trim());
                                        Log.e("telefono_fijo", cursor.getString(5).trim());
                                        Log.e("telefono_movil", cursor.getString(6).trim());
                                        Log.e("perceptor", cursor.getString(7));
                                        Log.e("retenedor", cursor.getString(8));
                                        Log.e("porcentaje", cursor.getString(9));
                                        //Log.e("calificacion", cursor.getString(10).trim());
                                        Log.e("cond_pago", cursor.getString(11).trim());
                                        Log.e("cond_venta",cursor.getString(12).trim());
                                        Log.e("nombre_comercial", cursor.getString(13).trim());
                                        Log.e("correo", cursor.getString(14).trim());
                                        //Log.e("zona", cursor.getString(16).trim());
                                        Log.e("tipo_cliente", cursor.getString(17).trim());
                                        Log.e("tipo", cursor.getString(18).trim());
                                        Log.e("rubro", cursor.getString(19));
                                        Log.e("sector", cursor.getString(20));
                                        //Log.e("current_ruc", cursor.getString(21));
                                        System.out.println( cursor.getString(0).trim()+" Estado: " +cursor.getString(15) +" "+ cursor.getString(20));

                                        Log.e("Cantidad params",String.valueOf(params.size()));
                                        Log.e("Params",params.toString());
                                        httppost.setEntity(new UrlEncodedFormEntity(params));
                                        HttpResponse resp = httpclient.execute(httppost);
                                        HttpEntity ent = resp.getEntity();/*y obtenemos una respuesta*/
                                        if (HttpStatus.SC_ACCEPTED == resp.getStatusLine().getStatusCode() || HttpStatus.SC_OK == resp.getStatusLine().getStatusCode() ) {

                                            control.updateEstadoCliente(cursor.getString(1).trim());
                                        }else{
                                            System.out.print("Hubo un error al sincronizar");
                                            String text = EntityUtils.toString(ent);
                                            JSONObject json = new JSONObject(text);
                                            msj = json.get("ruc").toString();
                                            mensj = centa(cursor.getString(1).trim(),cursor.getString(0).trim());
                                            database.delete(Tb_Clientes.nombreTabla,Tb_Clientes.ruc +" = '"+cursor.getString(1).trim()+"'",null);
                                        }
                                    } catch (Exception e) {
                                        System.out.println("Error Sync Subieda 0: " + e.toString());
                                    }

                                } while (cursor.moveToNext());
                            }
                            cursor.close();
                        } catch (Exception e) {
                            System.out.println("Error-GetFullClientes: " + e.toString());
                        }

                    }

                    if (control.CheckDataSyncVisitas()) {
                        try {
                            String selectQuery = "SELECT " + Tb_Visita.cliente + "," + Tb_Visita.fecha_inicio + "," + Tb_Visita.fecha_final + "," +
                                    Tb_Visita.latitud_inicio+ "," + Tb_Visita.longitud_inicio + "," + Tb_Visita.latitud_final + "," + Tb_Visita.longitud_final
                                    + "," + Tb_Visita.motivo + "," + Tb_Visita.estado+",id"+"," + Tb_Visita.fecha_paraEnviar+"," + Tb_Visita.pendiente+
                                    "," + Tb_Visita.codigo+"," + Tb_Visita.direccion+" FROM " + Tb_Visita.nombreTabla + " WHERE " + Tb_Visita.estado + "= '0'";
                            SQLiteDatabase database = control.getWritableDatabase();
                            Cursor cursor = database.rawQuery(selectQuery, null);
                            if (cursor.moveToFirst()) {
                                do {
                                    try {
                                        HttpClient httpclient = new DefaultHttpClient();
                                        HttpPost httppost = new HttpPost(urlsV);
                                        httppost.setHeader("x-api-key", Preferences.getkeyUser());
                                        List<NameValuePair> params = new ArrayList<>();
                                        String id = String.valueOf(cursor.getInt(9));
                                        params.add(new BasicNameValuePair("cliente", cursor.getString(0)));
                                        params.add(new BasicNameValuePair("fecha_inicio", cursor.getString(1)));
                                        params.add(new BasicNameValuePair("fecha_fin", cursor.getString(2)));
                                        params.add(new BasicNameValuePair("latitud_inicio", cursor.getString(3)));
                                        params.add(new BasicNameValuePair("longitud_inicio", cursor.getString(4)));
                                        params.add(new BasicNameValuePair("latitud_fin", cursor.getString(5)));
                                        params.add(new BasicNameValuePair("longitud_fin", cursor.getString(6)));
                                        params.add(new BasicNameValuePair("motivo", cursor.getString(7)));
                                        params.add(new BasicNameValuePair("proxima_visita", cursor.getString(10)));
                                        params.add(new BasicNameValuePair("action", cursor.getString(11)));
                                        params.add(new BasicNameValuePair("id", id));         //Se cambia 12 por 9
                                        params.add(new BasicNameValuePair("direccion",cursor.getString(13)));

                                        System.out.println( cursor.getString(0)+" ID "+cursor.getString(1)+" :  " + cursor.getString(10) + " Action: " +cursor.getString(11));
                                        httppost.setEntity(new UrlEncodedFormEntity(params));
                                        HttpResponse resp = httpclient.execute(httppost);
                                        HttpEntity ent = resp.getEntity();/*y obtenemos una respuesta*/
                                        if (HttpStatus.SC_CREATED == resp.getStatusLine().getStatusCode()) {
                                            String text = EntityUtils.toString(ent);
                                            JSONObject json = new JSONObject(text);
                                            String iddd = cursor.getString(11).compareTo("1")==0?"":json.getString("id");
                                            System.out.println(iddd+"Error Sync Subieda 0: " + text + json.getString("id")  + " Action: " +cursor.getString(11));
                                            control.UpdateEstadoVisita(id,iddd);
                                        }else{
                                            String text = EntityUtils.toString(ent);
                                            System.out.println("Error111 Sync Subieda 0: " + text  + " Action: " +cursor.getString(11)+" " + cursor.getString(1) + " " +cursor.getString(2) );


                                        }
                                    } catch (Exception e) {
                                        System.out.println("Error Sync Subieda 0: " + e.toString());
                                    }

                                } while (cursor.moveToNext());
                            }
                            cursor.close();
                        } catch (Exception e) {
                            System.out.println("Error-GetFullClientes: " + e.toString());
                        }

                    }else{
                        System.out.println("NO HAY NADA");
                    }

                    try {
                        URL cliente = new URL(urls);
                        HttpURLConnection conCliente = (HttpURLConnection) cliente.openConnection();
                        conCliente.setRequestProperty("x-api-key", Preferences.getkeyUser());
                        if (conCliente.getResponseCode() == HttpURLConnection.HTTP_OK) {
                            InputStream inputStreamResponse = conCliente.getInputStream();
                            String linea;
                            StringBuilder respuestaCadena = new StringBuilder();
                            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStreamResponse, "UTF-8"));
                            while ((linea = bufferedReader.readLine()) != null) {
                                respuestaCadena.append(linea);
                            }

                            response = respuestaCadena.toString();
                            Log.d("clientes_response",response);
                            JSONArray jsonArray = new JSONArray(respuestaCadena.toString());
                            if(jsonArray.length()>0) {
                                control.CapturaDatosClientesJSONV2(jsonArray);
                            }else{
                                control.BorradaGeneral(Tb_Clientes.nombreTabla,"");
                            }
                            clientes = control.GetFullClientes();
                            adapter = new ListClienteAdapter(cont, clientes);
                        }else{
                            control.BorradaGeneral(Tb_Clientes.nombreTabla,"");
                            clientes = control.GetFullClientes();
                            adapter = new ListClienteAdapter(cont, clientes);
                        }
                    } catch (IOException e) {
                        System.out.println("Error1: " + e.toString());
                    } catch (JSONException e) {
                        control.BorradaGeneral(Tb_Clientes.nombreTabla, "");

                        e.printStackTrace();
                    }
                }else{
                    internet = false;
                }
            Log.e("DatosSynco","onBackGroundFinal");
            return 0;
        }


        @Override
        protected void onPostExecute(Integer integer) {
            super.onPostExecute(integer);
            view.setVisibility(View.VISIBLE);
            progreso.setVisibility(View.GONE);
            if(!msj.isEmpty()){
                Toast.makeText(cont,mensj,Toast.LENGTH_LONG).show();
            }
            recView.setAdapter(adapter);
            Log.e("DatosSynco", "onPostExecute");
        }


    }


    @Override
    public void onBackPressed() {
        Intent intent = new Intent(this,Menu_Principal.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
        finish();
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.actividad_principal, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.sync:

                new DatosSynco(ListaClientes.this).execute();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    Dialog customDialog;
    public void Cerrar() {
        customDialog = new Dialog(this);
        customDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        customDialog.setCancelable(false);
        customDialog.setContentView(R.layout.notificacion_01);
        (customDialog.findViewById(R.id.si)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view)
            {
                customDialog.dismiss();
                Intent intent = new Intent(ListaClientes.this,Inicio.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
                finish();
            }
        });
        (customDialog.findViewById(R.id.no)).setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view)
            {
                customDialog.dismiss();

            }
        });

        assert customDialog != null;
        customDialog.show();

    }
}
