package com.felixcabrita.cotizacionesamconsultores;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.HttpHeaderParser;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.felixcabrita.cotizacionesamconsultores.utils.Const;
import com.felixcabrita.cotizacionesamconsultores.utils.Util;
import com.felixcabrita.cotizacionesamconsultores.models.Tb_cotizacion_productos;
import com.felixcabrita.cotizacionesamconsultores.models.Tb_Cotizaciones;
import com.felixcabrita.cotizacionesamconsultores.models.Tb_EstadoCotizacion;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Historial_cotizacion extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {
    public ArrayList<ContentValues> cotizacion = new ArrayList<>();
    BaseDatosSqlite control;
    ListView historial;
    adapter_cotizacion adapter;
    private SharedPreferences.Editor editor;
    ProgressBar progreso;
    EditText busqueda;
    View view;
    SharedPreferences pref;
    int status = 0;
    boolean estadoSyntodo = false,yaya = false,sincronizacionactiva = false;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_principal_historial_cotizacion);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();
        view = findViewById(R.id.view);
        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        View headerView = navigationView.getHeaderView(0);
        busqueda = (EditText)findViewById(R.id.buscar);
        TextView tvHeaderName= (TextView) headerView.findViewById(R.id.usuario);
        pref = PreferenceManager.getDefaultSharedPreferences(this);
        estadoSyntodo = pref.getString("synctodo","0").compareTo("1")==0;
        tvHeaderName.setText(pref.getString("nombre","usuario"));
        historial = (ListView)findViewById(R.id.hisrotial);
        progreso = (ProgressBar) findViewById(R.id.progreso);
        control = new BaseDatosSqlite(this);

        historial.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                if(status==0){
                    Intent p = new Intent(Historial_cotizacion.this,DetalleHistorial.class);
                    p.putExtra(Tb_Cotizaciones.codigo,adapter.lista.get(i).get("id").toString());

                    p.putExtra("code", (adapter.lista.get(i).get(Tb_Cotizaciones.codigo) != null &&
                            adapter.lista.get(i).get(Tb_Cotizaciones.codigo).toString().compareTo("No stock disponible") == 0) ||
                            Integer.parseInt(adapter.lista.get(i).get(Tb_Cotizaciones.items).toString())<1);

                    p.putExtra("estado",adapter.lista.get(i).get(Tb_Cotizaciones.codigo) != null &&
                            adapter.lista.get(i).getAsInteger(Tb_Cotizaciones.estado)==5 &&
                            adapter.lista.get(i).getAsInteger("pdf")== 0 );

                    p.putExtra(Tb_Cotizaciones.moneda,Integer.parseInt(adapter.lista.get(i).get(Tb_Cotizaciones.moneda).toString()));
                    //Log.i("PASAR", );
                    startActivity(p);
                    finish();
                }else{
                    Toast.makeText(Historial_cotizacion.this,"Espere a que termine de cargar la lista",Toast.LENGTH_SHORT).show();
                }
            }
        });

        busqueda.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                cotizacion = control.GetFullFiltroCotizacion(s.toString());
                adapter = new adapter_cotizacion(Historial_cotizacion.this, cotizacion);
                historial.setAdapter(adapter);
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }

        });

    }

    @Override
    protected void onResume() {
        super.onResume();
        new Datos(this).execute();
        SincronizarTodo();
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {

        int id = item.getItemId();

        switch (id){
            case R.id.inicio:
                Intent i33 = new Intent(Historial_cotizacion.this, Menu_Principal.class);
                startActivity(i33);
                finish();
                break;
            case R.id.usuari:
                Intent i = new Intent(Historial_cotizacion.this,DatosUsuario.class);
                startActivity(i);
                finish();
                break;
            case R.id.clientes:
                Intent i1 = new Intent(Historial_cotizacion.this, ListaClientes.class);
                startActivity(i1);
                finish();
                break;
            case R.id.cerrar:
                Cerrar();
                break;
            case R.id.cotizar:
                Intent i3 = new Intent(Historial_cotizacion.this,ActividadPrincipal.class);
                i3.putExtra("edit",0);
                startActivity(i3);
                finish();
                break;
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);

        return false;
    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent(this,Menu_Principal.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
        finish();
    }


    Dialog customDialog;
    public void Cerrar() {
        customDialog = new Dialog(this);
        customDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        customDialog.setCancelable(false);
        customDialog.setContentView(R.layout.notificacion_01);
        (customDialog.findViewById(R.id.si)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view)
            {
                customDialog.dismiss();
                finish();
                Intent intent = new Intent(Historial_cotizacion.this,Inicio.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);

            }
        });
        (customDialog.findViewById(R.id.no)).setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view)
            {
                customDialog.dismiss();

            }
        });

        assert customDialog != null;
        customDialog.show();

    }

    private class Datos extends AsyncTask<Void,Void,Integer> {


        ProgressDialog pd;
        Context cont;

        public Datos(Context c){
            try{
                cont = c;
                pd =  new ProgressDialog(c);
                pd.setIndeterminate(true);
                pd.setMessage("Cargando Datos");
                pd.setCancelable(false);
                pd.show();
            }catch (Exception e){
                System.out.println("Error1: "+e.toString());
            }

        }


        @Override
        protected void onPreExecute() {
            super.onPreExecute();

        }

        @Override
        protected Integer doInBackground(Void... voids) {
            cotizacion = control.GetFullCotizacion();
            adapter = new adapter_cotizacion(cont, cotizacion);
            return 0;
        }


        @Override
        protected void onPostExecute(Integer integer) {
            super.onPostExecute(integer);
            historial.setAdapter(adapter);
            pd.dismiss();

        }


    }

    public void EnviarCorreo(final String idcotizacion){
        RequestQueue queue = Volley.newRequestQueue(this);
        Log.i("IDCOTIZACION", idcotizacion);
        String url = Const.URL_V1 + "email/cotizacion";
        StringRequest postRequest = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>()
                {
                    @Override
                    public void onResponse(String response) {
                        // response
                        Log.d("ResponseCorreo", response);
                    }
                },
                new Response.ErrorListener()
                {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.i("LOGIN1", "error");
                        NetworkResponse response = error.networkResponse;
                        if (error instanceof AuthFailureError && response != null) {
                            Log.i("LOGIN2", error.toString());
                            try {
                                String res = new String(response.data,
                                        HttpHeaderParser.parseCharset(response.headers, "utf-8"));
                                // Now you can use any deserializer to make sense of data
                                JSONObject obj = new JSONObject(res);
                                Log.i("ERROR", obj.toString());
                            } catch (UnsupportedEncodingException e1) {
                                // Couldn't properly decode data to string
                                Log.i("error1", e1.getMessage());
                            } catch (JSONException e2) {
                                // returned data is not JSONObject?
                                Log.i("error2", e2.getMessage());
                            }
                        }
                    }
                }
        ) {
            @Override
            protected Map<String, String> getParams()
            {
                Map<String, String>  params = new HashMap<>();
                params.put("codigo", idcotizacion);

                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String>  params = new HashMap<>();
                params.put("x-api-key", pref.getString("key", ""));
                return params;
            }
        };
        postRequest.setRetryPolicy(new DefaultRetryPolicy(0, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        queue.add(postRequest);
    }

    public void SincronizarCoti(final int estadoSynco){
        Log.i("SINCRONIZADO", estadoSynco+"");
        String selectQuery = "SELECT id,"+Tb_Cotizaciones.fecha+","+Tb_Cotizaciones.fecha_registro+
                ","+Tb_Cotizaciones.cliente+","+Tb_Cotizaciones.vendedor+","+Tb_Cotizaciones.cond_pago+
                ","+Tb_Cotizaciones.cond_venta+","+Tb_Cotizaciones.flete+","+Tb_Cotizaciones.tiempo_validez+
                ","+Tb_Cotizaciones.lugar_cotizacion+","+Tb_Cotizaciones.moneda+","+Tb_Cotizaciones.subtotal+
                ","+Tb_Cotizaciones.monto_igv+","+Tb_Cotizaciones.monto_total+","+Tb_Cotizaciones.observaciones+
                ","+Tb_Cotizaciones.por_revision+","+Tb_Cotizaciones.items+","+Tb_Cotizaciones.estadoSynco+
                ","+Tb_Cotizaciones.estado+","+Tb_Cotizaciones.porcentaje+","+Tb_Cotizaciones.latirud+","+Tb_Cotizaciones.longitud+
                ","+Tb_Cotizaciones.lugar+","+Tb_Cotizaciones.icotem+",pdf,"+Tb_Cotizaciones.codigo+","+Tb_Cotizaciones.lugar_direccion+" FROM "+Tb_Cotizaciones.nombreTabla +" WHERE "+Tb_Cotizaciones.estadoSynco + " = "+estadoSynco;

        SQLiteDatabase database = control.getWritableDatabase();
        Cursor cursor = database.rawQuery(selectQuery, null);
        if (cursor.moveToFirst()) {
            do {
                try {
                    final Map<String, String> parames = new HashMap<>();
                    final String id = cursor.getString(0);
                    final int envio = Integer.parseInt(cursor.getString(24));
                    Log.i("ENVIO PDF", envio+" a ID " + id);
                    if(estadoSynco==2){
                        parames.put("codigo", cursor.getString(25));
                    }

                    parames.put("fecha", cursor.getString(1));
                    parames.put("fecha_registro", cursor.getString(2));
                    parames.put("ruc", cursor.getString(3));
                    parames.put("vendedor", cursor.getString(4));
                    parames.put("cond_pago", cursor.getString(5));
                    parames.put("cond_venta", cursor.getString(6));
                    parames.put("flete", cursor.getString(7));
                    parames.put("tiempo_validez", cursor.getString(8));
                    parames.put("lugar_cotizacion", cursor.getString(9));
                    parames.put("moneda", cursor.getString(10));
                    parames.put("subtotal", cursor.getString(11));
                    parames.put("monto_igv", cursor.getString(12));
                    parames.put("monto_total", cursor.getString(13));
                    parames.put("estado", cursor.getString(18));
                    parames.put("observaciones", cursor.getString(14));
                    parames.put("monto_percepcion", cursor.getString(19));
                    parames.put("latitud", cursor.getString(20));
                    parames.put("longitud", cursor.getString(21));
                    parames.put("lugar_entrega", cursor.getString(22));
                    parames.put("icoterms", cursor.getString(23));
                    //parames.put("estado", cursor.getString(18));
                    Log.e("ContactoLugarDireccion2",cursor.getString(26));
                    parames.put("lugar_direccion",cursor.getString(26));
                    parames.put("enviar",String.valueOf(envio));

                    String selectQuery1 = "SELECT " + Tb_cotizacion_productos.producto + "," + Tb_cotizacion_productos.cantidad + ","
                            + Tb_cotizacion_productos.precio + "," + Tb_cotizacion_productos.produccion + "," +
                            Tb_cotizacion_productos.cunio + "," + Tb_cotizacion_productos.subtotal + "," + Tb_cotizacion_productos.igv +
                            "," + Tb_cotizacion_productos.total + "," + Tb_cotizacion_productos.color +","+Tb_cotizacion_productos.producto_nombre+
                            ","+Tb_cotizacion_productos.requerido+ ","+Tb_cotizacion_productos.action+ ","+Tb_cotizacion_productos.idproduc+ " FROM " + Tb_cotizacion_productos.nombreTabla + " WHERE " +
                            Tb_cotizacion_productos.cotizacion + " = " + id;

                    Cursor cursor1 = database.rawQuery(selectQuery1, null);
                    int contador1 = 0;
                    try {
                        if (cursor1.moveToFirst()) {
                            do {
                                Log.e("Producto " + contador1,"codigo: " + cursor1.getString(0) + "\naction: " + cursor1.getString(11) + "\nid: " + cursor1.getString(12));
                                parames.put("productos[codigo][" + contador1 + "]", cursor1.getString(0));
                                parames.put("productos[cantidad]["+contador1+"]", cursor1.getString(1));
                                parames.put("productos[precio]["+contador1+"]", cursor1.getString(2));
                                parames.put("productos[para]["+contador1+"]", cursor1.getString(3));
                                parames.put("productos[cunio]["+contador1+"]", cursor1.getString(4));
                                parames.put("productos[subtotal]["+contador1+"]", cursor1.getString(5));
                                parames.put("productos[igv]["+contador1+"]", cursor1.getString(6));
                                parames.put("productos[total]["+contador1+"]", cursor1.getString(7));
                                parames.put("productos[color]["+contador1+"]", cursor1.getString(8));
                                parames.put("productos[producto]["+contador1+"]", cursor1.getString(9));

                                parames.put("productos[action]["+contador1+"]", cursor1.getString(11));
                                Log.e("id_producto " + cursor1.getString(0), cursor1.getString(12));
                                parames.put("productos[id]["+contador1+"]", cursor1.getString(12));

                                contador1++;

                            } while (cursor1.moveToNext());
                        }
                        cursor1.close();
                    } catch (Exception e) {
                        System.out.println("Error-GetItemsCotizacion(): " + e.toString());
                    }
                    Log.e("cantidad objetos","iterador " + contador1);
                    Log.i("PARAMETROS VOLLEY "+estadoSynco, parames.toString());

                    RequestQueue queue = Volley.newRequestQueue(this);
                    String url = Const.URL_V1 + "cotizaciones";
                    if(estadoSynco==2){
                        url = Const.URL_V1 + "cotizaciones/update";
                    }
                    StringRequest postRequest = new StringRequest(Request.Method.POST, url,
                            new Response.Listener<String>()
                            {
                                @Override
                                public void onResponse(String response) {
                                    // response
                                    Log.d("Response", response);
                                    try {
                                        JSONObject objeto = new JSONObject(response);
                                        String idrespuesta = objeto.getString("id");
                                        if(idrespuesta.equals("0")){
                                            control.updateEstadoCotiza(id, "No stock disponible",0);
                                        }else{
                                            control.updateEstadoCotiza(id, idrespuesta, envio);
                                            if(envio==1){
                                                Log.e("EnviarCorreo","Coti " + objeto.getString("id") + " Envio " + envio);
                                                //EnviarCorreo(objeto.getString("id"));
                                            }
                                        }


                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }

                                }
                            },
                            new Response.ErrorListener()
                            {
                                @Override
                                public void onErrorResponse(VolleyError error) {
                                    // As of f605da3 the following should work
                                    NetworkResponse response = error.networkResponse;
                                    if (error instanceof ServerError && response != null) {
                                        try {
                                            String res = new String(response.data,
                                                    HttpHeaderParser.parseCharset(response.headers, "utf-8"));
                                            // Now you can use any deserializer to make sense of data
                                            JSONObject obj = new JSONObject(res);
                                            Log.i("ERROR", obj.toString());
                                        } catch (UnsupportedEncodingException e1) {
                                            // Couldn't properly decode data to string
                                            e1.printStackTrace();
                                        } catch (JSONException e2) {
                                            // returned data is not JSONObject?
                                            e2.printStackTrace();
                                        }
                                    }
                                }
                            }
                    ) {

                        @Override
                        public Map<String, String> getHeaders() throws AuthFailureError {
                            Map<String, String>  params = new HashMap<>();
                            params.put("x-api-key", pref.getString("key", ""));
                            return params;
                        }

                        @Override
                        protected String getParamsEncoding() {
                            return super.getParamsEncoding();
                        }

                        @Override
                        protected Map<String, String> getParams()
                        {
                            return parames;
                        }
                    };
                    postRequest.setRetryPolicy(new DefaultRetryPolicy(0, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                    queue.add(postRequest);

                } catch (Exception e) {
                    System.out.println("Error Sync Subieda 0: " + e.toString());
                }

            } while (cursor.moveToNext());
        }
        cursor.close();


    }

    public void Aprobar(final String idcotizacion){
        RequestQueue queue = Volley.newRequestQueue(this);
        Log.i("IDCOTIZACION", idcotizacion);
        String url = Const.URL_V1 + "cotizaciones/aprobar";
        StringRequest postRequest = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>()
                {
                    @Override
                    public void onResponse(String response) {
                        // response
                        Log.d("Response", response);
                        control.updateEstadoCotizaPPP(idcotizacion);
                    }
                },
                new Response.ErrorListener()
                {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.i("LOGIN1", "error");

                    }
                }
        ) {
            @Override
            protected Map<String, String> getParams()
            {
                Map<String, String>  params = new HashMap<>();
                params.put("codigo", idcotizacion);

                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String>  params = new HashMap<>();
                params.put("x-api-key", pref.getString("key", ""));
                return params;
            }
        };
        postRequest.setRetryPolicy(new DefaultRetryPolicy(0, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        queue.add(postRequest);
    }

    public void Rechazar(final String idcotizacion){
        RequestQueue queue = Volley.newRequestQueue(this);
        Log.i("IDCOTIZACION", idcotizacion);
        String url = Const.URL_V1 + "cotizaciones/rechazar";
        StringRequest postRequest = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>()
                {
                    @Override
                    public void onResponse(String response) {
                        // response
                        Log.d("Response", response);
                        control.updateEstadoCotizaPPP(idcotizacion);
                    }
                },
                new Response.ErrorListener()
                {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.i("LOGIN1", "error");

                    }
                }
        ) {
            @Override
            protected Map<String, String> getParams()
            {
                Map<String, String>  params = new HashMap<String, String>();
                params.put("codigo", idcotizacion);

                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String>  params = new HashMap<String, String>();
                params.put("x-api-key", pref.getString("key", ""));
                return params;
            }
        };
        postRequest.setRetryPolicy(new DefaultRetryPolicy(0, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        queue.add(postRequest);
    }

    private String getQuery(List<NameValuePair> params) throws UnsupportedEncodingException
    {
        StringBuilder result = new StringBuilder();
        boolean first = true;

        for (NameValuePair pair : params)
        {
            if (first)
                first = false;
            else
                result.append("&");

            result.append(URLEncoder.encode(pair.getName(), "UTF-8"));
            result.append("=");
            result.append(URLEncoder.encode(pair.getValue(), "UTF-8"));
        }

        return result.toString();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.actividad_principal, menu);
        return super.onCreateOptionsMenu(menu);
    }



    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.sync:
                if(!yaya){
                    yaya = true;
                    //new SyncCotizaciones(Historial_cotizacion.this).execute();
                    SincronizarTodo();
                }else{
                    Toast.makeText(Historial_cotizacion.this,"Espere a que termine de cargar la lista",Toast.LENGTH_SHORT).show();
                }

                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public void SincronizarTodo(){
        new SyncCotizacionesUltimo(this).execute();
    }

    private class SyncCotizacionesUltimo extends AsyncTask<Void,Void,Integer> {

        RequestQueue queue;
        Context cont;
        String urls = Const.URL_V1 + "cotizaciones";
        String mensajeTask;
        public SyncCotizacionesUltimo(Context c){
            try{
                cont = c;
                queue = Volley.newRequestQueue(c);
            }catch (Exception e){
                System.out.println("Error1: "+e.toString());
            }

        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            status = 1;
            progreso.setVisibility(View.VISIBLE);
            view.setVisibility(View.GONE);
        }

        @Override
        protected Integer doInBackground(Void... voids) {
            int flagreturn = 0;
            if (Util.isNetworkAvailable(cont)) {


                SincronizarCoti(0);
                SincronizarCoti(2);

                if (control.CheckDataSyncCotizaEstado()) {
                    try {
                        String selectQuery = "SELECT "+Tb_EstadoCotizacion.codigo+" FROM "+Tb_EstadoCotizacion.nombreTabla +
                                " WHERE "+Tb_EstadoCotizacion.estadoSynco + " = 0 AND " + Tb_EstadoCotizacion.estado +" = 3";
                        SQLiteDatabase database = control.getWritableDatabase();
                        Cursor cursor = database.rawQuery(selectQuery, null);
                        if (cursor.moveToFirst()) {
                            do {
                                Aprobar(cursor.getString(0));

                            } while (cursor.moveToNext());
                        }
                        cursor.close();


                        String selectQuery1 = "SELECT "+Tb_EstadoCotizacion.codigo+" FROM "+Tb_EstadoCotizacion.nombreTabla +
                                " WHERE "+Tb_EstadoCotizacion.estadoSynco + " = 0 AND " + Tb_EstadoCotizacion.estado +" = 4";
                        database = control.getWritableDatabase();
                        Cursor cursor1 = database.rawQuery(selectQuery1, null);
                        if (cursor1.moveToFirst()) {
                            do {
                                Rechazar(cursor1.getString(0));


                            } while (cursor1.moveToNext());
                        }
                        cursor1.close();

                        cursor.close();
                    } catch (Exception e) {
                        System.out.println("Error-GetFullClientes: " + e.toString());
                    }

                }

                try {
                    URL producto = new URL(urls);
                    HttpURLConnection conProducto = (HttpURLConnection) producto.openConnection();
                    Log.e("key",pref.getString("key", ""));
                    conProducto.setRequestProperty("x-api-key", pref.getString("key", ""));
                    //connection.setRequestProperty("Authorization", "Basic " + Base64.encodeToString((user + ":" + pass).getBytes(), Base64.NO_WRAP));
                    if (conProducto.getResponseCode() == HttpURLConnection.HTTP_OK) {
                        InputStream inputStreamResponse = conProducto.getInputStream();
                        String linea;
                        StringBuilder respuestaCadena = new StringBuilder();
                        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStreamResponse, "UTF-8"));
                        while ((linea = bufferedReader.readLine()) != null) {
                            respuestaCadena.append(linea);
                        }
                        Log.e("coti_response",respuestaCadena.toString());
                        Object json = new JSONTokener(respuestaCadena.toString()).nextValue();
                        if (json instanceof JSONObject){
                            Log.i("error", respuestaCadena.toString());
                            JSONObject vacio = (JSONObject) json;
                            mensajeTask = vacio.getString("message");
                            control.BorradaCotizacion();
                            cotizacion = control.GetFullCotizacion();
                            adapter = new adapter_cotizacion(cont, cotizacion);
                            flagreturn = -1;
                        }
                        else if (json instanceof JSONArray){
                            Log.i("SINCRONIZADOS COTI", respuestaCadena.toString());
                            JSONArray jsonArray = (JSONArray) json;
                            Log.e("JSON_ARRAY",jsonArray.toString());
                            if (jsonArray.length() > 0) {
                                control.BorradaCotizacion();
                                for (int i = 0; i < jsonArray.length(); i++) {
                                    JSONObject cotizacionObject = jsonArray.getJSONObject(i);

                                    String codigo = cotizacionObject.get("codigo").toString();
                                    String sync = cotizacionObject.get("sync").toString();

                                    sincronizacionactiva = true;
                                    SQLiteDatabase database1 = control.getWritableDatabase();
                                    try {
                                        Log.i("EXESQL", "DELETE FROM  cotizaciones WHERE id = " + "(SELECT id FROM cotizaciones WHERE coti_codigo = '" + codigo + "')");
                                        database1.execSQL("DELETE FROM  cotizacion_productos WHERE copr_cotizacion = " + "(SELECT id FROM cotizaciones WHERE coti_codigo = '" + codigo + "')");
                                        database1.execSQL("DELETE FROM  cotizaciones WHERE id = " + "(SELECT id FROM cotizaciones WHERE coti_codigo = '" + codigo + "')");
                                    } catch (Exception e) {
                                        System.out.println("Error-GetProducto: " + e.toString());
                                    }

                                    control.capturaCabezeraCotizacionV2(cotizacionObject);
                                }
                                cotizacion = control.GetFullCotizacion();
                                Log.e("CotizacionesSize",String.valueOf(cotizacion.size()));
                                adapter = new adapter_cotizacion(cont, cotizacion);
                                //cotizacion = control.GetFullCotizacion();
                                //adapter = new adapter_cotizacion(cont, cotizacion);
                            } else {
                                control.BorradaCotizacion();
                                cotizacion = control.GetFullCotizacion();
                                adapter = new adapter_cotizacion(cont, cotizacion);
                            }
                        }

                        if (sincronizacionactiva) {
                            HttpClient httpclient = new DefaultHttpClient();
                            HttpPost httppost = new HttpPost(urls + "/sincronizadas");
                            httppost.setHeader("x-api-key", pref.getString("key", ""));
                            HttpResponse resp = httpclient.execute(httppost);
                            HttpEntity ent = resp.getEntity();/*y obtenemos una respuesta*/
                            if (HttpStatus.SC_OK == resp.getStatusLine().getStatusCode()) {
                                System.out.println("Sincronización hecha ");
                                editor = pref.edit();
                                editor.putString("synctodo", "1");
                                editor.apply();
                                estadoSyntodo = pref.getString("synctodo","0").compareTo("1")==0;
                            }
                        }
                    } else {
                        control.BorradaCotizacion();
                        control.BorradaCotizacion();
                        cotizacion = control.GetFullCotizacion();
                        adapter = new adapter_cotizacion(cont, cotizacion);
                        System.out.println("Error2 " + conProducto.getResponseCode());
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }else{
                flagreturn = -2;
                mensajeTask = "No cuenta con señal de internet";
            }
            return flagreturn;
        }

        @Override
        protected void onPostExecute(Integer integer) {
            super.onPostExecute(integer);
            if(integer==-1){
                Toast.makeText(Historial_cotizacion.this, mensajeTask, Toast.LENGTH_LONG).show();
            }else{
                if(integer==-2){
                    Util.mostrarMensaje(Historial_cotizacion.this, mensajeTask);
                }
            }
            view.setVisibility(View.VISIBLE);
            progreso.setVisibility(View.GONE);
            historial.setAdapter(adapter);
            status = 0;
            yaya = false;
        }

    }
}
