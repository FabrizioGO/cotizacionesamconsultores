package com.felixcabrita.cotizacionesamconsultores;

/**
 * Creado por Felix Cabrita en 29/6/2017.
*/
import android.content.ContentValues;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.ArrayList;
import java.util.Locale;

import com.felixcabrita.cotizacionesamconsultores.models.Tb_cotizacion_productos;

public class adapter_producto extends BaseAdapter {

    private DecimalFormat convertidor = new DecimalFormat("#,##0.00", DecimalFormatSymbols.getInstance(Locale.ITALIAN));   // CONVERTIDOR FLOAT \\
    ArrayList<ContentValues> lista = new ArrayList<>();
    private Context propio;
    int moneda = 0;
    adapter_producto(Context c, ArrayList<ContentValues> l,int _m){
        this.propio = c;
        this.lista = l;
        moneda = _m;
        DecimalFormatSymbols dfs = new DecimalFormatSymbols();
        dfs.setDecimalSeparator('.');
        dfs.setGroupingSeparator(',');
        convertidor.setDecimalFormatSymbols(dfs);
    }

    @Override
    public int getCount() {
        return lista.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        TextView producto,cantidad,precio;
        LayoutInflater inf = (LayoutInflater) propio.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        if (null == convertView) {
            convertView = inf.inflate(R.layout.adapter_producto, parent, false);
        }
        producto = (TextView)convertView.findViewById(R.id.producto);
        cantidad = (TextView)convertView.findViewById(R.id.cantidad);
        precio = (TextView)convertView.findViewById(R.id.precio);
        producto.setText(lista.get(position).get(Tb_cotizacion_productos.producto).toString());
        cantidad.setText(lista.get(position).get(Tb_cotizacion_productos.cantidad).toString());
        float too = Float.parseFloat(lista.get(position).get(Tb_cotizacion_productos.total).toString());
        precio.setText(((moneda==0)?"US$ ":"S/. ")+convertidor.format(too));
        return convertView;
    }
}

