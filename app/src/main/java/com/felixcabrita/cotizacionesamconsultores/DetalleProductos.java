package com.felixcabrita.cotizacionesamconsultores;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.ContentValues;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageRequest;
import com.android.volley.toolbox.Volley;
import com.felixcabrita.cotizacionesamconsultores.models.Tb_Productos;
import com.felixcabrita.cotizacionesamconsultores.models.Tb_cotizacion_productos;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.ArrayList;
import java.util.Locale;

public class DetalleProductos extends AppCompatActivity implements View.OnClickListener {

    private static final String TAG = "PostAdapter";
    private DecimalFormat convertidor = new DecimalFormat("#,##0.00", DecimalFormatSymbols.getInstance(Locale.ITALIAN));   // CONVERTIDOR FLOAT \\
    String[] Item = new String[12];
    BaseDatosSqlite control;
    public  ArrayList<ContentValues> producto = new ArrayList<>(),productoOriging;
    float converSolesDolar = 3.27f;
    TextView nombre, codigo, stock, igv,total,cuniomuestra,subtotal,moneda1,compro,dispo;
    EditText cantidad,precio;
    int moneda;
    CheckBox cunio,produc,requerido;
    float precio1 = 0,ttotal,sbtotal,figv,precioOriginal;
    float valorcunia = 0;
    int CTD = 0;
    int stocks = 0,comprometidos = 0,disponibles = 0;
    LinearLayout vcunio;
    int estatus = 0;
    int poss = 0;
    String codigoS = "";
    int monedaProducto = 0;
    ListClienteAdapter adapter;
    int cantitempo = 0,codigocotizacion = 0,convertido;
    float totaltempo = 0;
    SharedPreferences pref;
    private SharedPreferences.Editor editor;
    /*SI MONEDA = 0  ES SOLES SI ES 1 ES US$*/
    private RequestQueue requestQueue;
    String URL_BASE = "http://ventas.novatecpe.com:8080/novatec_desarrollo/public/resources/products/";
    String item = "";
    Bitmap ImagenProducto;
    FloatingActionButton visual;
    int flete =0;
    int para = 0,esta=0,cantidadEscogidos = 0,tipo =1;
    float valorIGV = 0.18f;
    int tienecunio = 0;
    int rango_menor = 0,EstatusEdición = 0;
    float valorCunioOrig = 42.50f;
    String id  ="0";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.detalle_producto);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        converSolesDolar = Inicio.conversorMonedas;
        DecimalFormatSymbols dfs = new DecimalFormatSymbols();
        dfs.setDecimalSeparator('.');
        dfs.setGroupingSeparator(',');
        convertidor.setDecimalFormatSymbols(dfs);
        control = new BaseDatosSqlite(this);
        estatus = getIntent().getExtras().getInt("estatus");
        convertido = getIntent().getExtras().getInt("convertido");
        moneda = getIntent().getExtras().getInt("moneda");
        flete =  getIntent().getExtras().getInt("flete");
        EstatusEdición =  getIntent().getExtras().getInt("EstatusEdición");
        codigocotizacion = getIntent().getExtras().getInt(Tb_cotizacion_productos.cotizacion);
       // Toast.makeText(this,"Toas:" + codigocotizacion,Toast.LENGTH_SHORT).show();
        tipo = getIntent().getExtras().getInt("tipo");
        Log.i("TIPO", tipo+"");
        valorIGV = (tipo==1)?0.18f:0;

        pref = PreferenceManager.getDefaultSharedPreferences(this);
        editor = pref.edit();
        visual = (FloatingActionButton)findViewById(R.id.imagen);
        visual.setVisibility(View.GONE);
        editor.putInt("Services",1);
        editor.apply();
        nombre = (TextView)findViewById(R.id.nombre);
        codigo =(TextView)findViewById(R.id.codigo);
        stock = (TextView) findViewById(R.id.stock);
        compro = (TextView)findViewById(R.id.comprometido);
        dispo = (TextView)findViewById(R.id.disponibles);
        precio =(EditText)findViewById(R.id.precio);
        subtotal =(TextView)findViewById(R.id.subtotal);
        igv = (TextView) findViewById(R.id.igv);
        total =(TextView)findViewById(R.id.total);
        cantidad  = (EditText)findViewById(R.id.cantidad);
        moneda1 =(TextView)findViewById(R.id.moneda);
        codigocotizacion = getIntent().getExtras().getInt(Tb_cotizacion_productos.cotizacion);
        cunio = (CheckBox)findViewById(R.id.cunio);
        cunio.setChecked(false);
        produc = (CheckBox)findViewById(R.id.produccion);
        vcunio = (LinearLayout)findViewById(R.id.view_cunio);
        requerido = (CheckBox)findViewById(R.id.requerido);
        cuniomuestra = (TextView) findViewById(R.id.vcunio);
        if(tipo==2){
            produc.setVisibility(View.GONE);
        }
        moneda1.setText(((moneda==0)?"US$ ":"S/. "));
        PrecioTrue(flete != 0);
        CantiTrue(false);
        //new Datos(this).execute();

        producto = control.GetFullProductos();
        productoOriging = producto;

        precio.setText("");
        valorCunioOrig = tipo==2?50.15f:42.50f;
        valorcunia = (moneda == 0)?valorCunioOrig: 42.50f* converSolesDolar;


        cantidad.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                try{
                    CTD = (s.toString().compareTo("") == 0)?0:Integer.parseInt(s.toString());
                    sbtotal = (CTD * precio1) + ((cunio.isChecked() && (CTD<500))?valorcunia:0);
                    figv = (float)(sbtotal * valorIGV);
                    ttotal = sbtotal + figv;
                    imprecion();
                }catch (Exception e){
                    Toast.makeText(DetalleProductos.this,"No debes colocar simbolos " + e.toString(),Toast.LENGTH_SHORT).show();
                    System.out.println("Error: " + e.toString());

                }
            }
            @Override
            public void afterTextChanged(Editable s) {
            }
        });


        produc.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                if(esta == 1){
                    if (para == 0) {
                        produc.setChecked(true);
                    } else {
                        produc.setChecked(false);
                    }
                }

                if (produc.isChecked()) {
                    requerido.setChecked(false);
                }
            }
        });

        requerido.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(esta != 1){
                    if (requerido.isChecked()) {
                        produc.setChecked(false);
                    }
                }

                if(tipo==2){
                    if(esta == 1){
                        if (para == 2) {
                            requerido.setChecked(false);
                        } else {
                            requerido.setChecked(true);
                        }
                    }

                }


            }
        });


        precio.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }
            @Override
            public void afterTextChanged(Editable s) {
                try{
                    if(flete==1){
                        precio1 = (s.toString().compareTo("") == 0)?0:Float.parseFloat(s.toString());
                        sbtotal = (CTD * precio1) + ((cunio.isChecked() && (CTD<rango_menor))?valorcunia:0);
                        figv = (float)(sbtotal * valorIGV);
                        ttotal = sbtotal + figv;
                        imprecion();
                    }
                }catch (Exception e){
                    Toast.makeText(DetalleProductos.this,"No debes colocar simbolos " + e.toString(),Toast.LENGTH_SHORT).show();
                    System.out.println("Error: " + e.toString());
                }

            }
        });

        cunio.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if(cunio.isChecked()){
                    if(CTD<1){
                        Toast.makeText(DetalleProductos.this,"Debe colocar una cantidad",Toast.LENGTH_SHORT).show();
                        cunio.setChecked(false);
                    }else{
                        //if((cunio.isChecked() && (CTD<500))){
                            vcunio.setVisibility(View.VISIBLE);
                            cuniomuestra.setText(((moneda==0)?"US$ ":"S/. ")+ convertidor.format(valorcunia));
                            sbtotal = sbtotal +valorcunia;
                            figv = (sbtotal *valorIGV);
                            ttotal = sbtotal + figv;
                            imprecion();
                        //}else{
                            //cuniomuestra.setText(((moneda==0)?"US$ ":"S/. ")+ 0);
                        //}

                    }
                }else{
                    if((CTD<500)) {
                        vcunio.setVisibility(View.GONE);
                        sbtotal = sbtotal - valorcunia;
                        figv = (float) (sbtotal * valorIGV);
                        ttotal = sbtotal + figv;
                        imprecion();
                    }
                }

                if(CTD<1){
                    Toast.makeText(DetalleProductos.this,"Debe colocar una cantidad",Toast.LENGTH_SHORT).show();
                }

            }
        });

        if(estatus == 1){
            poss = getIntent().getExtras().getInt("posicion");
            Log.i("POSICION1", poss+"");
            Log.i("POSICION2", ActividadPrincipal.productos.size()+"");
            Log.i("POSICION3", ActividadPrincipal.productos.get(poss).get(Tb_cotizacion_productos.producto)+"");
            int colo = getIntent().getExtras().getInt(Tb_cotizacion_productos.color);
            codigoS = getIntent().getExtras().getString(Tb_cotizacion_productos.producto);
            //control.GetFullProductos();
            ContentValues auxcontent = control.GetProducto(ActividadPrincipal.productos.get(poss).get(Tb_cotizacion_productos.producto).toString());
            if(auxcontent.get(Tb_Productos.nombre)!=null){
                LLenadoInfor(control.GetProducto(ActividadPrincipal.productos.get(poss).get(Tb_cotizacion_productos.producto).toString()),estatus);
                CantiTrue(true);
                action = ActividadPrincipal.productos.get(poss).getAsInteger(Tb_cotizacion_productos.action);
                ContentValues datos = ActividadPrincipal.productos.get(poss);
                id = datos.getAsString(Tb_cotizacion_productos.idproduc);
                esta = datos.get(Tb_cotizacion_productos.color)!=null?datos.getAsInteger(Tb_cotizacion_productos.color):0;
                para = esta ==1 ? (datos.getAsInteger(Tb_cotizacion_productos.produccion)==0?1:0):0;
                CTD =Integer.parseInt(datos.get(Tb_cotizacion_productos.cantidad).toString());
                disponibles =disponibles + CTD;
                comprometidos = comprometidos - CTD;
                dispo.setText(disponibles + "");
                compro.setText(comprometidos + "");
                if(monedaProducto== moneda){
                    precio1 = Float.parseFloat(datos.get(Tb_cotizacion_productos.precio).toString());
                }else{
                    precio1 =  Float.parseFloat(datos.get(Tb_cotizacion_productos.precio).toString());
                }
                Log.i("POS", disponibles+" - "+comprometidos+" - "+CTD);
                cantidad.setText(""+ CTD);
                totaltempo = Float.parseFloat(datos.get(Tb_cotizacion_productos.total).toString());
                cantitempo = Integer.parseInt(cantidad.getText().toString());
                cunio.setChecked((datos.get(Tb_cotizacion_productos.cunio).toString().compareTo("1") == 0));
                produc.setChecked((datos.get(Tb_cotizacion_productos.produccion).toString().compareTo("1") == 0));
                requerido.setChecked((datos.get(Tb_cotizacion_productos.requerido) != null && datos.get(Tb_cotizacion_productos.requerido).toString().compareTo("1") == 0) || (datos.get(Tb_cotizacion_productos.produccion).toString().compareTo("2") == 0));
                cuniomuestra.setText(((moneda==0)?"US$ ":"S/. ")+ convertidor.format(valorcunia));
                sbtotal = (CTD * precio1) + (((cunio.isChecked() && (CTD<500)))?valorcunia:0);
                figv = (float)(sbtotal *valorIGV);
                ttotal = sbtotal + figv;
                precio.setText(""+precio1);
                imprecion();
            }else{
                Toast.makeText(DetalleProductos.this, "Producto ya no existe", Toast.LENGTH_LONG).show();
                DetalleProductos.this.finish();
            }


        }

    }


    void imprecion(){
        subtotal.setText(((moneda==0)?"US$ ":"S/. ")+ convertidor.format(sbtotal));
        igv.setText(((moneda==0)?"US$ ":"S/. ")+ convertidor.format(figv));
        total.setText(((moneda==0)?"US$ ":"S/. ")+ convertidor.format(ttotal));
    }

    void Limpiar(){
        CTD = 0;
        ttotal = 0;
        figv = 0;
        sbtotal =0;
        CantiTrue(true);
        cunio.setChecked(false);
        cantidad.setText("");
        subtotal.setText("");
        igv.setText("");
        total.setText("");
    }

    void CantiTrue(Boolean t){
        if(t){
            cantidad.setBackgroundResource(R.drawable.edittex);
            cantidad.setEnabled(true);
        }else{
            cantidad.setBackgroundResource(R.drawable.edittex_enable);
            cantidad.setEnabled(false);
        }
    }

    void PrecioTrue(Boolean t){
        if(t){
            precio.setBackgroundResource(R.drawable.edittex);
            precio.setEnabled(true);
        }else{
            precio.setBackgroundResource(R.drawable.edittex_enable);
            precio.setEnabled(false);
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.nombre:
                ListadoClientes();
                break;
            case R.id.cancelar:
                if(cantidad.getText().toString().isEmpty()){
                    finish();
                }else{
                    Cancelar();
                }
                break;
            case R.id.guardar:
                Log.i("POS", disponibles+" - "+comprometidos+" - "+CTD);
                if((CTD <= disponibles || produc.isChecked() || requerido.isChecked())  && CTD>0){
                    float auxoriginal = 0;
                    try{
                        auxoriginal = Float.parseFloat(String.format("%.2f", precioOriginal));
                        Log.i("AUX PRECIO ORIGINAL", auxoriginal+"");
                    }catch (Exception e){
                        e.printStackTrace();
                    }
                    if(auxoriginal>precio1){
                        Log.i("PRECIOS AMBOS", precioOriginal+" - "+precio1);
                        //Toast.makeText(DetalleProductos.this,"Debe colocar un precio mayor o igual a " + ((moneda==0)?"US$ ":"S/. ") +precioOriginal,Toast.LENGTH_SHORT).show();
                        //precio.setText(""+precioOriginal);
                        ActividadPrincipal.flagDescuento = 1;
                    }

                    if(precio1>0){
                        Guardar();
                    }else{
                        Toast.makeText(DetalleProductos.this,"Usted no puede agregar este producto porque no tiene precio unitario",Toast.LENGTH_SHORT).show();
                    }


                }else{
                    Toast.makeText(DetalleProductos.this,CTD<1?"Debe ingresar la cantidad de producto":"No hay suficiente stock disponible",Toast.LENGTH_SHORT).show();
                }

                break;
            case R.id.imagen:
                if(!codigo.getText().toString().isEmpty()){
                    if(ImagenProducto != null){
                        Ver();
                    }
                }
                break;
        }
    }

    private class Datos extends AsyncTask<Void,Void,Integer> {

        ProgressDialog pd;

        Datos(Context c){
            try{
                pd =  new ProgressDialog(c);
                pd.setIndeterminate(true);
                pd.setMessage("Cargando Datos");
                pd.setCancelable(false);
                pd.show();
            }catch (Exception e){
                System.out.println("Error1: "+e.toString());
            }

        }


        @Override
        protected void onPreExecute() {
            super.onPreExecute();

        }

        @Override
        protected Integer doInBackground(Void... voids) {
            producto = control.GetFullProductos();
            productoOriging = producto;
            /*Item = new String[producto.size()];
            for (int i = 0;i<Item.length;i++){
                Item[i] = producto.get(i).get(Tb_Productos.nombre).toString();
            }*/
            return 0;
        }


        @Override
        protected void onPostExecute(Integer integer) {
            super.onPostExecute(integer);
            pd.dismiss();
        }

    }



    int action = 3;
    void LLenadoInfor(ContentValues dato,int i){
        requestQueue= Volley.newRequestQueue(DetalleProductos.this);
        nombre.setText(dato.get(Tb_Productos.nombre).toString());
        visual.setVisibility(View.GONE);
        codigo.setText(dato.get(Tb_Productos.codigo).toString());
        tienecunio = 0;
        int comprometidas = comprobaconStock(dato.get(Tb_Productos.codigo).toString());
        stocks = Integer.parseInt(dato.get(Tb_Productos.stock).toString());
        comprometidos = Integer.parseInt(dato.get(Tb_Productos.comprometida).toString()) + comprometidas;
        stock.setText(String.valueOf(stocks));
        monedaProducto = Integer.parseInt(dato.get(Tb_Productos.moneda).toString());
        monedaProducto = (monedaProducto==2)?0:monedaProducto;
        compro.setText(String.valueOf(comprometidos));
        disponibles = stocks - comprometidos;
        disponibles = disponibles<0?0:disponibles;
        dispo.setText(String.valueOf(disponibles));
        tienecunio = dato.getAsInteger(Tb_Productos.cunio);
        rango_menor = dato.getAsInteger(Tb_Productos.rango_menor);
        Log.i("POS", disponibles+" - "+comprometidos+" - "+CTD);
        cunio.setVisibility(tienecunio==0?View.GONE:View.VISIBLE);
        if(monedaProducto== moneda){
            precio1 = Float.parseFloat(dato.get(Tb_Productos.precio).toString());
            precioOriginal=Float.parseFloat(dato.get(Tb_Productos.precio).toString());
        }else{
            if(moneda == 0 && monedaProducto == 1){
                if(i==0){ precio1 =  Float.parseFloat(dato.get(Tb_Productos.precio).toString())/converSolesDolar;}
                precioOriginal=precio1;
            }else if(moneda == 1 && monedaProducto == 0){
                if(i==0){ precio1 =  Float.parseFloat(dato.get(Tb_Productos.precio).toString())*converSolesDolar;}
                precioOriginal=precio1;
            }
        }
        ImageRequest request = new ImageRequest(
                URL_BASE + dato.get(Tb_Productos.foto).toString(),
                new Response.Listener<Bitmap>() {
                    @Override
                    public void onResponse(Bitmap bitmap) {
                        ImagenProducto = bitmap;
                        visual.setVisibility(View.VISIBLE);
                    }
                }, 0, 0, null,null,
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.d(TAG, "Error en respuesta Bitmap: "+ error.getMessage());
                    }
                });

        // Añadir petición a la cola
        requestQueue.add(request);
        String sprec = String.format("%.2f", precio1);
        precio.setText(""+sprec);
        if(i==0){
            if (cantidadEscogidos>=2){
                Toast.makeText(DetalleProductos.this, "No puedes escoger mas este producto", Toast.LENGTH_SHORT).show();
                Limpiar();
                nombre.setText("");
                visual.setVisibility(View.GONE);
                codigo.setText("");
                produc.setEnabled(true);
                stocks = 0;
                produc.setChecked(false);
                comprometidos =0;
                disponibles=0;
                stock.setText("");
                monedaProducto = 2;
                monedaProducto = (monedaProducto==2)?0:monedaProducto;
                compro.setText("");
                disponibles =0;
                dispo.setText(String.valueOf(""));
                precio.setText("");
            }
        }

    }

    @Override
    public void onBackPressed() {

    }

    ContentValues datos(){
        ContentValues datos = new ContentValues();
        datos.put(Tb_cotizacion_productos.cotizacion,codigocotizacion);
        datos.put(Tb_cotizacion_productos.producto,codigo.getText().toString().trim());
        datos.put(Tb_cotizacion_productos.producto_nombre,nombre.getText().toString());
        datos.put(Tb_cotizacion_productos.cantidad,cantidad.getText().toString());
        datos.put(Tb_cotizacion_productos.subtotal,String.valueOf(sbtotal));
        datos.put(Tb_cotizacion_productos.material,"0");
        datos.put(Tb_cotizacion_productos.color,esta);
        ActividadPrincipal.ctd = ActividadPrincipal.ctd + Integer.parseInt(cantidad.getText().toString());
        ActividadPrincipal.porcentajePerceptor =  ActividadPrincipal.tiponegocio==0? ActividadPrincipal.porcentajePerceptor:( ( ActividadPrincipal.ttl + ( ActividadPrincipal.ttl* ActividadPrincipal.porcentajePerceptor)) >  ActividadPrincipal.valorNatural)?2/100:0;
        ActividadPrincipal.ttl = ActividadPrincipal.ttl  +ttotal;
        datos.put(Tb_cotizacion_productos.igv,String.valueOf(figv));
        datos.put(Tb_cotizacion_productos.precio,String.valueOf(precio1));
        datos.put(Tb_cotizacion_productos.total,String.valueOf(ttotal));
        datos.put(Tb_cotizacion_productos.cunio, cunio.isChecked() ?"1":"0");
        datos.put(Tb_cotizacion_productos.requerido, requerido.isChecked() ?"1":"0");
        datos.put(Tb_cotizacion_productos.action,EstatusEdición==0?3:1+(action==1?0:estatus));
        datos.put(Tb_cotizacion_productos.produccion,produc.isChecked()?"1": requerido.isChecked()?"2":"0");
        Log.e("id_producto " + codigo.getText().toString().trim(), id);
        datos.put(Tb_cotizacion_productos.idproduc,id);
        return  datos;
    }

    Dialog customDialog,customDialog1,customDialog2,customDialog3;
    public void Regresar() {
        customDialog1 = new Dialog(DetalleProductos.this);
        customDialog1.requestWindowFeature(Window.FEATURE_NO_TITLE);
        customDialog1.setCancelable(false);
        customDialog1.setContentView(R.layout.notificacion_01);

        TextView titulo = (TextView) customDialog1.findViewById(R.id.mensaje);
        titulo.setText("¿Desea usted regresar?\n" +
                "Si regresa no se guardara los datos");
        (customDialog1.findViewById(R.id.si)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view)
            {
                finish();
            }
        });
        (customDialog1.findViewById(R.id.no)).setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view)
            {
                customDialog1.dismiss();

            }
        });

        assert customDialog1 != null;
        customDialog1.show();

    }

    public void Guardar() {
        customDialog2 = new Dialog(DetalleProductos.this);
        customDialog2.requestWindowFeature(Window.FEATURE_NO_TITLE);
        customDialog2.setCancelable(false);
        customDialog2.setContentView(R.layout.notificacion_01);

        TextView titulo = (TextView) customDialog2.findViewById(R.id.mensaje);
        titulo.setText("¿Desea guardar el producto?");
        TextView s = (TextView) customDialog2.findViewById(R.id.si);
        s.setText("Si");
        TextView n = (TextView) customDialog2.findViewById(R.id.no);
        n.setText("No");
        (customDialog2.findViewById(R.id.si)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view)
            {
                if(estatus == 0){
                    ActividadPrincipal.productos.add(datos());
                }else{
                    ActividadPrincipal.ctd = ActividadPrincipal.ctd - cantitempo;
                    ActividadPrincipal.porcentajePerceptor =  ActividadPrincipal.tiponegocio==0? ActividadPrincipal.porcentajePerceptor:( ActividadPrincipal.ttl >  ActividadPrincipal.valorNatural)?2/100:0;
                    ActividadPrincipal.ttl = ( ActividadPrincipal.ttl ) -totaltempo;
                    ActividadPrincipal.productos.remove(poss);
                    ActividadPrincipal.productos.add(datos());
                }
                finish();
            }
        });
        (customDialog2.findViewById(R.id.no)).setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view)
            {
                customDialog2.dismiss();

            }
        });

        assert customDialog2 != null;
        customDialog2.show();

    }

    public void Ver() {
        customDialog2 = new Dialog(DetalleProductos.this);
        customDialog2.requestWindowFeature(Window.FEATURE_NO_TITLE);
        customDialog2.setCancelable(false);
        customDialog2.setContentView(R.layout.notificacion_02);
        ImageView ver = (ImageView) customDialog2.findViewById(R.id.img);
        ver.setImageBitmap(ImagenProducto);
        (customDialog2.findViewById(R.id.no)).setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view)
            {
                customDialog2.dismiss();

            }
        });

        assert customDialog2 != null;
        customDialog2.show();

    }

    public void Cancelar() {
        customDialog3 = new Dialog(DetalleProductos.this);
        customDialog3.requestWindowFeature(Window.FEATURE_NO_TITLE);
        customDialog3.setCancelable(false);
        customDialog3.setContentView(R.layout.notificacion_01);

        TextView titulo = (TextView) customDialog3.findViewById(R.id.mensaje);
        titulo.setText("¿Desea cancelar?");
        (customDialog3.findViewById(R.id.si)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view)
            {
                finish();
            }
        });
        (customDialog3.findViewById(R.id.no)).setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view)
            {
                customDialog3.dismiss();

            }
        });

        assert customDialog3 != null;
        customDialog3.show();

    }

    int comprobaconStock(String _c){
        int ii = 0;
        for(int i=0;i<ActividadPrincipal.productos.size();i++){
            ContentValues v = ActividadPrincipal.productos.get(i);
            if(v.get(Tb_cotizacion_productos.producto).toString().compareTo(_c)==0){
                ii = Integer.parseInt(v.get(Tb_cotizacion_productos.cantidad).toString());
                para = Integer.parseInt(v.get(Tb_cotizacion_productos.produccion).toString());
                esta = 1;
                cantidadEscogidos += 1;
                //Toast.makeText(DetalleProductos.this,"Catidad de veces: " + cantidadEscogidos,Toast.LENGTH_SHORT).show();
                if(estatus == 0){
                    if(tipo==1){
                        requerido.setEnabled(false);
                        requerido.setChecked(false);
                        if (para == 0) {
                            produc.setChecked(true);
                        } else {
                            produc.setChecked(false);
                        }
                        produc.setEnabled(false);
                    }else{
                        if (para == 2) {
                            requerido.setChecked(false);
                        } else {
                            requerido.setChecked(true);
                        }
                    }

                }
            }
        }


        return ii;
    }

    public void ListadoClientes(){
        customDialog = new Dialog(this);
        customDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        customDialog.setCancelable(false);
        customDialog.setContentView(R.layout.notificacion_03_cargalista);
        producto = productoOriging;
        TextView titulo = (TextView) customDialog.findViewById(R.id.titulo);
        titulo.setText("Listado de productos");
        EditText busqueda = (EditText) customDialog.findViewById(R.id.buscar);
        final RecyclerView lista = (RecyclerView) customDialog.findViewById(R.id.clientes);
        lista.setHasFixedSize(true);
        adapter = new ListClienteAdapter(DetalleProductos.this, producto);
        lista.setAdapter(adapter);
        busqueda.setHint("Buscar producto");
        busqueda.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence s, int i, int i1, int i2) {
                producto = control.GetFullProductoBusqueda(s.toString());
                adapter = new ListClienteAdapter(DetalleProductos.this, producto);
                lista.setAdapter(adapter);
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        adapter.setOnItemClickListener(new ListClienteAdapter.ClickListener() {
            @Override
            public void onItemClick(int position, View v) {
                para = 0;
                esta = 0;
                cantidadEscogidos =0;
                LLenadoInfor(control.GetProducto(adapter.lista.get(position).get(Tb_Productos.codigo).toString()),0);
                Limpiar();
                produc.setEnabled(true);
                customDialog.dismiss();
            }
        });
        lista.setLayoutManager(new LinearLayoutManager(this,LinearLayoutManager.VERTICAL,false));
        (customDialog.findViewById(R.id.no)).setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view)
            {
                customDialog.dismiss();
            }
        });

        assert customDialog != null;
        customDialog.show();
    }
}
