package com.felixcabrita.cotizacionesamconsultores;

import android.content.Context;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.preference.PreferenceManager;

/**
 * Creado por Felix Cabrita en 4/7/2017.
 */

public class CheckServicios extends AsyncTask<Void,Void,Void> {

    private Context c;
    private SharedPreferences pref ;
    private boolean internet = false;
    private SharedPreferences.Editor editor;
    CheckServicios(Context _c){
        pref = PreferenceManager.getDefaultSharedPreferences(_c);
        editor = pref.edit();
        this.c = _c;
    }

    @Override
    protected Void doInBackground(Void... voids) {

        while (!internet ){
            ConnectivityManager connMgr = (ConnectivityManager)
                    c.getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();
            if (networkInfo != null && networkInfo.isConnected()) {
                System.out.println("Termino de sincronizar (Internet:"+internet+")");
                if(pref.getInt("Services",0)==1){

                    editor.putInt("syncC",1);
                    editor.apply();
                    editor.putInt("syncP",1);
                    editor.apply();
                    editor.putInt("syncCo",1);
                    editor.apply();
                }else{
                    editor.putInt("syncC",0);
                    editor.apply();
                    editor.putInt("syncP",0);
                    editor.apply();
                    editor.putInt("syncCo",0);
                    editor.apply();
                }
                internet = true;
            }

        }

        return null;
    }

    @Override
    protected void onPostExecute(Void aVoid) {
        super.onPostExecute(aVoid);
        if(internet && pref.getInt("Services",0)==1){
            new SyncClientes(c).execute();
            new SyncProductos(c).execute();
            System.out.println("Empieza a sincronizar");
        }
    }
}
