package com.felixcabrita.cotizacionesamconsultores;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;
import android.util.SparseArray;

import com.felixcabrita.cotizacionesamconsultores.models.Tb_Clientes;
import com.felixcabrita.cotizacionesamconsultores.models.Tb_Productos;
import com.felixcabrita.cotizacionesamconsultores.models.Tb_cotizacion_productos;
import com.felixcabrita.cotizacionesamconsultores.models.Tb_Cotizaciones;
import com.felixcabrita.cotizacionesamconsultores.models.Tb_EstadoCotizacion;
import com.felixcabrita.cotizacionesamconsultores.models.Tb_Usuarios;
import com.felixcabrita.cotizacionesamconsultores.models.Tb_Visita;
import com.felixcabrita.cotizacionesamconsultores.models.Tb_contactos;
import com.felixcabrita.cotizacionesamconsultores.models.Tb_pedidos;
import com.felixcabrita.cotizacionesamconsultores.models.Tb_situacion;
import com.felixcabrita.cotizacionesamconsultores.net.ModelsPedidos;
import com.felixcabrita.cotizacionesamconsultores.net.model.responses.AllContacts;
import com.felixcabrita.cotizacionesamconsultores.net.model.responses.ModelsSituacion;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.Locale;
import java.util.StringTokenizer;

/**
 * Creado por Felix Cabrita en 27/6/2017.
 */

public class BaseDatosSqlite extends SQLiteOpenHelper {
    private static String name = "cotizAM";
    //SimpleDateFormat FechaFormat = new SimpleDateFormat("dd/MM/yyyy");
    private SQLiteDatabase database;
    private SQLiteDatabase databaseReadOnly;
    private static SQLiteDatabase.CursorFactory factory = null;
    private static int version = 1;

    public BaseDatosSqlite(Context context) {
        super(context, name, factory, version);
        database = getWritableDatabase();
        databaseReadOnly = getReadableDatabase();
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        String query;
        Log.e("Tabla","" + Tb_Clientes.nombreTabla);
        //TABLA CLIENTES
        query = "CREATE TABLE [" + Tb_Clientes.nombreTabla + "] (\n" +
                "[id] INTEGER DEFAULT 0,\n" +
                "\t[" + Tb_Clientes.razon_social + "] VARCHAR(20) NOT NULL,\n" +
                "\t[" + Tb_Clientes.nombre_comercial + "] VARCHAR(50) NULL DEFAULT ' ',\n" +
                "\t[" + Tb_Clientes.nombres + "] VARCHAR(50) NULL DEFAULT ' ',\n" +
                "\t[" + Tb_Clientes.apellidos + "] VARCHAR(50) NULL DEFAULT ' ',\n" +
                "\t[" + Tb_Clientes.ruc + "] VARCHAR(15) NULL DEFAULT ' ',\n" +
                "\t[" + Tb_Clientes.current_ruc + "] VARCHAR(15) NULL DEFAULT ' ',\n" +
                "\t[" + Tb_Clientes.contacto + "] VARCHAR(50) NULL DEFAULT ' ',\n" +
                "\t[" + Tb_Clientes.code + "] INTEGER DEFAULT 0,\n" +
                "\t[" + Tb_Clientes.direccion + "] VARCHAR(100) NOT NULL,\n" +
                "\t[" + Tb_Clientes.telefono_movil + "] VARCHAR(15) NOT NULL,\n" +
                "\t[" + Tb_Clientes.telefono_fijo + "] VARCHAR(15) NULL DEFAULT ' ',\n" +
                "\t[" + Tb_Clientes.correo + "] VARCHAR(30) NULL DEFAULT ' ',\n" +
                "\t[" + Tb_Clientes.cond_pago + "] INTEGER DEFAULT 0,\n" +
                "\t[" + Tb_Clientes.cond_venta + "] INTEGER DEFAULT 0,\n" +
                "\t[" + Tb_Clientes.zona + "] INTEGER DEFAULT 0,\n" +
                "\t[" + Tb_Clientes.perceptor + "]INTEGER DEFAULT 0,\n" +
                "\t[" + Tb_Clientes.por_perceptor + "] float DEFAULT 0,\n" +
                "\t[" + Tb_Clientes.tipoExtaNa + "] INTEGER DEFAULT 1,\n" +
                "\t[" + Tb_Clientes.tiponegocio + "] INTEGER DEFAULT 0,\n" +
                "\t[" + Tb_Clientes.retenedor + "] INTEGER DEFAULT 0,\n" +
                "\t[" + Tb_Clientes.rubro + "] INTEGER DEFAULT 1,\n" +
                "\t[" + Tb_Clientes.sector + "] INTEGER DEFAULT 1,\n" +
                "\t[" + Tb_Clientes.calificacion + "] INTEGER NULL,\n" +
                "\t[" + Tb_Clientes.estado + "] INTEGER DEFAULT 0,\n" +
                "\t[" + Tb_Clientes.visita_cotizacion + "] INTEGER NULL)";
        sqLiteDatabase.execSQL(query);

        //TABLA CLIENTES CONTACTO
        query = "CREATE TABLE [" + Tb_contactos.nombreTabla + "] (\n" +
                "[id] INTEGER DEFAULT 0,\n" +
                "\t[" + Tb_contactos.cliente + "] INTEGER NOT NULL references " + Tb_Clientes.nombreTabla + "( " + Tb_Clientes.ruc + " ) on delete cascade,\n" +
                "\t[" + Tb_contactos.tipo + "] INTEGER NULL,\n" +
                "\t[" + Tb_contactos.descripcion + "] VARCHAR(100) NULL DEFAULT ' ',\n" +
                "\t[" + Tb_contactos.contacto + "] VARCHAR(100) NULL DEFAULT ' ',\n" +
                "\t[" + Tb_contactos.direccion + "] VARCHAR(200) NULL,\n" +
                "\t[" + Tb_contactos.telefonoMovil + "] VARCHAR(15) NOT NULL,\n" +
                "\t[" + Tb_contactos.telefonoFijo + "] VARCHAR(15) NULL DEFAULT ' ',\n" +
                "\t[" + Tb_contactos.correo + "] VARCHAR(100) NULL DEFAULT ' ',\n" +
                "\t[" + Tb_contactos.zona + "] INTEGER DEFAULT 0 NULL,\n" +
                "\t[" + Tb_contactos.vendedor + "]INTEGER DEFAULT 0 NULL,\n" +
                "\t[" + Tb_contactos.condVenta + "] INTEGER DEFAULT 0 NULL,\n" +
                "\t[" + Tb_contactos.condPago + "] INTEGER DEFAULT 0 NULL,\n" +
                "\t[" + Tb_contactos.estado + "] INTEGER NULL)";
        sqLiteDatabase.execSQL(query);
        //TABLA COTIZACIONES
        query = "CREATE TABLE [" + Tb_Cotizaciones.nombreTabla + "] (\n" +
                "[id] INTEGER DEFAULT 0,\n" +
                "\t[" + Tb_Cotizaciones.codigo + "] VARCHAR(30) NULL DEFAULT NULL ,\n" +
                "\t[" + Tb_Cotizaciones.fecha + "] VARCHAR(30) NOT NULL,\n" +
                "\t[" + Tb_Cotizaciones.fecha_registro + "] datetime NULL DEFAULT NULL,\n" +
                "\t[" + Tb_Cotizaciones.nombre + "] VARCHAR(200) NULL DEFAULT NULL,\n" +
                "\t[" + Tb_Cotizaciones.cliente + "] VARCHAR(15) NULL DEFAULT ' ',\n" +
                "\t[" + Tb_Cotizaciones.vendedor + "] INT(11) NOT NULL,\n" +
                "\t[" + Tb_Cotizaciones.cond_pago + "] INT(11) NOT NULL,\n" +
                "\t[" + Tb_Cotizaciones.cond_venta + "] INT(11) NOT NULL,\n" +
                "\t[" + Tb_Cotizaciones.flete + "] TINYINT(4) NULL DEFAULT NULL,\n" +
                "\t[" + Tb_Cotizaciones.tiempo_validez + "] TINYINT(4) NULL DEFAULT NULL,\n" +
                "\t[" + Tb_Cotizaciones.lugar_cotizacion + "] TINYINT(4) NULL DEFAULT NULL,\n" +
                "\t[" + Tb_Cotizaciones.longitud + "] VARCHAR(50) NULL DEFAULT NULL,\n" +
                "\t[" + Tb_Cotizaciones.latirud + "] VARCHAR(50) NULL DEFAULT NULL,\n" +
                "\t[" + Tb_Cotizaciones.moneda + "] TINYINT(4) NOT NULL,\n" +
                "\t[" + Tb_Cotizaciones.icotem + "] TINYINT(4) DEFAULT 1,\n" +
                "\t[" + Tb_Cotizaciones.porcentaje + "] REAL(12,2) NULL DEFAULT NULL,\n" +
                "\t[" + Tb_Cotizaciones.subtotal + "] REAL(12,2) NULL DEFAULT NULL,\n" +
                "\t[" + Tb_Cotizaciones.monto_igv + "] REAL(12,2) NULL DEFAULT NULL,\n" +
                "\t[" + Tb_Cotizaciones.monto_total + "] REAL(12,2) NULL DEFAULT NULL,\n" +
                "\t[" + Tb_Cotizaciones.lugar + "] TEXT NULL,\n" +
                "\t[" + Tb_Cotizaciones.observaciones + "] TEXT NULL,\n" +
                "\t[" + Tb_Cotizaciones.por_revision + "] INTEGER NULL,\n" +
                "\t[" + Tb_Cotizaciones.items + "] INTEGER NULL,\n" +
                "\t[" + Tb_Cotizaciones.estadoSynco + "] INTEGER DEFAULT 0,\n" +
                "\t[" + Tb_Cotizaciones.lugar_direccion + "] INTEGER DEFAULT 0,\n" +
                "\t[cot_www] INTEGER DEFAULT 0,\n" +
                "\t[pdf] INTEGER DEFAULT 0,\n" +
                "\t[" + Tb_Cotizaciones.estado + "] INT(11) NULL DEFAULT NULL)";
        sqLiteDatabase.execSQL(query);

        query = "CREATE TABLE ["+ Tb_EstadoCotizacion.nombreTabla+"] (\n" +
                "\t[" + Tb_EstadoCotizacion.codigo + "] VARCHAR(30) NULL DEFAULT NULL ,\n" +
                "\t[" + Tb_Cotizaciones.estadoSynco + "] INTEGER DEFAULT 0,\n" +
                "\t[" + Tb_EstadoCotizacion.estado + "] INT(11) NULL DEFAULT NULL)";
        sqLiteDatabase.execSQL(query);

        //TABLA COTIZACIONES PRODUCTOS
        query = "CREATE TABLE [" + Tb_cotizacion_productos.nombreTabla + "] (\n" +
                "\t[" + Tb_cotizacion_productos.cotizacion + "] INT(11) NOT NULL,\n" +
                "\t[" + Tb_cotizacion_productos.idproduc + "] INT(11) DEFAULT 0,\n" +
                "\t[" + Tb_cotizacion_productos.producto + "] VARCHAR(50) NOT NULL,\n" +
                "\t[" + Tb_cotizacion_productos.producto_nombre + "] VARCHAR(255) NOT NULL,\n" +
                "\t[" + Tb_cotizacion_productos.material + "] INT(11) DEFAULT 1,\n" +
                "\t[" + Tb_cotizacion_productos.color + "] INT(11) DEFAULT 1,\n" +
                "\t[" + Tb_cotizacion_productos.cantidad + "] INT(11) NOT NULL,\n" +
                "\t[" + Tb_cotizacion_productos.precio + "] REAL(12,2) NOT NULL,\n" +
                "\t[" + Tb_cotizacion_productos.subtotal + "] REAL(12,2) NULL DEFAULT NULL,\n" +
                "\t[" + Tb_cotizacion_productos.igv + "] REAL(12,2) NULL DEFAULT NULL,\n" +
                "\t[" + Tb_cotizacion_productos.total + "] REAL(12,2) NULL DEFAULT NULL,\n" +
                "\t[" + Tb_cotizacion_productos.cunio + "] INTEGER NULL,\n" +
                "\t[" + Tb_cotizacion_productos.requerido + "] INTEGER NULL,\n" +
                "\t[" + Tb_cotizacion_productos.action + "] INTEGER NULL,\n" +
                "\t[" + Tb_cotizacion_productos.produccion + "] INTEGER  NULL )";

        sqLiteDatabase.execSQL(query);
        //TABLA PRODUCTOS
        query = "CREATE TABLE [" + Tb_Productos.nombreTabla + "] (\n" +
                "\t[" + Tb_Productos.codigo + "] VARCHAR(50) NOT NULL,\n" +
                "\t[" + Tb_Productos.nombre + "] VARCHAR(50) NOT NULL,\n" +
                "\t[" + Tb_Productos.moneda + "] TINYINT(1) NULL DEFAULT NULL,\n" +
                "\t[" + Tb_Productos.tamanio + "] VARCHAR(60) NULL DEFAULT NULL,\n" +
                "\t[" + Tb_Productos.peso + "] VARCHAR(20) NULL DEFAULT NULL,\n" +
                "\t[" + Tb_Productos.volumen + "] VARCHAR(20) NULL DEFAULT NULL,\n" +
                "\t[" + Tb_Productos.descripcion + "] VARCHAR(255) NULL DEFAULT NULL,\n" +
                "\t[" + Tb_Productos.stock + "] INT(11) NULL DEFAULT NULL,\n" +
                "\t[" + Tb_Productos.comprometida + "] INT(11) NULL DEFAULT NULL,\n" +
                "\t[" + Tb_Productos.disponible + "] INT(11) NULL DEFAULT NULL,\n" +
                "\t[" + Tb_Productos.material + "] TEXT NULL,\n" +
                "\t[" + Tb_Productos.colores + "] INT(11) NULL DEFAULT NULL,\n" +
                "\t[" + Tb_Productos.precio + "] REAL(12,2) NULL DEFAULT NULL,\n" +
                "\t[" + Tb_Productos.igv + "] REAL(12,2) NULL DEFAULT NULL,\n" +
                "\t[" + Tb_Productos.total + "] REAL(12,2) NULL DEFAULT NULL,\n" +
                "\t[" + Tb_Productos.cunio + "] REAL(12,2) NULL DEFAULT NULL,\n" +
                "\t[" + Tb_Productos.rango_menor + "] REAL(12,2) NULL DEFAULT NULL,\n" +
                "\t[" + Tb_Productos.ficha_tecnica + "] VARCHAR(80) NULL DEFAULT NULL,\n" +
                "\t[" + Tb_Productos.foto + "] VARCHAR(50) NULL DEFAULT NULL)";

        sqLiteDatabase.execSQL(query);
        //TABLA USUARIOS
        query = "CREATE TABLE [" + Tb_Usuarios.tb_nombreTabla + "] (\n" +
                "[" + Tb_Usuarios.tb_codigo + "] VARCHAR(11)  NULL,\n" +
                "[" + Tb_Usuarios.tb_nombre + "] VARCHAR(50)  NULL,\n" +
                "[" + Tb_Usuarios.tb_apellidos + "] VARCHAR(50)  NULL,\n" +
                "[" + Tb_Usuarios.tb_tipoUsuario + "] VARCHAR(11)  NULL,\n" +
                "[" + Tb_Usuarios.tb_correo + "] VARCHAR(11)  NULL,\n" +
                "[" + Tb_Usuarios.tb_telefijo + "] VARCHAR(15)  NULL,\n" +
                "[" + Tb_Usuarios.tb_pw + "] VARCHAR(100)  NULL,\n" +
                "[" + Tb_Usuarios.tb_estado + "] VARCHAR(100)  NULL,\n" +
                "[" + Tb_Usuarios.tb_telemovil + "] VARCHAR(15)  NULL\n" +
                ")";


        sqLiteDatabase.execSQL(query);
        //TABLA VISITAS
        query = "CREATE TABLE [" + Tb_Visita.nombreTabla + "] (\n" +
                "[id] INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,\n" +
                "[" + Tb_Visita.cliente + "] VARCHAR(11)  NULL,\n" +
                "[" + Tb_Visita.codigo + "] VARCHAR(25)  NULL,\n" +
                "[" + Tb_Visita.fecha_inicio + "] VARCHAR(50)  NULL DEFAULT ' ',\n" +
                "[" + Tb_Visita.fecha_proxima + "] VARCHAR(50)   NULL DEFAULT ' ',\n" +
                "[" + Tb_Visita.fecha_paraEnviar + "] VARCHAR(50)   NULL DEFAULT ' ',\n" +
                "[" + Tb_Visita.fecha_final + "] VARCHAR(50)   NULL DEFAULT ' ',\n" +
                "[" + Tb_Visita.latitud_inicio + "] VARCHAR(50)   NULL DEFAULT '0',\n" +
                "[" + Tb_Visita.longitud_inicio + "] VARCHAR(50)   NULL DEFAULT '0' ,\n" +
                "[" + Tb_Visita.latitud_final + "] VARCHAR(50)   NULL DEFAULT '0',\n" +
                "[" + Tb_Visita.longitud_final + "] VARCHAR(50)   NULL DEFAULT '0',\n" +
                "[" + Tb_Visita.motivo + "] TEXT   NULL DEFAULT ' ',\n" +
                "[" + Tb_Visita.pendiente + "] VARCHAR(15)  NULL,\n" +
                "[" + Tb_Visita.direccion + "] VARCHAR(15)  NULL,\n" +
                "[" + Tb_Visita.estado + "] VARCHAR(15)  NULL\n" +
                ")";
        sqLiteDatabase.execSQL(query);

        //TABLA PEDIDOS
        query = "CREATE TABLE [" + Tb_pedidos.nombreTabla + "] (\n" +
                "[id] INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,\n" +
                "[" + Tb_pedidos.pedido + "] INTEGER NOT NULL,\n" +
                "[" + Tb_pedidos.ruc + "] VARCHAR(15) NOT NULL references " + Tb_Clientes.nombreTabla + "( " + Tb_Clientes.ruc + " ) on delete cascade,\n" +
                "[" + Tb_pedidos.fecha + "] VARCHAR(50)  NULL DEFAULT ' ',\n" +
                "[" + Tb_pedidos.direccion + "]VARCHAR(200) NULL DEFAULT ' ',\n" +
                "[" + Tb_pedidos.cliente + "] VARCHAR(50)   NULL DEFAULT ' ',\n" +
                "[" + Tb_pedidos.tipoMoneda + "] VARCHAR(20)   NULL DEFAULT ' ',\n" +
                "[" + Tb_pedidos.tipoPedido + "] VARCHAR(50)   NULL DEFAULT '0',\n" +
                "[" + Tb_pedidos.tipoPago + "] VARCHAR(50)   NULL DEFAULT '0',\n" +
                "[" + Tb_pedidos.montoNeto + "] VARCHAR(15)  NULL,\n" +
                "[" + Tb_pedidos.montoIgv + "] VARCHAR(15)  NULL,\n" +
                "[" + Tb_pedidos.percepcion + "] REAL  NULL,\n" +
                "[" + Tb_pedidos.montoTotal + "] VARCHAR(15)  NULL\n" +
                ")";
        sqLiteDatabase.execSQL(query);

        //TABLA SITUACION
        query = "CREATE TABLE [" + Tb_situacion.nombreTabla + "] (\n" +
                "[id] INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,\n" +
                "[" + Tb_situacion.fechaDoc + "] VARCHAR(20) NOT NULL,\n" +
                "[" + Tb_situacion.docmto + "] VARCHAR(50) NULL DEFAULT ' ',\n" +
                "[" + Tb_situacion.vcSerie + "] VARCHAR(50)  NULL DEFAULT ' ',\n" +
                "[" + Tb_situacion.vcNroDoc + "]VARCHAR(50) NULL DEFAULT ' ',\n" +
                "[" + Tb_situacion.codVta + "] VARCHAR(50)   NULL DEFAULT ' ',\n" +
                "[" + Tb_situacion.fechaVenc + "] VARCHAR(20)   NULL DEFAULT ' ',\n" +
                "[" + Tb_situacion.ubicacion + "] VARCHAR(50)   NULL DEFAULT ' ',\n" +
                "[" + Tb_situacion.situacion + "] VARCHAR(50)   NULL DEFAULT ' ',\n" +
                "[" + Tb_situacion.mnd + "] VARCHAR(15)  NULL,\n" +
                "[" + Tb_situacion.totalDeuda + "]VARCHAR(50)  NULL,\n" +
                "[" + Tb_situacion.amortizado + "] VARCHAR(50)  NULL,\n" +
                "[" + Tb_situacion.saldo + "] VARCHAR(50)  NULL,\n" +
                "[" + Tb_situacion.vendedor + "] VARCHAR(50)  NULL,\n" +
                "[" + Tb_situacion.clave + "] VARCHAR(50)  NULL,\n" +
                "[" + Tb_situacion.ccbCordCom + "] VARCHAR(50)  NULL,\n" +
                "[" + Tb_situacion.TD + "] VARCHAR(50)  NULL,\n" +
                "[" + Tb_situacion.Documento + "] VARCHAR(50)  NULL,\n" +
                "[" + Tb_situacion.vcDescripcion + "] VARCHAR(50)  NULL,\n" +
                "[" + Tb_situacion.vcRuc + "] VARCHAR(15) NOT NULL references " + Tb_Clientes.nombreTabla + "( " + Tb_Clientes.ruc + " ) on delete cascade\n" +
                ")";
        sqLiteDatabase.execSQL(query);

        //TABLA CONDICION VENTA
        query = "create table [condicion_venta] (\n" +
                "[id] varchar (11),\n" +
                "[nombre] varchar (180)\n" +
                ")";
        sqLiteDatabase.execSQL(query);

        SparseArray<String> condicionesVenta = new SparseArray<>();
        condicionesVenta.put(2,"FACTURA A 15 DIAS");
        condicionesVenta.put(3,"DONACION");
        condicionesVenta.put(4,"FACTURA A 30 DIAS");
        condicionesVenta.put(5,"FACTURA A 90 DIAS");
        condicionesVenta.put(6,"FACTURA A 60 DIAS");
        condicionesVenta.put(7,"FACT A 90 DIAS DE EMBARQUE");
        condicionesVenta.put(8,"CARTA DE CREDITO 120 DIAS FECHA DE EMBARQUE");
        condicionesVenta.put(9,"LEASING/CONTADO");
        condicionesVenta.put(10,"CARTA DE CREDITO 150 DIAS IRREVOCABLE FECHA DE EMBARQUE");
        condicionesVenta.put(11,"FACT 60 DIAS DE EMBARQUE");
        condicionesVenta.put(12,"FACTURA A 45 DIAS");
        condicionesVenta.put(13,"FACTURA 30, 60, 90, 120");
        condicionesVenta.put(14,"CARTA DE CREDITO A 30 DIAS DE FECHA DE EMBARQUE");
        condicionesVenta.put(15,"FACTURA 60, 90, 120 DIAS");
        condicionesVenta.put(16,"LETRA A 15, 30 DIAS");
        condicionesVenta.put(17,"LETRA A 30, 60 DIAS");
        //condicionesVenta.put(18,"CONTADO");
        condicionesVenta.put(19,"LETRA A 57 Y 63 DIAS");
        condicionesVenta.put(20,"LETRA A 60 Y 75 DIAS");
        condicionesVenta.put(21,"LETRA A 45 Y 60 DIAS");
        condicionesVenta.put(22,"LETRA A 60 DIAS");
        condicionesVenta.put(23,"LETRA A 90 DIAS");
        condicionesVenta.put(24,"LETRA A 60 Y 90 DIAS");
        condicionesVenta.put(25,"LETRA A 57-60-63 DIAS");
        condicionesVenta.put(26,"LETRA A 45 DIAS");
        condicionesVenta.put(27,"LETRA A 80,90 Y 100 DIAS");
        condicionesVenta.put(28,"LETRA A 90,100 Y 110 DIAS");
        condicionesVenta.put(29,"LETRA A 60, 70, 80, Y 90 DIAS");
        condicionesVenta.put(30,"LETRA A 30 DIAS");
        condicionesVenta.put(31,"LETRA A 60, 90 Y 120 DIAS");
        condicionesVenta.put(32,"LETRA A 30, 60, Y 90 DIAS");
        condicionesVenta.put(33,"LETRA A 60,90,120,150 Y 180");
        condicionesVenta.put(34,"LETRA A 45 Y 75 DIAS");
        condicionesVenta.put(35,"LETRA A 30 Y 60 DIAS");
        condicionesVenta.put(36,"LETRA A 59 Y 61 DIAS");
        condicionesVenta.put(37,"LETRA A 70 DIAS");
        condicionesVenta.put(38,"LETRA A 75 DIAS");
        condicionesVenta.put(39,"LETRA A 80, 87, 93  DIAS");
        condicionesVenta.put(40,"LETRA A 120 DIAS");
        condicionesVenta.put(41,"LETRA A 150 DIAS");
        condicionesVenta.put(42,"LETRA A 25,55, Y 85 DIAS");
        condicionesVenta.put(43,"LETRA A 45 Y 90 DIAS");
        condicionesVenta.put(44,"LETRA A 37 Y 82 DIAS");
        condicionesVenta.put(45,"LETRA A 75 Y 90 DIAS");
        condicionesVenta.put(46,"LETRA A 86, 88 Y 90 DIAS");
        condicionesVenta.put(47,"LETRA A 55 DIAS");
        condicionesVenta.put(48,"LETRA A 50, 65 Y 75 DIAS");
        condicionesVenta.put(49,"LETRA A 47 DIAS");
        condicionesVenta.put(50,"LETRA  A 56 DIAS");
        condicionesVenta.put(51,"LETRA A 30, 45 Y 60 DIAS");
        condicionesVenta.put(52,"LETRA A 30, 45, 60, 75 Y 90 DIAS");
        condicionesVenta.put(53,"LETRA A 60, 75 Y 90 DIAS");
        condicionesVenta.put(54,"LETRA A 30 Y 90 DIAS");
        condicionesVenta.put(55,"LETRA A 30, 45 Y 75 DIAS");
        condicionesVenta.put(56,"LETRA A 30 Y 45 DIAS");
        condicionesVenta.put(57,"LETRA A 60, 75, 90, 105 Y 120 DIAS");
        condicionesVenta.put(58,"LETRA A 35 DIAS");
        condicionesVenta.put(59,"FACTURA A 60, 90 DIAS");
        condicionesVenta.put(60,"LETRA A 45, 50 DIAS");
        condicionesVenta.put(61,"FACTURA A 75 DIAS");
        condicionesVenta.put(62,"LETRA A 90, 120 DIAS");
        condicionesVenta.put(63,"FACTURA A 20 DIAS");
        condicionesVenta.put(64,"LETRA A 45, 60, 90 DIAS");
        condicionesVenta.put(65,"LETRA A 78, 108 DIAS");
        condicionesVenta.put(66,"LETRA A 45, 60, 75 DIAS");
        condicionesVenta.put(67,"CARTA DE CREDITO 90 DIAS IRREVOCABLE FEC/EMBARQUE");
        condicionesVenta.put(68,"LETRA A 80, 100 DIAS");
        condicionesVenta.put(69,"LETRA A 100, 120 DIAS");
        condicionesVenta.put(70,"LETRA A 53, 60, 63, 66 DIAS");
        condicionesVenta.put(71,"FACTURA A 30, 60 DIAS");
        condicionesVenta.put(72,"FACT 30 DIAS F. DE EMBARQUE");
        condicionesVenta.put(73,"FACTURA A 120 DIAS");
        condicionesVenta.put(74,"50% ANTICIPO / 50% 60 DIAS");
        condicionesVenta.put(75,"LETRA A 100, 110, 120 DIAS");
        condicionesVenta.put(76,"FACTURA A 64 DIAS");
        condicionesVenta.put(77,"FACTURA A 74 DIAS");
        condicionesVenta.put(78,"FACTURA A 101 DIAS");
        condicionesVenta.put(79,"CARTA FIANZA BANCARIA IRREVOCABLE  A 120 DIAS");
        condicionesVenta.put(80,"FACTURA A 180 DIAS");
        condicionesVenta.put(81,"FACT. 120 DIAS F. DE EMBARQUE");
        condicionesVenta.put(82,"LETRA A 90, 100, 110, 120 DIAS");
        condicionesVenta.put(83,"LETRA A 45, 70, 90 DIAS");
        condicionesVenta.put(84,"LETRA A 54, 56, 58, 60 DIAS");
        condicionesVenta.put(85,"CARTA FIANZA BANCARIA IRREVOCABLE AL 21/02/2015");
        condicionesVenta.put(86,"FACTURA A 7 DIAS");
        condicionesVenta.put(87,"LETRA A 15 DIAS");
        condicionesVenta.put(88,"50% CONTADO 50% CONTRA EMBARQUE");
        condicionesVenta.put(89,"LETRA A 54, 61, 68, 75 DIAS");
        condicionesVenta.put(90,"FACTURA A 5 DIAS");
        condicionesVenta.put(91,"LETRA A 15, 30, 45 DIAS");

        for (int i = 0; i < condicionesVenta.size(); i++){
            int id = condicionesVenta.keyAt(i);
            String nombre = condicionesVenta.valueAt(i);
            query = "insert into [condicion_venta] ([id], [nombre]) values('" + id + "','" + nombre +"');";
            sqLiteDatabase.execSQL(query);
        }

        query = "create table [sectores] (\n" +
                "    [id] integer unique not null,\n" +
                "    [cod] varchar (11) not null,\n" +
                "    [nombre] varchar (60) not null\n" +
                "); ";
        sqLiteDatabase.execSQL(query);

        query = "create table [rubros] (\n" +
                "    [id] integer unique not null,\n" +
                "    [nombre] varchar (60) not null\n" +
                "); ";
        sqLiteDatabase.execSQL(query);
        /*query = "insert into [rubros] ([id], [nombre]) values('1','AVICOLA')";sqLiteDatabase.execSQL(query);
        query = "insert into [rubros] ([id], [nombre]) values('2','AUTOSERVICIOS')";sqLiteDatabase.execSQL(query);
        query = "insert into [rubros] ([id], [nombre]) values('3','COMERCIO')";sqLiteDatabase.execSQL(query);
        query = "insert into [rubros] ([id], [nombre]) values('4','INDUSTRIA')";sqLiteDatabase.execSQL(query);
        query = "insert into [rubros] ([id], [nombre]) values('5','GOLOSINAS')";sqLiteDatabase.execSQL(query);
        query = "insert into [rubros] ([id], [nombre]) values('6','PESCA')";sqLiteDatabase.execSQL(query);
        query = "insert into [rubros] ([id], [nombre]) values('7','FARMACIA')";sqLiteDatabase.execSQL(query);
        query = "insert into [rubros] ([id], [nombre]) values('8','LABORATORIO')";sqLiteDatabase.execSQL(query);
        query = "insert into [rubros] ([id], [nombre]) values('9','FRIGORIFICO')";sqLiteDatabase.execSQL(query);
        query = "insert into [rubros] ([id], [nombre]) values('10','LOGISTICA')";sqLiteDatabase.execSQL(query);
        query = "insert into [rubros] ([id], [nombre]) values('11','BANCO')";sqLiteDatabase.execSQL(query);
        query = "insert into [rubros] ([id], [nombre]) values('12','ALIMENTOS')";sqLiteDatabase.execSQL(query);
        query = "insert into [rubros] ([id], [nombre]) values('13','TRANSPORTE')";sqLiteDatabase.execSQL(query);
        query = "insert into [rubros] ([id], [nombre]) values('14','PROCESADORA')";sqLiteDatabase.execSQL(query);
        query = "insert into [rubros] ([id], [nombre]) values('15','IMPORTADOR / EXPORTADOR')";sqLiteDatabase.execSQL(query);
        query = "insert into [rubros] ([id], [nombre]) values('16','UNIVERSIDAD')";sqLiteDatabase.execSQL(query);
        query = "insert into [rubros] ([id], [nombre]) values('17','AGRICOLA')";sqLiteDatabase.execSQL(query);
        query = "insert into [rubros] ([id], [nombre]) values('18','RESTAURANTES')";sqLiteDatabase.execSQL(query);
        query = "insert into [rubros] ([id], [nombre]) values('19','TRANSPORTE')";sqLiteDatabase.execSQL(query);
        query = "insert into [rubros] ([id], [nombre]) values('20','PUBLICIDAD')";sqLiteDatabase.execSQL(query);
        query = "insert into [rubros] ([id], [nombre]) values('21','SERVICIOS GRAFICOS')";sqLiteDatabase.execSQL(query);
        query = "insert into [rubros] ([id], [nombre]) values('22','VENTA DE RESINAS')";sqLiteDatabase.execSQL(query);
        query = "insert into [rubros] ([id], [nombre]) values('23','VENTA DE ADITIVOS')";sqLiteDatabase.execSQL(query);
        query = "insert into [rubros] ([id], [nombre]) values('24','VENTA DE COLORANTES - MB')";sqLiteDatabase.execSQL(query);
        query = "insert into [rubros] ([id], [nombre]) values('25','VENTA DE COLORANTES - PIGMENTOS')";sqLiteDatabase.execSQL(query);
        query = "insert into [rubros] ([id], [nombre]) values('26','VENTA DE CAJAS EN DESUSO')";sqLiteDatabase.execSQL(query);
        query = "insert into [rubros] ([id], [nombre]) values('27','HOTELES')";sqLiteDatabase.execSQL(query);
        query = "insert into [rubros] ([id], [nombre]) values('28','SUMINISTROS Y REPARACION DE EQUIPOS')";sqLiteDatabase.execSQL(query);
        query = "insert into [rubros] ([id], [nombre]) values('29','OTROS')";sqLiteDatabase.execSQL(query);*/
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {
        sqLiteDatabase.execSQL("drop table if exists " + Tb_Clientes.nombreTabla);
        sqLiteDatabase.execSQL("drop table if exists " + Tb_cotizacion_productos.nombreTabla);
        sqLiteDatabase.execSQL("drop table if exists " + Tb_Cotizaciones.nombre);
        sqLiteDatabase.execSQL("drop table if exists " + Tb_EstadoCotizacion.nombreTabla);
        sqLiteDatabase.execSQL("drop table if exists " + Tb_Productos.nombreTabla);
        sqLiteDatabase.execSQL("drop table if exists " + Tb_Usuarios.tb_nombreTabla);
        sqLiteDatabase.execSQL("drop table if exists " + Tb_Visita.nombreTabla);
        sqLiteDatabase.execSQL("drop table if exists " + Tb_contactos.nombreTabla);
        sqLiteDatabase.execSQL("drop table if exists " + Tb_pedidos.nombreTabla);

        onCreate(sqLiteDatabase);
    }

    //CONDICOON VENTA

    public ArrayList<ContentValues> GetFullVenta() {
        ArrayList<ContentValues> venta = new ArrayList<>();
        try {
            String selectQuery = "SELECT id,nombre FROM condicion_venta ORDER BY id";

            Cursor cursor = database.rawQuery(selectQuery, null);
            if (cursor.moveToFirst()) {
                do {
                    ContentValues map = new ContentValues();
                    map.put("id",cursor.getString(0));
                    map.put("nombre", cursor.getString(1));
                    venta.add(map);
                } while (cursor.moveToNext());
            }
            cursor.close();
        } catch (Exception e) {
            System.out.println("Error-GetFullVenta: " + e.toString());
        }
        return venta;
    }

    ArrayList<ContentValues> GetFullSectores() {
        ArrayList<ContentValues> venta = new ArrayList<>();
        try {
            String selectQuery = "SELECT id,nombre FROM rubros ORDER BY id";

            Cursor cursor = database.rawQuery(selectQuery, null);
            if (cursor.moveToFirst()) {
                do {
                    ContentValues map = new ContentValues();
                    map.put("id",cursor.getString(0));
                    map.put("nombre", cursor.getString(1));
                    venta.add(map);
                } while (cursor.moveToNext());
            }
            cursor.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return venta;
    }

    String GetSector(String id){
        String nm = "";
        try {
            String selectQuery = "SELECT id,nombre FROM rubros WHERE id = '"+id+"' ORDER BY id";

            Cursor cursor = database.rawQuery(selectQuery, null);
            if (cursor.moveToFirst()) {
                do {
                    nm= cursor.getString(1);

                } while (cursor.moveToNext());
            }
            cursor.close();
        } catch (Exception e) {
            System.out.println("Error-GetCondicionPago: " + e.toString());
        }
        return nm;
    }

    ArrayList<ContentValues> GetFullRubros() {
        ArrayList<ContentValues> venta = new ArrayList<>();
        try {
            String selectQuery = "SELECT id,nombre FROM sectores ORDER BY id";

            Cursor cursor = database.rawQuery(selectQuery, null);
            if (cursor.moveToFirst()) {
                do {
                    ContentValues map = new ContentValues();
                    map.put("id",cursor.getString(0));
                    map.put("nombre", cursor.getString(1));
                    venta.add(map);
                } while (cursor.moveToNext());
            }
            cursor.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return venta;
    }

    public String GetCondicionPago(String id){
        String nm = "";
        try {
            String selectQuery = "SELECT id,nombre FROM condicion_venta WHERE id = '"+id+"' ORDER BY id";

            Cursor cursor = database.rawQuery(selectQuery, null);
            if (cursor.moveToFirst()) {
                do {
                    nm= cursor.getString(1);
                    Log.e("NombreCondicion",nm);
                    return nm;
                } while (cursor.moveToNext());
            }
            cursor.close();
        } catch (Exception e) {
           e.printStackTrace();
        }
        Log.e("Nombre",nm);
        return nm;
    }

    public ArrayList<ContentValues> GetFullVentaBusqueda(String c) {
        ArrayList<ContentValues> venta = new ArrayList<>();
        try {
            String nomtempoFiltro = SepararParlabras(c, "nombre");
            String selectQuery = "SELECT id,nombre FROM condicion_venta"+
                    " WHERE ((" + (nomtempoFiltro.isEmpty()?" nombre LIKE '% %'":nomtempoFiltro) + ") ) ORDER BY id";

            Cursor cursor = database.rawQuery(selectQuery, null);
            if (cursor.moveToFirst()) {
                do {
                    ContentValues map = new ContentValues();
                    map.put("id",cursor.getString(0));
                    map.put("nombre", cursor.getString(1));
                    venta.add(map);
                } while (cursor.moveToNext());
            }
            cursor.close();
        } catch (Exception e) {
            System.out.println("Error-GetFullClientesBusqueda: " + e.toString());
        }
        return venta;
    }

    ArrayList<ContentValues> GetFullSectoresBusqueda(String c) {
        ArrayList<ContentValues> venta = new ArrayList<>();
        try {
            String nomtempoFiltro = SepararParlabras(c, "nombre");
            String selectQuery = "SELECT id,nombre FROM rubros"+
                    " WHERE ((" + (nomtempoFiltro.isEmpty()?" nombre LIKE '% %'":nomtempoFiltro) + ") OR id LIKE '%"+c+"%') ORDER BY id";

            Cursor cursor = database.rawQuery(selectQuery, null);
            if (cursor.moveToFirst()) {
                do {
                    ContentValues map = new ContentValues();
                    map.put("id",cursor.getString(0));
                    map.put("nombre", cursor.getString(1));
                    venta.add(map);
                } while (cursor.moveToNext());
            }
            cursor.close();
        } catch (Exception e) {
            System.out.println("Error-GetFullSectoresBusqueda: " + e.toString());
        }
        return venta;
    }

    //CLIENTES
    void InsertClientes(ContentValues valores){
        try {
            database.insert(Tb_Clientes.nombreTabla, null, valores);
            Log.d("INSERT","Success");
        } catch (Exception e) {
            System.out.println("Error-InsertClientes: " + e.toString());
        }
    }

    void UpdateCliente(ContentValues valores, String _c,String id){
        try {
            database.update(Tb_Clientes.nombreTabla,valores,Tb_Clientes.ruc + "= '"+_c+"' OR id = "+id,null);
            Log.d("UPDATE","Success");
        } catch (Exception e) {
            System.out.println("Error-UpdateCliente: " + e.toString());
        }
    }

    void updateCorreoCliente(String _c, String correo){
        Log.i("CORREO", correo+" - "+ _c);
        ContentValues cv = new ContentValues();
        cv.put(Tb_Clientes.estado, 2);
        cv.put(Tb_Clientes.correo, correo);
        cv.put(Tb_Clientes.current_ruc, _c);

        //String selectQuery = "UPDATE "+tb_clientes.nombreTabla+ " SET "+tb_clientes.estado+" = 2, "+tb_clientes.correo+" = "+correo+ " WHERE " + tb_clientes.ruc + " = '"+_c+"'";
        //database.execSQL(selectQuery);
        database.update(Tb_Clientes.nombreTabla, cv, Tb_Clientes.ruc + " = '"+_c+"'", null);
    }

    void updateContactoCliente(String _c, String contacto){
        Log.i("CORREO", contacto+" - "+ _c);
        ContentValues cv = new ContentValues();
        cv.put(Tb_Clientes.estado, 2);
        cv.put(Tb_Clientes.contacto, contacto);
        cv.put(Tb_Clientes.current_ruc, _c);
        //TODO: ver si poner code
        //String selectQuery = "UPDATE "+tb_clientes.nombreTabla+ " SET "+tb_clientes.estado+" = 2, "+tb_clientes.correo+" = "+correo+ " WHERE " + tb_clientes.ruc + " = '"+_c+"'";
        //database.execSQL(selectQuery);
        database.update(Tb_Clientes.nombreTabla, cv, Tb_Clientes.ruc + " = '"+_c+"'", null);
    }

    void CapturaDatosClientesJSONV2(JSONArray datos){
        try {
            BorradaGeneral(Tb_Clientes.nombreTabla," WHERE clie_estado = 1" );
            for (int i = 0; i < datos.length(); i++) {
                JSONObject objeto = datos.getJSONObject(i);
                ContentValues valores = new ContentValues();
                valores.put("id",objeto.getString("id"));
                valores.put("clie_razonsocial",objeto.getString("razon_social"));
                valores.put("clie_nombrecomercial",objeto.getString("nombre_comercial"));
                valores.put(Tb_Clientes.rubro,objeto.getString("rubro"));
                valores.put(Tb_Clientes.sector,objeto.getString("sector"));
                String ruc = objeto.getString("ruc");
                valores.put("clie_ruc",ruc);



                valores.put("clie_perceptor",objeto.getString("perceptor"));
                valores.put(Tb_Clientes.por_perceptor,objeto.getString("porcentaje"));
                valores.put("clie_retenedor",objeto.getString("retenedor"));
                valores.put("clie_calificacion",objeto.getString("calificacion"));
                valores.put("clie_estado","1");
                valores.put("clie_tipo_negocio",objeto.getString("tipo_cliente"));
                valores.put(Tb_Clientes.tipoExtaNa,objeto.getString("tipo"));


                BorradaGeneral(Tb_contactos.nombreTabla," WHERE " + Tb_contactos.cliente + " = " + ruc);
                ArrayList<AllContacts> contactsArrayList = new ArrayList<>();
                JSONArray contactos = objeto.getJSONArray("contactos");
                for (int j = 0; j < contactos.length(); j++){
                    JSONObject contacto = contactos.getJSONObject(j);
                    AllContacts contact = new Gson().fromJson(contacto.toString(),AllContacts.class);
                    if (j==0){
                        valores.put("clie_contacto",contact.getContacto());

                        valores.put("clie_direccion",contact.getDireccion());
                        valores.put("clie_telemovil",contact.getTelefonoMovil());
                        valores.put("clie_telefijo",contact.getTelefonoFijo());
                        valores.put("clie_correo",contact.getCorreo());
                        valores.put("clie_pago",contact.getCondPago());
                        valores.put(Tb_Clientes.cond_venta,contact.getCondVenta());
                        valores.put("clie_zona",contact.getZona());
                        //valores.put(Tb_Clientes.vendedor,contact.getVendedor());
                    }
                    contactsArrayList.add(contact);
                }
                saveClienteContact(ruc,contactsArrayList,1);
                InsertClientes(valores);
            }
        }catch (JSONException e) {
                e.printStackTrace();
        }
    }

    void CapturaDatosClientesJSON(JSONArray datos){
        try {
            BorradaGeneral(Tb_Clientes.nombreTabla," WHERE clie_estado = 1" );
            for (int i = 0; i < datos.length(); i++) {
                JSONObject objeto = datos.getJSONObject(i);
                ContentValues valores = new ContentValues();
                valores.put("id",objeto.getString("id"));
                valores.put("clie_razonsocial",objeto.getString("razon_social"));
                valores.put("clie_nombrecomercial",objeto.getString("nombre_comercial"));
                valores.put(Tb_Clientes.rubro,objeto.getString("rubro"));
                valores.put(Tb_Clientes.sector,objeto.getString("sector"));
                valores.put("clie_ruc",objeto.getString("ruc"));
                valores.put("clie_contacto",objeto.getString("contacto"));
                valores.put("clie_direccion",objeto.getString("direccion"));
                valores.put("clie_telemovil",objeto.getString("telefono_movil"));
                valores.put("clie_telefijo",objeto.getString("telefono_fijo"));
                valores.put("clie_correo",objeto.getString("correo"));
                valores.put("clie_pago",objeto.getString("cond_pago"));
                valores.put(Tb_Clientes.cond_venta,objeto.getString("cond_venta"));
                valores.put("clie_zona",objeto.getString("zona"));
                valores.put("clie_perceptor",objeto.getString("perceptor"));
                valores.put(Tb_Clientes.por_perceptor,objeto.getString("porcentaje"));
                valores.put("clie_retenedor",objeto.getString("retenedor"));
                valores.put("clie_calificacion",objeto.getString("calificacion"));
                valores.put("clie_estado","1");
                valores.put("clie_tipo_negocio",objeto.getString("tipo_cliente"));
                valores.put(Tb_Clientes.tipoExtaNa,objeto.getString("tipo_cliente"));
                InsertClientes(valores);
            }
        }catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public ArrayList<ContentValues> getClientesListFromJSON(JSONArray datos){
        try {
            ArrayList<ContentValues> list = new ArrayList<>();
            for (int i = 0; i < datos.length(); i++) {
                JSONObject objeto = datos.getJSONObject(i);
                ContentValues valores = new ContentValues();
                valores.put("id",objeto.getString("id"));
                valores.put("clie_razonsocial",objeto.getString("razon_social"));
                valores.put("clie_nombrecomercial",objeto.getString("nombre_comercial"));
                valores.put(Tb_Clientes.rubro,objeto.getString("rubro"));
                valores.put(Tb_Clientes.sector,objeto.getString("sector"));
                valores.put("clie_ruc",objeto.getString("ruc"));
                valores.put("clie_contacto",objeto.getString("contacto"));
                valores.put("clie_direccion",objeto.getString("direccion"));
                valores.put("clie_telemovil",objeto.getString("telefono_movil"));
                valores.put("clie_telefijo",objeto.getString("telefono_fijo"));
                valores.put("clie_correo",objeto.getString("correo"));
                valores.put("clie_pago",objeto.getString("cond_pago"));
                valores.put("clie_zona",objeto.getString("zona"));
                valores.put("clie_perceptor",objeto.getString("perceptor"));
                valores.put(Tb_Clientes.por_perceptor,objeto.getString("porcentaje"));
                valores.put("clie_retenedor",objeto.getString("retenedor"));
                valores.put("clie_calificacion",objeto.getString("calificacion"));
                valores.put("clie_estado","1");
                valores.put("clie_tipo_negocio",objeto.getString("tipo_cliente"));
                valores.put(Tb_Clientes.tipoExtaNa,objeto.getString("tipo"));
                list.add(valores);
            }
            return list;
        }catch (JSONException e) {
            e.printStackTrace();
        }
        return null;
    }

    void updateEstadoCliente(String _c){
        String selectQuery = "UPDATE "+Tb_Clientes.nombreTabla+ " SET "+Tb_Clientes.estado+" = 1  WHERE " + Tb_Clientes.ruc + " = '"+_c+"'";
        database.execSQL(selectQuery);
    }

    public void updateEstadoContacto(String id){
        String selectQuery = "UPDATE "+Tb_contactos.nombreTabla+ " SET "+Tb_contactos.estado+" = 1  WHERE " + Tb_contactos.cliente + " = '"+id+"'";
        database.execSQL(selectQuery);
    }

    boolean CheckDataSync(){
        String selectQuery = "SELECT * FROM " + Tb_Clientes.nombreTabla  + " WHERE " + Tb_Clientes.estado +"= 0 OR "+ Tb_Clientes.estado +"= 2"  ;
        Cursor cursor = database.rawQuery(selectQuery, null);
        if (cursor.getCount()>0) {
            System.out.println("mayor: >0");
            cursor.close();
            return true;
        }
        cursor.close();
        return false;
    }

    public boolean CheckContactosSync(){
        String selectQuery = "SELECT * FROM " + Tb_contactos.nombreTabla  + " WHERE " + Tb_contactos.estado +"= 0 OR "+ Tb_contactos.estado +"= 2"  ;
        Cursor cursor = database.rawQuery(selectQuery, null);
        if (cursor.getCount()>0) {
            System.out.println("mayor: >0");
            cursor.close();
            return true;
        }
        cursor.close();
        return false;
    }

    int DataSync(String tabla){
        String selectQuery = "SELECT Count(*) FROM " + tabla;
        Cursor cursor = database.rawQuery(selectQuery, null);
        if (cursor.moveToFirst()) {
            //do {
            return  cursor.getInt(0);
            //} while (cursor.moveToNext());
        }
        cursor.close();
        return 0;
    }

    ArrayList<ContentValues> GetFullClientes() {
        ArrayList<ContentValues> cliente = new ArrayList<>();
        try {
            String selectQuery = "SELECT "+Tb_Clientes.razon_social+","+Tb_Clientes.ruc+","+Tb_Clientes.contacto+","+Tb_Clientes.code + "," +
                    Tb_Clientes.por_perceptor+","+Tb_Clientes.tiponegocio+","+Tb_Clientes.cond_pago+","+Tb_Clientes.cond_venta
                    + ","+Tb_Clientes.tipoExtaNa+","+Tb_Clientes.estado+ ","+Tb_Clientes.correo+ " FROM " + Tb_Clientes.nombreTabla + " ORDER BY " + Tb_Clientes.razon_social;

            Cursor cursor = database.rawQuery(selectQuery, null);
            if (cursor.moveToFirst()) {
                do {
                    ContentValues map = new ContentValues();
                    map.put(Tb_Clientes.razon_social,cursor.getString(0));
                    map.put(Tb_Clientes.ruc, cursor.getString(1));
                    map.put(Tb_Clientes.contacto, cursor.getString(2));
                    map.put(Tb_Clientes.code, cursor.getString(3));
                    map.put(Tb_Clientes.por_perceptor, cursor.getString(4));
                    map.put(Tb_Clientes.tiponegocio, cursor.getString(5));
                    map.put(Tb_Clientes.cond_pago, cursor.getString(6));
                    map.put(Tb_Clientes.cond_venta,cursor.getString(7));
                    map.put(Tb_Clientes.tipoExtaNa, cursor.getString(8));
                    map.put(Tb_Clientes.estado, cursor.getString(9));
                    map.put(Tb_Clientes.correo, cursor.getString(10));
                    cliente.add(map);
                } while (cursor.moveToNext());
            }
            cursor.close();
        } catch (Exception e) {
            System.out.println("Error-GetFullClientes: " + e.toString());
        }
        return cliente;
    }

    ArrayList<ContentValues> GetFullClientesBusqueda(String c) {
        ArrayList<ContentValues> cliente = new ArrayList<>();
        try {
            String nomtempoFiltro = SepararParlabras(c, Tb_Clientes.razon_social);
            String selectQuery = "SELECT "+Tb_Clientes.razon_social+","+Tb_Clientes.ruc+","+Tb_Clientes.contacto+"," + Tb_Clientes.code + ","
                    + Tb_Clientes.por_perceptor+","+Tb_Clientes.tiponegocio+","+Tb_Clientes.cond_pago+","+Tb_Clientes.cond_venta
                    +","+Tb_Clientes.tipoExtaNa+","+Tb_Clientes.estado+","+Tb_Clientes.correo+" FROM " + Tb_Clientes.nombreTabla +
                    " WHERE ((" + (nomtempoFiltro.isEmpty()?" "+Tb_Clientes.razon_social+" LIKE '% %'":nomtempoFiltro)
                    + ") OR ("+Tb_Clientes.ruc+" LIKE '%"+c+"%')) ORDER BY " + Tb_Clientes.razon_social;

            Cursor cursor = database.rawQuery(selectQuery, null);
            if (cursor.moveToFirst()) {
                do {
                    ContentValues map = new ContentValues();
                    map.put(Tb_Clientes.razon_social,cursor.getString(0));
                    map.put(Tb_Clientes.ruc, cursor.getString(1));
                    map.put(Tb_Clientes.contacto, cursor.getString(2));
                    map.put(Tb_Clientes.code, cursor.getString(3));
                    map.put(Tb_Clientes.por_perceptor, cursor.getString(4));
                    map.put(Tb_Clientes.tiponegocio, cursor.getString(5));
                    map.put(Tb_Clientes.cond_pago, cursor.getString(6));
                    map.put(Tb_Clientes.cond_venta,cursor.getString(7));
                    map.put(Tb_Clientes.tipoExtaNa, cursor.getString(8));
                    map.put(Tb_Clientes.estado, cursor.getString(9));
                    map.put(Tb_Clientes.correo, cursor.getString(10));
                    cliente.add(map);
                } while (cursor.moveToNext());
            }
            cursor.close();
        } catch (Exception e) {
            System.out.println("Error-GetFullClientesBusqueda: " + e.toString());
        }
        return cliente;
    }

    ContentValues GetCliente(String _c) {
        ContentValues cliente = new ContentValues();
        try {
            String selectQuery = "SELECT "+Tb_Clientes.razon_social+","+Tb_Clientes.ruc+","+Tb_Clientes.contacto+","+ Tb_Clientes.code + "," +
                    Tb_Clientes.direccion+","+Tb_Clientes.telefono_fijo+","+ Tb_Clientes.telefono_movil+","+
                    Tb_Clientes.perceptor+","+Tb_Clientes.retenedor+","+Tb_Clientes.por_perceptor
                    +","+Tb_Clientes.calificacion+ ","+Tb_Clientes.cond_pago+","+Tb_Clientes.cond_venta+","+Tb_Clientes.nombre_comercial+","+
                    Tb_Clientes.correo+","+Tb_Clientes.zona+","+Tb_Clientes.tiponegocio +","+Tb_Clientes.tipoExtaNa +","+Tb_Clientes.rubro +
                    ","+Tb_Clientes.sector +",id,"+Tb_Clientes.estado+" FROM " + Tb_Clientes.nombreTabla + " WHERE " +
                    Tb_Clientes.ruc + " = '"+_c+"'";

            Cursor cursor = database.rawQuery(selectQuery, null);
            if (cursor.moveToFirst()) {
                do {
                    cliente.put(Tb_Clientes.razon_social,cursor.getString(0));
                    cliente.put(Tb_Clientes.ruc, cursor.getString(1));
                    cliente.put(Tb_Clientes.contacto,cursor.getString(2));
                    cliente.put(Tb_Clientes.code,cursor.getString(3));///
                    cliente.put(Tb_Clientes.direccion, cursor.getString(4));
                    cliente.put(Tb_Clientes.telefono_fijo,cursor.getString(5));
                    cliente.put(Tb_Clientes.telefono_movil, cursor.getString(6));
                    cliente.put(Tb_Clientes.perceptor,cursor.getString(7).isEmpty()?"0":cursor.getString(7));
                    cliente.put(Tb_Clientes.retenedor, cursor.getString(8).isEmpty()?"0":cursor.getString(8));
                    cliente.put(Tb_Clientes.por_perceptor,cursor.getString(9).isEmpty()?"0":cursor.getString(9));
                    cliente.put(Tb_Clientes.calificacion,cursor.getString(10).isEmpty()?"0":cursor.getString(10));
                    Log.e("captura_pago",cursor.getString(11).isEmpty()?"0":cursor.getString(11));
                    Log.e("captura_venta",cursor.getString(12).isEmpty()?"0":cursor.getString(12));
                    cliente.put(Tb_Clientes.cond_pago, cursor.getString(11).isEmpty()?"0":cursor.getString(11));
                    cliente.put(Tb_Clientes.cond_venta,cursor.getString(12).isEmpty()?"0":cursor.getString(12));
                    cliente.put(Tb_Clientes.nombre_comercial, cursor.getString(13));
                    cliente.put(Tb_Clientes.correo, cursor.getString(14));
                    cliente.put(Tb_Clientes.zona, cursor.getString(15).isEmpty()?"0":cursor.getString(15));
                    cliente.put(Tb_Clientes.tiponegocio, cursor.getString(16).isEmpty()?"0":cursor.getString(16));
                    cliente.put(Tb_Clientes.tipoExtaNa,cursor.getString(17));
                    cliente.put(Tb_Clientes.rubro, cursor.getString(18));
                    cliente.put(Tb_Clientes.sector,cursor.getString(19));
                    cliente.put(Tb_Clientes.estado,cursor.getString(21));
                    cliente.put("id",cursor.getString(20));
                } while (cursor.moveToNext());
            }
            cursor.close();
        } catch (Exception e) {
            System.out.println("Error-GetFullClientes: " + e.toString());
        }
        return cliente;
    }


    Boolean GetClienteRuc(String _c) {
        Boolean cliente = false;
        try {
            String selectQuery = "SELECT id FROM " + Tb_Clientes.nombreTabla + " WHERE " +
                    Tb_Clientes.ruc + " = '"+_c+"'";

            Cursor cursor = database.rawQuery(selectQuery, null);
            if (cursor.moveToFirst()) {
                do {
                    cliente = true;
                } while (cursor.moveToNext());
            }
            cursor.close();
        } catch (Exception e) {
            System.out.println("Error-GetFullClientes: " + e.toString());
        }
        return cliente;
    }
    public boolean CheckData(){
        String selectQuery = "SELECT * FROM " + Tb_Clientes.nombreTabla;
        Cursor cursor = database.rawQuery(selectQuery, null);
        if (cursor.getCount()>0) {
            return true;
        }
        return false;
    }

    //PRODUCTOS

    void InsertProductos(ContentValues valores){
        try {
            database.insert(Tb_Productos.nombreTabla, null, valores);
        } catch (Exception e) {
            System.out.println("Error-InsertClientes: " + e.toString());
        }
    }

    void CapturaDatosProductosJSON(JSONArray datos){
        try {
            BorradaGeneral(Tb_Productos.nombreTabla,"");
            for (int i = 0; i < datos.length(); i++) {
                JSONObject objeto = datos.getJSONObject(i);
                ContentValues valores = new ContentValues();
                valores.put("prod_codigo",objeto.getString("codigo"));
                valores.put("prod_nombre",objeto.getString("nombre"));
                valores.put("prod_moneda",objeto.getString("moneda"));
                valores.put("prod_peso",objeto.getString("peso"));
                valores.put("prod_volumen",objeto.getString("volumen"));
                valores.put("prod_stock",objeto.getString("stock"));
                valores.put("prod_comprometida",objeto.getString("comprometidas"));
                valores.put("prod_disponible",objeto.getString("disponibles"));
                valores.put("prod_material",objeto.getString("material"));
                valores.put("prod_colores",objeto.getString("colores"));
                valores.put("prod_descripcion",objeto.getString("descripcion_material"));
                valores.put("prod_precio",objeto.getString("precio"));
                valores.put("prod_igv",objeto.getString("igv"));
                valores.put("prod_total",objeto.getString("total"));
                valores.put("prod_ficha_tecnica",objeto.getString("ficha_tecnica"));
                valores.put("prod_foto",objeto.getString("foto"));
                valores.put("prod_cunio",objeto.getString("cunio"));
                valores.put("prod_rango_menor",objeto.getString("rango_menor"));
                //Log.i("captura producto", "nombre: "+objeto.getString("nombre")+" - comprometidas:"+objeto.getString("comprometidas")+" - stock:"+objeto.getString("stock")+" - disponibles:"+objeto.getString("disponibles"));
                InsertProductos(valores);
            }
        }catch (JSONException e) {
            e.printStackTrace();
        }
    }

    ArrayList<ContentValues> GetFullProductos() {
        String selectQuery = "SELECT "+ Tb_Productos.codigo+","+ Tb_Productos.nombre+" FROM " + Tb_Productos.nombreTabla;
        ArrayList<ContentValues> productos = new ArrayList<>();
        Cursor cursor = database.rawQuery(selectQuery, null);
        try {
            if (cursor.moveToFirst()) {
                do {
                    ContentValues map = new ContentValues();
                    map.put(Tb_Productos.codigo,cursor.getString(0));
                    map.put(Tb_Clientes.razon_social, cursor.getString(1));
                    //Log.i("FULL",map.toString());
                    productos.add(map);
                } while (cursor.moveToNext());
            }
        } catch (Exception e) {
            System.out.println("Error-GetFullProductos: " + e.toString());
        }finally {
            cursor.close();
        }
        return productos;
    }


    ArrayList<ContentValues> GetFullProductoBusqueda(String c) {
        ArrayList<ContentValues> cliente = new ArrayList<>();
        try {
            c = c.trim();
            String nomtempoFiltro = SepararParlabras(c, Tb_Productos.nombre);
            String selectQuery = "SELECT "+ Tb_Productos.codigo+","+ Tb_Productos.nombre+","+ Tb_Productos.comprometida+","+ Tb_Productos.stock+" FROM "
                    + Tb_Productos.nombreTabla + " WHERE ((" + (nomtempoFiltro.isEmpty()?" "
                    + Tb_Productos.nombre+" LIKE '% %'":nomtempoFiltro) + ") OR ("+ Tb_Productos.codigo+" LIKE '%"+c+"%')) ORDER BY " + Tb_Productos.nombre ;
            Log.i("QUERY PRODUCTO", selectQuery);

            Cursor cursor = database.rawQuery(selectQuery, null);
            if (cursor.moveToFirst()) {
                do {
                    ContentValues map = new ContentValues();
                    map.put(Tb_Productos.codigo,cursor.getString(0));
                    map.put(Tb_Clientes.razon_social, cursor.getString(1));
                    Log.i("PROBUSCADO", cursor.getString(0)+" - "+cursor.getString(1)+" - "+cursor.getString(2)+" - "+cursor.getString(3));
                    cliente.add(map);
                } while (cursor.moveToNext());
            }
            cursor.close();
        } catch (Exception e) {
            System.out.println("Error-GetFullProductoBusqueda: " + e.toString());
        }
        return cliente;
    }

    ContentValues GetProducto(String _c) {
        String selectQuery = "SELECT "+ Tb_Productos.nombre+","+ Tb_Productos.codigo+","+ Tb_Productos.precio+","+
                Tb_Productos.stock+","+ Tb_Productos.moneda+","+ Tb_Productos.foto+","+ Tb_Productos.descripcion+","+ Tb_Productos.material+
                ","+ Tb_Productos.comprometida + ","+ Tb_Productos.disponible + ","+ Tb_Productos.cunio +
                ","+ Tb_Productos.rango_menor +","+ Tb_Productos.igv +  " FROM " + Tb_Productos.nombreTabla + " WHERE TRIM("+
                Tb_Productos.codigo + ")=TRIM('"+_c+"')";
        ContentValues producto = new ContentValues();

        Cursor cursor = database.rawQuery(selectQuery, null);
        try {
            if (cursor.moveToFirst()) {
                do {
                    Log.i("Content", cursor.getString(0));
                    producto.put(Tb_Productos.nombre,cursor.getString(0));
                    producto.put(Tb_Productos.codigo, cursor.getString(1));
                    producto.put(Tb_Productos.precio,cursor.getString(2));
                    producto.put(Tb_Productos.stock, cursor.getString(3));
                    producto.put(Tb_Productos.moneda, cursor.getString(4));
                    producto.put(Tb_Productos.foto, cursor.getString(5));
                    producto.put(Tb_Productos.descripcion, cursor.getString(6));
                    producto.put(Tb_Productos.material, cursor.getString(7));
                    producto.put(Tb_Productos.comprometida, cursor.getString(8));
                    producto.put(Tb_Productos.disponible, cursor.getString(9));
                    producto.put(Tb_Productos.cunio, cursor.getString(10));
                    producto.put(Tb_Productos.rango_menor, cursor.getString(11));
                    producto.put(Tb_Productos.igv, cursor.getString(12));
                } while (cursor.moveToNext());
            }
        } catch (Exception e) {
            System.out.println("Error-GetProducto: " + e.toString());
        }finally {
            cursor.close();
        }
        return producto;
    }

    void updateComprometidoProducto(String _c,int camo,int cantidad,SQLiteDatabase database){
        String selectQuery = "SELECT "+ Tb_Productos.comprometida +","+ Tb_Productos.stock +" FROM " + Tb_Productos.nombreTabla + " WHERE "+
                Tb_Productos.codigo + "='"+_c+"'";
        Cursor cursor = database.rawQuery(selectQuery, null);
        try {
            if (cursor.moveToFirst()) {
                do {
                    if(camo==1){
                        int i = Integer.parseInt(cursor.getString(1)) -cantidad;
                        String selectQuery1 = "UPDATE "+ Tb_Productos.nombreTabla+ " SET "+ Tb_Productos.stock +" = "+i+"  WHERE " + Tb_Productos.codigo + "='"+_c+"'";
                        database.execSQL(selectQuery1);
                    }else if(camo == 2){
                        int i = Integer.parseInt(cursor.getString(0)) +cantidad;
                        String selectQuery1 = "UPDATE "+ Tb_Productos.nombreTabla+ " SET "+ Tb_Productos.comprometida +" = "+i+"  WHERE " + Tb_Productos.codigo + "='"+_c+"'";
                        database.execSQL(selectQuery1);
                    }

                } while (cursor.moveToNext());
            }
        } catch (Exception e) {
            System.out.println("Error-GetProducto: " + e.toString());
        }finally {
            cursor.close();
        }

    }

    //COTIZACION Y COTIZACION_PRODUCTO

    public void InsertCotizacion(ContentValues valores){
        //try {
        if (database.insert(Tb_Cotizaciones.nombreTabla, null, valores)>0){
            Log.e("GuardarCotizazacion","Success");
        }
        //} catch (Exception e) {
         //   System.out.println("Error-InsertCotizacion: " + e.toString());
        //}
    }

    public void InsertCotizacionEstado(ContentValues valores){
        database.insert(Tb_EstadoCotizacion.nombreTabla, null, valores);
    }

    /*public void CapturaArrayListCotiPro(ArrayList<ContentValues> datos,int b){
        for (int i = 0; i < datos.size();i++){
            System.out.println("CapturaArrayListCotiPro: " + datos.get(i).get(Tb_cotizacion_productos.producto));
            int hasEstado = Integer.parseInt(datos.get(i).get(Tb_cotizacion_productos.cantidad).toString());
            updateComprometidoProducto(datos.get(i).get(Tb_cotizacion_productos.producto).toString(),b,hasEstado);
            System.out.println("Cambiando stock: " + datos.get(i).get(Tb_cotizacion_productos.producto));
            InsertCotizacionProducto(datos.get(i));
        }

    }*/

    public void InsertCotizacionProducto(ContentValues valores){
        try {
            if (database.insert(Tb_cotizacion_productos.nombreTabla, null, valores)>0){
                Log.e("GuardarProductoCotiza","Success");
            }
        } catch (Exception e) {
            System.out.println("Error-InsertCotizacionProducto: " + e.toString());
        }
    }

    int GetIdCotizacion(){
        try {
            String selectQuery = "SELECT MAX(id) FROM " + Tb_Cotizaciones.nombreTabla;

            Cursor cursor = database.rawQuery(selectQuery, null);
            if (cursor.moveToFirst()) {
                do {
                    return cursor.getInt(0);
                } while (cursor.moveToNext());
            }
            cursor.close();
        } catch (Exception e) {
            System.out.println("Error-GetIdCotizacion: " + e.toString());
        }
        return 0;
    }

    int GetIdCliente(){
        try {
            String selectQuery = "SELECT MAX(id) FROM " + Tb_Clientes.nombreTabla;
            Cursor cursor = database.rawQuery(selectQuery, null);
            if (cursor.moveToFirst()) {
                do {
                    return cursor.getInt(0);
                } while (cursor.moveToNext());
            }
            cursor.close();
        } catch (Exception e) {
            System.out.println("Error-GetIdCotizacion: " + e.toString());
        }
        return 0;
    }
    boolean CheckDataSyncCotiza(int i){
        String selectQuery = "SELECT * FROM " + Tb_Cotizaciones.nombreTabla  + " WHERE " + Tb_Cotizaciones.estadoSynco +"= "+i  ;
        Cursor cursor = database.rawQuery(selectQuery, null);
        if (cursor.getCount()>0) {
            System.out.println("mayor: >0");
            cursor.close();
            return true;
        }
        cursor.close();
        return false;
    }




    boolean CheckDataSyncCotizaEstado(){
        String selectQuery = "SELECT * FROM " + Tb_EstadoCotizacion.nombreTabla  + " WHERE " + Tb_EstadoCotizacion.estadoSynco +" = 0 "  ;
        Cursor cursor = database.rawQuery(selectQuery, null);
        if (cursor.getCount()>0) {
            System.out.println("mayor111: >0");
            cursor.close();
            return true;
        }
        cursor.close();
        return false;
    }

    ArrayList<ContentValues> GetFullCotizacion() {
        String selectQuery = "SELECT id,"+ Tb_Cotizaciones.cliente +","+
                Tb_Cotizaciones.fecha_registro+","+ Tb_Cotizaciones.monto_total+","+
                Tb_Cotizaciones.moneda+","+ Tb_Cotizaciones.items+","+ Tb_Cotizaciones.codigo+","+ Tb_Cotizaciones.estado+
                ","+ Tb_Cotizaciones.estadoSynco+","+ Tb_Cotizaciones.subtotal+","+ Tb_Cotizaciones.monto_igv+","+ Tb_Cotizaciones.porcentaje+
                ","+ Tb_Cotizaciones.nombre+",pdf,"+ Tb_Cotizaciones.fecha+ " FROM " + Tb_Cotizaciones.nombreTabla + " ORDER BY datetime(coti_fecha_registo) DESC" ;
        ArrayList<ContentValues> coti = new ArrayList<>();
        Cursor cursor = database.rawQuery(selectQuery, null);
        try {
            if (cursor.moveToFirst()) {
                do {

                    String nombre = nombreCliente(cursor.getString(1),database);
                     if(nombre != null) {
                         if(!nombre.isEmpty()){
                             ContentValues map = new ContentValues();
                             map.put("id", cursor.getString(0));
                             map.put(Tb_Cotizaciones.nombre, cursor.getString(12));
                             map.put(Tb_Cotizaciones.fecha, cursor.getString(2));
                             map.put(Tb_Cotizaciones.monto_total,  cursor.getString(3));
                             map.put(Tb_Cotizaciones.moneda, cursor.getString(4));
                             map.put(Tb_Cotizaciones.items, GetCantidadTotalCotizacion(cursor.getString(0)));
                             map.put(Tb_Cotizaciones.codigo, cursor.getString(6));
                             map.put(Tb_Cotizaciones.estado, cursor.getString(7));
                             map.put(Tb_Cotizaciones.estadoSynco, cursor.getString(8));
                             map.put("pdf", cursor.getString(13));
                             //Log.i("COTIZACION", cursor.getString(6)+" - "+cursor.getString(2)+" - "+cursor.getString(7)+" - "+cursor.getString(14));
                             coti.add(map);
                         }
                    }
                } while (cursor.moveToNext());
            }
            Collections.sort(coti, new Comparator<ContentValues>() {
                @Override
                public int compare(ContentValues o1, ContentValues o2) {
                    SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss",Locale.getDefault());


                    String str1 = o1.getAsString(Tb_Cotizaciones.fecha);
                    String str2 = o2.getAsString(Tb_Cotizaciones.fecha);
                    Date date1;
                    Date date2;
                    try {
                        date1 = formatter.parse(str1);
                        date2 = formatter.parse(str2);

                        return date2.compareTo(date1);
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }

                    return 0;
                }
            });
            Log.i("COTIZACION1", "FINAL");
        } catch (Exception e) {
            System.out.println("Error-GetFullCotizacion(): " + e.toString());
        }finally {
            cursor.close();
        }
        return coti;
    }

    ArrayList<ContentValues> GetFullFiltroCotizacion(String d) {
        String nomtempoFiltro = SepararParlabras(d, Tb_Cotizaciones.nombre);
        String selectQuery = "SELECT id,"+ Tb_Cotizaciones.cliente +","+
                Tb_Cotizaciones.fecha_registro+","+ Tb_Cotizaciones.monto_total+","+
                Tb_Cotizaciones.moneda+","+ Tb_Cotizaciones.items+","+ Tb_Cotizaciones.codigo+","+ Tb_Cotizaciones.estado+
                ","+ Tb_Cotizaciones.estadoSynco+","+ Tb_Cotizaciones.subtotal+","+ Tb_Cotizaciones.monto_igv+","+ Tb_Cotizaciones.porcentaje+
                ","+ Tb_Cotizaciones.nombre+",pdf FROM " + Tb_Cotizaciones.nombreTabla +   " WHERE ((" + (nomtempoFiltro.isEmpty()?" "+ Tb_Cotizaciones.nombre
                +" LIKE '% %'":nomtempoFiltro) + ") OR ("+ Tb_Cotizaciones.cliente +" LIKE '%"+d+"%')  OR ("+ Tb_Cotizaciones.fecha_registro+" LIKE '%"+d+
                "%') OR ("+ Tb_Cotizaciones.monto_total+" LIKE '%"+d+"%') ) " + " ORDER BY datetime(coti_fecha_registo) DESC";
        ArrayList<ContentValues> coti = new ArrayList<>();

        int contador=0;
        Cursor cursor = database.rawQuery(selectQuery, null);
        try {
            if (cursor.moveToFirst()) {
                do {

                    ContentValues map = new ContentValues();
                    map.put("id", cursor.getString(0));
                    map.put(Tb_Cotizaciones.nombre, cursor.getString(12));
                    map.put(Tb_Cotizaciones.fecha, cursor.getString(2));
                    map.put(Tb_Cotizaciones.monto_total,  cursor.getString(3));
                    map.put(Tb_Cotizaciones.moneda, cursor.getString(4));
                    map.put(Tb_Cotizaciones.items, GetCantidadTotalCotizacion(cursor.getString(0)));
                    map.put(Tb_Cotizaciones.codigo, cursor.getString(6));
                    map.put(Tb_Cotizaciones.estado, cursor.getString(7));
                    map.put(Tb_Cotizaciones.estadoSynco, cursor.getString(8));
                    map.put("pdf", cursor.getString(13));

                    coti.add(map);
                    contador++;
                } while (cursor.moveToNext());
            }
            Log.e("ContadosCotizacion",String.valueOf(contador));
            Collections.sort(coti, new Comparator<ContentValues>() {
                @Override
                public int compare(ContentValues o1, ContentValues o2) {
                    SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss", Locale.getDefault());


                    String str1 = o1.getAsString(Tb_Cotizaciones.fecha);
                    String str2 = o2.getAsString(Tb_Cotizaciones.fecha);
                    Date date1 = null;
                    Date date2 = null;
                    try {
                        date1 = formatter.parse(str1);
                        date2 = formatter.parse(str2);
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }

                    return date2.compareTo(date1);
                }
            });
        } catch (Exception e) {
            System.out.println("Error-GetFullCotizacion(): " + e.toString());
        }finally {
            cursor.close();
        }
        return coti;
    }

    void updateEstadoPPPCotiza(String codigo,String cotizacion){
        String selectQuery = "UPDATE "+ Tb_Cotizaciones.nombreTabla+ " SET "+ Tb_Cotizaciones.estado+" = "+cotizacion+" WHERE "+ Tb_Cotizaciones.codigo+" = '"+codigo+"'";
        ContentValues c = new ContentValues();
        c.put(Tb_EstadoCotizacion.codigo,codigo);
        c.put(Tb_EstadoCotizacion.estado,cotizacion);
        InsertCotizacionEstado(c);
        database.execSQL(selectQuery);
    }

    void updateEstadoCotizaPPP(String co){
        System.out.println("Codigo cotización: " + co);
        String selectQuery = "UPDATE "+ Tb_EstadoCotizacion.nombreTabla+ " SET "+ Tb_EstadoCotizacion.estadoSynco+" = 1 WHERE "+ Tb_EstadoCotizacion.codigo+" = '"+co+"'";
        database.execSQL(selectQuery);
    }

    void updateEstadoCotiza(String _c, String co, int pdf){
        System.out.println("Codigo cotización: " + co+" - "+pdf);
        String selectQuery = "UPDATE "+ Tb_Cotizaciones.nombreTabla+ " SET "+ Tb_Cotizaciones.estadoSynco+" = 1, "+ Tb_Cotizaciones.codigo+" = '"+co+"',pdf = "+pdf+"  WHERE id = " + _c;
        database.execSQL(selectQuery);
    }

    void updateActionProductos(String _c){
        System.out.println("Accion producto: " +_c);
        String selectQuery = "UPDATE "+ Tb_cotizacion_productos.nombreTabla+ " SET "+ Tb_cotizacion_productos.action+" = 3  WHERE copr_cotizacion = " + _c;
        database.execSQL(selectQuery);
    }

    void updateEstadoCotizaOff(String _c,String co,int pdf, int estado){
        System.out.println("Codigo cotizaciónOff: " + _c+" - "+pdf);

        ContentValues values = new ContentValues();

        values.put(Tb_Cotizaciones.estadoSynco,estado);
        values.put("pdf",pdf);

        if (database.update(Tb_Cotizaciones.nombreTabla,values,"id = ?",new String[]{_c}) > 0){
            Log.e("Update estado pdf","successfull");
        }
    }

    int GetEstadoCotizacion(String _id) {
        String selectQuery = "SELECT "+ Tb_Cotizaciones.estado+" FROM " + Tb_Cotizaciones.nombreTabla + " WHERE "+ Tb_Cotizaciones.codigo+" = '" + _id+"'";
        int estado = 5;
        Cursor cursor = database.rawQuery(selectQuery, null);
        try {
            if (cursor.moveToFirst()) {
                do {
                    estado = Integer.parseInt(cursor.getString(0));
                } while (cursor.moveToNext());
            }
        } catch (Exception e) {
            System.out.println("Error-GetEstadoCotizacion(): " + e.toString());
        }finally {
            cursor.close();
        }
        return estado;
    }

    boolean GetEstadoProductId(String _id) {
        String selectQuery = "SELECT "+ Tb_cotizacion_productos.idproduc+" FROM " +
                Tb_cotizacion_productos.nombreTabla + " WHERE "+ Tb_cotizacion_productos.cotizacion+" = " + _id+" AND "+ Tb_cotizacion_productos.idproduc+" = 0";
        boolean estado = false;
        Cursor cursor = database.rawQuery(selectQuery, null);
        try {
            if (cursor.moveToFirst()) {
                do {
                    estado = cursor.getInt(0)==0;
                    System.out.println("GetEstadoCotizacion(): " +  cursor.getInt(0));
                } while (cursor.moveToNext());
            }
        } catch (Exception e) {
            System.out.println("Error-GetEstadoCotizacion(): " + e.toString());
        }finally {
            cursor.close();
        }
        return estado;
    }

    String nombreCliente(String _c,SQLiteDatabase database){
        String ss= "";
        try {

            String selectQuery = "SELECT "+Tb_Clientes.razon_social+" FROM " + Tb_Clientes.nombreTabla + " WHERE " +
                    Tb_Clientes.ruc + " = '"+_c+"'";
            Cursor cursor = database.rawQuery(selectQuery, null);
            if (cursor.moveToFirst()) {
                do {
                    ss  = cursor.getString(0);
                } while (cursor.moveToNext());
            }
            cursor.close();
        } catch (Exception e) {
            System.out.println("Error-GetFullClientes: " + e.toString());
        }
        return ss;
    }

    String GetNombreContacto(int id){
        String contacto= "";
        try {

            String selectQuery = "SELECT "+Tb_contactos.contacto+" FROM " + Tb_contactos.nombreTabla + " WHERE " +
                    "id" + " = '"+id+"'";
            Cursor cursor = database.rawQuery(selectQuery, null);
            if (cursor.moveToFirst()) {
                do {
                    contacto  = cursor.getString(0);
                    Log.e("ContactoID",String.valueOf(id));
                    Log.e("ContactoNombre",contacto);
                } while (cursor.moveToNext());
            }
            cursor.close();
        } catch (Exception e) {
            System.out.println("Error-GetFullClientes: " + e.toString());
        }
        return contacto;
    }

    ContentValues GetCotizacion(String _id) {
        String selectQuery = "SELECT id,"+ Tb_Cotizaciones.cliente +","+ Tb_Cotizaciones.fecha_registro+","+ Tb_Cotizaciones.monto_total+","+
                Tb_Cotizaciones.moneda+","+ Tb_Cotizaciones.items+","+ Tb_Cotizaciones.codigo+","+ Tb_Cotizaciones.lugar_cotizacion+","+
                Tb_Cotizaciones.fecha+","+ Tb_Cotizaciones.cond_pago+","+ Tb_Cotizaciones.cond_venta+","+ Tb_Cotizaciones.tiempo_validez+","
                + Tb_Cotizaciones.flete+","+ Tb_Cotizaciones.porcentaje+","+ Tb_Cotizaciones.estadoSynco+","+ Tb_Cotizaciones.estado+","+
                Tb_Cotizaciones.subtotal+","+ Tb_Cotizaciones.monto_igv+",cot_www"+","+ Tb_Cotizaciones.lugar+","+ Tb_Cotizaciones.icotem+","+
                Tb_Cotizaciones.estadoSynco+","+Tb_Cotizaciones.lugar_direccion+","+Tb_Cotizaciones.observaciones + ","+Tb_Cotizaciones.nombre + " FROM " + Tb_Cotizaciones.nombreTabla + " WHERE id = " + _id;
        ContentValues map = new ContentValues();
        Cursor cursor = database.rawQuery(selectQuery, null);
        try {
            if (cursor.moveToFirst()) {
                do {
                    map.put("id",cursor.getString(0));
                    ContentValues v = GetCliente(cursor.getString(1).trim());
                    map.put(Tb_Clientes.razon_social,v.get(Tb_Clientes.razon_social).toString());
                    map.put(Tb_Clientes.ruc,v.get(Tb_Clientes.ruc).toString());
                    map.put(Tb_Clientes.contacto,v.get(Tb_Clientes.contacto).toString());
                    map.put(Tb_Clientes.correo,v.get(Tb_Clientes.correo).toString());
                    map.put(Tb_Cotizaciones.fecha, cursor.getString(8));
                    map.put(Tb_Cotizaciones.fecha_registro, cursor.getString(2));
                    map.put(Tb_Cotizaciones.monto_total, cursor.getString(3));
                    map.put(Tb_Cotizaciones.moneda, cursor.getString(4));
                    map.put(Tb_Cotizaciones.items, cursor.getString(5));
                    map.put(Tb_Cotizaciones.codigo, cursor.getString(6));
                    map.put(Tb_Cotizaciones.lugar_cotizacion, cursor.getString(7));
                    map.put(Tb_Cotizaciones.fecha, cursor.getString(8));
                    map.put(Tb_Cotizaciones.cond_pago, cursor.getString(9));
                    map.put(Tb_Cotizaciones.cond_venta, cursor.getString(10));
                    map.put(Tb_Cotizaciones.tiempo_validez, cursor.getString(11));
                    map.put(Tb_Cotizaciones.flete, Float.parseFloat(cursor.getString(12)));
                    map.put(Tb_Cotizaciones.porcentaje, Float.parseFloat(cursor.getString(13)));
                    map.put(Tb_Cotizaciones.estado, Float.parseFloat(cursor.getString(15)));
                    map.put(Tb_Cotizaciones.subtotal, cursor.getString(16));
                    map.put(Tb_Cotizaciones.monto_igv, cursor.getString(17));
                    map.put(Tb_Clientes.tiponegocio,v.get(Tb_Clientes.tiponegocio).toString());
                    map.put(Tb_Clientes.tipoExtaNa,v.get(Tb_Clientes.tipoExtaNa).toString());
                    map.put(Tb_Cotizaciones.estadoSynco,cursor.getString(21));
                    map.put("cot_www",cursor.getString(18));
                    map.put(Tb_Cotizaciones.lugar,cursor.getString(19));
                    map.put(Tb_Cotizaciones.icotem,cursor.getString(20));
                    map.put(Tb_Clientes.por_perceptor, v.get(Tb_Clientes.por_perceptor).toString());
                    map.put(Tb_Cotizaciones.lugar_direccion,cursor.getString(22));
                    map.put(Tb_Cotizaciones.observaciones,cursor.getString(23));
                } while (cursor.moveToNext());
            }
        } catch (Exception e) {
            System.out.println("Error-GetCotizacion(): " + e.toString());
        }finally {
            cursor.close();
        }
        return map.size()>0 ? map : null;
    }

    String  GetNombreProducto(String _c) {
        String selectQuery = "SELECT " + Tb_Productos.nombre + " FROM " + Tb_Productos.nombreTabla + " WHERE " + Tb_Productos.codigo + "='" + _c + "'";
        Cursor cursor = database.rawQuery(selectQuery, null);
        try {
            if (cursor.moveToFirst()) {
                do {
                    return cursor.getString(0);
                } while (cursor.moveToNext());
            }
        } catch (Exception e) {
            System.out.println("Error-GetProducto: " + e.toString());
        } finally {
            cursor.close();
        }
        return "";
    }

    public void BorradaCotizacion(){
        String selectQuery = "SELECT id FROM " + Tb_Cotizaciones.nombreTabla + " WHERE " + Tb_Cotizaciones.estadoSynco + "=1 ";
        Cursor cursor = database.rawQuery(selectQuery, null);
        try {
            if (cursor.moveToFirst()) {
                do {
                    database.execSQL("DELETE FROM  "+ Tb_cotizacion_productos.nombreTabla + " WHERE " + Tb_cotizacion_productos.cotizacion + " = " + cursor.getInt(0));
                    database.execSQL("DELETE FROM  "+ Tb_Cotizaciones.nombreTabla + " WHERE id ="+cursor.getInt(0));
                } while (cursor.moveToNext());
            }
        } catch (Exception e) {
            System.out.println("Error-GetProducto: " + e.toString());
        } finally {
            cursor.close();
        }
    }
    //DateFormat inputFormat = new SimpleDateFormat("yyyy-MM-dd");
    //DateFormat outputFormat = new SimpleDateFormat("dd/MM/yyyy");
    //Date date;
    public void  capturaCabezeraCotizacionV2(JSONObject datos){
        ContentValues d = new ContentValues();
        try {
            //Log.i("ID SINCRONIZADO", cotizaciones.get("id").toString());
            d.put("id", datos.get("id").toString());
            d.put(Tb_Cotizaciones.codigo, datos.get("codigo").toString());
            d.put(Tb_Cotizaciones.fecha, datos.get("fecha").toString());
            d.put(Tb_Cotizaciones.fecha_registro,datos.get("fecha_registro").toString());
            d.put(Tb_Cotizaciones.nombre,datos.get("cliente").toString());
            String ruc = datos.get("ruc").toString();
            d.put(Tb_Cotizaciones.cliente,ruc);
            d.put(Tb_Cotizaciones.vendedor, datos.get("vendedor").toString()); //BUSCAR EN USUARIO
            d.put(Tb_Cotizaciones.cond_pago, datos.get("cond_pago").toString());
            d.put(Tb_Cotizaciones.cond_venta,datos.get("cond_venta").toString());
            d.put(Tb_Cotizaciones.flete, datos.get("flete").toString());
            d.put(Tb_Cotizaciones.tiempo_validez, datos.get("tiempo_validez").toString());
            d.put(Tb_Cotizaciones.lugar_cotizacion, datos.get("lugar_cotizacion").toString());
            d.put(Tb_Cotizaciones.moneda, datos.get("moneda").toString() );
            d.put(Tb_Cotizaciones.observaciones,  datos.get("observaciones").toString());
            d.put(Tb_Cotizaciones.por_revision,  datos.get("por_revision").toString());
            d.put(Tb_Cotizaciones.monto_total,  datos.get("monto_total").toString());
            d.put(Tb_Cotizaciones.subtotal, datos.get("subtotal").toString());
            d.put(Tb_Cotizaciones.monto_igv,datos.get("monto_igv").toString());
            d.put(Tb_Cotizaciones.lugar,datos.get("lugar_entrega").toString());
            d.put(Tb_Cotizaciones.estadoSynco, "1");
            d.put(Tb_Cotizaciones.estado,  datos.get("estado").toString());
            d.put(Tb_Cotizaciones.lugar_direccion,datos.get("lugar_direccion").toString());

            /*JSONArray contactos = datos.getJSONArray("contacto");
            if (contactos.length() > 0) {
                for (int i = 0; i < contactos.length(); i++) {
                    JSONObject contactoObject = contactos.getJSONObject(i);

                    AllContacts contact = new Gson().fromJson(contactoObject.toString(), AllContacts.class);
                    saveClienteContact(ruc,contact,1);
                }
            }*/

            d.put("cot_www","1");
            d.put(Tb_Cotizaciones.porcentaje,  datos.get("monto_percepcion").toString());
            JSONArray productos = datos.getJSONArray("productos");
            d.put(Tb_Cotizaciones.items, productos.length());
            d.put("pdf", datos.get("enviada").toString());

            capturaItemsJSONArray(productos,datos.get("id").toString());
            InsertCotizacion(d);
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    public void  capturaCabezeraCotizacion(JSONObject datos){
        ContentValues d = new ContentValues();
        try {
            JSONObject cotizaciones = datos.getJSONObject("cotizacion");
            //Log.i("ID SINCRONIZADO", cotizaciones.get("id").toString());
            d.put("id", cotizaciones.get("id").toString());
            d.put(Tb_Cotizaciones.codigo, cotizaciones.get("codigo").toString());
            d.put(Tb_Cotizaciones.fecha, cotizaciones.get("fecha").toString());
            d.put(Tb_Cotizaciones.fecha_registro,cotizaciones.get("fecha_registro").toString());
            d.put(Tb_Cotizaciones.nombre,cotizaciones.get("cliente").toString());
            d.put(Tb_Cotizaciones.cliente,cotizaciones.get("ruc").toString());
            d.put(Tb_Cotizaciones.vendedor, cotizaciones.get("vendedor_id").toString()); //BUSCAR EN USUARIO
            d.put(Tb_Cotizaciones.cond_pago, cotizaciones.get("cond_pago").toString());
            d.put(Tb_Cotizaciones.cond_venta,cotizaciones.get("cond_venta").toString());
            d.put(Tb_Cotizaciones.flete, cotizaciones.get("flete").toString());
            d.put(Tb_Cotizaciones.tiempo_validez, cotizaciones.get("tiempo_validez").toString());
            d.put(Tb_Cotizaciones.lugar_cotizacion, cotizaciones.get("lugar_cotizacion").toString());
            d.put(Tb_Cotizaciones.moneda, cotizaciones.get("moneda").toString() );
            d.put(Tb_Cotizaciones.observaciones,  cotizaciones.get("observaciones").toString());
            d.put(Tb_Cotizaciones.por_revision,  cotizaciones.get("por_revision").toString());
            d.put(Tb_Cotizaciones.monto_total,  cotizaciones.get("monto_total").toString());
            d.put(Tb_Cotizaciones.subtotal, cotizaciones.get("subtotal").toString());
            d.put(Tb_Cotizaciones.monto_igv,cotizaciones.get("monto_igv").toString());
            d.put(Tb_Cotizaciones.lugar,cotizaciones.get("lugar_entrega").toString());
            d.put(Tb_Cotizaciones.estadoSynco, "1");
            d.put(Tb_Cotizaciones.estado,  cotizaciones.get("estado").toString());
            d.put("cot_www","1");
            d.put(Tb_Cotizaciones.porcentaje,  cotizaciones.get("monto_percepcion").toString());
            JSONArray productos = datos.getJSONArray("productos");
            d.put(Tb_Cotizaciones.items, productos.length());
            d.put("pdf", cotizaciones.get("enviada").toString());
            d.put(Tb_Cotizaciones.lugar_direccion,cotizaciones.get("lugar_direccion").toString());

            capturaItemsJSONArray(productos,cotizaciones.get("id").toString());
            InsertCotizacion(d);
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    private void capturaItemsJSONArray(JSONArray d,String id){
        try {
            for (int i =0;i<d.length();i++) {
                capturaItemsCotizacion(d.getJSONObject(i), id);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void capturaItemsCotizacion(JSONObject data, String id){
        ContentValues datos = new ContentValues();
        datos.put(Tb_cotizacion_productos.cotizacion,id);
        try {
            datos.put(Tb_cotizacion_productos.producto,data.get("codigo").toString().trim());
            datos.put(Tb_cotizacion_productos.producto_nombre,data.get("descripcion").toString());
            datos.put(Tb_cotizacion_productos.cantidad,data.get("cantidad").toString());
            datos.put(Tb_cotizacion_productos.subtotal,data.get("subtotal").toString());
            datos.put(Tb_cotizacion_productos.igv,data.get("igv").toString());
            datos.put(Tb_cotizacion_productos.precio,data.get("precio").toString());
            datos.put(Tb_cotizacion_productos.total,data.get("total").toString());
            datos.put(Tb_cotizacion_productos.cunio,data.get("cunio").toString());
            datos.put(Tb_cotizacion_productos.produccion,data.get("produccion").toString());
            datos.put(Tb_cotizacion_productos.action,"3");
            datos.put(Tb_cotizacion_productos.idproduc,data.get("id").toString());
            System.out.println("IDIDDIDID: " + data.get("id").toString());
            //datos.put(Tb_cotizacion_productos.requerido,data.get("requerido").toString());

            InsertCotizacionProducto(datos);
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    ArrayList<ContentValues> GetItemsCotizacion(String _id,boolean extrangero, int moneda) {
        String selectQuery = "SELECT "+ Tb_cotizacion_productos.producto+","+ Tb_cotizacion_productos.cantidad+","+ Tb_cotizacion_productos.precio+","+
                Tb_cotizacion_productos.produccion+","+ Tb_cotizacion_productos.cunio+","+ Tb_cotizacion_productos.subtotal+","+ Tb_cotizacion_productos.igv+
                ","+ Tb_cotizacion_productos.total+","+ Tb_cotizacion_productos.requerido+","+ Tb_cotizacion_productos.cotizacion +
                ","+ Tb_cotizacion_productos.action +","+ Tb_cotizacion_productos.idproduc +","+ Tb_cotizacion_productos.producto_nombre + " FROM " + Tb_cotizacion_productos.nombreTabla
                + " WHERE " + Tb_cotizacion_productos.cotizacion + " = " + _id + " AND (" + Tb_cotizacion_productos.action+ " = 1 OR " + Tb_cotizacion_productos.action+  " = 2  OR " + Tb_cotizacion_productos.action+  " = 3)";
        ArrayList<ContentValues> coti = new ArrayList<>();
        Cursor cursor = database.rawQuery(selectQuery, null);
        float valor_soles = Inicio.conversorMonedas;
        try {
            if (cursor.moveToFirst()) {
                do {
                    ContentValues map = new ContentValues();
                    map.put(Tb_cotizacion_productos.producto,cursor.getString(0));
                    map.put(Tb_cotizacion_productos.producto_nombre,cursor.getString(12));
                    map.put(Tb_cotizacion_productos.cantidad, cursor.getString(1));
                    map.put(Tb_cotizacion_productos.precio, cursor.getString(2));
                    map.put("por", cursor.getString(3).compareTo("1")==0?"Por producción:":cursor.getString(3).compareTo("2")==0?"Por Req.":"Por Stock:");
                    map.put(Tb_cotizacion_productos.produccion +"print","SI");
                    map.put(Tb_cotizacion_productos.produccion,cursor.getString(3));
                    map.put(Tb_cotizacion_productos.cunio +"print", cursor.getString(4).compareTo("1")==0?"SI":"NO");
                    map.put(Tb_cotizacion_productos.cunio, cursor.getString(4));
                    float val1 = 50.15f;
                    float val2 = 42.50f;

                    if(moneda==1){
                        val1 = val1*valor_soles;
                        val2 = val2*valor_soles;
                    }

                    float valorCunio =   cursor.getString(4).compareTo("1")==0?(extrangero?val1:val2):0f;
                    float precio =Float.parseFloat(cursor.getString(2));
                    float cantidad = Float.parseFloat(cursor.getString(1));
                    float subtotal = (precio*cantidad) + valorCunio;
                    float igv = subtotal *(!extrangero?0.18f:0);
                    float total = subtotal + igv;
                    map.put(Tb_cotizacion_productos.subtotal,subtotal);
                    map.put(Tb_cotizacion_productos.igv, igv);
                    map.put(Tb_cotizacion_productos.total, total);
                    map.put(Tb_cotizacion_productos.cotizacion,cursor.getString(9));
                    map.put(Tb_cotizacion_productos.action,cursor.getString(10));
                    map.put(Tb_cotizacion_productos.idproduc,cursor.getString(11));
                    coti.add(map);
                } while (cursor.moveToNext());
            }
        } catch (Exception e) {
            System.out.println("Error-GetItemsCotizacion(): " + e.toString());
        }finally {
            cursor.close();
        }
        return coti;
    }

    ArrayList<ContentValues> GetItemsCotizacionEdit(String _id,boolean extrangero) {
        String selectQuery = "SELECT "+ Tb_cotizacion_productos.producto+","+ Tb_cotizacion_productos.cantidad+","+ Tb_cotizacion_productos.precio+","+
                Tb_cotizacion_productos.produccion+","+ Tb_cotizacion_productos.cunio+","+ Tb_cotizacion_productos.subtotal+","+ Tb_cotizacion_productos.igv+
                ","+ Tb_cotizacion_productos.total+","+ Tb_cotizacion_productos.requerido+","+ Tb_cotizacion_productos.cotizacion +
                ","+ Tb_cotizacion_productos.action +","+ Tb_cotizacion_productos.idproduc +","+ Tb_cotizacion_productos.producto_nombre + " FROM " + Tb_cotizacion_productos.nombreTabla
                + " WHERE " + Tb_cotizacion_productos.cotizacion + " = " + _id + " AND (" + Tb_cotizacion_productos.action+ " = 1 OR " + Tb_cotizacion_productos.action+  " = 2  OR " + Tb_cotizacion_productos.action+  " = 3)";
        Log.i("SELECTEQUERY",selectQuery);
        ArrayList<ContentValues> coti = new ArrayList<>();
        Cursor cursor = database.rawQuery(selectQuery, null);
        try {
            if (cursor.moveToFirst()) {
                do {
                    ContentValues map = new ContentValues();
                    map.put(Tb_cotizacion_productos.producto,cursor.getString(0));
                    map.put(Tb_cotizacion_productos.producto_nombre,cursor.getString(12));
                    map.put(Tb_cotizacion_productos.cantidad, cursor.getString(1));
                    map.put(Tb_cotizacion_productos.precio, cursor.getString(2));
                    map.put("por", cursor.getString(3).compareTo("1")==0?"Por producción:":cursor.getString(3).compareTo("2")==0?"Por Req.":"Por Stock:");
                    map.put(Tb_cotizacion_productos.produccion +"print","SI");
                    map.put(Tb_cotizacion_productos.produccion,cursor.getString(3));
                    map.put(Tb_cotizacion_productos.cunio +"print", cursor.getString(4).compareTo("1")==0?"SI":"NO");
                    map.put(Tb_cotizacion_productos.cunio, cursor.getString(4));


                    float valorCunio =   cursor.getString(4).compareTo("1")==0?(extrangero?50.15f:42.50f):0f;
                    float precio =Float.parseFloat(cursor.getString(2));
                    float cantidad = Float.parseFloat(cursor.getString(1));
                    float subtotal = (precio*cantidad) + valorCunio;
                    float igv = subtotal *(!extrangero?0.18f:0);
                    float total = subtotal + igv;
                    Log.i("DATOS EDIT", "total: "+total+" subtotal: "+subtotal+" igv: "+igv);
                    map.put(Tb_cotizacion_productos.subtotal,subtotal);
                    map.put(Tb_cotizacion_productos.igv, igv);
                    map.put(Tb_cotizacion_productos.total, total);
                    map.put(Tb_cotizacion_productos.cotizacion,cursor.getString(9));
                    map.put(Tb_cotizacion_productos.action,cursor.getString(10));
                    map.put(Tb_cotizacion_productos.idproduc,cursor.getString(11));
                    coti.add(map);
                } while (cursor.moveToNext());
            }
        } catch (Exception e) {
            System.out.println("Error-GetItemsCotizacion(): " + e.toString());
        }finally {
            cursor.close();
        }
        return coti;
    }


    int GetCantidadTotalCotizacion(String _id) {
        String selectQuery = "SELECT SUM("+ Tb_cotizacion_productos.cantidad+") FROM " + Tb_cotizacion_productos.nombreTabla + " WHERE " + Tb_cotizacion_productos.cotizacion + " = " + _id + " AND (" + Tb_cotizacion_productos.action+ " = 1 OR " + Tb_cotizacion_productos.action+  " = 2  OR " + Tb_cotizacion_productos.action+  " = 3)";
        int ii= 0;
        Cursor cursor = database.rawQuery(selectQuery, null);
        try {
            if (cursor.moveToFirst()) {
                do {
                    ContentValues map = new ContentValues();
                    ii =cursor.getInt(0);
                } while (cursor.moveToNext());
            }
        } catch (Exception e) {
            System.out.println("Error-GetItemsCotizacion(): " + e.toString());
        }finally {
            cursor.close();
        }
        return ii;
    }
    //USUARIOS
    void CapturaDatosUsuarioJSON(JSONObject objeto,String p){
        try {
            BorradaGeneral(Tb_Usuarios.tb_nombreTabla,"" );
            ContentValues valores = new ContentValues();
            valores.put(Tb_Usuarios.tb_codigo,objeto.getString("id"));
            valores.put(Tb_Usuarios.tb_nombre,objeto.getString("nombres"));
            valores.put(Tb_Usuarios.tb_apellidos,objeto.getString("apellidos"));
            valores.put(Tb_Usuarios.tb_tipoUsuario,objeto.getString("tipo"));
            valores.put(Tb_Usuarios.tb_correo,objeto.getString("correo").toLowerCase());
            valores.put(Tb_Usuarios.tb_pw,p);
            valores.put(Tb_Usuarios.tb_telefijo,objeto.getString("telefono_movil"));
            valores.put(Tb_Usuarios.tb_telemovil,objeto.getString("telefono_fijo"));
            InsertUsuarios(valores);
        }catch (JSONException e) {
            e.printStackTrace();
        }
    }

    void InsertUsuarios(ContentValues valores){
        try {
            database.insert(Tb_Usuarios.tb_nombreTabla, null, valores);
        } catch (Exception e) {
            System.out.println("Error-InsertUsuarios: " + e.toString());
        }
    }

    void allUser(){
        String selectQuery = "SELECT "+ Tb_Usuarios.tb_correo+" FROM " + Tb_Usuarios.tb_nombreTabla;
        Cursor cursor = database.rawQuery(selectQuery, null);
        try {
            if (cursor.moveToFirst()) {
                do {cursor.close();
                    Log.i("correo", cursor.getString(0));
                } while (cursor.moveToNext());
            }
        } catch (Exception e) {
            System.out.println("Error-GetUsuarios: " + e.toString());
        }
    }

    Boolean loginOffline(String _u,String _p){
        String selectQuery = "SELECT "+ Tb_Usuarios.tb_telemovil+" FROM " + Tb_Usuarios.tb_nombreTabla + " WHERE " + Tb_Usuarios.tb_pw +" = '"+_p+"' AND " + Tb_Usuarios.tb_correo +" = '"+_u+"'";
        Cursor cursor = database.rawQuery(selectQuery, null);
        try {
            if (cursor.moveToFirst()) {
                do {cursor.close();
                    return true;
                } while (cursor.moveToNext());
            }
        } catch (Exception e) {
            System.out.println("Error-GetUsuarios: " + e.toString());
        }
        return false;
    }

    void Update(String pass){
        String selectQuery = "UPDATE "+ Tb_Usuarios.tb_nombreTabla+ " SET "+ Tb_Usuarios.tb_pw+" = '"+pass+"', "+ Tb_Usuarios.tb_estado + " = '0'";
        database.execSQL(selectQuery);
    }
    void UpdateEstado(){
        String selectQuery = "UPDATE "+ Tb_Usuarios.tb_nombreTabla+ " WHERE " + Tb_Usuarios.tb_estado + " = '1'";
        database.execSQL(selectQuery);
    }

    ContentValues GetUsuarios() {
        String selectQuery = "SELECT "+ Tb_Usuarios.tb_codigo+","+ Tb_Usuarios.tb_nombre+","+ Tb_Usuarios.tb_apellidos+","+ Tb_Usuarios.tb_tipoUsuario+","+
                Tb_Usuarios.tb_correo+","+ Tb_Usuarios.tb_telefijo+","+ Tb_Usuarios.tb_pw+","+ Tb_Usuarios.tb_telemovil+" FROM " + Tb_Usuarios.tb_nombreTabla;
        ContentValues producto = new ContentValues();
        Cursor cursor = database.rawQuery(selectQuery, null);
        try {
            if (cursor.moveToFirst()) {
                do {
                    producto.put(Tb_Usuarios.tb_codigo,cursor.getString(0));
                    producto.put(Tb_Usuarios.tb_nombre, cursor.getString(1));
                    producto.put(Tb_Usuarios.tb_apellidos,cursor.getString(2));
                    producto.put(Tb_Usuarios.tb_tipoUsuario, cursor.getString(3));
                    producto.put(Tb_Usuarios.tb_correo, cursor.getString(4));
                    producto.put(Tb_Usuarios.tb_telefijo, cursor.getString(5));
                    producto.put(Tb_Usuarios.tb_pw, cursor.getString(6));
                    producto.put(Tb_Usuarios.tb_telemovil, cursor.getString(7));

                } while (cursor.moveToNext());
            }
        } catch (Exception e) {
            System.out.println("Error-GetUsuarios: " + e.toString());
        }
        cursor.close();
        return producto;
    }

    String GetCodigoUsuarios() {
        String selectQuery = "SELECT "+ Tb_Usuarios.tb_codigo+" FROM " + Tb_Usuarios.tb_nombreTabla;
        ContentValues producto = new ContentValues();
        Cursor cursor = database.rawQuery(selectQuery, null);
        try {
            if (cursor.moveToFirst()) {
                do {
                    return cursor.getString(0);

                } while (cursor.moveToNext());
            }
        } catch (Exception e) {
            System.out.println("Error-GetUsuarios: " + e.toString());
        }
        cursor.close();
        return "";
    }

    public void BorradaGeneral(String _c, String ex){
        String selectQuery = "DELETE FROM " + _c + ex;
        database.execSQL(selectQuery);
    }

    private String SepararParlabras(String filtro,String campo){
        StringTokenizer st =new StringTokenizer(filtro);
        String temporan = "";
        //int i = 0;

        while(st.hasMoreElements()){
            if(!temporan.isEmpty()){
                temporan = temporan + " AND ";
            }

            String token = campo+" LIKE '%"+st.nextElement().toString()+"%'";

            temporan = temporan + token;

            System.out.println("SepararParlabras " + temporan);
        }

        return temporan;
    }


    void InsertVisitas(ContentValues valores){
        try {
            database.insert(Tb_Visita.nombreTabla, null, valores);
        } catch (Exception e) {
            System.out.println("Error-InsertVisitas: " + e.toString());
        }
    }

    void UpdateEstadoVisita(String id,String idCodigo){
        String selectQuery;
        if(idCodigo.isEmpty()){
            selectQuery = "DELETE FROM "+ Tb_Visita.nombreTabla+ " WHERE id = " + id+"";
        }else{
            selectQuery = "UPDATE "+ Tb_Visita.nombreTabla+ " SET "+ Tb_Visita.estado+" = '1', "+ Tb_Visita.codigo+" = '"+idCodigo+"' WHERE id = " + id + "";
        }
        database.execSQL(selectQuery);
    }

    void BorrarVisitasViejas(String id,String idCodigo){
        String selectQuery = "DELETE FROM "+ Tb_Visita.nombreTabla+ " WHERE id = " + id+" ";
        database.execSQL(selectQuery);
    }

    boolean CheckDataSyncVisitas(){
        String selectQuery = "SELECT * FROM " + Tb_Visita.nombreTabla  + " WHERE " + Tb_Visita.estado +"= '0' OR "+ Tb_Visita.estado +"= '2'" ;
        Cursor cursor = database.rawQuery(selectQuery, null);
        if (cursor.getCount()>0) {
            System.out.println("mayor visita: >0");
            return true;
        }

        return false;
    }

    ContentValues GetVisita(String ci) {
        String selectQuery = "SELECT "+ Tb_Visita.codigo+","+ Tb_Visita.fecha_proxima+","+ Tb_Visita.fecha_inicio+" FROM "
                + Tb_Visita.nombreTabla + " WHERE ("+ Tb_Visita.pendiente + " = '2' OR "+ Tb_Visita.pendiente + " = '3') AND " + Tb_Visita.cliente +
                " = '"+ci.trim()+"'";
        ContentValues producto = null;
        Cursor cursor = database.rawQuery(selectQuery, null);
        try {
            if (cursor.moveToFirst()) {
                do {
                    if(cursor.getString(2).isEmpty()){
                        producto = new ContentValues();
                        producto.put(Tb_Visita.codigo,cursor.getString(0));
                        producto.put(Tb_Visita.fecha_proxima, cursor.getString(1));
                    }
                } while (cursor.moveToNext());
            }
        } catch (Exception e) {
            System.out.println("Error-GetVisita: " + e.toString());
        }
        cursor.close();
        return producto;
    }

    public ContentValues getFullVisita(String cliente){
        String[] args = new String[]{cliente};
        Cursor cursor = databaseReadOnly.query(Tb_Visita.nombreTabla,new String[]{"*"},"("+ Tb_Visita.pendiente + " = '2' OR "+ Tb_Visita.pendiente + " = '3') AND " + Tb_Visita.cliente + " = ?",args,null,null,null);
        ContentValues visita = null;
        try {
            if (cursor.moveToFirst()) {
                do {
                    visita = new ContentValues();
                    visita.put(Tb_Visita.codigo,cursor.getString(cursor.getColumnIndexOrThrow(Tb_Visita.codigo)));
                    visita.put(Tb_Visita.cliente,cursor.getString(cursor.getColumnIndexOrThrow(Tb_Visita.cliente)));
                    visita.put(Tb_Visita.direccion,cursor.getString(cursor.getColumnIndexOrThrow(Tb_Visita.direccion)));

                    visita.put(Tb_Visita.latitud_final,cursor.getString(cursor.getColumnIndexOrThrow(Tb_Visita.latitud_final)));
                    visita.put(Tb_Visita.latitud_inicio,cursor.getString(cursor.getColumnIndexOrThrow(Tb_Visita.latitud_inicio)));
                    visita.put(Tb_Visita.longitud_final,cursor.getString(cursor.getColumnIndexOrThrow(Tb_Visita.longitud_final)));
                    visita.put(Tb_Visita.longitud_inicio,cursor.getString(cursor.getColumnIndexOrThrow(Tb_Visita.longitud_inicio)));

                    visita.put(Tb_Visita.fecha_inicio,cursor.getString(cursor.getColumnIndexOrThrow(Tb_Visita.fecha_inicio)));
                    visita.put(Tb_Visita.fecha_final,cursor.getString(cursor.getColumnIndexOrThrow(Tb_Visita.fecha_final)));
                    visita.put(Tb_Visita.fecha_proxima,cursor.getString(cursor.getColumnIndexOrThrow(Tb_Visita.fecha_proxima)));
                    visita.put(Tb_Visita.fecha_paraEnviar,cursor.getString(cursor.getColumnIndexOrThrow(Tb_Visita.fecha_paraEnviar)));

                    visita.put(Tb_Visita.motivo,cursor.getString(cursor.getColumnIndexOrThrow(Tb_Visita.motivo)));
                    visita.put(Tb_Visita.pendiente,cursor.getString(cursor.getColumnIndexOrThrow(Tb_Visita.pendiente)));
                    visita.put(Tb_Visita.estado,cursor.getString(cursor.getColumnIndexOrThrow(Tb_Visita.estado)));
                } while (cursor.moveToNext());
            }
        } catch (Exception e) {
            System.out.println("Error-GetFullVisita: " + e.toString());
        }
        cursor.close();
        return visita;
    }

    void UpdateVisitaCliente(ContentValues valores, String _c){
        try {
            database.update(Tb_Visita.nombreTabla,valores, Tb_Visita.cliente + "= '"+_c+"'",null);
        } catch (Exception e) {
            System.out.println("Error-UpdateVisitaCliente: " + e.toString());
        }
    }

    public void saveClienteContact(String ruc, ArrayList<AllContacts> list, int estado){
        for (AllContacts contact : list) {
            saveClienteContact(ruc,contact,estado);
            /*params.put("id",contact.getItem());
            params.put(Tb_contactos.cliente,ruc);
            params.put(Tb_contactos.tipo,contact.getTipo());
            params.put(Tb_contactos.descripcion,contact.getDescripcion());
            params.put(Tb_contactos.contacto,contact.getContacto());
            params.put(Tb_contactos.direccion,contact.getDireccion());
            params.put(Tb_contactos.correo,contact.getCorreo());
            params.put(Tb_contactos.telefonoFijo,contact.getTelefonoFijo());
            params.put(Tb_contactos.telefonoMovil,contact.getTelefonoMovil());
            params.put(Tb_contactos.zona,contact.getZona());
            params.put(Tb_contactos.vendedor,contact.getVendedor());
            params.put(Tb_contactos.condVenta,contact.getCondVenta());
            params.put(Tb_contactos.condPago,contact.getCondPago());
            params.put(Tb_contactos.estado,0);

            database.insert(Tb_contactos.nombreTabla,null,params);*/
        }
    }

    public void saveClienteContact(String ruc, AllContacts contact, int estado){
        ContentValues params = new ContentValues();

        params.put("id",contact.getItem());
        params.put(Tb_contactos.cliente,ruc);
        params.put(Tb_contactos.tipo,contact.getTipo());
        params.put(Tb_contactos.descripcion,contact.getDescripcion());
        params.put(Tb_contactos.contacto,contact.getContacto());
        params.put(Tb_contactos.direccion,contact.getDireccion());
        params.put(Tb_contactos.correo,contact.getCorreo());
        params.put(Tb_contactos.telefonoFijo,contact.getTelefonoFijo());
        params.put(Tb_contactos.telefonoMovil,contact.getTelefonoMovil());
        params.put(Tb_contactos.zona,contact.getZona());
        params.put(Tb_contactos.vendedor,contact.getVendedor());
        params.put(Tb_contactos.condVenta,contact.getCondVenta());
        params.put(Tb_contactos.condPago,contact.getCondPago());
        params.put(Tb_contactos.estado,estado);

        database.insert(Tb_contactos.nombreTabla,null,params);
    }

    public void updateClienteContact(String ruc, AllContacts contact, int estado){
        ContentValues params = new ContentValues();

        params.put("id",contact.getItem());
        params.put(Tb_contactos.cliente,ruc);
        params.put(Tb_contactos.tipo,contact.getTipo());
        params.put(Tb_contactos.descripcion,contact.getDescripcion());
        params.put(Tb_contactos.contacto,contact.getContacto());
        params.put(Tb_contactos.direccion,contact.getDireccion());
        params.put(Tb_contactos.correo,contact.getCorreo());
        params.put(Tb_contactos.telefonoFijo,contact.getTelefonoFijo());
        params.put(Tb_contactos.telefonoMovil,contact.getTelefonoMovil());
        params.put(Tb_contactos.zona,contact.getZona());
        params.put(Tb_contactos.vendedor,contact.getVendedor());
        params.put(Tb_contactos.condVenta,contact.getCondVenta());
        params.put(Tb_contactos.condPago,contact.getCondPago());
        params.put(Tb_contactos.estado,estado);

        database.update(Tb_contactos.nombreTabla,params,"id = " + contact.getItem(),null);
    }

    public ArrayList<AllContacts> getContactsByRUC(String ruc, String query){

        ArrayList<AllContacts> list = new ArrayList<>();
        Cursor cursor = databaseReadOnly.rawQuery(query, null);
        try {
            if (cursor.moveToFirst()) {
                do {
                    int id = cursor.getInt(cursor.getColumnIndexOrThrow("id"));
                    String cliente = cursor.getString(cursor.getColumnIndexOrThrow(Tb_contactos.cliente));
                    int tipo = cursor.getInt(cursor.getColumnIndexOrThrow(Tb_contactos.tipo));
                    String descripcion = cursor.getString(cursor.getColumnIndexOrThrow(Tb_contactos.descripcion));
                    String contacto = cursor.getString(cursor.getColumnIndexOrThrow(Tb_contactos.contacto));
                    String direccion = cursor.getString(cursor.getColumnIndexOrThrow(Tb_contactos.direccion));
                    String correo = cursor.getString(cursor.getColumnIndexOrThrow(Tb_contactos.correo));
                    String telFijo = cursor.getString(cursor.getColumnIndexOrThrow(Tb_contactos.telefonoFijo));
                    String telMovil = cursor.getString(cursor.getColumnIndexOrThrow(Tb_contactos.telefonoMovil));
                    int zona = cursor.getInt(cursor.getColumnIndexOrThrow(Tb_contactos.zona));
                    int vendedor = cursor.getInt(cursor.getColumnIndexOrThrow(Tb_contactos.vendedor));
                    int condVenta = cursor.getInt(cursor.getColumnIndexOrThrow(Tb_contactos.condVenta));
                    int condPago = cursor.getInt(cursor.getColumnIndexOrThrow(Tb_contactos.condPago));
                    int estado = cursor.getInt(cursor.getColumnIndexOrThrow(Tb_contactos.estado));

                    AllContacts contacts = new AllContacts(
                            id,id,
                            tipo,
                            descripcion,
                            contacto,
                            direccion,
                            correo,
                            telFijo,
                            telMovil,
                            zona, "",
                            vendedor, "",
                            condVenta, "",
                            condPago, ""
                    );

                    list.add(contacts);
                } while (cursor.moveToNext());
                cursor.close();
            }
        } catch (Exception e) {
            System.out.println("Error-GetContact: " + e.toString());
        }finally {
            cursor.close();
        }

        return list;
    }

    public ArrayList<AllContacts> getContactsByRUC(String ruc){
        String selectQuery = "SELECT * FROM "
                + Tb_contactos.nombreTabla + " WHERE ("+Tb_contactos.cliente + " = '" + ruc + "')";

        return getContactsByRUC(ruc,selectQuery);
    }

    public void saveClientePedido(String ruc, ArrayList<ModelsPedidos> list){
        ContentValues params = new ContentValues();

        for (ModelsPedidos pedidos : list) {
            params.put(Tb_pedidos.pedido,pedidos.getPedido());
            params.put(Tb_pedidos.fecha,pedidos.getFecha());
            params.put(Tb_pedidos.tipoPedido,pedidos.getTipoPedido());
            params.put(Tb_pedidos.tipoPago,pedidos.getTipoPago());
            params.put(Tb_pedidos.cliente,pedidos.getCliente());
            params.put(Tb_pedidos.ruc,ruc);
            params.put(Tb_pedidos.direccion ,pedidos.getDireccion());
            params.put(Tb_pedidos.montoNeto ,pedidos.getMontoNeto());
            params.put(Tb_pedidos.montoIgv,pedidos.getMontoIgv());
            params.put(Tb_pedidos.montoTotal,pedidos.getMontoTotal());
            params.put(Tb_pedidos.tipoMoneda,pedidos.getTipoMoneda());
            params.put(Tb_pedidos.percepcion,pedidos.getPercepcion());

            database.insert(Tb_pedidos.nombreTabla,null,params);
        }
    }

    public ArrayList<ModelsPedidos> getPedidosByRUC(String ruc){
        String selectQuery = "SELECT * FROM "
                + Tb_pedidos.nombreTabla + " WHERE ("+Tb_pedidos.ruc + " = '" + ruc + "')";

        ArrayList<ModelsPedidos> list = new ArrayList<>();

        Cursor cursor = databaseReadOnly.rawQuery(selectQuery, null);
        try {
            if (cursor.moveToFirst()) {
                do {
                    int pedido = cursor.getInt(cursor.getColumnIndexOrThrow(Tb_pedidos.pedido));
                    String fecha = cursor.getString(cursor.getColumnIndexOrThrow(Tb_pedidos.fecha));
                    String tipoPedido = cursor.getString(cursor.getColumnIndexOrThrow(Tb_pedidos.tipoPedido));
                    String cliente = cursor.getString(cursor.getColumnIndexOrThrow(Tb_pedidos.cliente));
                    String mRuc = cursor.getString(cursor.getColumnIndexOrThrow(Tb_pedidos.ruc));
                    String direccion = cursor.getString(cursor.getColumnIndexOrThrow(Tb_pedidos.direccion));
                    String montoNeto = cursor.getString(cursor.getColumnIndexOrThrow(Tb_pedidos.montoNeto));
                    String igv = cursor.getString(cursor.getColumnIndexOrThrow(Tb_pedidos.montoIgv));
                    String total = cursor.getString(cursor.getColumnIndexOrThrow(Tb_pedidos.montoTotal));
                    String moneda = cursor.getString(cursor.getColumnIndexOrThrow(Tb_pedidos.tipoMoneda));
                    int percepcion = cursor.getInt(cursor.getColumnIndexOrThrow(Tb_pedidos.percepcion));

                    ModelsPedidos pedidos = new ModelsPedidos(
                            pedido,
                            fecha,
                            tipoPedido,
                            cliente,
                            mRuc,
                            direccion,
                            montoNeto,
                            igv,
                            total,
                            moneda,
                            percepcion
                    );

                    list.add(pedidos);
                } while (cursor.moveToNext());
                cursor.close();
            }
        } catch (Exception e) {
            System.out.println("Error-GetContact: " + e.toString());
            cursor.close();
        }

        return list;

    }

    public void saveSituacionCliente(String ruc, ArrayList<ModelsSituacion> list){
        ContentValues params = new ContentValues();

        for (ModelsSituacion situacion : list) {
            params.put(Tb_situacion.fechaDoc,situacion.getFechaDocmto());
            params.put(Tb_situacion.docmto,situacion.getDocmto());
            params.put(Tb_situacion.vcSerie,situacion.getVcSerie());
            params.put(Tb_situacion.vcNroDoc,situacion.getVcNrodocumento());
            params.put(Tb_situacion.codVta,situacion.getCODVTA());
            params.put(Tb_situacion.fechaVenc,situacion.getFechaVecnto());
            params.put(Tb_situacion.ubicacion,situacion.getUbicacion());
            params.put(Tb_situacion.situacion,situacion.getSituacion());
            params.put(Tb_situacion.mnd,situacion.getMnd());
            params.put(Tb_situacion.totalDeuda,situacion.getTotalDeuda());
            params.put(Tb_situacion.amortizado,situacion.getAmortizado());
            params.put(Tb_situacion.saldo,situacion.getSaldo());
            params.put(Tb_situacion.vendedor,situacion.getVENDEDOR());
            params.put(Tb_situacion.clave,situacion.getSituacion());
            params.put(Tb_situacion.ccbCordCom,situacion.getCCBCORDCOM());
            params.put(Tb_situacion.TD,situacion.getTD());
            params.put(Tb_situacion.Documento,situacion.getDOCUMENTO());
            params.put(Tb_situacion.vcDescripcion,situacion.getVCDescrip());
            params.put(Tb_situacion.vcRuc,ruc);

            database.insert(Tb_situacion.nombreTabla,null,params);
        }
    }

    public ArrayList<ModelsSituacion> getSituacionsCliente(String ruc){
        String selectQuery = "SELECT * FROM "
                + Tb_situacion.nombreTabla + " WHERE ("+Tb_situacion.vcRuc + " = '" + ruc + "')";

        ArrayList<ModelsSituacion> list = new ArrayList<>();
        Cursor cursor = databaseReadOnly.rawQuery(selectQuery, null);
        try {
            if (cursor.moveToFirst()) {
                do {
                    String fechaDoc = cursor.getString(cursor.getColumnIndexOrThrow(Tb_situacion.fechaDoc));
                    String docmto = cursor.getString(cursor.getColumnIndexOrThrow(Tb_situacion.docmto));
                    String vcSerie = cursor.getString(cursor.getColumnIndexOrThrow(Tb_situacion.vcSerie));
                    String vcNroDoc = cursor.getString(cursor.getColumnIndexOrThrow(Tb_situacion.vcNroDoc));
                    String codVta = cursor.getString(cursor.getColumnIndexOrThrow(Tb_situacion.codVta));
                    String fechaVenc = cursor.getString(cursor.getColumnIndexOrThrow(Tb_situacion.fechaVenc));
                    String ubicacion = cursor.getString(cursor.getColumnIndexOrThrow(Tb_situacion.ubicacion));
                    String situacion = cursor.getString(cursor.getColumnIndexOrThrow(Tb_situacion.situacion));
                    String mnd  = cursor.getString(cursor.getColumnIndexOrThrow(Tb_situacion.mnd));
                    String totalDeuda = cursor.getString(cursor.getColumnIndexOrThrow(Tb_situacion.totalDeuda));
                    String amortizado = cursor.getString(cursor.getColumnIndexOrThrow(Tb_situacion.amortizado));
                    String saldo = cursor.getString(cursor.getColumnIndexOrThrow(Tb_situacion.saldo));
                    String vendedor = cursor.getString(cursor.getColumnIndexOrThrow(Tb_situacion.vendedor));
                    String clave = cursor.getString(cursor.getColumnIndexOrThrow(Tb_situacion.clave));
                    String ccbCordCom = cursor.getString(cursor.getColumnIndexOrThrow(Tb_situacion.ccbCordCom));
                    String TD = cursor.getString(cursor.getColumnIndexOrThrow(Tb_situacion.TD));
                    String Documento = cursor.getString(cursor.getColumnIndexOrThrow(Tb_situacion.Documento));
                    String vcDescripcion = cursor.getString(cursor.getColumnIndexOrThrow(Tb_situacion.vcDescripcion));
                    String vcRuc = cursor.getString(cursor.getColumnIndexOrThrow(Tb_situacion.vcRuc));

                    ModelsSituacion situacion1 = new ModelsSituacion(
                            fechaDoc,
                            docmto,
                            vcSerie,
                            vcNroDoc,
                            codVta,
                            fechaVenc,
                            ubicacion,
                            situacion,
                            mnd,
                            totalDeuda,
                            amortizado,
                            saldo,
                            vendedor,
                            clave,
                            ccbCordCom,
                            TD,
                            Documento,
                            vcDescripcion,
                            vcRuc
                    );

                    list.add(situacion1);
                } while (cursor.moveToNext());
                cursor.close();
            }
        } catch (Exception e) {
            System.out.println("Error-GetContact: " + e.toString());
            cursor.close();
        }

        return list;

    }

    public boolean deleteContactoClientes(String ruc){
        String[] args = {ruc};
        int result = database.delete(Tb_contactos.nombreTabla, Tb_contactos.cliente + " = ?",args);
        return result > 0;
    }

    public boolean deletePedidosClientes(String ruc){
        String[] args = {ruc};
        int result = database.delete(Tb_pedidos.nombreTabla, Tb_pedidos.ruc + " = ?",args);
        return result > 0;
    }

    public boolean deleteSituacionClientes(String ruc){
        String[] args = {ruc};
        int result = database.delete(Tb_situacion.nombreTabla, Tb_situacion.vcRuc + " = ?",args);
        return result > 0;
    }

    public void vaciarSectoresYRubros(){
        database.delete("sectores",null,null);
        database.delete("rubros",null,null);
    }

    public boolean insertSectores(ContentValues values){
        return database.insert("rubros",null,values) > 0;
    }

    public boolean insertRubros(ContentValues values){
        return database.insert("sectores",null,values) > 0;
    }
}

