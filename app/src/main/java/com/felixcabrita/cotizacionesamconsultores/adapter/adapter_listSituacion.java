package com.felixcabrita.cotizacionesamconsultores.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.felixcabrita.cotizacionesamconsultores.R;
import com.felixcabrita.cotizacionesamconsultores.net.model.responses.ModelsSituacion;

import java.util.ArrayList;

/**
 * Created by Joseris on 16/01/2018.
 */

public class adapter_listSituacion extends RecyclerView.Adapter<adapter_listSituacion.ViewHolder>{
    ArrayList<ModelsSituacion> ListSituacion = new ArrayList<>();

    public adapter_listSituacion(ArrayList<ModelsSituacion> listSituacion) {
        ListSituacion = listSituacion;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.items_situacion_actual, parent, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        holder.bindData(ListSituacion.get(position));
    }

    @Override
    public int getItemCount() {
        return ListSituacion.size();
    }
    public static class ViewHolder extends RecyclerView.ViewHolder {
        public View v;
        TextView text_tipodecliente;
        TextView text_razondecliente;
        TextView text_nombrecomercial;
        TextView text_ruc;
        TextView text_rubro;
        TextView text_sector;
        TextView text_contacto;
        TextView text_direccion;
        TextView text_tlf_movil;
        TextView text_fijo;
        TextView text_correo;
        TextView text_nuemro;
        TextView text_nombre_vendedor;


        public ViewHolder(View v) {
            super(v);
            this.v = v;
             text_nuemro = (TextView) v.findViewById(R.id.text_nuemro);
             text_tipodecliente=(TextView) v.findViewById(R.id.text_tipo_cliente);
             text_razondecliente=(TextView) v.findViewById(R.id.text_razon_social);
             text_nombrecomercial=(TextView) v.findViewById(R.id.text_nombre_comercial);
             text_ruc=(TextView) v.findViewById(R.id.text_ruc);
             text_rubro=(TextView) v.findViewById(R.id.text_rubro);
             text_sector=(TextView) v.findViewById(R.id.text_sector);
             text_contacto=(TextView) v.findViewById(R.id.text_contacto);
             text_direccion=(TextView) v.findViewById(R.id.text_direccion);
             text_tlf_movil=(TextView) v.findViewById(R.id.text_telefono_movil_item);
             text_fijo=(TextView) v.findViewById(R.id.text_telefono_fijo_item);
             text_correo=(TextView) v.findViewById(R.id.text_correo_item);
             text_nombre_vendedor=(TextView) v.findViewById(R.id.text_nombre_vendedor);


        }
        public void bindData(ModelsSituacion c) {

             text_tipodecliente.setText(""+c.getFechaDocmto());
             text_razondecliente.setText(c.getDocmto());
             text_nombrecomercial.setText(c.getVcSerie());
             text_nuemro.setText(""+c.getVcNrodocumento());
             text_ruc.setText(c.getCODVTA());
             text_rubro.setText(""+c.getFechaVecnto());
             //text_rubro.setText(""+c.getDOCUMENTO());
             text_sector.setText(""+c.getUbicacion());
             text_contacto.setText(c.getSituacion());
             text_direccion.setText(c.getMnd());
             text_tlf_movil.setText(c.getTotalDeuda());
             text_fijo.setText(c.getAmortizado());
             text_correo.setText(c.getSaldo());
             text_nombre_vendedor.setText(c.getVENDEDOR());

        }
    }

}
