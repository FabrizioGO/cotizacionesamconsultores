package com.felixcabrita.cotizacionesamconsultores.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.felixcabrita.cotizacionesamconsultores.R;
import com.felixcabrita.cotizacionesamconsultores.activity.ActivityHistorialPedidos;

import com.felixcabrita.cotizacionesamconsultores.net.ModelsPedidos;

import java.util.ArrayList;

/**
 * Created by Joseris on 16/01/2018.
 **/

public class adapter_listPedidos extends RecyclerView.Adapter<adapter_listPedidos.ViewHolder>{
    private ArrayList<ModelsPedidos> List_pedidos=new ArrayList<>();
    private Context context;
    public adapter_listPedidos(ArrayList<ModelsPedidos> lista_pedidos, ActivityHistorialPedidos act) {
        List_pedidos=lista_pedidos;
        context=act;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.items_historial_pedidos, parent, false);
        Log.d("Test","--->ADATER"+List_pedidos.size());
        return new adapter_listPedidos.ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.bindData(List_pedidos.get(position),context);
    }

    @Override
    public int getItemCount() {
        return List_pedidos.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        public View v;
        TextView pedido;
        //TextView fecha;
        TextView tipo_pedido;
        TextView cliente;
        TextView ruc;
        TextView monto_igv;
        TextView monto_neto;
        TextView monto_total;
        TextView tipo_moneda;
        TextView percepcion;
        TextView direccion;
        Context context;
        ViewHolder(View v) {
            super(v);
            this.v = v;
            v.setOnClickListener(this);
            pedido = (TextView) v.findViewById(R.id.text_pedido_pedido);
            //fecha = (TextView) v.findViewById(R.id.text_ruc_direccion);
            tipo_pedido = (TextView) v.findViewById(R.id.text_tipo_pago_pedido);
            cliente = (TextView) v.findViewById(R.id.text_cliente_pedido);
            ruc = (TextView) v.findViewById(R.id.text_ruc_direccion);
            monto_igv = (TextView) v.findViewById(R.id.text_monto_igv);
            monto_neto = (TextView) v.findViewById(R.id.text_monto_neto);
            monto_total = (TextView) v.findViewById(R.id.text_monto_total);
            tipo_moneda = (TextView) v.findViewById(R.id.text_tipo_moneda);
            direccion = (TextView) v.findViewById(R.id.text_direccion_pdido);
            percepcion = (TextView) v.findViewById(R.id.text_percepcion);
        }
        void bindData(ModelsPedidos c,final Context context) {
            this.context=context;
            pedido.setText(""+c.getPedido());
            //fecha.setText(""+c.getFecha());
            tipo_pedido.setText(""+c.getTipoPedido());
            cliente.setText(""+c.getCliente());
            ruc.setText(""+c.getRuc());
            monto_igv.setText(""+c.getMontoIgv());
            monto_neto.setText(""+c.getMontoNeto());
            monto_total.setText(""+c.getMontoTotal());
            tipo_moneda.setText(""+c.getTipoMoneda());
            direccion.setText(c.getDireccion());
            percepcion.setText(String.valueOf(c.getPercepcion()));
        }

        @Override
        public void onClick(View v) {
            int pedido = Integer.parseInt(((TextView)v.findViewById(R.id.text_pedido_pedido)).getText().toString());
            ((ActivityHistorialPedidos)context).detallePedido(pedido);
        }
    }
}
