package com.felixcabrita.cotizacionesamconsultores.adapter;

import android.content.Context;
import android.content.DialogInterface;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.felixcabrita.cotizacionesamconsultores.ListaClientes;
import com.felixcabrita.cotizacionesamconsultores.R;
import com.felixcabrita.cotizacionesamconsultores.activity.ActivityListContactos;
import com.felixcabrita.cotizacionesamconsultores.net.model.responses.AllContacts;
import com.felixcabrita.cotizacionesamconsultores.utils.Preferences;

import java.util.ArrayList;

/**
 * Created by Joseris on 09/01/2018.
 */

public class adapter_listContactos extends RecyclerView.Adapter<adapter_listContactos.ViewHolder> {
    public static ArrayList<AllContacts> List_contactos=new ArrayList<>();
    Context context;
    public adapter_listContactos(ArrayList<AllContacts> lista_contactos, Context contextt) {
           List_contactos=lista_contactos;
           this.context=contextt;
    }


    public static class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        TextView nombre;
        TextView direccion;
        TextView telefono_fijo;
        TextView telefono_movil;
        TextView correo_electronico;
        TextView condicion_venta;
        TextView condicion_pago;
        LinearLayout id_linear_items;
        TextView text_descriocion;
        Context context;
        public ViewHolder(View v) {
            super(v);
            v.setOnClickListener(this);
            nombre = (TextView) v.findViewById(R.id.text_nombre);
            direccion = (TextView) v.findViewById(R.id.text_direccion);
            telefono_fijo = (TextView) v.findViewById(R.id.text_telefono_fijo);
            telefono_movil = (TextView) v.findViewById(R.id.text_telefono_movil);
            correo_electronico = (TextView) v.findViewById(R.id.text_correo);
            condicion_venta = (TextView) v.findViewById(R.id.text_condiciones_venta);
            condicion_pago = (TextView) v.findViewById(R.id.text_condiciones_pago);
            id_linear_items = (LinearLayout) v.findViewById(R.id.id_linear_items);
            text_descriocion = (TextView) v.findViewById(R.id.text_descriocion);

        }

        public void bindData(AllContacts c,final Context contextt) {
            this.context=contextt;
            nombre.setText(c.getContacto());
            direccion.setText(c.getDireccion());
            telefono_fijo.setText(c.getTelefonoFijo());
            telefono_movil.setText(c.getTelefonoMovil());
            correo_electronico.setText(c.getCorreo());
            condicion_venta.setText(c.getCondVentaText());
            condicion_pago.setText(c.getCondPagoText());
            text_descriocion.setText(c.getDescripcion());


        }


        @Override
        public void onClick(View v) {
            ((ActivityListContactos)context).editContacto();
            Preferences.saveDireccionContacto(text_descriocion.getText().toString(),"descripcion");
            Preferences.saveDireccionLugarContacto(direccion.getText().toString(),"direccionLugar");
            Preferences.saveCorreoContacto(correo_electronico.getText().toString(),"correo");
            Preferences.saveNumeroMovilContacto(telefono_movil.getText().toString(),"telefonoMovil");
            Preferences.saveNumeroFijoContacto(telefono_fijo.getText().toString(),"telefonoFijo");
            Preferences.saveNombreContacto(nombre.getText().toString(),"nombre");
            Preferences.saveCode(List_contactos.get(this.getPosition()).getItem(),"code");
            Log.d("Adaptador_item",""+List_contactos.get(this.getPosition()).getItem());
            Log.d("Adaptador_item",""+List_contactos.get(this.getPosition()).getValue());
                   // Preferences.savePosicion(this.getPosition(),"posicion");
            //Log.e("condicionpago",String.valueOf(List_contactos.get(this.getPosition()).getCondPago()));
            //Log.e("condicionventa",String.valueOf(List_contactos.get(this.getPosition()).getCondVenta()));
            Preferences.saveCodPagoContacto(List_contactos.get(this.getPosition()).getCondPago(),"condicionPago");
            Preferences.saveCodVenta(List_contactos.get(this.getPosition()).getCondVenta(),"condicionVenta");
        }
    }
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflate = LayoutInflater.from(parent.getContext());
        View view= inflate.inflate(R.layout.list_contactos_de_un_cliente, parent, false);



        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        holder.bindData(List_contactos.get(position),context);

    }

    @Override
    public int getItemCount() {
        return List_contactos.size();
    }



    }
