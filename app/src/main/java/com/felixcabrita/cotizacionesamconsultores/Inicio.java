package com.felixcabrita.cotizacionesamconsultores;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.HttpHeaderParser;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.crashlytics.android.Crashlytics;
import com.felixcabrita.cotizacionesamconsultores.utils.Const;
import com.felixcabrita.cotizacionesamconsultores.utils.Preferences;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import io.fabric.sdk.android.Fabric;

public class Inicio extends AppCompatActivity implements View.OnClickListener{

    BaseDatosSqlite control;
    EditText user,pass;
    String key = "";
    String u = "";
    String p = "";
    private SharedPreferences pref ;
    private SharedPreferences.Editor editor;
    CheckBox recordar;
    boolean sucessLogin = false;
    public static float conversorMonedas;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Fabric.with(this, new Crashlytics());
        setContentView(R.layout.actividad_login);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        control = new BaseDatosSqlite(this);
        user = (EditText)findViewById(R.id.user);
        pass = (EditText)findViewById(R.id.pass);
        pref = PreferenceManager.getDefaultSharedPreferences(this);
        editor = pref.edit();
        editor.putInt("syncC",0);
        editor.apply();
        editor.putInt("syncP",0);
        editor.apply();
        editor.putInt("syncCo",0);
        editor.apply();
        recordar = (CheckBox) findViewById(R.id.recordar);

        new PersonalizadoProducto(this).execute();
        user.setText(pref.getString("user",""));
        pass.setText(pref.getString("pass",""));

        recordar.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if(!recordar.isChecked()){
                    editor.putString("user","");
                    editor.putString("pass","");
                }
            }
        });

        RequestQueue queue = Volley.newRequestQueue(this);
        String url = Const.URL_V1 + "cotizaciones/cambio";

        // Request a string response from the provided URL_V1.
        StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        // Display the first 500 characters of the response string.
                        Log.i("MONEDA", response);
                        try {
                            JSONObject obj = new JSONObject(response);
                            conversorMonedas = (float) obj.getDouble("cambio");
                        } catch (JSONException e) {
                            conversorMonedas = 3.27f;
                            e.printStackTrace();

                        }

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
            }
        }){

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String>  params = new HashMap<String, String>();
                params.put("x-api-key", pref.getString("key", ""));
                return params;
            }
        };
        queue.add(stringRequest);

        /*Intent intent = new Intent(this, sincronizacion.class);
          startService(intent);*/
    }

    @Override
    public void onClick(View view) {
        int id = view.getId();
        switch (id){
            case R.id.btnEntrar:
                u = user.getText().toString().trim();
                p = pass.getText().toString().trim();
                if(!u.isEmpty() && !p.isEmpty()){
                    Ingresar();
                }else{
                    Toast.makeText(Inicio.this,"No debe dejar campos vacios", Toast.LENGTH_SHORT).show();
                }

                break;
            case R.id.cambio:
                RecuperarContraseña();
                break;
        }
    }

    private class EnvioEmail extends AsyncTask<Void,Void,Integer> {

        boolean sucess = false;
        boolean internet = true;
        ProgressDialog pd;
        String msj = "";
        String correo;
        LinearLayout i, o;
        public EnvioEmail(Context c,String _u,LinearLayout _i,LinearLayout _o){
            try{
                correo = _u;
                i = _i;
                o=_o;
                pd =  new ProgressDialog(c);
                pd.setIndeterminate(true);
                pd.setMessage("Enviando correo");
                pd.setCancelable(false);
                pd.show();
            }catch (Exception e){
                Toast.makeText(Inicio.this,"Error: "+  e.getMessage(), Toast.LENGTH_SHORT).show();
            }

        }
        @Override
        protected void onPreExecute() {
            super.onPreExecute();

        }
        @Override
        protected Integer doInBackground(Void... voids) {
            ConnectivityManager connMgr = (ConnectivityManager)
                    getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();
            if (networkInfo != null && networkInfo.isConnected()) {
                try {
                    System.out.println("User: " + correo);
                    HttpClient httpclient = new DefaultHttpClient();
                    HttpPost httppost = new HttpPost(Const.URL_V1 + "auth/restore");
                    List<NameValuePair> params = new ArrayList<>();
                    params.add(new BasicNameValuePair("email",correo));
                    httppost.setEntity(new UrlEncodedFormEntity(params));
                    HttpResponse resp = httpclient.execute(httppost);
                    HttpEntity ent = resp.getEntity();/*y obtenemos una respuesta*/
                    if(HttpStatus.SC_ACCEPTED == resp.getStatusLine().getStatusCode() || HttpStatus.SC_OK == resp.getStatusLine().getStatusCode()){
                        sucess = true;
                        String text = EntityUtils.toString(ent);
                        JSONObject dato = new JSONObject(text);
                        msj = dato.getString("message");
                    }else{
                        msj = "Correo inválido";
                    }
                    System.out.println(msj);
                } catch (IOException e) {
                    System.out.println("Error1: "+e.toString());
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else{
                if(control.loginOffline(u,p)){
                    sucess = true;
                }
                internet = false;
            }
            return 0;
        }
        @Override
        protected void onPostExecute(Integer integer) {
            super.onPostExecute(integer);
            pd.dismiss();
            if(!internet){
                Toast.makeText(Inicio.this,"No tiene servicio de internet",Toast.LENGTH_SHORT).show();
            }
            if(sucess){
                i.setVisibility(View.GONE);
                o.setVisibility(View.VISIBLE);
            }
            Toast.makeText(Inicio.this,msj,Toast.LENGTH_SHORT).show();
        }
    }

    private class EnvioContraseña extends AsyncTask<Void,Void,Integer> {

        boolean sucess = false;
        boolean internet = true;
        ProgressDialog pd;
        String msj = "";
        String contra;
        String codigo;
        public EnvioContraseña(Context c,String _ccon,String _codigo){
            try{
                contra = _ccon;
                codigo =_codigo;
                pd =  new ProgressDialog(c);
                pd.setIndeterminate(true);
                pd.setMessage("Enviando correo");
                pd.setCancelable(false);
                pd.show();
            }catch (Exception e){
                Toast.makeText(Inicio.this,"Error: "+  e.getMessage(), Toast.LENGTH_SHORT).show();
            }

        }


        @Override
        protected void onPreExecute() {
            super.onPreExecute();

        }

        @Override
        protected Integer doInBackground(Void... voids) {
            ConnectivityManager connMgr = (ConnectivityManager)
                    getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();
            if (networkInfo != null && networkInfo.isConnected()) {
                try {
                    System.out.println("User: " + contra);
                    HttpClient httpclient = new DefaultHttpClient();
                    HttpPost httppost = new HttpPost("http://ventas.novatecpe.com:8080/novatec/acceso/cambiar_dato");
                    List<NameValuePair> params = new ArrayList<>();
                    params.add(new BasicNameValuePair("codigo",codigo));
                    params.add(new BasicNameValuePair("password",contra));
                    params.add(new BasicNameValuePair("password_confirm",contra));
                    httppost.setEntity(new UrlEncodedFormEntity(params));
                    HttpResponse resp = httpclient.execute(httppost);
                    HttpEntity ent = resp.getEntity();/*y obtenemos una respuesta*/
                    if(HttpStatus.SC_ACCEPTED == resp.getStatusLine().getStatusCode() || HttpStatus.SC_OK == resp.getStatusLine().getStatusCode()){
                        String text = EntityUtils.toString(ent);
                        JSONObject dato = new JSONObject(text);
                        sucess = dato.getBoolean("success");
                        msj = (sucess)?"Operacion realizada con exito":"Codigo ya utilizado o incorrecto";
                    }else{
                        msj = "Error inesperado";
                    }
                    System.out.println(msj);
                } catch (IOException e) {
                    System.out.println("Error1: "+e.toString());
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else{
                if(control.loginOffline(u,p)){
                    sucess = true;
                }
                internet = false;
            }
            return 0;
        }


        @Override
        protected void onPostExecute(Integer integer) {
            super.onPostExecute(integer);
            pd.dismiss();
            if(!internet){
                Toast.makeText(Inicio.this,"No tiene servicio de internet",Toast.LENGTH_SHORT).show();
            }
            Toast.makeText(Inicio.this,msj,Toast.LENGTH_SHORT).show();
        }
    }

    Dialog customDialog;
    public void Cerrar() {
        customDialog = new Dialog(this);
        customDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        customDialog.setCancelable(false);
        customDialog.setContentView(R.layout.notificacion_01);
        TextView titulo = (TextView) customDialog.findViewById(R.id.mensaje);
        titulo.setText("¿Desea salir de la aplicación?");
        (customDialog.findViewById(R.id.si)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view)
            {
                customDialog.dismiss();
                finish();
            }
        });
        (customDialog.findViewById(R.id.no)).setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view)
            {
                customDialog.dismiss();

            }
        });

        assert customDialog != null;
        customDialog.show();

    }

    public void EnviarContraseña(final String password, final String codigo){
        final ProgressDialog dialog = new ProgressDialog(this);
        dialog.setTitle("Cargando...");
        dialog.show();
        RequestQueue queue = Volley.newRequestQueue(this);
        String url = "http://ventas.novatecpe.com:8080/novatec/acceso/cambiar_dato";
        StringRequest postRequest = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>()
                {
                    @Override
                    public void onResponse(String response) {
                        // response
                        Log.d("Response", response);
                        try {
                            JSONObject dato = new JSONObject(response);
                            Boolean sucess = dato.getBoolean("success");
                            String msj = (sucess)?"Operacion realizada con exito":"Codigo ya utilizado o incorrecto";
                            Toast.makeText(Inicio.this,msj,Toast.LENGTH_SHORT).show();

                            //msj = "Estatus: " + resp.getStatusLine().getStatusCode() + " key" + dato.get("key");
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        dialog.dismiss();
                    }
                },
                new Response.ErrorListener()
                {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        // error
                        Toast.makeText(Inicio.this,"Error inesperado",Toast.LENGTH_SHORT).show();
                        //textResultado.setText("Error al iniciar sesion");
                        dialog.dismiss();
                    }
                }
        ) {
            @Override
            protected Map<String, String> getParams()
            {
                Map<String, String>  params = new HashMap<String, String>();
                params.put("codigo", codigo);
                params.put("password", password);
                params.put("password_confirm", password);

                return params;
            }
        };
        queue.add(postRequest);
    }

    public void EnviarEmail(final String correo, final LinearLayout i, final LinearLayout o){
        final ProgressDialog dialog = new ProgressDialog(this);
        dialog.setTitle("Cargando...");
        dialog.show();
        RequestQueue queue = Volley.newRequestQueue(this);
        String url = Const.URL_V1 + "auth/restore";
        StringRequest postRequest = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>()
                {
                    @Override
                    public void onResponse(String response) {
                        // response
                        Log.d("Response", response);
                        try {
                            JSONObject dato = new JSONObject(response);
                            String msj = dato.getString("message");
                            Toast.makeText(Inicio.this,msj,Toast.LENGTH_SHORT).show();
                            i.setVisibility(View.GONE);
                            o.setVisibility(View.VISIBLE);
                            //msj = "Estatus: " + resp.getStatusLine().getStatusCode() + " key" + dato.get("key");
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        dialog.dismiss();
                    }
                },
                new Response.ErrorListener()
                {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        // error
                        Toast.makeText(Inicio.this,"Correo inválido",Toast.LENGTH_SHORT).show();
                        //textResultado.setText("Error al iniciar sesion");
                        dialog.dismiss();
                    }
                }
        ) {
            @Override
            protected Map<String, String> getParams()
            {
                Map<String, String>  params = new HashMap<String, String>();
                params.put("email", correo);

                return params;
            }
        };
        queue.add(postRequest);
    }

    public void Ingresar(){

        boolean internet = true;
        ConnectivityManager connMgr = (ConnectivityManager)
                getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();
        if (networkInfo != null && networkInfo.isConnected()) {
            final ProgressDialog dialog = new ProgressDialog(this);
            dialog.setTitle("Cargando...");
            dialog.show();
            RequestQueue queue = Volley.newRequestQueue(this);
            String url = Const.URL_V1 + "auth";
            StringRequest postRequest = new StringRequest(Request.Method.POST, url,
                    new Response.Listener<String>()
                    {
                        @Override
                        public void onResponse(String response) {
                            // response
                            Log.d("Response", response);
                            try {
                                JSONObject dato = new JSONObject(response);
                                key =  dato.getString("key");
                                Log.e("KEY",key);
                                String nombre = dato.getString("nombres")+" "+dato.getString("apellidos");
                                control.CapturaDatosUsuarioJSON(dato,p);
                                Preferences.setKeyUser(key);
                                editor.putString("key",key);
                                editor.apply();
                                editor.putString("nombre",nombre);
                                editor.apply();
                                sucessLogin = true;
                                editor.putInt("Services",1);
                                editor.putInt("permiso_aprobar", dato.getInt("permiso_aprobar"));
                                editor.apply();
                                if(recordar.isChecked()){
                                    editor.putString("synctodo","0");
                                    editor.apply();

                                    editor.putString("user",user.getText().toString().toLowerCase());
                                    editor.apply();
                                    editor.putString("pass",pass.getText().toString());
                                    editor.apply();
                                }


                                Crashlytics.setUserIdentifier("12345");
                                Crashlytics.setUserEmail("ecasimiro@amconsultores.com.pe");
                                Crashlytics.setUserName(user.getText().toString().toLowerCase());


                                Intent i = new Intent(Inicio.this,Menu_Principal.class);
                                startActivity(i);
                                finish();
                                //msj = "Estatus: " + resp.getStatusLine().getStatusCode() + " key" + dato.get("key");
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                            dialog.dismiss();
                        }
                    },
                    new Response.ErrorListener()
                    {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            Log.i("LOGIN1", "error");
                            NetworkResponse response = error.networkResponse;
                            if (error instanceof AuthFailureError && response != null) {
                                Log.i("LOGIN2", error.toString());
                                try {
                                    String res = new String(response.data,
                                            HttpHeaderParser.parseCharset(response.headers, "utf-8"));
                                    // Now you can use any deserializer to make sense of data
                                    JSONObject obj = new JSONObject(res);
                                    Log.i("ERROR", obj.toString());
                                } catch (UnsupportedEncodingException e1) {
                                    // Couldn't properly decode data to string
                                    Log.i("error1", e1.getMessage());
                                } catch (JSONException e2) {
                                    // returned data is not JSONObject?
                                    Log.i("error2", e2.getMessage());
                                }
                            }
                            dialog.dismiss();
                        }
                    }
            ) {
                @Override
                protected Map<String, String> getParams()
                {
                    Map<String, String>  params = new HashMap<String, String>();
                    params.put("username", user.getText().toString().trim());
                    params.put("password", pass.getText().toString().trim());

                    return params;
                }
            };
            queue.add(postRequest);
        }else{
            if(control.loginOffline(u,p)){
                sucessLogin = true;
            }
            Toast.makeText(Inicio.this,"Inicio de sesion offline",Toast.LENGTH_SHORT).show();
            System.out.println("No hay internet");

            if(sucessLogin){
                editor.putInt("Services",1);
                editor.apply();
                if(recordar.isChecked()){
                    editor.putString("synctodo","0");
                    editor.apply();

                    editor.putString("user",user.getText().toString());
                    editor.apply();
                    editor.putString("pass",pass.getText().toString());
                    editor.apply();
                }
                Intent i = new Intent(Inicio.this,Menu_Principal.class);
                startActivity(i);
                finish();
            }else{
                Toast.makeText(Inicio.this,"Datos de usuario incorrectos",Toast.LENGTH_SHORT).show();
            }
        }

    }

    private class Login extends AsyncTask<Void,Void,Integer> {

        boolean sucess = false;
        boolean internet = true;
        ProgressDialog pd;
        String msj = "";

        public Login(Context c){
            try{
                pd =  new ProgressDialog(c);
                pd.setIndeterminate(true);
                pd.setMessage("Iniciando sesión");
                pd.setCancelable(false);
                pd.show();
            }catch (Exception e){
                Toast.makeText(Inicio.this,"Error: "+  e.getMessage(), Toast.LENGTH_SHORT).show();
            }

        }


        @Override
        protected void onPreExecute() {
            super.onPreExecute();

        }

        @Override
        protected Integer doInBackground(Void... voids) {
            ConnectivityManager connMgr = (ConnectivityManager)
                    getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();
            if (networkInfo != null && networkInfo.isConnected()) {
                try {
                    System.out.println("User: " + u + " Pass: " + p);
                    HttpClient httpclient = new DefaultHttpClient();
                    HttpPost httppost = new HttpPost(Const.URL_V1 + "auth");
                    List<NameValuePair> params = new ArrayList<>();
                    params.add(new BasicNameValuePair("username",u));
                    params.add(new BasicNameValuePair("password", p));
                    httppost.setEntity(new UrlEncodedFormEntity(params));
                    HttpResponse resp = httpclient.execute(httppost);
                    HttpEntity ent = resp.getEntity();/*y obtenemos una respuesta*/
                    if(HttpStatus.SC_ACCEPTED == resp.getStatusLine().getStatusCode() || HttpStatus.SC_OK == resp.getStatusLine().getStatusCode()){
                        System.out.println("Estatus: Aceptado");
                        sucess = true;
                        String text = EntityUtils.toString(ent);
                        JSONObject dato = new JSONObject(text);
                        key =  dato.get("key").toString();
                        String nombre = dato.getString("nombres")+" "+dato.getString("apellidos");
                        control.CapturaDatosUsuarioJSON(dato,p);
                        editor.putString("key",key);
                        editor.apply();
                        editor.putString("nombre",nombre);
                        editor.apply();
                        msj = "Estatus: " + resp.getStatusLine().getStatusCode() + " key" + dato.get("key");
                    }
                    System.out.println(msj);
                } catch (IOException e) {
                    System.out.println("Error1: "+e.toString());
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else{
                if(control.loginOffline(u,p)){
                    sucess = true;
                }
                internet = false;
            }
            return 0;
        }


        @Override
        protected void onPostExecute(Integer integer) {
            super.onPostExecute(integer);
            pd.dismiss();
            if(!internet){
                Toast.makeText(Inicio.this,"Inicio de sesion offline",Toast.LENGTH_SHORT).show();
                System.out.println("No hay internet");
            }
            if(sucess){
                editor.putInt("Services",1);
                editor.apply();
                if(recordar.isChecked()){
                    editor.putString("synctodo","0");
                    editor.apply();

                    editor.putString("user",user.getText().toString());
                    editor.apply();
                    editor.putString("pass",pass.getText().toString());
                    editor.apply();
                }
                Intent i = new Intent(Inicio.this,Menu_Principal.class);
                startActivity(i);
                finish();
            }else{
                Toast.makeText(Inicio.this,"Datos de usuario incorrectos",Toast.LENGTH_SHORT).show();
            }
        }
    }

    @Override
    public void onBackPressed() {
        Cerrar();
    }

    public void RecuperarContraseña() {
        customDialog = new Dialog(this);
        customDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        customDialog.setCancelable(false);
        customDialog.setContentView(R.layout.notificacion_05);

        final EditText correo =(EditText) customDialog.findViewById(R.id.correo);
        final EditText codigo =(EditText) customDialog.findViewById(R.id.codigo);
        final EditText pw =(EditText) customDialog.findViewById(R.id.pw);
        final EditText repw =(EditText) customDialog.findViewById(R.id.repw);
        final LinearLayout notiene =(LinearLayout) customDialog.findViewById(R.id.notiene);
        final LinearLayout tiene =(LinearLayout) customDialog.findViewById(R.id.tiene);


        (customDialog.findViewById(R.id.si)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view)
            {
                if(pw.getText().toString().compareTo(repw.getText().toString())==0) {
                    //new EnvioContraseña(Inicio.this,pw.getText().toString(),codigo.getText().toString()).execute();
                    EnviarContraseña(pw.getText().toString(), codigo.getText().toString());
                }else{
                    Toast.makeText(Inicio.this,"No coinciden las contraseñas",Toast.LENGTH_SHORT).show();
                }
                customDialog.dismiss();
            }
        });
        (customDialog.findViewById(R.id.nno)).setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view)
            {
                customDialog.dismiss();

            }
        });

        (customDialog.findViewById(R.id.enviar)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view)
            {
                EnviarEmail(correo.getText().toString(), notiene, tiene);
                //new EnvioEmail(Inicio.this,correo.getText().toString(),notiene,tiene).execute();
            }
        });
        (customDialog.findViewById(R.id.volver)).setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view)
            {
                customDialog.dismiss();
            }
        });
        (customDialog.findViewById(R.id.ya)).setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view)
            {
                notiene.setVisibility(View.GONE);
                tiene.setVisibility(View.VISIBLE);
            }
        });
        assert customDialog != null;
        customDialog.show();

    }

    class PersonalizadoProducto extends AsyncTask<Void,Void,Void> {


        boolean internet = true;
        ProgressDialog pd;
        private BaseDatosSqlite cnn;
        String urls = Const.URL_V1 + "productos";
        Context c;
        boolean error = false;

        private SharedPreferences pref ;
        private SharedPreferences.Editor editor;
        public PersonalizadoProducto(Context c) {
            cnn = new BaseDatosSqlite(c);
            pref = PreferenceManager.getDefaultSharedPreferences(c);
            this.c = c;
            editor = pref.edit();
            /*try{
                pd =  new ProgressDialog(c);
                pd.setIndeterminate(true);
                pd.setMessage("Iniciando seción");
                pd.setCancelable(false);
                pd.show();
            }catch (Exception e){
                System.out.println("Error1: "+e.toString());
            }*/

        }


        @Override
        protected void onPreExecute() {
            super.onPreExecute();

        }

        @Override
        protected Void doInBackground(Void... voids) {
            ConnectivityManager connMgr = (ConnectivityManager)
                    c.getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();
            if (networkInfo != null && networkInfo.isConnected()) {
                try {
                    URL producto = new URL(urls);
                    HttpURLConnection conProducto = (HttpURLConnection) producto.openConnection();
                    //connection.setRequestProperty("Authorization", "Basic " + Base64.encodeToString((user + ":" + pass).getBytes(), Base64.NO_WRAP));
                    if (conProducto.getResponseCode() == HttpURLConnection.HTTP_OK) {
                        InputStream inputStreamResponse = conProducto.getInputStream();
                        String linea;
                        StringBuilder respuestaCadena = new StringBuilder();
                        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStreamResponse, "UTF-8"));
                        while ((linea = bufferedReader.readLine()) != null) {
                            respuestaCadena.append(linea);
                        }
                        // JSONObject datos = new JSONObject(respuestaCadena.toString());
                        try {
                            JSONArray jsonArray = new JSONArray(respuestaCadena.toString());// datos.getJSONArray("");
                            cnn.CapturaDatosProductosJSON(jsonArray);
                        } catch (JSONException e) {
                            error = true;
                            e.printStackTrace();
                        }
                        editor.putInt("syncP",0);
                        editor.apply();

                    } else {
                        error = true;
                        System.out.println("Error2 " + conProducto.getResponseCode());
                    }

                } catch (IOException e) {
                    error = true;
                    System.out.println("Error1: " + e.toString());
                }

                /*if(!error){
                    cotizacion = control.GetFullClientes();
                    nombre = new String[cotizacion.size()];
                    for (int i = 0;i<nombre.length;i++){
                        nombre[i] = cotizacion.get(i).get(tb_cliente.razon_social);
                    }
                }*/
            }
            return null;
        }


        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
        }


    }


}
