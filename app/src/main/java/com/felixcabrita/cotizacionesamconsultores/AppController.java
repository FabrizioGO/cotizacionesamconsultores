package com.felixcabrita.cotizacionesamconsultores;

import android.app.Application;
import android.content.SharedPreferences;

import com.android.volley.RequestQueue;

import com.felixcabrita.cotizacionesamconsultores.utils.Preferences;

import com.crashlytics.android.Crashlytics;
import io.fabric.sdk.android.Fabric;

public class AppController extends Application {

    public static final String TAG = AppController.class.getSimpleName();

    private RequestQueue mRequestQueue;
    private static AppController mInstance;
    SharedPreferences sharedPreferences;

    @Override
    public void onCreate() {
        super.onCreate();
        Fabric.with(this, new Crashlytics());
        mInstance = this;
        initApplication();

    }
    public static synchronized AppController getInstance() {
        return mInstance;
    }

    private void initApplication() {

        Preferences.init(getApplicationContext());
    }

}