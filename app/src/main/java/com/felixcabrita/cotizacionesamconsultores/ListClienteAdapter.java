package com.felixcabrita.cotizacionesamconsultores;

import android.content.ContentValues;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;

import com.felixcabrita.cotizacionesamconsultores.models.Tb_Clientes;

class ListClienteAdapter extends RecyclerView.Adapter<ListClienteAdapter.ClienteViewHolder> {

    public  ArrayList<ContentValues> lista = new ArrayList<>();
    private static ClickListener clickListener;
    ListClienteAdapter(Context c, ArrayList<ContentValues> l){
        this.lista = l;

    }

    static class ClienteViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{

        public TextView cliente;
        TextView codigo;

        ClienteViewHolder(View v) {
            super(v);
            v.setOnClickListener(this);
            cliente = (TextView)v.findViewById(R.id.nombre);

        }

        @Override
        public void onClick(View v) {
            clickListener.onItemClick(getAdapterPosition(), v);
        }
    }


    void setOnItemClickListener(ClickListener clickListener) {
        ListClienteAdapter.clickListener = clickListener;
    }

    interface ClickListener {
        void onItemClick(int position, View v);
    }

    @Override
    public int getItemCount() {
        return lista.size();
    }

    @Override
    public ClienteViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.conten_clientes_adapter, viewGroup, false);
        return new ClienteViewHolder(v);
    }

    @Override
    public void onBindViewHolder(ClienteViewHolder viewHolder, int position) {
        viewHolder.cliente.setText(lista.get(position).get(Tb_Clientes.razon_social).toString());
    }
}

