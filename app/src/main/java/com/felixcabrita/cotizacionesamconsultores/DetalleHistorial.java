package com.felixcabrita.cotizacionesamconsultores;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.content.res.Resources;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import com.felixcabrita.cotizacionesamconsultores.utils.Const;
import com.felixcabrita.cotizacionesamconsultores.models.Tb_Clientes;
import com.felixcabrita.cotizacionesamconsultores.models.Tb_cotizacion_productos;
import com.felixcabrita.cotizacionesamconsultores.models.Tb_Cotizaciones;
import com.felixcabrita.cotizacionesamconsultores.models.Tb_EstadoCotizacion;
import com.felixcabrita.cotizacionesamconsultores.utils.Util;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;

import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public class DetalleHistorial extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener,View.OnClickListener {

    private DecimalFormat convertidor = new DecimalFormat("#,##0.00", DecimalFormatSymbols.getInstance(Locale.ITALIAN));   // CONVERTIDOR FLOAT \\
    ArrayList<ContentValues> items = new ArrayList<>();
    ContentValues datos;
    BaseDatosSqlite control;
    String idx = "";
    int moneda = 0,cantidaddd = 0;
    TextView razon,ruc,cantidad,flete,fecha,codigo,aprobada;
    ListView Listitems;

    adapter_producto_cotizacion adapter;
    int estado, estadoSynco, estadoWWW = 0;
    FloatingActionButton estus;
    SharedPreferences pref;
    float subtotal = 0;
    Menu menee;
    String[] icotems;
    int extrangero= 1;
    boolean hasCode, hasEstado;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_principal_detalle_historial_cotizacion);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        Bundle extras = getIntent().getExtras();
        if (extras!=null){
            hasCode = extras.getBoolean("code",false);
            hasEstado = extras.getBoolean("estado",false);
        }

        setSupportActionBar(toolbar);

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        pref = PreferenceManager.getDefaultSharedPreferences(this);
        View headerView = navigationView.getHeaderView(0);
        TextView tvHeaderName= (TextView) headerView.findViewById(R.id.usuario);
        tvHeaderName.setText(pref.getString("nombre","usuario"));
        DecimalFormatSymbols dfs = new DecimalFormatSymbols();
        dfs.setDecimalSeparator('.');
        dfs.setGroupingSeparator(',');
        convertidor.setRoundingMode(RoundingMode.DOWN);
        convertidor.setDecimalFormatSymbols(dfs);
        estus = (FloatingActionButton)findViewById(R.id.pendiente);
        control = new BaseDatosSqlite(this);
        razon = (TextView) findViewById(R.id.razon);
        ruc = (TextView) findViewById(R.id.ruc);
        cantidad = (TextView) findViewById(R.id.cantidad);
        flete = (TextView) findViewById(R.id.flete);
        Listitems = (ListView) findViewById(R.id.itemcotiza);
        codigo = (TextView) findViewById(R.id.codigo);
        fecha = (TextView) findViewById(R.id.fecha);
        aprobada = (TextView) findViewById(R.id.estado);
        idx = getIntent().getExtras().getString(Tb_Cotizaciones.codigo);
        moneda= getIntent().getExtras().getInt(Tb_Cotizaciones.moneda);
        datos = control.GetCotizacion(idx);

        if (datos!=null) {
            razon.setText(datos.get(Tb_Clientes.razon_social).toString());
            ruc.setText(datos.get(Tb_Clientes.ruc).toString());
            cantidad.setText(datos.get(Tb_Cotizaciones.items).toString());
            estado = datos.getAsInteger(Tb_Cotizaciones.estado);
            codigo.setText(datos.get(Tb_Cotizaciones.codigo) != null ? datos.get(Tb_Cotizaciones.codigo).toString() : "No sincronizado");
            aprobada.setText(hasCode ? "No sincronizado" : estadoCotizacion(datos.getAsInteger(Tb_Cotizaciones.estado)));
            float flete1 = Float.parseFloat(datos.get(Tb_Cotizaciones.flete).toString());
            estadoWWW = datos.getAsInteger("cot_www");
            estadoSynco = datos.getAsInteger(Tb_Cotizaciones.estadoSynco);
            flete.setVisibility((flete1 < 1) ? View.GONE : View.VISIBLE);
            extrangero = getNumberI(datos.get(Tb_Clientes.tipoExtaNa).toString(), 1);
            String part1 = datos.get(Tb_Cotizaciones.fecha_registro).toString();
            fecha.setText(part1);
            Log.i("DETALLE", datos.toString());

            new Datos(this, idx, moneda).execute();
        }else{
            Toast.makeText(this,"Datos son null",Toast.LENGTH_SHORT).show();
        }

    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {

        int id = item.getItemId();

        switch (id){

            case R.id.inicio:
                Intent i33 = new Intent(DetalleHistorial.this, Menu_Principal.class);
                startActivity(i33);
                finish();
                break;
            case R.id.usuari:
                Intent i = new Intent(DetalleHistorial.this,DatosUsuario.class);
                startActivity(i);
                finish();
                break;
            case R.id.clientes:
                Intent i1 = new Intent(DetalleHistorial.this, ListaClientes.class);
                startActivity(i1);
                finish();
                break;
            case R.id.cerrar:
                Cerrar();
                break;
            case R.id.cotizar:
                Intent i3 = new Intent(DetalleHistorial.this,ActividadPrincipal.class);
                i3.putExtra("edit",0);
                startActivity(i3);
                finish();
                break;
            case R.id.histo:
                Intent i4 = new Intent(DetalleHistorial.this,Historial_cotizacion.class);
                i4.putExtra("volver","0");
                startActivity(i4);
                finish();
                break;
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);

        return false;
    }

    @Override
    public void onBackPressed() {
        Intent i4 = new Intent(DetalleHistorial.this,Historial_cotizacion.class);
        i4.putExtra("volver","0");
        startActivity(i4);
        finish();
    }

    private class SyncCotizaciones extends AsyncTask<Void,Void,Integer> {


        Context cont;
        ProgressDialog pd;
        int envio= 0;
        String urls = Const.URL_V1 + "cotizaciones";
        public SyncCotizaciones(Context c,int i){

            envio = i;
            try{
                cont = c;
                pd =  new ProgressDialog(c);
                pd.setIndeterminate(true);
                pd.setMessage("Cambiando de estado la cotizaciòn");
                pd.setCancelable(false);
                pd.show();
            }catch (Exception e){
                System.out.println("Error1: "+e.toString());
            }

        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

        }


        @Override
        protected Integer doInBackground(Void... voids) {
            if (Util.isNetworkAvailable(DetalleHistorial.this)) {
                if (control.CheckDataSyncCotizaEstado()) {
                    try {
                        String selectQuery = "SELECT "+Tb_EstadoCotizacion.codigo+" FROM "+Tb_EstadoCotizacion.nombreTabla +
                                " WHERE "+Tb_EstadoCotizacion.estadoSynco + " = 0 AND " + Tb_EstadoCotizacion.estado +" = 3";
                        SQLiteDatabase database = control.getWritableDatabase();
                        Cursor cursor = database.rawQuery(selectQuery, null);
                        if (cursor.moveToFirst()) {
                            do {
                                try {
                                    HttpClient httpclient = new DefaultHttpClient();
                                    HttpPost httppost = new HttpPost(urls+ "/aprobar");
                                    httppost.setHeader("x-api-key", pref.getString("key", ""));
                                    List<NameValuePair> params = new ArrayList<>();
                                    params.add(new BasicNameValuePair("codigo", cursor.getString(0)));
                                    httppost.setEntity(new UrlEncodedFormEntity(params));
                                    HttpResponse resp = httpclient.execute(httppost);
                                    HttpEntity ent = resp.getEntity();/*y obtenemos una respuesta*/
                                    if (HttpStatus.SC_OK == resp.getStatusLine().getStatusCode()) {
                                        control.updateEstadoCotizaPPP(cursor.getString(0));
                                        if(envio == 1){
                                            HttpClient httpclient111 = new DefaultHttpClient();
                                            HttpPost httppost11 = new HttpPost(Const.URL_V1 + "email/cotizacion");
                                            httppost11.setHeader("x-api-key", pref.getString("key", ""));
                                            List<NameValuePair> params11 = new ArrayList<>();
                                            params11.add(new BasicNameValuePair("codigo",cursor.getString(0)));
                                            httppost11.setEntity(new UrlEncodedFormEntity(params11));
                                            HttpResponse resp111 = httpclient111.execute(httppost11);
                                        }
                                    }else{
                                        String text = EntityUtils.toString(ent);
                                        System.out.println("sube0: " + text + " ) ");
                                    }

                                } catch (Exception e) {
                                    System.out.println("Error Sync Subieda 0: " + e.toString());
                                }

                            } while (cursor.moveToNext());
                        }
                        cursor.close();


                        String selectQuery1 = "SELECT "+Tb_EstadoCotizacion.codigo+" FROM "+Tb_EstadoCotizacion.nombreTabla +
                                " WHERE "+Tb_EstadoCotizacion.estadoSynco + " = 0 AND " + Tb_EstadoCotizacion.estado +" = 4";
                        database = control.getWritableDatabase();
                        Cursor cursor1 = database.rawQuery(selectQuery1, null);
                        if (cursor1.moveToFirst()) {
                            do {
                                try {
                                    HttpClient httpclient = new DefaultHttpClient();
                                    HttpPost httppost = new HttpPost(urls+ "/rechazar");
                                    httppost.setHeader("x-api-key", pref.getString("key", ""));
                                    List<NameValuePair> params = new ArrayList<>();
                                    params.add(new BasicNameValuePair("codigo", cursor1.getString(0)));
                                    httppost.setEntity(new UrlEncodedFormEntity(params));
                                    HttpResponse resp = httpclient.execute(httppost);
                                    HttpEntity ent = resp.getEntity();/*y obtenemos una respuesta*/
                                    if (HttpStatus.SC_OK == resp.getStatusLine().getStatusCode()) {
                                        control.updateEstadoCotizaPPP(cursor1.getString(0));
                                    }else{
                                        String text = EntityUtils.toString(ent);
                                        System.out.println("sube0: " + text + " ) ");
                                    }

                                } catch (Exception e) {
                                    System.out.println("Error Sync Subieda 0: " + e.toString());
                                }

                            } while (cursor1.moveToNext());
                        }
                        cursor1.close();

                        cursor.close();
                    } catch (Exception e) {
                        System.out.println("Error-GetFullClientes: " + e.toString());
                    }

                }

            }
            return 0;
        }

        @Override
        protected void onPostExecute(Integer integer) {
            super.onPostExecute(integer);
            Intent i4 = new Intent(DetalleHistorial.this,Historial_cotizacion.class);
            i4.putExtra("volver","0");
            startActivity(i4);
            finish();
            pd.dismiss();
        }

    }

    Dialog customDialog;
    public void Cerrar() {
        customDialog = new Dialog(this);
        customDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        customDialog.setCancelable(false);
        customDialog.setContentView(R.layout.notificacion_01);
        (customDialog.findViewById(R.id.si)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view)
            {
                customDialog.dismiss();
                Intent intent = new Intent(DetalleHistorial.this,Inicio.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
                finish();
            }
        });
        (customDialog.findViewById(R.id.no)).setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view)
            {
                customDialog.dismiss();

            }
        });

        assert customDialog != null;
        customDialog.show();

    }
    public void getDetalleHistorial() {
        customDialog = new Dialog(this);
        customDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        customDialog.setCancelable(false);
        customDialog.setContentView(R.layout.notificacion_04);

        TextView moneda = (TextView) customDialog.findViewById(R.id.moneda);
        TextView condi_pago = (TextView) customDialog.findViewById(R.id.condi_pago);
        TextView condi_venta =  (TextView)customDialog.findViewById(R.id.condi_venta);
        TextView dias = (TextView) customDialog.findViewById(R.id.tiemp);
        TextView lugar  = (TextView) customDialog.findViewById(R.id.lugar);
        TextView fecha  = (TextView) customDialog.findViewById(R.id.fecha);
        TextView lugarEntrega  = (TextView) customDialog.findViewById(R.id.lugarEntrega);
        TextView icoterms  = (TextView) customDialog.findViewById(R.id.icoterms);
        LinearLayout icotenLy  = (LinearLayout) customDialog.findViewById(R.id.icotenLy);
        moneda.setText(datos.get(Tb_Cotizaciones.moneda).toString().compareTo("1")==0?"Soles S/.":"Dolares $");
        Log.e("EstadoSyncro",datos.get(Tb_Cotizaciones.estadoSynco).toString());
        if (datos.get(Tb_Cotizaciones.estadoSynco).toString().equalsIgnoreCase("2") || datos.get(Tb_Cotizaciones.estadoSynco).toString().equalsIgnoreCase("0")) {
            int codipago = Integer.parseInt(datos.get(Tb_Cotizaciones.cond_pago).toString());
            String cod_venta = codipago == 2 ? "Crédito" : "Contado";
            condi_venta.setText(cod_venta);
            String cod_pago = control.GetCondicionPago(datos.get(Tb_Cotizaciones.cond_venta).toString());
            condi_pago.setText(cod_pago);
            Log.e("condiciones", "pago: " + datos.get(Tb_Cotizaciones.cond_pago).toString() + " venta: " + datos.get(Tb_Cotizaciones.cond_venta).toString());
        }else{
            int codipago = Integer.parseInt(datos.get(Tb_Cotizaciones.cond_venta).toString());
            String cod_venta = codipago == 2 ? "Crédito" : "Contado";
            condi_venta.setText(cod_venta);
            String cod_pago = control.GetCondicionPago(datos.get(Tb_Cotizaciones.cond_pago).toString());
            condi_pago.setText(cod_pago);
            Log.e("condiciones", "pago: " + datos.get(Tb_Cotizaciones.cond_pago).toString() + " venta: " + datos.get(Tb_Cotizaciones.cond_venta).toString());
        }
        if(extrangero == 1){
            icotenLy.setVisibility(View.GONE);
        }

        lugarEntrega.setText(datos.get(Tb_Cotizaciones.lugar).toString());
        icoterms.setText(icotems[datos.getAsInteger(Tb_Cotizaciones.icotem)]);
        dias.setText(Ttt(datos.getAsInteger(Tb_Cotizaciones.tiempo_validez)));
        lugar.setText(lugar(Integer.parseInt(datos.get(Tb_Cotizaciones.lugar_cotizacion).toString())-1));
        String[] parts = datos.get(Tb_Cotizaciones.fecha).toString().split(" ");
        String part1 = parts[0];
        fecha.setText(part1);

        (customDialog.findViewById(R.id.no)).setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view)
            {
                customDialog.dismiss();

            }
        });

        assert customDialog != null;
        customDialog.show();

    }

    String Ttt(int i){
        return (i<10)?"0"+(i<2?i+ " día":i+" días"):i + " días";
    }

    String condicion(int i){
        if(i==0){
            return "30 días";
        }
        if(i==1){
            return "60 días";
        }

        if(i==2){
            return "90 días";
        }
        return "";
    }

    String lugar(int i){
        if(i==0){
            return "Desde la calle";
        }
        if(i==1){
            return "Casa del vendedor";
        }

        if(i==2){
            return "Oficina de Novatec";
        }
        if(i==3){
            return "Oficina del cliente";
        }
        return "";
    }

    @Override
    public void onClick(View view) {
        if(view.getId()  == R.id.pendiente){
            CambiaEstado();
        }
    }
    String estadoCotizacion(int i){
        String estado= " ";
        switch (i){
            case 3:
                estado = "Aprobado";
                break;
            case 4:
                estado = "Rechazado";
                break;
            case 1:
                estado = "Aprobado";
                break;
            case 0:
                estado = "Rechazado";
                break;
            case 2:
                estado = "Pendiente por revisión";
                break;
            case 5:
                estado = "Ninguno";
                break;
        }
        return estado;

    }
    int StatusCotiza(int i){
        int ii =0;
        switch (i){
            case 2:
                ii=1;//RECHAZADA
                break;
            case 4:
                ii=1;//RECHAZADA
                break;
            case 3:
                ii=0;//APROBADA
                break;
            case 0:
                ii=1;//RECHAZADA
                break;
            case 1:
                ii=0;//APROBADA
                break;
            case 5:
                ii=2;//Ninguno
                break;

        }
        return ii;
    }

    private class Datos extends AsyncTask<Void,Void,Integer> {

        ProgressDialog pd;
        Context cont;
        String id;
        int moneda= 0;
        Resources res = getResources();
        String urls = Const.URL_V1 + "cotizaciones";
        boolean internet;
        String codigoo ="";
        public Datos(Context c,String _id,int _moneda){
            try{
                moneda = _moneda;
                id = _id;
                cont = c;
                pd =  new ProgressDialog(c);
                pd.setIndeterminate(true);
                pd.setMessage("Cargando Datos");
                pd.setCancelable(false);
                pd.show();
            }catch (Exception e){
                System.out.println("Error1: "+e.toString());
            }

        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            codigoo = codigo.getText().toString().trim();
        }

        @Override
        protected Integer doInBackground(Void... voids) {
            /*if (Util.isNetworkAvailable(getDetalleHistorial.this)) {
                Log.i(id, datos.get(Tb_Cotizaciones.codigo).toString());
                if(control.GetEstadoProductId(id) && datos.get(Tb_Cotizaciones.codigo)!=null){
                    try {
                        SQLiteDatabase database = control.getWritableDatabase();
                        URL producto1 = new URL(urls + "?codigo=" + codigoo);
                        HttpURLConnection conProducto1 = null;
                        conProducto1 = (HttpURLConnection) producto1.openConnection();
                        conProducto1.setRequestProperty("x-api-key", pref.getString("key", ""));
                        //connection.setRequestProperty("Authorization", "Basic " + Base64.encodeToString((user + ":" + pass).getBytes(), Base64.NO_WRAP));
                       // database.beginTransaction();
                        if (conProducto1.getResponseCode() == HttpURLConnection.HTTP_OK) {
                            try {
                                database.execSQL("DELETE FROM  cotizacion_productos WHERE copr_cotizacion = " + "(SELECT id FROM cotizaciones WHERE coti_codigo = '" + codigoo + "')");
                                database.execSQL("DELETE FROM  cotizaciones WHERE id = " + "(SELECT id FROM cotizaciones WHERE coti_codigo = '" + codigoo + "')");
                            } catch (Exception e) {
                                System.out.println("Error-GetProducto: " + e.toString());
                            }
                            System.out.println("BAJANDO INFORMACION NUEVA " +codigoo);
                            InputStream inputStreamResponse1 = conProducto1.getInputStream();
                            String linea1;
                            StringBuilder respuestaCadena1 = new StringBuilder();
                            BufferedReader bufferedReader1 = new BufferedReader(new InputStreamReader(inputStreamResponse1, "UTF-8"));
                            while ((linea1 = bufferedReader1.readLine()) != null) {
                                respuestaCadena1.append(linea1);
                            }
                            JSONObject datosJson = new JSONObject(respuestaCadena1.toString());
                            id = datosJson.getJSONObject("cotizacion").getString("id");
                            idx = id;
                            Log.i("COTI_DETALLE", datosJson.toString());

                            control.capturaCabezeraCotizacion(datosJson);
                            datos = control.GetCotizacion(idx);
                            estadoWWW = 1;
                        } else {
                            System.out.println("Error: Pediciom");
                        }
                       // database.endTransaction();
                    } catch (IOException | JSONException e) {
                        e.printStackTrace();
                    }
                }
                internet = true;
            }else{
                internet = false;
            }*/

            icotems = res.getStringArray(R.array.icotermn);
            items = control.GetItemsCotizacion(id,extrangero==2, moneda);
            for(int i=0;i<items.size();i++){
                cantidaddd += (items.get(i).getAsInteger(Tb_cotizacion_productos.cantidad));
            }


            adapter = new adapter_producto_cotizacion(cont, items,moneda);
            return 0;
        }

        @Override
        protected void onPostExecute(Integer integer) {
            super.onPostExecute(integer);
            Listitems.setAdapter(adapter);
            View footerView =  ((LayoutInflater)cont.getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.footer_lista, null, false);
            TextView sst = (TextView) footerView.findViewById(R.id.subtotal);
            TextView total = (TextView) footerView.findViewById(R.id.total);
            TextView igv = (TextView) footerView.findViewById(R.id.igb);
            TextView totalventas = (TextView) footerView.findViewById(R.id.totalventas);
            TextView perce = (TextView) footerView.findViewById(R.id.perce);
            TextView monto_porcentaje = (TextView) footerView.findViewById(R.id.monto_porcentaje);
            LinearLayout perceto = (LinearLayout) footerView.findViewById(R.id.perceto);

            float igbF,flete,porcentaje,po_per,totalfinal;
            int tiponegocio =getNumberI(datos.get(Tb_Clientes.tiponegocio).toString(),0);
            po_per =getNumberF(datos.get(Tb_Clientes.por_perceptor).toString(),0);
            System.out.println("Porcentaje: " + po_per);
            igbF = Float.parseFloat(datos.get(Tb_Cotizaciones.monto_igv).toString());
            if(estadoWWW ==1 && estadoSynco == 1){
                subtotal = datos.getAsFloat(Tb_Cotizaciones.subtotal) - igbF;
            }else{
                subtotal = datos.getAsFloat(Tb_Cotizaciones.subtotal);
            }
            porcentaje = getNumberF(datos.get(Tb_Cotizaciones.porcentaje).toString(),0);
            cantidad.setText("" + cantidaddd);
            internet = Util.isNetworkAvailable(DetalleHistorial.this);
            if (internet) {
                if(estado == 4 || estado == 0 || estado == 1  || hasCode){
                    estus.setVisibility(View.GONE);
                }
            }else{
                estus.setVisibility(View.GONE);
            }
            totalfinal = datos.getAsFloat(Tb_Cotizaciones.monto_total) ;
            if(tiponegocio == 1){
                if(po_per!=0){
                    perce.setText((po_per==0.5f)?"Perceptor 0.5%: ":"Perceptor 2%: ");
                    convertidor.setRoundingMode(RoundingMode.DOWN);
                    monto_porcentaje.setText(((moneda==2)?"US$ ":"S/. ") +convertidor.format(porcentaje));
                    perceto.setVisibility(View.VISIBLE);
                }else{
                    perceto.setVisibility(View.GONE);
                }
            }else{
                perce.setText("Perceptor 2%: ");
                convertidor.setRoundingMode(RoundingMode.DOWN);
                monto_porcentaje.setText(((moneda==2)?"US$ ":"S/. ") +convertidor.format(porcentaje));
                perceto.setVisibility(View.VISIBLE);
            }


            if(extrangero == 2){
                perceto.setVisibility(View.GONE);
            }
            convertidor.setRoundingMode(RoundingMode.DOWN);
            total.setText(((moneda==2)?"US$ ":"S/. ") +convertidor.format((subtotal+igbF)));
            igv.setText(((moneda==2)?"US$ ":"S/. ") +convertidor.format(igbF));
            sst.setText(((moneda==2)?"US$ ":"S/. ") +convertidor.format(subtotal));
            totalventas.setText(((moneda==2)?"US$ ":"S/. ") +convertidor.format(totalfinal));

            Listitems.addFooterView(footerView);
            pd.dismiss();
        }

    }
    int getNumberI(String d, int i){
        try{
            return Integer.parseInt(d);
        }catch (Exception e){
            return i;
        }
    }

    float getNumberF(String d,float i){
        try{
            return Float.parseFloat(d);
        }catch (Exception e){
            return i;
        }
    }

    private void CambiaEstado() {
        boolean internet = Util.isNetworkAvailable(DetalleHistorial.this);
        if (internet) {
            EstadoCotizacionCambiar(this);
        }else{
            Toast.makeText(this, "No tiene internet para realizar esta operación", Toast.LENGTH_SHORT).show();
            estus.setVisibility(View.GONE);
        }

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.historial_detalle_menu, menu);
        menee = menu;
        MenuItem iiimenu = menee.findItem(R.id.borra);
        iiimenu.setVisible(hasCode);
        MenuItem iiiimenu = menee.findItem(R.id.editar);
        iiiimenu.setVisible(hasEstado && !hasCode);
        //iiiimenu.setVisible(false);
        return super.onCreateOptionsMenu(menu);
    }
    
    public void EstadoCotizacionCambiar(Context t) {
        customDialog = new Dialog(t);
        customDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        customDialog.setCancelable(false);
        customDialog.setContentView(R.layout.notificacion_08);

        final RadioButton aprobada,reprobada,ninguno;
        aprobada = (RadioButton)customDialog.findViewById(R.id.aprobada);
        reprobada = (RadioButton)customDialog.findViewById(R.id.reprobada);
        ninguno = (RadioButton)customDialog.findViewById(R.id.ninguno);
        int i = estado;
        switch (i){
            case 0:
                //hasCode=2;//PENDIENTE
                reprobada.setChecked(true);
                break;
            case 2:
                //hasCode=2;//PENDIENTE
                ninguno.setVisibility(View.GONE);
                aprobada.setVisibility(View.GONE);
                reprobada.setChecked(true);
                break;
            case 4:
                //hasCode=4;//RECHAZADA
                reprobada.setChecked(true);
                break;
            case 3:
               // hasCode=3;//APROBADA
                ninguno.setVisibility(View.GONE);
                aprobada.setChecked(true);
                break;
            case 1:
                // hasCode=3;//APROBADA
                ninguno.setVisibility(View.GONE);
                aprobada.setChecked(true);
                break;
            case 5:
                //hasCode=5;//Ninguno
                ninguno.setChecked(true);
                break;

        }
//   EstadoCotizacionCheck("¿Quiere usted aprobar esta cotización?",0);
        //customDialog.dismiss();

        ninguno.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                customDialog.dismiss();
            }
        });


        aprobada.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                customDialog.dismiss();
                if(estado!=3 && estado!=1 ){
                    EstadoCotizacionCheck("¿Quiere usted aprobar esta cotización?",0,1);
                }

            }
        });
        reprobada.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                customDialog.dismiss();
                EstadoCotizacionCheck("¿Quiere usted rechazar esta cotización?",1,0);
            }
        });


        (customDialog.findViewById(R.id.no)).setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view)
            {
                customDialog.dismiss();
            }
        });



        assert customDialog != null;
        customDialog.show();

    }


    public void EnviarCorreo() {
        customDialog = new Dialog(this);
        customDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        customDialog.setCancelable(true);
        customDialog.setContentView(R.layout.notificacion_01);

        TextView titulo = (TextView) customDialog.findViewById(R.id.mensaje);
        titulo.setText("¿Deseas usted enviar por correo electrónico al cliente la cotización en PDF?");
        (customDialog.findViewById(R.id.si)).setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view)
            {
                customDialog.dismiss();
                new SyncCotizaciones(DetalleHistorial.this,1).execute();
            }
        });
        (customDialog.findViewById(R.id.no)).setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view)
            {
                customDialog.dismiss();
                new SyncCotizaciones(DetalleHistorial.this,0).execute();
            }
        });

        assert customDialog != null;
        customDialog.show();

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.info:
                getDetalleHistorial();
                return true;
            case R.id.borra:
                Borrar();
                break;
            case R.id.editar:
                Guardar();
                break;
            default:
                return super.onOptionsItemSelected(item);
        }
        return super.onOptionsItemSelected(item);
    }


    public void EstadoCotizacionCheck(String msj,final int i,final int ii) {
        customDialog = new Dialog(this);
        customDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        customDialog.setCancelable(false);
        customDialog.setContentView(R.layout.notificacion_01);
        TextView titulo = (TextView) customDialog.findViewById(R.id.mensaje);
        titulo.setText(msj);
        (customDialog.findViewById(R.id.si)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view)
            {
                customDialog.dismiss();
                control.updateEstadoPPPCotiza(codigo.getText().toString(), String.valueOf(i + 3));
                aprobada.setText(estadoCotizacion(control.GetEstadoCotizacion(codigo.getText().toString())));
                estado = i + 3;
                if (estado == 4 || estado == 2) {
                    estus.setVisibility(View.GONE);
                }

                if(estado==3){
                    EnviarCorreo();
                }else{
                    new SyncCotizaciones(DetalleHistorial.this,0).execute();
                }



            }
        });
        (customDialog.findViewById(R.id.no)).setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view)
            {
                customDialog.dismiss();

            }
        });

        assert customDialog != null;
        customDialog.show();

    }

    public void Borrar() {
        customDialog = new Dialog(this);
        customDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        customDialog.setCancelable(false);
        customDialog.setContentView(R.layout.notificacion_01);
        TextView titulo = (TextView) customDialog.findViewById(R.id.mensaje);
        titulo.setText("¿Usted desea borrar esta cotización?");
        ( customDialog.findViewById(R.id.si)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view)
            {
                customDialog.dismiss();
                SQLiteDatabase database1 = control.getWritableDatabase();
                try {
                    database1.execSQL("DELETE FROM  " + Tb_cotizacion_productos.nombreTabla + " WHERE " + Tb_cotizacion_productos.cotizacion + " = " +idx);
                    database1.execSQL("DELETE FROM  " + Tb_Cotizaciones.nombreTabla + " WHERE id = " +idx);
                } catch (Exception e) {
                    System.out.println("Error-Eliminar: " + e.toString());
                }
                Intent intent = new Intent(DetalleHistorial.this,Historial_cotizacion.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
                finish();
            }
        });
        (customDialog.findViewById(R.id.no)).setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view)
            {
                customDialog.dismiss();

            }
        });

        assert customDialog != null;
        customDialog.show();

    }
    int StatusCotizaO(int i){
        int ii =0;
        switch (i){
            case 1:
                ii=2;//PENDIENTE
                break;
            case 3:
                ii=4;//RECHAZADA
                break;
            case 2:
                ii=3;//APROBADA
                break;
            case 0:
                ii=5;//Ninguno
                break;

        }
        return ii;
    }

    public void Guardar() {
        customDialog = new Dialog(this);
        customDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        customDialog.setCancelable(true);
        customDialog.setContentView(R.layout.notificacion_01);

        TextView titulo = (TextView) customDialog.findViewById(R.id.mensaje);
        titulo.setText("¿Desea usted editar esta cotización?");
        (customDialog.findViewById(R.id.si)).setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view)
            {
                customDialog.dismiss();
                Intent i3 = new Intent(DetalleHistorial.this,ActividadPrincipal.class);
                i3.putExtra("edit",1);
                i3.putExtra("coti",codigo.getText().toString().trim());
                i3.putExtra(Tb_Cotizaciones.codigo,idx);
                startActivity(i3);
                finish();

            }
        });
        (customDialog.findViewById(R.id.no)).setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view)
            {
                customDialog.dismiss();

            }
        });

        assert customDialog != null;
        customDialog.show();

    }

}
