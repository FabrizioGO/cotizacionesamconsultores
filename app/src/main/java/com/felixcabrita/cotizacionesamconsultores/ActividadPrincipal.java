package com.felixcabrita.cotizacionesamconsultores;

import android.Manifest;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.location.Location;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.NavigationView;
import android.support.v4.app.ActivityCompat;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.HttpHeaderParser;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.felixcabrita.cotizacionesamconsultores.interfaces.actualizarSpinner;
import com.felixcabrita.cotizacionesamconsultores.models.GuardarContactos;
import com.felixcabrita.cotizacionesamconsultores.net.RetrofitAdapter;
import com.felixcabrita.cotizacionesamconsultores.net.ServicesContact;
import com.felixcabrita.cotizacionesamconsultores.net.model.responses.AllContacts;
import com.felixcabrita.cotizacionesamconsultores.utils.Const;
import com.felixcabrita.cotizacionesamconsultores.utils.Util;
import com.felixcabrita.cotizacionesamconsultores.models.Tb_Clientes;
import com.felixcabrita.cotizacionesamconsultores.models.Tb_cotizacion_productos;
import com.felixcabrita.cotizacionesamconsultores.models.Tb_Cotizaciones;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationServices;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;

public class ActividadPrincipal extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener,View.OnClickListener, GoogleApiClient.OnConnectionFailedListener, GoogleApiClient.ConnectionCallbacks, actualizarSpinner, AdapterView.OnItemSelectedListener {

    private String currentRuc;
    private ServicesContact api;
    private ProgressDialog dialog;
    private ArrayAdapter<String> contactosArrayAdapter;
    private int lugarDireccion;
    private boolean isFirstTimeLoadingSpinner;

    private Call<List<AllContacts>> call;
    private ArrayList<String> ListContactos = new ArrayList<>();
    private ArrayList<AllContacts> ListContactosDestalle = new ArrayList<>();
    private boolean iprine =false;
    private int EstatusEdición =0;
    private String cotizacionID = "";
    private Boolean porporpor = false;
    private SimpleDateFormat Fecha = new SimpleDateFormat("dd/MM/yyyy");
    private SimpleDateFormat Hora = new SimpleDateFormat("HH:mm:ss");
    private String datecrear = Fecha.format(new Date());
    private CondicionVentaAdapter adapterVentas;
    private String time = Hora.format(new Date());
    String nombre[];
    private DecimalFormat convertidor = new DecimalFormat("#,##0.00", DecimalFormatSymbols.getInstance(Locale.ITALIAN));   // CONVERTIDOR FLOAT \\
    BaseDatosSqlite control;
    public static ArrayList<ContentValues> productos = new ArrayList<>();
    public static int flagDescuento = 0;
    private ArrayList<ContentValues> productosEliminados = new ArrayList<>();
    public static int ctd = 0;
    public static float ttl = 0.0f;
    public ArrayList<ContentValues> clientes = new ArrayList<>(),clientesOrigen = new ArrayList<>();
    public ArrayList<ContentValues> condiVenta = new ArrayList<>(),condiVentaOrigen = new ArrayList<>();
    public TextView razon, ruc, date;
    public TextView correo;
    DatePickerDialog datePickerDialog;
    Spinner tiempo, lugar, moneda,revision,icotems;
    ListClienteAdapter adapter;
    ListView listaproduc;
    LinearLayout lyICOTE,idcoti;
    ArrayAdapter pagoAdapter, pagoAdapter1;
    TextView condi_pago, condi_venta,cantidad, total,coti;
    CheckBox transporte;
    //float fflete = 0;/
    EditText flete, observacion,lugarEntrega;
    TextView contacto;
    String codigo = "";
    int sigNumeroCotizacion = 0,convertido = 0;
    String idPago = "18";
    NavigationView navigationView;
    SharedPreferences pref;
    private SharedPreferences.Editor editor;
    String codigoVendedor = "";
    int monedaAnterior;
    public static float porcentajePerceptor = 0;
    public static  int tiponegocio = 1,tipo = 1;
    public static  float valorNatural = 0;
    float vD = 1500,vS = 458.72f;
    GoogleApiClient apiClient;
    private String Latitudi = "0";
    private String Longitudi = "0";
    private Boolean cotrizando = false;
    private Boolean cambioMoneda = false;
    private int Aprobar;
    Spinner spinner;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_principal_cotizacion);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        spinner = (Spinner) findViewById(R.id.spinner_contactos);

        control = new BaseDatosSqlite(this);

        condiVenta = control.GetFullVenta();

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        initProvidedNetwork();

        try {
            EstatusEdición = getIntent().getExtras().getInt("edit");//CUANDO VIENE DE EDICIN
        }catch (Exception e){
            EstatusEdición =0;//CUANDO VIENE DE EDICIN
        }
        spinner.setOnItemSelectedListener(this);

        flagDescuento = 0;
        porcentajePerceptor = 0;
        tiponegocio = 1;
        valorNatural = 1500;
        productos = new ArrayList<>();
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();
        apiClient = new GoogleApiClient.Builder(this)
                .enableAutoManage(this, this)
                .addConnectionCallbacks(this)
                .addApi(LocationServices.API)
                .build();

        navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        pref = PreferenceManager.getDefaultSharedPreferences(this);
        editor = pref.edit();
        View headerView = navigationView.getHeaderView(0);
        TextView tvHeaderName= (TextView) headerView.findViewById(R.id.usuario);
        tvHeaderName.setText(pref.getString("nombre","usuario"));
        ctd = 0;
        ttl = 0.0f;
////        OcultarItem();
        DecimalFormatSymbols dfs = new DecimalFormatSymbols();
        dfs.setDecimalSeparator('.');
        dfs.setGroupingSeparator(',');
        convertidor.setDecimalFormatSymbols(dfs);
        cantidad = (TextView) findViewById(R.id.cantidad);
        total = (TextView)findViewById(R.id.total);
        lyICOTE = (LinearLayout)findViewById(R.id.lyICOTE);
        pagoAdapter = ArrayAdapter.createFromResource(this,R.array.pago,android.R.layout.simple_spinner_item);
        pagoAdapter1 = ArrayAdapter.createFromResource(this,R.array.pago2,android.R.layout.simple_spinner_item);
        pagoAdapter1.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        pagoAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        transporte = (CheckBox) findViewById(R.id.transporte);
        revision = (Spinner) findViewById(R.id.revision);
        contacto = (TextView)findViewById(R.id.contacto_cliente);
        flete = (EditText)findViewById(R.id.flete);
        observacion = (EditText)findViewById(R.id.observacion);
        lugarEntrega = (EditText)findViewById(R.id.lugarEntrega);
        razon = (TextView)findViewById(R.id.razon);
        ruc = (TextView)findViewById(R.id.ruc);
        correo = (TextView) findViewById(R.id.correo_cliente);
        date = (TextView)findViewById(R.id.fecha);
        coti = (TextView)findViewById(R.id.coti);
        idcoti = (LinearLayout) findViewById(R.id.idcoti);
        observacion.setText("");
        condi_pago = (TextView)findViewById(R.id.venta);
        condi_venta = (TextView)findViewById(R.id.pago);
        tiempo = (Spinner)findViewById(R.id.tiemp);
        lugar = (Spinner)findViewById(R.id.lugar);
        moneda = (Spinner)findViewById(R.id.moneda);
        icotems= (Spinner)findViewById(R.id.icotems);
        listaproduc = (ListView)findViewById(R.id.listaproducto);
        codigoVendedor = control.GetCodigoUsuarios();
        date.setText(datecrear);
        monedaAnterior = moneda.getSelectedItemPosition();
        observacion.setVisibility(View.VISIBLE);
        drawer.closeDrawer(GravityCompat.START);
        listaproduc.setAdapter(new adapter_producto(this,productos,monedaAnterior));
        listaproduc.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {
                Accion(position);
            }
        });

        Aprobar = pref.getInt("permiso_aprobar", 0);
        if(Aprobar==1){
            String cotiestados[] = {"Ninguno","Aprobada"};
            ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<String>(this,   android.R.layout.simple_spinner_item, cotiestados);
            spinnerArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item); // The drop down view
            revision.setAdapter(spinnerArrayAdapter);
        }


        Datos d = new Datos(this);
        d.execute();

        if(EstatusEdición==0){
            moneda.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                    //total.setText(((moneda.getSelectedItemPosition()==0)?"US$ ":"S/. ")+ convertidor.format(ttl));
                    Log.i("le france", monedaAnterior+"");
                    if(productos.size()>0){
                        //Toast.makeText(ActividadPrincipal.this,"Moneda anterior: " + monedaAnterior + " Moneda:"+i,Toast.LENGTH_SHORT).show();
                        new conversionMoneda(i,monedaAnterior,ActividadPrincipal.this).execute();
                    }else{
                        monedaAnterior = moneda.getSelectedItemPosition();
                    }

                    valorNatural = i==0? vS:vD;
                }

                @Override
                public void onNothingSelected(AdapterView<?> adapterView) {

                }
            });
        }

        razon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(EstatusEdición==0){
                    initProvidedNetwork();
                    ListadoClientes();
                }else{
                    Toast.makeText(ActividadPrincipal.this,"No puede cambiar de cliente editando la cotización",Toast.LENGTH_SHORT).show();
                }
            }
        });

        condi_pago.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(condi_venta.getText().toString().compareTo("Crédito")==0){
                    ListadoCondicionPago();
                }
            }
        });
        fecha();

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, 1);
        }

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION,}, 1000);
        }
      //  final AlertDialog alert = null;
        final LocationManager manager = (LocationManager) getSystemService( Context.LOCATION_SERVICE );

        if ( !manager.isProviderEnabled( LocationManager.GPS_PROVIDER ) ) {
            AlertNoGps();
        }


        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

            if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                    Manifest.permission.ACCESS_FINE_LOCATION)) {
                // Aquí muestras confirmación explicativa al usuario
                // por si rechazó los permisos anteriormente
            } else {
                ActivityCompat.requestPermissions(
                        this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                        1);
                Location lastLocation =
                        LocationServices.FusedLocationApi.getLastLocation(apiClient);
                updateUI(lastLocation);
            }

            if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                    Manifest.permission.ACCESS_COARSE_LOCATION)) {
                // Aquí muestras confirmación explicativa al usuario
                // por si rechazó los permisos anteriormente
            } else {
                ActivityCompat.requestPermissions(
                        this, new String[]{Manifest.permission.ACCESS_COARSE_LOCATION},
                        1);
                Location lastLocation =
                        LocationServices.FusedLocationApi.getLastLocation(apiClient);
                updateUI(lastLocation);
            }
        }
        Location lastLocation =
                LocationServices.FusedLocationApi.getLastLocation(apiClient);
        updateUI(lastLocation);


        if(EstatusEdición==1){
            cotizacionID = getIntent().getExtras().getString(Tb_Cotizaciones.codigo);
            //Toast.makeText(ActividadPrincipal.this,"Toas:" + cotizacionID,Toast.LENGTH_SHORT).show();
            coti.setText(getIntent().getExtras().getString("coti"));
            idcoti.setVisibility(View.VISIBLE);
        }

    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        switch (id) {
            case R.id.inicio:
                Intent i33 = new Intent(ActividadPrincipal.this, Menu_Principal.class);
                startActivity(i33);
                finish();
                break;
            case R.id.usuari:
                Intent i = new Intent(ActividadPrincipal.this, DatosUsuario.class);
                startActivity(i);
                finish();
                break;
            case R.id.clientes:
                Intent i1 = new Intent(ActividadPrincipal.this, ListaClientes.class);
                startActivity(i1);
                finish();
                break;
            case R.id.cerrar:
                /*AlertDialog.Builder builder = new AlertDialog.Builder(ActividadPrincipal.this);
                builder.setMessage("¿Desea cerrar la sesión?")
                        .setTitle("Notificación")
                        .setCancelable(false)
                        .setPositiveButton("No",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        dialog.cancel();
                                    }
                                })
                        .setNegativeButton("Si",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        Intent intent = new Intent(ActividadPrincipal.this,Inicio.class);
                                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                        startActivity(intent);
                                        finish();
                                    }
                                });
                AlertDialog alert = builder.create();
                alert.show();*/
                Cerrar();
                break;
            case R.id.histo:
                Intent i3 = new Intent(ActividadPrincipal.this,Historial_cotizacion.class);
                i3.putExtra("volver","1");
                startActivity(i3);
                finish();
                break;
            case R.id.cotizar:
                Intent i11 = new Intent(ActividadPrincipal.this,ActividadPrincipal.class);
                startActivity(i11);
                finish();
                break;

        }


        return true;
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }
    private void initProvidedNetwork() {
        RetrofitAdapter.mBaseUrl = Const.URL_V1;
        api = RetrofitAdapter.getApiService();
    }
    private void requestContacts(String ruc) {
        if (Util.isNetworkAvailable(this)) {
            dialog = new ProgressDialog(this);
            dialog.setTitle("Cargando contactos");
            dialog.show();
            call = api.allContacts(ruc);
            call.enqueue(new Callback<List<AllContacts>>() {

                @Override
                public void onResponse(Call<List<AllContacts>> call, retrofit2.Response<List<AllContacts>> response) {
                    dialog.dismiss();
                    Log.d("Test", "Response: " + response.code() + " call: " + call);
                    switch (response.code()) {
                        case 200:
                            if (!response.body().isEmpty()) {
                                //  progressDoalog.dismiss();
                                Log.d("Test", "-->isEmpty()" + call.request().url());
                                ListContactosDestalle = new ArrayList<>();
                                ListContactos = new ArrayList<>();
                                ListContactosDestalle.addAll(response.body());
                                for (AllContacts contacts: ListContactosDestalle) {
                                    ListContactos.add(contacts.getContacto());
                                }

                                new GuardarContactos(control,ListContactosDestalle,currentRuc,1).execute();

                            } else {
                                ListContactos = new ArrayList<>();
                            }
                            contactosArrayAdapter = new ArrayAdapter<>(ActividadPrincipal.this, android.R.layout.simple_spinner_item, ListContactos);
                            contactosArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item); // The drop down view

                            if (EstatusEdición==1){
                                spinner.setOnItemSelectedListener(null);
                                isFirstTimeLoadingSpinner = true;
                                Log.e("ContactoLugarAfterReq",String.valueOf(lugarDireccion));
                                String nombreContacto = control.GetNombreContacto(lugarDireccion);
                                contacto.setText(nombreContacto);
                            }

                            spinner.setAdapter(contactosArrayAdapter);
                            spinner.setSelection(ListContactos.indexOf(contacto.getText().toString()));
                            spinner.setOnItemSelectedListener(ActividadPrincipal.this);

                            break;
                        default:
                            spinner.setOnItemSelectedListener(null);
                            ListContactos = new ArrayList<>();
                            contactosArrayAdapter = new ArrayAdapter<>(ActividadPrincipal.this, android.R.layout.simple_spinner_item, ListContactos);
                            contactosArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item); // The drop down view
                            isFirstTimeLoadingSpinner = true;
                            spinner.setAdapter(contactosArrayAdapter);
                            spinner.setSelection(ListContactos.indexOf(contacto.getText().toString()));
                            spinner.setOnItemSelectedListener(ActividadPrincipal.this);
                            Log.d("Test", "onResponse: El codigo no es 200, ha ocurrido un error");
                    }
                }

                @Override
                public void onFailure(Call<List<AllContacts>> call, Throwable t) {
                    dialog.dismiss();
                    Log.d("Test", "Throwable: " + t.getCause() + " call: " + call.toString());
                }
            });
        }else{
            new GetContactos(control,currentRuc).execute();
        }
    }

    @Override
    public void actualizarInterfz(int posi) {
        AllContacts contact = ListContactosDestalle.get(posi);
        correo.setText(contact.getCorreo());
        contacto.setText(contact.getContacto());
        lugarDireccion = contact.getValue();

        //TODO: Cambiar condiciones
        int condVentaPosition = contact.getCondVenta() - 1;
        if (!contact.getCondVentaText().isEmpty()){
            condi_venta.setText(contact.getCondVentaText());
        }else{
            condi_venta.setText(condVentaPosition == 0 ? "Contado" : "Crédito");
        }

        idPago = String.valueOf(contact.getCondPago());
        if (idPago.equalsIgnoreCase("18")){
            condi_pago.setText("Contado");
        }else {
            condi_pago.setText(control.GetCondicionPago(idPago));
        }
    }

    //ESPACIO PARA LA TAREA ASYN DE LA CARGA DE LA COTIZACIÖN
    private class DatosCotizcionEdit extends AsyncTask<Void,Void,Void> {


        ProgressDialog pd;
        Context cont;
        String id;
        ContentValues datosTemporeales = new ContentValues();
        String contac = "";
        DatosCotizcionEdit(Context c,String _id){
            try{
                id = _id;
                cont = c;
                pd =  new ProgressDialog(c);
                pd.setIndeterminate(true);
                pd.setMessage("Cargando datos de la cotizacion");
                pd.setCancelable(false);
                pd.show();
            }catch (Exception e){
                System.out.println("Error1: "+e.toString());
            }

        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

        }

        @Override
        protected Void doInBackground(Void... voids) {
            datosTemporeales = control.GetCotizacion(id);
            Log.i("EXTRAN", getNumberI(datosTemporeales.get(Tb_Clientes.tipoExtaNa).toString(),1)+"");
            productos = control.GetItemsCotizacionEdit(id,getNumberI(datosTemporeales.get(Tb_Clientes.tipoExtaNa).toString(),1)==2);
            Log.i("PRODUCTOS NUM", productos.size()+"");
            for(int i=0;i<productos.size();i++){
                ctd += (productos.get(i).getAsInteger(Tb_cotizacion_productos.cantidad));
                ttl += (productos.get(i).getAsFloat(Tb_cotizacion_productos.total));
                System.out.println("Codigo CorizacionProdi: " +productos.get(i).get(Tb_cotizacion_productos.cotizacion).toString());
            }
            contac = control.GetCliente(datosTemporeales.getAsString(Tb_Cotizaciones.cliente)).getAsString(Tb_Clientes.contacto);
            return null;
        }

        @Override
        protected void onPostExecute(Void integer) {
            super.onPostExecute(integer);
            razon.setText(datosTemporeales.get(Tb_Clientes.razon_social).toString());
            currentRuc = datosTemporeales.get(Tb_Clientes.ruc).toString();
            ruc.setText(datosTemporeales.get(Tb_Clientes.ruc).toString());
            correo.setText(datosTemporeales.get(Tb_Clientes.correo).toString());
            codigo = datosTemporeales.get(Tb_Clientes.ruc).toString();
            if (datosTemporeales.containsKey(Tb_Cotizaciones.lugar_direccion)){
                lugarDireccion = Integer.parseInt(datosTemporeales.get(Tb_Cotizaciones.lugar_direccion).toString());
                Log.e("ContactoLugarDireccion",String.valueOf(lugarDireccion));
                String nombreContacto = control.GetNombreContacto(lugarDireccion);
                if (nombreContacto.isEmpty())
                    initProvidedNetwork();
                else
                    contacto.setText(nombreContacto);
                Log.e("LugarDireccion",String.valueOf(lugarDireccion));
            }else {
                contacto.setText(datosTemporeales.get(Tb_Clientes.contacto).toString());
            }
            correo.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                        final Dialog dialog = new Dialog(ActividadPrincipal.this);
                        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                        dialog.setCancelable(true);
                        dialog.setContentView(R.layout.dialog_cambiar_correo);
                        final EditText editCorreo = (EditText) dialog.findViewById(R.id.editdialogCorreo);
                        editCorreo.setText(correo.getText().toString().trim());
                        TextView accionCambiar = (TextView) dialog.findViewById(R.id.cambiar);
                        accionCambiar.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                control.updateCorreoCliente(ruc.getText().toString(), editCorreo.getText().toString().trim());
                                correo.setText(editCorreo.getText().toString().trim());
                                CambiarDatoClienteService(editCorreo.getText().toString().trim(), ruc.getText().toString());
                                dialog.dismiss();
                            }
                        });
                        dialog.show();


                }
            });
            contacto.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                        final Dialog dialog = new Dialog(ActividadPrincipal.this);
                        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                        dialog.setCancelable(true);
                        dialog.setContentView(R.layout.dialog_cambiar_correo);
                        final EditText editCorreo = (EditText) dialog.findViewById(R.id.editdialogCorreo);
                        editCorreo.setText(contacto.getText().toString().trim());
                        TextView accionCambiar = (TextView) dialog.findViewById(R.id.cambiar);
                        accionCambiar.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                control.updateContactoCliente(ruc.getText().toString(), editCorreo.getText().toString().trim());
                                contacto.setText(editCorreo.getText().toString().trim());
                                CambiarDatoClienteService(editCorreo.getText().toString().trim(), ruc.getText().toString());
                                dialog.dismiss();
                            }
                        });
                        dialog.show();


                }
            });
            requestContacts(currentRuc);
            tipo = getNumberI(datosTemporeales.get(Tb_Clientes.tipoExtaNa).toString(),1);
            tiponegocio = getNumberI(datosTemporeales.get(Tb_Clientes.tiponegocio).toString(),1)-1;

            if (datosTemporeales.get(Tb_Cotizaciones.estadoSynco).toString().equalsIgnoreCase("2") || datosTemporeales.get(Tb_Cotizaciones.estadoSynco).toString().equalsIgnoreCase("0")) {
                String cod_venta = datosTemporeales.getAsInteger(Tb_Cotizaciones.cond_pago) == 2 ? "Crédito" : "Contado";
                condi_venta.setText(cod_venta);
                idPago = datosTemporeales.getAsString(Tb_Cotizaciones.cond_venta);
                String cod_pago = cod_venta.equalsIgnoreCase("Crédito") ? control.GetCondicionPago(idPago) : "Contado";
                condi_pago.setText(cod_pago);
            }else {
                String cod_venta = datosTemporeales.getAsInteger(Tb_Cotizaciones.cond_venta) == 2 ? "Crédito" : "Contado";
                condi_venta.setText(cod_venta);
                idPago = datosTemporeales.getAsString(Tb_Cotizaciones.cond_pago);
                String cod_pago = cod_venta.equalsIgnoreCase("Crédito") ? control.GetCondicionPago(idPago) : "Contado";
                condi_pago.setText(cod_pago);
            }

            date.setText(datosTemporeales.getAsString(Tb_Cotizaciones.fecha));
            tiempo.setSelection(datosTemporeales.getAsInteger(Tb_Cotizaciones.tiempo_validez)-1);
            transporte.setChecked(datosTemporeales.getAsInteger(Tb_Cotizaciones.flete)==1);
            lugar.setSelection(datosTemporeales.getAsInteger(Tb_Cotizaciones.lugar_cotizacion)-1);
            int moned = datosTemporeales.getAsInteger(Tb_Cotizaciones.moneda)==2?0:1;
            moneda.setSelection(moned, false);
            monedaAnterior = moneda.getSelectedItemPosition();
            moneda.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                    //total.setText(((moneda.getSelectedItemPosition()==0)?"US$ ":"S/. ")+ convertidor.format(ttl));
                    Log.i("le france", monedaAnterior+"");
                    if(productos.size()>0){
                        //Toast.makeText(ActividadPrincipal.this,"Moneda anterior: " + monedaAnterior + " Moneda:"+i,Toast.LENGTH_SHORT).show();
                        new conversionMoneda(i,monedaAnterior,ActividadPrincipal.this).execute();
                    }else{
                        monedaAnterior = moneda.getSelectedItemPosition();
                    }

                    valorNatural = i==0? vS:vD;
                }

                @Override
                public void onNothingSelected(AdapterView<?> adapterView) {

                }
            });

            observacion.setText(datosTemporeales.getAsString(Tb_Cotizaciones.observaciones));
            lugarEntrega.setText(datosTemporeales.getAsString(Tb_Cotizaciones.lugar));
            revision.setSelection(0);
            cantidad.setText(""+ctd);
            icotems.setSelection(datosTemporeales.getAsInteger(Tb_Cotizaciones.icotem));
            porcentajePerceptor = getNumberF(datosTemporeales.get(Tb_Clientes.por_perceptor).toString(),0);
            porcentajePerceptor= porcentajePerceptor>0?porcentajePerceptor/100:0;
            float tota = (ttl + (ttl*(tipo==2?0:porcentajePerceptor)));
            total.setText(((moneda.getSelectedItemPosition()==0)?"US$ ":"S/. ") +" "+ convertidor.format(tota));
            listaproduc.setAdapter(new adapter_producto(cont,productos,moned));
            sigNumeroCotizacion = Integer.parseInt(cotizacionID);
            new SyncProductos(cont).execute();
            pd.dismiss();
        }

    }

    ///////

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        if(!cotrizando){
            if (ActivityCompat.checkSelfPermission(ActividadPrincipal.this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED &&
                    ActivityCompat.checkSelfPermission(ActividadPrincipal.this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                // TODO: Consider calling
                //    ActivityCompat#requestPermissions
                // here to request the missing permissions, and then overriding
                //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                //                                          int[] grantResults)
                // to handle the case where the user grants the permission. See the documentation
                // for ActivityCompat#requestPermissions for more details.

            }
            Location lastLocation =
                    LocationServices.FusedLocationApi.getLastLocation(apiClient);
            updateUI(lastLocation);
        }

    }
    public void AlertNoGps() {
        customDialog = new Dialog(this);
        customDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        customDialog.setCancelable(false);
        customDialog.setContentView(R.layout.notificacion_01_gps);

        TextView titulo = (TextView) customDialog.findViewById(R.id.mensaje);
        TextView si = (TextView) customDialog.findViewById(R.id.si);
        si.setText("Aceptar");
        titulo.setText("El sistema GPS esta desactivado, ¿Desea activarlo?\nEsto puede ocasionar problemas con las coordenadas");
        (customDialog.findViewById(R.id.si)).setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view)
            {
                customDialog.dismiss();
                startActivity(new Intent(android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS));

            }
        });
        assert customDialog != null;
        customDialog.show();

    }
    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    protected void onResume() {
        super.onResume();

    }

    private class Datos extends AsyncTask<Void,Void,Integer> {

        ProgressDialog pd;
        Context cont;
        Datos(Context c){
            try{
                cont = c;
                pd =  new ProgressDialog(c);
                pd.setIndeterminate(true);
                pd.setMessage("Cargando Datos");
                pd.setCancelable(false);
                pd.show();
            }catch (Exception e){
                System.out.println("Error1: "+e.toString());
            }

        }


        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected Integer doInBackground(Void... voids) {
            if(control.DataSync(Tb_Clientes.nombreTabla)<1) {
                try {
                    URL cliente = new URL(Const.URL_V1 + "clientes");
                    HttpURLConnection conCliente = (HttpURLConnection) cliente.openConnection();
                    conCliente.setRequestProperty("x-api-key", pref.getString("key", ""));
                    if (conCliente.getResponseCode() == HttpURLConnection.HTTP_OK) {
                        InputStream inputStreamResponse = conCliente.getInputStream();
                        String linea;
                        StringBuilder respuestaCadena = new StringBuilder();
                        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStreamResponse, "UTF-8"));
                        while ((linea = bufferedReader.readLine()) != null) {
                            respuestaCadena.append(linea);
                        }
                        // JSONObject datos = new JSONObject(respuestaCadena.toString());
                        JSONArray jsonArray = new JSONArray(respuestaCadena.toString());
                        control.CapturaDatosClientesJSONV2(jsonArray);
                        System.out.println("ACTUALIZANDO CLIENTES..");
                    }
                } catch (IOException e) {
                    System.out.println("Error1: " + e.toString());
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            clientes = control.GetFullClientes();
            clientesOrigen = clientes;
            condiVenta = control.GetFullVenta();
            condiVentaOrigen = condiVenta;
            sigNumeroCotizacion = control.GetIdCotizacion() + 1;
            /* nombre = new String[clientes.size()];
            for (int i = 0;i<nombre.length;i++){
                nombre[i] = clientes.get(i).get(Tb_Clientes.razon_social).toString();
            }*/
            return 0;
        }

        @Override
        protected void onPostExecute(Integer integer) {
            super.onPostExecute(integer);
            pd.dismiss();
            adapterVentas = new CondicionVentaAdapter(cont, condiVenta);
            adapter = new ListClienteAdapter(cont,clientes);
            if(EstatusEdición==1){
                cotizacionID = getIntent().getExtras().getString(Tb_Cotizaciones.codigo);
                Log.i("DATOS COTI", cotizacionID);
                new DatosCotizcionEdit(cont, cotizacionID).execute();
            }else{
                new SyncProductos(cont).execute();
            }

        }

    }

    @Override
    public void onClick(View view) {
        int id = view.getId();
        switch (id){
            case R.id.addProd:
                if(!codigo.isEmpty()){
                    editor.putInt("Services",0);
                    editor.apply();
                    Intent i = new Intent(ActividadPrincipal.this,DetalleProductos.class);
                    i.putExtra("descuento", StatusCotiza(revision.getSelectedItemPosition()));
                    i.putExtra("estatus", 0);
                    i.putExtra("flete", 1);
                    i.putExtra("moneda",moneda.getSelectedItemPosition());
                    i.putExtra("tipo",tipo);
                    i.putExtra("EstatusEdición",EstatusEdición);
                    i.putExtra("convertido",convertido);
                    i.putExtra(Tb_cotizacion_productos.cotizacion, sigNumeroCotizacion);
                    Log.i("SIG", sigNumeroCotizacion+"");
                    startActivity(i);
                }else{
                    Toast.makeText(ActividadPrincipal.this,"Debe elegir un cliente",Toast.LENGTH_LONG).show();
                }

                break;
            case R.id.enviar:
                if(!ruc.getText().toString().isEmpty() && (!contacto.getText().toString().isEmpty() || ((spinner.getAdapter()!=null)&&(spinner.getAdapter().getCount() > 0) ))) {
                    if (productos.size() > 0) {
                        if (date.getText().toString().compareTo("Seleccionar fecha") != 0) {
                            if (tipo == 2) {
                                if (icotems.getSelectedItemPosition() > 0) {
                                    if (!lugarEntrega.getText().toString().trim().isEmpty()) {
                                        porporpor = true;
                                        Guardar();
                                    } else {
                                        Toast.makeText(ActividadPrincipal.this, "Debe colocar un lugar de entrega", Toast.LENGTH_LONG).show();
                                    }
                                } else {
                                    Toast.makeText(ActividadPrincipal.this, "Debe escojer un ICOTERMS", Toast.LENGTH_LONG).show();
                                }
                            } else {
                                if (!lugarEntrega.getText().toString().trim().isEmpty()) {
                                    porporpor = true;
                                    Guardar();
                                } else {
                                    Toast.makeText(ActividadPrincipal.this, "Debe colocar un lugar de entrega", Toast.LENGTH_LONG).show();
                                }
                            }
                        } else {
                            Toast.makeText(ActividadPrincipal.this, "Debe colocar una fecha", Toast.LENGTH_LONG).show();
                        }

                    } else {
                        Toast.makeText(ActividadPrincipal.this, "No hay productos agregados", Toast.LENGTH_LONG).show();
                    }
                }else{
                    Toast.makeText(ActividadPrincipal.this, ruc.getText().toString().isEmpty()?"Debe escoger un cliente":"Debe colocar un contacto", Toast.LENGTH_LONG).show();
                }
                break;
            case R.id.cancelar:
                if(productos.size()>0){
                    Cancelar();
                }else{
                    Intent i = new Intent(ActividadPrincipal.this,Menu_Principal.class);
                    startActivity(i);
                    finish();
                }
                break;
        }
    }
//GUARDAR
    private class GuardandoCotizacion extends AsyncTask<Void,Void,Integer> {

        ProgressDialog pd;
        int ii = 0;
        GuardandoCotizacion(Context c, int i){
            try{
                cotrizando = true;
                ii =i;
                pd =  new ProgressDialog(c);
                pd.setIndeterminate(true);
                pd.setMessage("Guardando cotización");
                pd.setCancelable(false);
                pd.show();
            }catch (Exception e){
                System.out.println("Error1: "+e.toString());
            }

        }


        @Override
        protected void onPreExecute() {
            super.onPreExecute();

        }

        @Override
        protected Integer doInBackground(Void... voids) {
            try{
                SQLiteDatabase database = control.getWritableDatabase();
                float ssy= 0, igvvv =0;
                Log.i("PRODUCTOS A GUARDAR", productos.size()+"");
                for(int i = 0;i<productos.size();i++){
                    ssy += productos.get(i).getAsFloat(Tb_cotizacion_productos.subtotal);
                    igvvv += productos.get(i).getAsFloat(Tb_cotizacion_productos.igv);
                }
                database.insert(Tb_Cotizaciones.nombreTabla, null, datosCotizacion(ssy,igvvv));//MODIFICADO AHORITA
                for (int i = 0; i < productos.size();i++){
                    int iii = Integer.parseInt(productos.get(i).get(Tb_cotizacion_productos.cantidad).toString());
                    //control.updateComprometidoProducto(productos.get(i).get(Tb_cotizacion_productos.producto).toString(),StatusCotiza(hasCode),hasEstado,database);//MODIFICADO AHORITA
                    ContentValues datos = new ContentValues();
                    Log.i("ID", sigNumeroCotizacion+" "+productos.get(i).getAsString(Tb_cotizacion_productos.cotizacion));
                    //System.out.println("COTI: Estoy insertando: " + productos.get(i).getAsString(Tb_cotizacion_productos.producto) + " Action: " + productos.get(i).getAsString(Tb_cotizacion_productos.action));
                    datos.put(Tb_cotizacion_productos.cotizacion,productos.get(i).getAsString(Tb_cotizacion_productos.cotizacion));
                    datos.put(Tb_cotizacion_productos.producto,productos.get(i).getAsString(Tb_cotizacion_productos.producto));
                    datos.put(Tb_cotizacion_productos.producto_nombre,productos.get(i).getAsString(Tb_cotizacion_productos.producto_nombre));
                    datos.put(Tb_cotizacion_productos.cantidad,productos.get(i).getAsString(Tb_cotizacion_productos.cantidad));
                    datos.put(Tb_cotizacion_productos.subtotal,productos.get(i).getAsString(Tb_cotizacion_productos.subtotal));
                    datos.put(Tb_cotizacion_productos.igv,productos.get(i).getAsString(Tb_cotizacion_productos.igv));
                    datos.put(Tb_cotizacion_productos.precio,productos.get(i).getAsString(Tb_cotizacion_productos.precio));
                    datos.put(Tb_cotizacion_productos.total,productos.get(i).getAsString(Tb_cotizacion_productos.total));
                    datos.put(Tb_cotizacion_productos.cunio,productos.get(i).getAsString(Tb_cotizacion_productos.cunio));
                    datos.put(Tb_cotizacion_productos.produccion,productos.get(i).getAsString(Tb_cotizacion_productos.produccion));
                    datos.put(Tb_cotizacion_productos.requerido,productos.get(i).getAsString(Tb_cotizacion_productos.requerido));
                    datos.put(Tb_cotizacion_productos.action,productos.get(i).getAsString(Tb_cotizacion_productos.action));
                    database.insert(Tb_cotizacion_productos.nombreTabla, null, datos);
                }
            }catch (Exception e){
                System.out.println("Error-InsertCotizacio " + e.toString());
                e.printStackTrace();
            }
            return 0;
        }


        @Override
        protected void onPostExecute(Integer integer) {
            super.onPostExecute(integer);
            pd.dismiss();
            EnviarCorreo(0);

        }

    }
//MODIFICAR

    private class ModificarCotización extends AsyncTask<Void,Void,Integer> {

        ProgressDialog pd;
        int ii = 0;
        String id = "";
        ModificarCotización(Context c, int i,String _id){
            try{
                id = _id;
                cotrizando = true;
                ii =i;
                pd =  new ProgressDialog(c);
                pd.setIndeterminate(true);
                pd.setMessage("Guardando cambios de la cotización");
                pd.setCancelable(false);
                pd.show();
            }catch (Exception e){
                System.out.println("Error1: "+e.toString());
            }

        }


        @Override
        protected void onPreExecute() {
            super.onPreExecute();

        }

        @Override
        protected Integer doInBackground(Void... voids) {
            try{
                SQLiteDatabase database = control.getWritableDatabase();
                float ssy= 0, igvvv =0;
                Log.i("CANT PROCDUCTOS", productos.size()+"");
                for(int i = 0;i<productos.size();i++){
                    ssy += productos.get(i).getAsFloat(Tb_cotizacion_productos.subtotal);
                    igvvv += productos.get(i).getAsFloat(Tb_cotizacion_productos.igv);
                }
                sigNumeroCotizacion =Integer.parseInt(cotizacionID.trim());
                database.update(Tb_Cotizaciones.nombreTabla, datosCotizacion(ssy,igvvv),"id = "+id.trim(),null);//MODIFICADO AHORITA
                for (int i = 0; i < productos.size();i++){
                    //control.updateComprometidoProducto(productos.get(i).get(Tb_cotizacion_productos.producto).toString(),StatusCotiza(hasCode),hasEstado,database);//MODIFICADO AHORITA
                    ContentValues datos = new ContentValues();
                    String codigoPro =productos.get(i).getAsString(Tb_cotizacion_productos.producto);
                    database.delete(Tb_cotizacion_productos.nombreTabla,Tb_cotizacion_productos.cotizacion + " = "+id.trim()+" AND TRIM(" + Tb_cotizacion_productos.producto + ") = TRIM('"+codigoPro+"') AND "+Tb_cotizacion_productos.idproduc +" = "+productos.get(i).getAsString(Tb_cotizacion_productos.idproduc),null);
                    //System.out.println("COTI: Estoy insertando: " + productos.get(i).getAsString(Tb_cotizacion_productos.producto) + " Action: " + productos.get(i).getAsString(Tb_cotizacion_productos.subtotal));
                    datos.put(Tb_cotizacion_productos.cotizacion,Integer.parseInt(cotizacionID.trim()));
                    datos.put(Tb_cotizacion_productos.producto,codigoPro.trim());
                    datos.put(Tb_cotizacion_productos.producto_nombre,productos.get(i).getAsString(Tb_cotizacion_productos.producto_nombre));
                    datos.put(Tb_cotizacion_productos.idproduc,productos.get(i).getAsString(Tb_cotizacion_productos.idproduc));
                    datos.put(Tb_cotizacion_productos.cantidad,productos.get(i).getAsString(Tb_cotizacion_productos.cantidad));
                    datos.put(Tb_cotizacion_productos.subtotal,productos.get(i).getAsString(Tb_cotizacion_productos.subtotal));
                    datos.put(Tb_cotizacion_productos.igv,productos.get(i).getAsString(Tb_cotizacion_productos.igv));
                    datos.put(Tb_cotizacion_productos.precio,productos.get(i).getAsString(Tb_cotizacion_productos.precio));
                    Log.i("TOTAL TOTAL", productos.get(i).getAsString(Tb_cotizacion_productos.total));
                    datos.put(Tb_cotizacion_productos.total,productos.get(i).getAsString(Tb_cotizacion_productos.total));
                    datos.put(Tb_cotizacion_productos.cunio,productos.get(i).getAsString(Tb_cotizacion_productos.cunio));
                    datos.put(Tb_cotizacion_productos.produccion,productos.get(i).getAsString(Tb_cotizacion_productos.produccion));
                    datos.put(Tb_cotizacion_productos.requerido,productos.get(i).getAsString(Tb_cotizacion_productos.requerido));
                    datos.put(Tb_cotizacion_productos.action,productos.get(i).getAsString(Tb_cotizacion_productos.action));
                    if(productos.get(i).getAsString(Tb_cotizacion_productos.action).equals("3")&&cambioMoneda){
                        datos.put(Tb_cotizacion_productos.action,2);
                    }
                    database.insert(Tb_cotizacion_productos.nombreTabla, null, datos);
                }

                if(productosEliminados.size()>0){
                    for (int i = 0; i < productosEliminados.size();i++){
                        ContentValues datos = new ContentValues();
                        String codigoPro =productosEliminados.get(i).getAsString(Tb_cotizacion_productos.producto);
                        database.delete(Tb_cotizacion_productos.nombreTabla,Tb_cotizacion_productos.cotizacion +
                                " = "+id.trim()+" AND TRIM(" + Tb_cotizacion_productos.producto + ") = TRIM('"+codigoPro+"') AND "+
                                Tb_cotizacion_productos.idproduc +" = "+productosEliminados.get(i).getAsString(Tb_cotizacion_productos.idproduc),null);
                        System.out.println("COTI: Estoy Eliminando: " + productosEliminados.get(i).getAsString(Tb_cotizacion_productos.producto) + " Action: " + productosEliminados.get(i).getAsString(Tb_cotizacion_productos.subtotal));
                        datos.put(Tb_cotizacion_productos.cotizacion,Integer.parseInt(cotizacionID.trim()));
                        datos.put(Tb_cotizacion_productos.producto,codigoPro.trim());
                        datos.put(Tb_cotizacion_productos.producto_nombre,productosEliminados.get(i).getAsString(Tb_cotizacion_productos.producto_nombre));
                        datos.put(Tb_cotizacion_productos.cantidad,productosEliminados.get(i).getAsString(Tb_cotizacion_productos.cantidad));
                        datos.put(Tb_cotizacion_productos.subtotal,productosEliminados.get(i).getAsString(Tb_cotizacion_productos.subtotal));
                        datos.put(Tb_cotizacion_productos.igv,productosEliminados.get(i).getAsString(Tb_cotizacion_productos.igv));
                        datos.put(Tb_cotizacion_productos.precio,productosEliminados.get(i).getAsString(Tb_cotizacion_productos.precio));
                        datos.put(Tb_cotizacion_productos.total,productosEliminados.get(i).getAsString(Tb_cotizacion_productos.total));
                        datos.put(Tb_cotizacion_productos.cunio,productosEliminados.get(i).getAsString(Tb_cotizacion_productos.cunio));
                        datos.put(Tb_cotizacion_productos.produccion,productosEliminados.get(i).getAsString(Tb_cotizacion_productos.produccion));
                        datos.put(Tb_cotizacion_productos.requerido,productosEliminados.get(i).getAsString(Tb_cotizacion_productos.requerido));
                        datos.put(Tb_cotizacion_productos.action,"0");
                        datos.put(Tb_cotizacion_productos.idproduc,productosEliminados.get(i).getAsString(Tb_cotizacion_productos.idproduc));
                        database.insert(Tb_cotizacion_productos.nombreTabla, null, datos);
                    }
                }
            }catch (Exception e){
                System.out.println("Error-InsertCotizacio " + e.toString());
                e.printStackTrace();
            }
            return 0;
        }


        @Override
        protected void onPostExecute(Integer integer) {
            super.onPostExecute(integer);
            pd.dismiss();
            EnviarCorreo(1);

        }

    }

    void fecha(){
        date.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // calender class's instance and get current date , month and year from calender
                final Calendar c = Calendar.getInstance();
                final int mYear = c.get(Calendar.YEAR); // current year
                final int mMonth = c.get(Calendar.MONTH); // current month
                final int mDay = c.get(Calendar.DAY_OF_MONTH); // current day
                // date picker dialog


                datePickerDialog = new DatePickerDialog(ActividadPrincipal.this,R.style.DialogTheme,
                        new DatePickerDialog.OnDateSetListener() {
                            @Override
                            public void onDateSet(DatePicker view, int year,
                                                  int monthOfYear, int dayOfMonth) {
                                // set day of month , month and year value in the edit text
                                String fe = (num(dayOfMonth) + "/"+ num(monthOfYear +1) + "/" + year);
                               // c.set;
                                if((dayOfMonth<mDay && (monthOfYear)==mMonth) || (monthOfYear)<mMonth || year<mYear){
                                    Toast.makeText(ActividadPrincipal.this, "La fecha debe ser mayor al dia de hoy", Toast.LENGTH_SHORT).show();
                                }else{
                                    date.setText(fe);
                                }
                            }
                        }, mYear, mMonth, mDay);
                datePickerDialog.show();
            }
        });
    }

    String num(int i){
        return (i<10)?"0"+i:""+i;
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        //AGREGAR MONEDA
        final LocationManager manager = (LocationManager) getSystemService( Context.LOCATION_SERVICE );

        if (manager!=null && !manager.isProviderEnabled( LocationManager.GPS_PROVIDER ) ) {
            AlertNoGps();
        }

        porcentajePerceptor = tiponegocio==0?porcentajePerceptor:(( (ttl + (ttl* porcentajePerceptor)) > valorNatural)?0.02f:0);
        listaproduc.setAdapter(new adapter_producto(this,productos,moneda.getSelectedItemPosition()));
        cantidad.setText(""+ctd);
        float tota;
        tota = (ttl + (ttl*(tipo==2?0:porcentajePerceptor)));
        total.setText(((moneda.getSelectedItemPosition()==0)?"US$ ":"S/. ") +" "+ convertidor.format(tota));

    }

    @Override
    public void onBackPressed() {

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        }

    }

    /*private class SyncCotizacionesa extends AsyncTask<Void,Void,Integer> {

        int envio = 0;
        Context cont;
        String urls = "http://ventas.novatecpe.com:8080/novatec/api/v1/cotizaciones";
        public SyncCotizacionesa(Context c,int i){
            envio = i;
            System.out.println("ACTIVADO: ");
            try{
                cont = c;
            }catch (Exception e){
                System.out.println("Error1: "+e.toString());
            }

        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected Integer doInBackground(Void... voids) {
            ConnectivityManager connMgr = (ConnectivityManager)
                    getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();
            if (networkInfo != null && networkInfo.isConnected()) {
                if(control.CheckDataSyncCotiza(0)) {
                    try {
                        String selectQuery = "SELECT id,"+Tb_Cotizaciones.fecha+","+Tb_Cotizaciones.fecha_registro+
                                ","+Tb_Cotizaciones.cliente+","+Tb_Cotizaciones.vendedor+","+Tb_Cotizaciones.cond_pago+","+
                                Tb_Cotizaciones.cond_venta+","+Tb_Cotizaciones.flete+","+Tb_Cotizaciones.tiempo_validez+
                                ","+Tb_Cotizaciones.lugar_cotizacion+","+Tb_Cotizaciones.moneda+","+Tb_Cotizaciones.subtotal+
                                ","+Tb_Cotizaciones.monto_igv+","+Tb_Cotizaciones.monto_total+","+Tb_Cotizaciones.observaciones+
                                ","+Tb_Cotizaciones.por_revision+","+Tb_Cotizaciones.items+","+Tb_Cotizaciones.estadoSynco+
                                ","+Tb_Cotizaciones.estado+","+Tb_Cotizaciones.porcentaje+","+Tb_Cotizaciones.latirud+","+Tb_Cotizaciones.longitud+
                                ","+Tb_Cotizaciones.lugar+","+Tb_Cotizaciones.icotem+" FROM "+Tb_Cotizaciones.nombreTabla +" WHERE "+Tb_Cotizaciones.estadoSynco + " = 0";

                        SQLiteDatabase database = control.getWritableDatabase();
                        Cursor cursor = database.rawQuery(selectQuery, null);
                        if (cursor.moveToFirst()) {
                            do {
                                try {
                                    HttpClient httpclient = new DefaultHttpClient();
                                    HttpPost httppost = new HttpPost(urls);
                                    httppost.setHeader("Content-Type", "multipart/form-data");
                                    httppost.setHeader("x-api-key", pref.getString("key", ""));
                                    List<NameValuePair> params = new ArrayList<>();
                                    String id = cursor.getString(0);
                                    params.add(new BasicNameValuePair("fecha", cursor.getString(1)));
                                    params.add(new BasicNameValuePair("fecha_registro",  cursor.getString(2)));
                                    params.add(new BasicNameValuePair("ruc", cursor.getString(3)));
                                    params.add(new BasicNameValuePair("vendedor", cursor.getString(4)));
                                    params.add(new BasicNameValuePair("cond_pago", cursor.getString(6)));
                                    params.add(new BasicNameValuePair("cond_venta", cursor.getString(5)));
                                    params.add(new BasicNameValuePair("flete", cursor.getString(7)));
                                    params.add(new BasicNameValuePair("tiempo_validez", cursor.getString(8)));
                                    params.add(new BasicNameValuePair("lugar_cotizacion", cursor.getString(9)));
                                    params.add(new BasicNameValuePair("moneda", cursor.getString(10)));
                                    params.add(new BasicNameValuePair("subtotal",cursor.getString(11)));
                                    params.add(new BasicNameValuePair("monto_igv",cursor.getString(12)));
                                    params.add(new BasicNameValuePair("monto_total", cursor.getString(13)));
                                    params.add(new BasicNameValuePair("estado",cursor.getString(18)));
                                    params.add(new BasicNameValuePair("observaciones", cursor.getString(14)));
                                    params.add(new BasicNameValuePair("monto_percepcion", cursor.getString(19)));
                                    params.add(new BasicNameValuePair("latitud", cursor.getString(20)));
                                    params.add(new BasicNameValuePair("longitud", cursor.getString(21)));
                                    System.out.println("Cotizacion holahola [" +cursor.getString(20) + ","+cursor.getString(21)+"]" );
                                    params.add(new BasicNameValuePair("lugar_entrega", cursor.getString(22)));
                                    params.add(new BasicNameValuePair("icoterms", cursor.getString(23)));
                                    String selectQuery1 = "SELECT " + Tb_cotizacion_productos.producto + "," + Tb_cotizacion_productos.cantidad + ","
                                            + Tb_cotizacion_productos.precio + "," + Tb_cotizacion_productos.produccion + "," +
                                            Tb_cotizacion_productos.cunio + "," + Tb_cotizacion_productos.subtotal + "," + Tb_cotizacion_productos.igv +
                                            "," + Tb_cotizacion_productos.total + "," + Tb_cotizacion_productos.color +","+Tb_cotizacion_productos.producto_nombre+
                                            ","+Tb_cotizacion_productos.requerido+ ","+Tb_cotizacion_productos.action+ ","+Tb_cotizacion_productos.idproduc+ " FROM " + Tb_cotizacion_productos.nombreTabla + " WHERE " +
                                            Tb_cotizacion_productos.cotizacion + " = " + cursor.getString(0);

                                    Cursor cursor1 = database.rawQuery(selectQuery1, null);
                                    try {
                                        if (cursor1.moveToFirst()) {
                                            do {
                                                params.add(new BasicNameValuePair("productos[codigo][]", cursor1.getString(0)));
                                                params.add(new BasicNameValuePair("productos[producto][]", cursor1.getString(9)));
                                                params.add(new BasicNameValuePair("productos[cantidad][]", cursor1.getString(1)));
                                                params.add(new BasicNameValuePair("productos[precio][]", cursor1.getString(2)));
                                                params.add(new BasicNameValuePair("productos[para][]", cursor1.getString(3)));
                                                params.add(new BasicNameValuePair("productos[cunio][]", cursor1.getString(4)));
                                                params.add(new BasicNameValuePair("productos[subtotal][]", cursor1.getString(5)));
                                                params.add(new BasicNameValuePair("productos[igv][]", cursor1.getString(6)));
                                                params.add(new BasicNameValuePair("productos[total][]", cursor1.getString(7)));
                                                params.add(new BasicNameValuePair("productos[color][]", cursor1.getString(8)));
                                                params.add(new BasicNameValuePair("productos[action][]", cursor1.getString(9)));
                                                params.add(new BasicNameValuePair("productos[id][]", cursor1.getString(10)));
                                                //params.add(new BasicNameValuePair("productos[requerido][]", cursor1.getString(9)));
                                                System.out.println("GetItemsCotizacion(): " + cursor1.getString(3) + " - " + cursor1.getString(4));
                                            } while (cursor1.moveToNext());
                                        }
                                    } catch (Exception e) {
                                        System.out.println("Error-GetItemsCotizacion(): " + e.toString());
                                    }
                                    Log.i("PARAMETROS", params.toString());
                                    httppost.setEntity(new UrlEncodedFormEntity(params));
                                    HttpResponse resp = httpclient.execute(httppost);
                                    HttpEntity ent = resp.getEntity();//y obtenemos una respuesta
                                    if (HttpStatus.SC_CREATED == resp.getStatusLine().getStatusCode() ) {
                                        String text = EntityUtils.toString(ent);
                                        JSONObject json = new JSONObject(text);

                                        if(envio == 1){
                                            HttpClient httpclient111 = new DefaultHttpClient();
                                            HttpPost httppost11 = new HttpPost("http://ventas.novatecpe.com:8080/novatec/api/v1/email/cotizacion");
                                            httppost11.setHeader("x-api-key", pref.getString("key", ""));
                                            List<NameValuePair> params11 = new ArrayList<>();
                                            params11.add(new BasicNameValuePair("codigo",json.get("id").toString()));
                                            httppost11.setEntity(new UrlEncodedFormEntity(params11));
                                            HttpResponse resp111 = httpclient111.execute(httppost11);
                                        }
                                        control.updateEstadoCotiza(id,json.get("id").toString(),envio);
                                    }else{
                                        String text = EntityUtils.toString(ent);
                                        System.out.println("sube0: " + text + " ) ");
                                        JSONObject json = new JSONObject(text);
                                        control.updateEstadoCotiza(id, json.get("id").toString().compareTo("0")==0?"No stock disponible":"",0);
                                    }
                                } catch (Exception e) {
                                    System.out.println("Error Sync Subieda 0: " + e.toString());
                                }

                            } while (cursor.moveToNext());
                        }
                        cursor.close();
                    } catch (Exception e) {
                        System.out.println("Error-GetFullClientes: " + e.toString());
                    }
                }
            }
            return 0;
        }

        @Override
        protected void onPostExecute(Integer integer) {
            //SincronizarCoti();
            super.onPostExecute(integer);
        }


    }*/

    public void EnviarCorreoService(final String idcotizacion){
        RequestQueue queue = Volley.newRequestQueue(this);
        Log.i("IDCOTIZACION", idcotizacion);
        String url = Const.URL_V1 + "email/cotizacion";
        StringRequest postRequest = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>()
                {
                    @Override
                    public void onResponse(String response) {
                        // response
                        Log.d("ResponseCorreo", response);
                    }
                },
                new Response.ErrorListener()
                {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.i("LOGIN1", "error");
                        NetworkResponse response = error.networkResponse;
                        if (error instanceof AuthFailureError && response != null) {
                            Log.i("LOGIN2", error.toString());
                            try {
                                String res = new String(response.data,
                                        HttpHeaderParser.parseCharset(response.headers, "utf-8"));
                                // Now you can use any deserializer to make sense of data
                                JSONObject obj = new JSONObject(res);
                                Log.i("ERROR", obj.toString());
                            } catch (UnsupportedEncodingException e1) {
                                // Couldn't properly decode data to string
                                Log.i("error1", e1.getMessage());
                            } catch (JSONException e2) {
                                // returned data is not JSONObject?
                                Log.i("error2", e2.getMessage());
                            }
                        }
                    }
                }
        ) {
            @Override
            protected Map<String, String> getParams()
            {
                Map<String, String>  params = new HashMap<String, String>();
                params.put("codigo", idcotizacion);

                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String>  params = new HashMap<String, String>();
                params.put("x-api-key", pref.getString("key", ""));
                return params;
            }
        };
        postRequest.setRetryPolicy(new DefaultRetryPolicy(0, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        queue.add(postRequest);
    }

    public void SincronizarCoti(final int envio){
        String selectQuery = "SELECT id,"+Tb_Cotizaciones.fecha+","+Tb_Cotizaciones.fecha_registro+
                ","+Tb_Cotizaciones.cliente +","+Tb_Cotizaciones.vendedor+","+Tb_Cotizaciones.cond_pago+","+
                Tb_Cotizaciones.cond_venta+","+Tb_Cotizaciones.flete+","+Tb_Cotizaciones.tiempo_validez+
                ","+Tb_Cotizaciones.lugar_cotizacion+","+Tb_Cotizaciones.moneda+","+Tb_Cotizaciones.subtotal+
                ","+Tb_Cotizaciones.monto_igv+","+Tb_Cotizaciones.monto_total+","+Tb_Cotizaciones.observaciones+
                ","+Tb_Cotizaciones.por_revision+","+Tb_Cotizaciones.items+","+Tb_Cotizaciones.estadoSynco+
                ","+Tb_Cotizaciones.estado+","+Tb_Cotizaciones.porcentaje+","+Tb_Cotizaciones.latirud+","+Tb_Cotizaciones.longitud+
                ","+Tb_Cotizaciones.lugar+","+Tb_Cotizaciones.icotem+","+Tb_Cotizaciones.lugar_direccion+" FROM "+Tb_Cotizaciones.nombreTabla +" WHERE "+Tb_Cotizaciones.estadoSynco + " = 0";

        SQLiteDatabase database = control.getWritableDatabase();
        Cursor cursor = database.rawQuery(selectQuery, null);
        if (cursor.moveToFirst()) {
            do {
                try {
                    final Map<String, String>  parames = new HashMap<>();
                    final String id = cursor.getString(0);
                    Log.i("IDIDIDIDID", id);
                    parames.put("fecha", cursor.getString(1));
                    parames.put("fecha_registro", cursor.getString(2));
                    parames.put("ruc", cursor.getString(3));
                    parames.put("vendedor", cursor.getString(4));
                    parames.put("cond_pago", cursor.getString(5));
                    parames.put("cond_venta", cursor.getString(6));
                    parames.put("flete", cursor.getString(7));
                    parames.put("tiempo_validez", cursor.getString(8));
                    parames.put("lugar_cotizacion", cursor.getString(9));
                    parames.put("moneda", cursor.getString(10));
                    parames.put("subtotal", cursor.getString(11));
                    parames.put("monto_igv", cursor.getString(12));
                    parames.put("monto_total", cursor.getString(13));
                    parames.put("estado", cursor.getString(18));
                    parames.put("observaciones", cursor.getString(14));
                    parames.put("monto_percepcion", cursor.getString(19));
                    parames.put("latitud", cursor.getString(20));
                    parames.put("longitud", cursor.getString(21));
                    parames.put("lugar_entrega", cursor.getString(22));
                    parames.put("icoterms", cursor.getString(23));
                    parames.put("lugar_direccion",cursor.getString(24));
                    parames.put("enviar",String.valueOf(envio));

                    String selectQuery1 = "SELECT " + Tb_cotizacion_productos.producto + "," + Tb_cotizacion_productos.cantidad + ","
                            + Tb_cotizacion_productos.precio + "," + Tb_cotizacion_productos.produccion + "," +
                            Tb_cotizacion_productos.cunio + "," + Tb_cotizacion_productos.subtotal + "," + Tb_cotizacion_productos.igv +
                            "," + Tb_cotizacion_productos.total + "," + Tb_cotizacion_productos.color +","+Tb_cotizacion_productos.producto_nombre+
                            ","+Tb_cotizacion_productos.requerido+ ","+Tb_cotizacion_productos.action+ ","+Tb_cotizacion_productos.idproduc+ " FROM " + Tb_cotizacion_productos.nombreTabla + " WHERE " +
                            Tb_cotizacion_productos.cotizacion + " = " + cursor.getString(0);

                    Cursor cursor1 = database.rawQuery(selectQuery1, null);
                    int contador1 = 0;
                    try {
                        if (cursor1.moveToFirst()) {
                            do {

                                parames.put("productos[codigo][" + contador1 + "]", cursor1.getString(0));
                                parames.put("productos[cantidad][" + contador1 + "]", cursor1.getString(1));
                                parames.put("productos[precio][" + contador1 + "]", cursor1.getString(2));
                                parames.put("productos[para][" + contador1 + "]", cursor1.getString(3));
                                parames.put("productos[cunio][" + contador1 + "]", cursor1.getString(4));
                                parames.put("productos[subtotal][" + contador1 + "]", cursor1.getString(5));
                                parames.put("productos[igv][" + contador1 + "]", cursor1.getString(6));
                                parames.put("productos[total][" + contador1 + "]", cursor1.getString(7));
                                parames.put("productos[color][" + contador1 + "]", cursor1.getString(8));
                                parames.put("productos[producto]["+contador1+"]", cursor1.getString(9));

                                parames.put("productos[action]["+contador1+"]", cursor1.getString(11));
                                parames.put("productos[id]["+contador1+"]", cursor1.getString(12));

                                    /*parames.put("productos[id]["+contador1+"]", cursor1.getString(12));
                                    parames.put("productos[action]["+contador1+"]", cursor1.getString(11));
                                    parames.put("productos[codigo]["+contador1+"]", cursor1.getString(0));
                                    parames.put("productos[producto]["+contador1+"]", cursor1.getString(9));
                                    parames.put("productos[cantidad]["+contador1+"]", cursor1.getString(1));
                                    parames.put("productos[precio]["+contador1+"]", cursor1.getString(2));
                                    parames.put("productos[para]["+contador1+"]", cursor1.getString(3));
                                    parames.put("productos[cunio]["+contador1+"]", cursor1.getString(4));
                                    parames.put("productos[subtotal]["+contador1+"]", cursor1.getString(5));
                                    parames.put("productos[igv]["+contador1+"]", cursor1.getString(6));
                                    parames.put("productos[total]["+contador1+"]", cursor1.getString(7));
                                    parames.put("productos[color]["+contador1+"]", cursor1.getString(8));

                                    parames.put("productos[codigo][" + contador1 + "]", cursor1.getString(0));
                                    parames.put("productos[producto][" + contador1 + "]", cursor1.getString(9));
                                    parames.put("productos[cantidad][" + contador1 + "]", cursor1.getString(1));
                                    parames.put("productos[precio][" + contador1 + "]", cursor1.getString(2));
                                    parames.put("productos[para][" + contador1 + "]", cursor1.getString(3));
                                    parames.put("productos[cunio][" + contador1 + "]", cursor1.getString(4));
                                    parames.put("productos[subtotal][" + contador1 + "]", cursor1.getString(5));
                                    parames.put("productos[igv][" + contador1 + "]", cursor1.getString(6));
                                    parames.put("productos[total][" + contador1 + "]", cursor1.getString(7));
                                    parames.put("productos[color][" + contador1 + "]", cursor1.getString(8));
                                    parames.put("productos[action][" + contador1 + "]", cursor1.getString(9));
                                    parames.put("productos[id][" + contador1 + "]", cursor1.getString(10));*/


                                Log.i("PRODUCTOS", cursor1.getString(9));
                                contador1++;

                            } while (cursor1.moveToNext());
                        }
                        cursor1.close();
                    } catch (Exception e) {
                        System.out.println("Error-GetItemsCotizacion(): " + e.toString());
                    }
                    Log.i("PARAMETROS VOLLEY", parames.toString());

                    if(Util.isNetworkAvailable(this)){
                        RequestQueue queue = Volley.newRequestQueue(this);
                        String url = Const.URL_V1 + "cotizaciones";
                        StringRequest postRequest = new StringRequest(Request.Method.POST, url,
                                new Response.Listener<String>()
                                {
                                    @Override
                                    public void onResponse(String response) {
                                        // response
                                        Log.d("Response", response);
                                        try {
                                            JSONObject objeto = new JSONObject(response);
                                            String idrespuesta = objeto.getString("id");
                                            if(!idrespuesta.equals("0")){
                                                control.updateEstadoCotiza(id, idrespuesta, envio);
                                                if(envio==1){
                                                    //EnviarCorreoService(objeto.getString("id"));
                                                }
                                            }else{
                                                control.updateEstadoCotiza(id, "No stock disponible",0);
                                            }


                                        } catch (JSONException e) {
                                            e.printStackTrace();
                                        }



                                    }
                                },
                                new Response.ErrorListener()
                                {
                                    @Override
                                    public void onErrorResponse(VolleyError error) {
                                        // error
                                        // As of f605da3 the following should work
                                        NetworkResponse response = error.networkResponse;
                                        if (error instanceof ServerError && response != null) {
                                            try {
                                                String res = new String(response.data,
                                                        HttpHeaderParser.parseCharset(response.headers, "utf-8"));
                                                // Now you can use any deserializer to make sense of data
                                                JSONObject obj = new JSONObject(res);
                                                Log.i("ERROR", obj.toString());
                                            } catch (UnsupportedEncodingException e1) {
                                                // Couldn't properly decode data to string
                                                e1.printStackTrace();
                                            } catch (JSONException e2) {
                                                // returned data is not JSONObject?
                                                e2.printStackTrace();
                                            }
                                        }
                                    }
                                }
                        ) {

                            @Override
                            public Map<String, String> getHeaders() throws AuthFailureError {
                                Map<String, String>  params = new HashMap<>();
                                params.put("x-api-key", pref.getString("key", ""));
                                return params;
                            }

                            @Override
                            protected String getParamsEncoding() {
                                return super.getParamsEncoding();
                            }

                            @Override
                            protected Map<String, String> getParams()
                            {
                                return parames;
                            }
                        };
                        postRequest.setRetryPolicy(new DefaultRetryPolicy(0, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                        queue.add(postRequest);
                    }else{
                        control.updateEstadoCotizaOff(id, "",envio,0);
                    }


                } catch (Exception e) {
                    System.out.println("Error Sync Subieda 0: " + e.toString());
                }

            } while (cursor.moveToNext());
        }
        cursor.close();
    }


    public void SincronizarCotiUpdate(final int envio){

        if(control.CheckDataSyncCotiza(2)) {
            String selectQuery = "SELECT id,"+Tb_Cotizaciones.fecha+","+Tb_Cotizaciones.fecha_registro+
                    ","+Tb_Cotizaciones.cliente +","+Tb_Cotizaciones.vendedor+","+Tb_Cotizaciones.cond_pago+","+
                    Tb_Cotizaciones.cond_venta+","+Tb_Cotizaciones.flete+","+Tb_Cotizaciones.tiempo_validez+
                    ","+Tb_Cotizaciones.lugar_cotizacion+","+Tb_Cotizaciones.moneda+","+Tb_Cotizaciones.subtotal+
                    ","+Tb_Cotizaciones.monto_igv+","+Tb_Cotizaciones.monto_total+","+Tb_Cotizaciones.observaciones+
                    ","+Tb_Cotizaciones.por_revision+","+Tb_Cotizaciones.items+","+Tb_Cotizaciones.estadoSynco+
                    ","+Tb_Cotizaciones.estado+","+Tb_Cotizaciones.porcentaje+","+Tb_Cotizaciones.latirud+","+Tb_Cotizaciones.longitud+
                    ","+Tb_Cotizaciones.lugar+","+Tb_Cotizaciones.icotem+","+Tb_Cotizaciones.codigo+","+Tb_Cotizaciones.lugar_direccion+" FROM "+Tb_Cotizaciones.nombreTabla +" WHERE "+Tb_Cotizaciones.estadoSynco + " = 2";

            SQLiteDatabase database = control.getWritableDatabase();
            Cursor cursor = database.rawQuery(selectQuery, null);
            if (cursor.moveToFirst()) {
                do {
                    try {
                        final Map<String, String>  parames = new HashMap<>();
                        final String id = cursor.getString(0);
                        final String codigo = cursor.getString(24);
                        Log.i("CODIGO AA", codigo);
                        parames.put("codigo", codigo);
                        parames.put("fecha", cursor.getString(1));
                        parames.put("fecha_registro", cursor.getString(2));
                        parames.put("ruc", cursor.getString(3));
                        parames.put("vendedor", cursor.getString(4));
                        parames.put("cond_pago", cursor.getString(5));
                        parames.put("cond_venta", cursor.getString(6));
                        parames.put("flete", cursor.getString(7));
                        parames.put("tiempo_validez", cursor.getString(8));
                        parames.put("lugar_cotizacion", cursor.getString(9));
                        parames.put("moneda", cursor.getString(10));
                        parames.put("subtotal", cursor.getString(11));
                        parames.put("monto_igv", cursor.getString(12));
                        parames.put("monto_total", cursor.getString(13));
                        parames.put("estado", cursor.getString(18));
                        parames.put("observaciones", cursor.getString(14));
                        parames.put("monto_percepcion", cursor.getString(19));
                        parames.put("latitud", cursor.getString(20));
                        parames.put("longitud", cursor.getString(21));
                        parames.put("lugar_entrega", cursor.getString(22));
                        parames.put("icoterms", cursor.getString(23));
                        Log.e("ContactoLugarDireccion3",cursor.getString(25));
                        parames.put("lugar_direccion",cursor.getString(25));

                        String selectQuery1 = "SELECT " + Tb_cotizacion_productos.producto + "," + Tb_cotizacion_productos.cantidad + ","
                                + Tb_cotizacion_productos.precio + "," + Tb_cotizacion_productos.produccion + "," +
                                Tb_cotizacion_productos.cunio + "," + Tb_cotizacion_productos.subtotal + "," + Tb_cotizacion_productos.igv +
                                "," + Tb_cotizacion_productos.total + "," + Tb_cotizacion_productos.color +","+Tb_cotizacion_productos.producto_nombre+
                                ","+Tb_cotizacion_productos.requerido+ ","+Tb_cotizacion_productos.action+ ","+Tb_cotizacion_productos.idproduc+ " FROM " + Tb_cotizacion_productos.nombreTabla + " WHERE " +
                                Tb_cotizacion_productos.cotizacion + " = " + id;

                        Cursor cursor1 = database.rawQuery(selectQuery1, null);
                        int contador1 = 0;
                        try {
                            if (cursor1.moveToFirst()) {
                                do {
                                    /*parames.put("productos[codigo][" + contador1 + "]", cursor1.getString(0));
                                    parames.put("productos[producto][" + contador1 + "]", cursor1.getString(9));
                                    parames.put("productos[cantidad][" + contador1 + "]", cursor1.getString(1));
                                    parames.put("productos[precio][" + contador1 + "]", cursor1.getString(2));
                                    parames.put("productos[para][" + contador1 + "]", cursor1.getString(3));
                                    parames.put("productos[cunio][" + contador1 + "]", cursor1.getString(4));
                                    parames.put("productos[subtotal][" + contador1 + "]", cursor1.getString(5));
                                    parames.put("productos[igv][" + contador1 + "]", cursor1.getString(6));
                                    parames.put("productos[total][" + contador1 + "]", cursor1.getString(7));
                                    parames.put("productos[color][" + contador1 + "]", cursor1.getString(8));
                                    parames.put("productos[action][" + contador1 + "]", cursor1.getString(9));
                                    parames.put("productos[id][" + contador1 + "]", cursor1.getString(10));
                                    */

                                    parames.put("productos[codigo][" + contador1 + "]", cursor1.getString(0));
                                    parames.put("productos[cantidad]["+contador1+"]", cursor1.getString(1));
                                    parames.put("productos[precio]["+contador1+"]", cursor1.getString(2));
                                    parames.put("productos[para]["+contador1+"]", cursor1.getString(3));
                                    parames.put("productos[cunio]["+contador1+"]", cursor1.getString(4));
                                    parames.put("productos[subtotal]["+contador1+"]", cursor1.getString(5));
                                    parames.put("productos[igv]["+contador1+"]", cursor1.getString(6));
                                    parames.put("productos[total]["+contador1+"]", cursor1.getString(7));
                                    parames.put("productos[color]["+contador1+"]", cursor1.getString(8));
                                    parames.put("productos[producto]["+contador1+"]", cursor1.getString(9));

                                    parames.put("productos[action]["+contador1+"]", cursor1.getString(11));
                                    parames.put("productos[id]["+contador1+"]", cursor1.getString(12));

                                    contador1++;

                                } while (cursor1.moveToNext());
                            }
                            cursor1.close();
                        } catch (Exception e) {
                            System.out.println("Error-GetItemsCotizacion(): " + e.toString());
                        }

                        Log.i("PARAMETROS VOLLEY", parames.toString());

                        if(Util.isNetworkAvailable(this)) {
                            Log.e("Modifica con internet","true");
                            RequestQueue queue = Volley.newRequestQueue(this);
                            String url = Const.URL_V1 + "cotizaciones/update";
                            StringRequest postRequest = new StringRequest(Request.Method.POST, url,
                                    new Response.Listener<String>() {
                                        @Override
                                        public void onResponse(String response) {
                                            // response
                                            Log.d("Response", response);
                                            try {
                                                JSONObject objeto = new JSONObject(response);
                                                if (!objeto.getBoolean("success")) {
                                                    Toast.makeText(ActividadPrincipal.this, objeto.getString("errors"), Toast.LENGTH_LONG).show();
                                                } else {
                                                    String idrespuesta = codigo;
                                                    if (idrespuesta.equals("0")) {
                                                        control.updateEstadoCotiza(id, "No stock disponible", 0);
                                                        control.updateActionProductos(id);
                                                    } else {
                                                        control.updateEstadoCotiza(id, idrespuesta, envio);
                                                        control.updateActionProductos(id);
                                                        if (envio == 1) {
                                                            EnviarCorreoService(idrespuesta);
                                                        }
                                                    }
                                                }


                                            } catch (JSONException e) {
                                                e.printStackTrace();
                                            }


                                        }
                                    },
                                    new Response.ErrorListener() {
                                        @Override
                                        public void onErrorResponse(VolleyError error) {
                                            // error
                                            // As of f605da3 the following should work
                                            NetworkResponse response = error.networkResponse;
                                            if (error instanceof ServerError && response != null) {
                                                try {
                                                    String res = new String(response.data,
                                                            HttpHeaderParser.parseCharset(response.headers, "utf-8"));
                                                    // Now you can use any deserializer to make sense of data
                                                    JSONObject obj = new JSONObject(res);
                                                    Log.i("ERROR", obj.toString());
                                                } catch (UnsupportedEncodingException e1) {
                                                    // Couldn't properly decode data to string
                                                    e1.printStackTrace();
                                                } catch (JSONException e2) {
                                                    // returned data is not JSONObject?
                                                    e2.printStackTrace();
                                                }
                                            }
                                        }
                                    }
                            ) {

                                @Override
                                public Map<String, String> getHeaders() throws AuthFailureError {
                                    Map<String, String> params = new HashMap<String, String>();
                                    params.put("x-api-key", pref.getString("key", ""));
                                    return params;
                                }

                                @Override
                                protected String getParamsEncoding() {
                                    return super.getParamsEncoding();
                                }

                                @Override
                                protected Map<String, String> getParams() {
                                    return parames;
                                }
                            };
                            postRequest.setRetryPolicy(new DefaultRetryPolicy(0, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                            queue.add(postRequest);
                        }else{
                            Log.e("Modifica con internet","false");
                            control.updateEstadoCotizaOff(id, "",envio,2);
                        }
                    } catch (Exception e) {
                        System.out.println("Error Sync Subieda 0: " + e.toString());
                    }

                } while (cursor.moveToNext());
            }
            cursor.close();
        }



    }

    private class SyncCotizacionesUpdate extends AsyncTask<Void,Void,Integer> {

        int envio = 0;
        Context cont;
        String urls = Const.URL_V1 + "cotizaciones/update";
        public SyncCotizacionesUpdate(Context c,int i){
            envio = i;
            System.out.println("ACTIVADO: ");
            try{
                cont = c;
            }catch (Exception e){
                System.out.println("Error1: "+e.toString());
            }

        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

        }

        @Override
        protected Integer doInBackground(Void... voids) {
            ConnectivityManager connMgr = (ConnectivityManager)
                    getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();
            if (networkInfo != null && networkInfo.isConnected()) {
                if(control.CheckDataSyncCotiza(2)) {
                    try {
                        String selectQuery = "SELECT id,"+Tb_Cotizaciones.fecha+","+Tb_Cotizaciones.fecha_registro+
                                ","+Tb_Cotizaciones.cliente +","+Tb_Cotizaciones.vendedor+","+Tb_Cotizaciones.cond_pago+","+
                                Tb_Cotizaciones.cond_venta+","+Tb_Cotizaciones.flete+","+Tb_Cotizaciones.tiempo_validez+
                                ","+Tb_Cotizaciones.lugar_cotizacion+","+Tb_Cotizaciones.moneda+","+Tb_Cotizaciones.subtotal+
                                ","+Tb_Cotizaciones.monto_igv+","+Tb_Cotizaciones.monto_total+","+Tb_Cotizaciones.observaciones+
                                ","+Tb_Cotizaciones.por_revision+","+Tb_Cotizaciones.items+","+Tb_Cotizaciones.estadoSynco+
                                ","+Tb_Cotizaciones.estado+","+Tb_Cotizaciones.porcentaje+","+Tb_Cotizaciones.latirud+","+Tb_Cotizaciones.longitud+
                                ","+Tb_Cotizaciones.lugar+","+Tb_Cotizaciones.icotem+","+Tb_Cotizaciones.codigo+","+Tb_Cotizaciones.lugar_direccion+" FROM "+Tb_Cotizaciones.nombreTabla +" WHERE "+Tb_Cotizaciones.estadoSynco + " = 2";

                        SQLiteDatabase database = control.getWritableDatabase();
                        Cursor cursor = database.rawQuery(selectQuery, null);
                        if (cursor.moveToFirst()) {
                            do {
                                try {
                                    HttpClient httpclient = new DefaultHttpClient();
                                    HttpPost httppost = new HttpPost(urls);
                                    httppost.setHeader("x-api-key", pref.getString("key", ""));
                                    List<NameValuePair> params = new ArrayList<>();
                                    String id = cursor.getString(0);
                                    params.add(new BasicNameValuePair("codigo", cursor.getString(24)));
                                    params.add(new BasicNameValuePair("fecha", cursor.getString(1)));
                                    params.add(new BasicNameValuePair("fecha_registro",  cursor.getString(2)));
                                    params.add(new BasicNameValuePair("ruc", cursor.getString(3)));
                                    params.add(new BasicNameValuePair("vendedor", cursor.getString(4)));
                                    params.add(new BasicNameValuePair("cond_pago", cursor.getString(5)));
                                    params.add(new BasicNameValuePair("cond_venta", cursor.getString(6)));
                                    params.add(new BasicNameValuePair("flete", cursor.getString(7)));
                                    params.add(new BasicNameValuePair("tiempo_validez", cursor.getString(8)));
                                    params.add(new BasicNameValuePair("lugar_cotizacion", cursor.getString(9)));
                                    params.add(new BasicNameValuePair("moneda", cursor.getString(10)));
                                    params.add(new BasicNameValuePair("subtotal",cursor.getString(11)));
                                    params.add(new BasicNameValuePair("monto_igv",cursor.getString(12)));
                                    params.add(new BasicNameValuePair("monto_total", cursor.getString(13)));
                                    params.add(new BasicNameValuePair("estado",cursor.getString(18)));
                                    params.add(new BasicNameValuePair("observaciones", cursor.getString(14)));
                                    params.add(new BasicNameValuePair("monto_percepcion", cursor.getString(19)));
                                    params.add(new BasicNameValuePair("latitud", cursor.getString(20)));
                                    params.add(new BasicNameValuePair("longitud", cursor.getString(21)));
                                    System.out.println("Cotizacion holahola [" +cursor.getString(20) + ","+cursor.getString(21)+"]" );
                                    params.add(new BasicNameValuePair("lugar_entrega", cursor.getString(22)));
                                    params.add(new BasicNameValuePair("icoterms", cursor.getString(23)));
                                    params.add(new BasicNameValuePair("lugar_direccion",cursor.getString(25)));
                                    String selectQuery1 = "SELECT " + Tb_cotizacion_productos.producto + "," + Tb_cotizacion_productos.cantidad + ","
                                            + Tb_cotizacion_productos.precio + "," + Tb_cotizacion_productos.produccion + "," +
                                            Tb_cotizacion_productos.cunio + "," + Tb_cotizacion_productos.subtotal + "," + Tb_cotizacion_productos.igv +
                                            "," + Tb_cotizacion_productos.total + "," + Tb_cotizacion_productos.color +","+Tb_cotizacion_productos.producto_nombre+
                                            ","+Tb_cotizacion_productos.requerido+ ","+Tb_cotizacion_productos.action+ ","+Tb_cotizacion_productos.idproduc+ " FROM " + Tb_cotizacion_productos.nombreTabla + " WHERE " +
                                            Tb_cotizacion_productos.cotizacion + " = " + cursor.getString(0);

                                    Cursor cursor1 = database.rawQuery(selectQuery1, null);
                                    try {
                                        if (cursor1.moveToFirst()) {
                                            do {
                                                params.add(new BasicNameValuePair("productos[codigo][]", cursor1.getString(0)));
                                                params.add(new BasicNameValuePair("productos[producto][]", cursor1.getString(9)));
                                                params.add(new BasicNameValuePair("productos[cantidad][]", cursor1.getString(1)));
                                                params.add(new BasicNameValuePair("productos[precio][]", cursor1.getString(2)));
                                                params.add(new BasicNameValuePair("productos[para][]", cursor1.getString(3)));
                                                params.add(new BasicNameValuePair("productos[cunio][]", cursor1.getString(4)));
                                                params.add(new BasicNameValuePair("productos[subtotal][]", cursor1.getString(5)));
                                                params.add(new BasicNameValuePair("productos[igv][]", cursor1.getString(6)));
                                                params.add(new BasicNameValuePair("productos[total][]", cursor1.getString(7)));
                                                params.add(new BasicNameValuePair("productos[color][]", cursor1.getString(8)));
                                                params.add(new BasicNameValuePair("productos[action][]", cursor1.getString(11)));
                                                params.add(new BasicNameValuePair("productos[id][]", cursor1.getString(12)));
                                                //params.add(new BasicNameValuePair("productos[requerido][]", cursor1.getString(9)));
                                                System.out.println("GetItemsCotizacion(): " + cursor1.getString(12) + " - " + cursor1.getString(4));
                                            } while (cursor1.moveToNext());
                                        }
                                    } catch (Exception e) {
                                        System.out.println("Error-GetItemsCotizacion(): " + e.toString());
                                    }
                                    System.out.println("Datos: " +params.toString());
                                    httppost.setEntity(new UrlEncodedFormEntity(params));
                                    HttpResponse resp = httpclient.execute(httppost);
                                    HttpEntity ent = resp.getEntity();/*y obtenemos una respuesta*/
                                    if (HttpStatus.SC_OK == resp.getStatusLine().getStatusCode() ) {
                                        if(envio == 1){
                                            HttpClient httpclient111 = new DefaultHttpClient();
                                            HttpPost httppost11 = new HttpPost(Const.URL_V1 + "email/cotizacion");
                                            httppost11.setHeader("x-api-key", pref.getString("key", ""));
                                            List<NameValuePair> params11 = new ArrayList<>();
                                            params11.add(new BasicNameValuePair("codigo",cursor.getString(24)));
                                            httppost11.setEntity(new UrlEncodedFormEntity(params11));
                                            HttpResponse resp111 = httpclient111.execute(httppost11);
                                        }
                                        control.updateEstadoCotiza(id,cursor.getString(24),envio);
                                    }else{
                                        String text = EntityUtils.toString(ent);
                                        System.out.println("sube0: " + text + " ) ");
                                        JSONObject json = new JSONObject(text);
                                        control.updateEstadoCotiza(id, json.get("id").toString().compareTo("0")==0?"No stock disponible":"",0);
                                    }
                                } catch (Exception e) {
                                    System.out.println("Error Sync Subieda 0: " + e.toString());
                                }

                            } while (cursor.moveToNext());
                        }
                        cursor.close();
                    } catch (Exception e) {
                        System.out.println("Error-GetFullClientes: " + e.toString());
                    }
                }
            }
            return 0;
        }

        @Override
        protected void onPostExecute(Integer integer) {

            super.onPostExecute(integer);
        }


    }
    private void updateUI(Location loc) {
        if (loc != null) {
            Latitudi = Latitudi.compareTo("0")==0?String.valueOf(loc.getLatitude()):Latitudi;
            Longitudi = Longitudi.compareTo("0")==0?String.valueOf(loc.getLongitude()):Longitudi;
        } else {
            Latitudi = "0";
            Longitudi = "0";
        }
    }

    ContentValues datosCotizacion(float sstt,float igv) {
        ContentValues d = new ContentValues();
        d.put("id", sigNumeroCotizacion);
        d.put(Tb_Cotizaciones.fecha, date.getText().toString().trim());

        try{
            SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy",Locale.getDefault());
            Date date = format.parse(datecrear);
            format = new SimpleDateFormat("yyyy-MM-dd",Locale.getDefault());
            datecrear = format.format(date);
        }catch (ParseException e){
            e.printStackTrace();
        }
        String fechaRegistro = datecrear + " " + time;
        Log.e("fecha registro",fechaRegistro); //   03/04/2018 14:28:23
        d.put(Tb_Cotizaciones.fecha_registro, fechaRegistro);
        d.put(Tb_Cotizaciones.cliente, ruc.getText().toString().trim());
        d.put(Tb_Cotizaciones.nombre, razon.getText().toString().trim());
        d.put(Tb_Cotizaciones.vendedor, codigoVendedor);
        d.put(Tb_Cotizaciones.cond_pago, condi_venta.getText().toString().compareTo("Crédito")==0?2:1);
        d.put(Tb_Cotizaciones.cond_venta, idPago);
        d.put(Tb_Cotizaciones.flete, transporte.isChecked()?1:0);
        d.put(Tb_Cotizaciones.tiempo_validez, tiempo.getSelectedItemPosition()+1);
        d.put(Tb_Cotizaciones.latirud, Latitudi);
        d.put(Tb_Cotizaciones.longitud, Longitudi);
        d.put(Tb_Cotizaciones.lugar_cotizacion, lugar.getSelectedItemPosition()+1);
        d.put(Tb_Cotizaciones.moneda,(moneda.getSelectedItemPosition()==0?2:1));
        d.put(Tb_Cotizaciones.observaciones, observacion.getText().toString());
        d.put(Tb_Cotizaciones.lugar, lugarEntrega.getText().toString());
        d.put(Tb_Cotizaciones.por_revision,revision.getSelectedItemPosition()==0?1:0);
        d.put(Tb_Cotizaciones.estado, StatusCotiza(revision.getSelectedItemPosition()));
        porcentajePerceptor = tiponegocio==0?porcentajePerceptor:(( (ttl + (ttl* porcentajePerceptor)) > valorNatural)?0.02f:0);
        d.put(Tb_Cotizaciones.porcentaje, (ttl*(tipo==2?0:porcentajePerceptor)));
        d.put(Tb_Cotizaciones.items, cantidad.getText().toString());
        d.put(Tb_Cotizaciones.icotem, icotems.getSelectedItemPosition());
        d.put(Tb_Cotizaciones.monto_total, (ttl + (ttl*(tipo==2?0:porcentajePerceptor))));
        d.put(Tb_Cotizaciones.subtotal,sstt);
        d.put(Tb_Cotizaciones.monto_igv, igv);
        Log.e("ContactoLugarDireccion",String.valueOf(lugarDireccion));
        d.put(Tb_Cotizaciones.lugar_direccion,lugarDireccion);
        d.put(Tb_Cotizaciones.estadoSynco, EstatusEdición==0?0:2);
        Log.i("PARTIR", d.toString());
        return d;
    }

    Dialog customDialog;

    public void Cerrar() {
        customDialog = new Dialog(this);
        customDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        customDialog.setCancelable(true);
        customDialog.setContentView(R.layout.notificacion_01);
        (customDialog.findViewById(R.id.si)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view)
            {
                customDialog.dismiss();
                Intent intent = new Intent(ActividadPrincipal.this,Inicio.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
                finish();
            }
        });
        (customDialog.findViewById(R.id.no)).setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view)
            {
                customDialog.dismiss();

            }
        });

        assert customDialog != null;
        customDialog.show();

    }

    public void Guardar() {
        customDialog = new Dialog(this);
        customDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        customDialog.setCancelable(true);
        customDialog.setContentView(R.layout.notificacion_01);

        TextView titulo = (TextView) customDialog.findViewById(R.id.mensaje);
        titulo.setText(EstatusEdición==0?"¿Usted esta seguro de enviar la cotización?":"¿Usted esta seguro de enviar el cambio\nde la cotización?");
        (customDialog.findViewById(R.id.si)).setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view)
            {
                customDialog.dismiss();

                if(porporpor){
                    if(EstatusEdición==1){
                        if(flagDescuento==1&&StatusCotiza(revision.getSelectedItemPosition())!=2&&Aprobar==0){
                            Toast.makeText(ActividadPrincipal.this, "La cotizacion debe estar pendiente por revision cuando existe descuentos", Toast.LENGTH_LONG).show();
                        }else{
                            new  ModificarCotización(ActividadPrincipal.this,revision.getSelectedItemPosition(), cotizacionID).execute();
                        }

                    }else{
                        if(flagDescuento==1&&StatusCotiza(revision.getSelectedItemPosition())!=2&&Aprobar==0){
                            Toast.makeText(ActividadPrincipal.this, "La cotizacion debe estar pendiente por revision cuando existe descuentos", Toast.LENGTH_LONG).show();
                        }else{
                            new GuardandoCotizacion(ActividadPrincipal.this,revision.getSelectedItemPosition()).execute();
                        }

                    }

                }


            }
        });
        (customDialog.findViewById(R.id.no)).setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view)
            {
                customDialog.dismiss();

            }
        });

        assert customDialog != null;
        customDialog.show();

    }

    public void EnviarCorreo(final int act) {
        Log.i("ENVIAR CORREO ", flagDescuento+" - status: "+StatusCotiza(revision.getSelectedItemPosition()));
        if(StatusCotiza(revision.getSelectedItemPosition())==3){
            customDialog = new Dialog(this);
            customDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            customDialog.setCancelable(false);
            customDialog.setContentView(R.layout.notificacion_01);

            TextView titulo = (TextView) customDialog.findViewById(R.id.mensaje);
            titulo.setText("¿Deseas usted enviar por correo electrónico al cliente la cotización en PDF?");
            (customDialog.findViewById(R.id.si)).setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View view)
                {
                    customDialog.dismiss();
                    if(act==1){

                        //new SyncCotizacionesUpdate(ActividadPrincipal.this,1).execute();
                        SincronizarCotiUpdate(1);
                    }else{
                        SincronizarCoti(1);
                        //new SyncCotizaciones(ActividadPrincipal.this,1).execute();
                    }
                    Intent i = new Intent(ActividadPrincipal.this,Menu_Principal.class);
                    startActivity(i);
                    finish();


                }
            });
            (customDialog.findViewById(R.id.no)).setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View view)
                {
                    customDialog.dismiss();
                    if(act==1){
                        //new SyncCotizacionesUpdate(ActividadPrincipal.this,0).execute();
                        SincronizarCotiUpdate(0);
                    }else{
                        SincronizarCoti(0);
                        //new SyncCotizaciones(ActividadPrincipal.this,0).execute();
                    }
                    Intent i = new Intent(ActividadPrincipal.this,Menu_Principal.class);
                    startActivity(i);
                    finish();
                }
            });

            assert customDialog != null;
            customDialog.show();


        }else{
            if(flagDescuento==1&&StatusCotiza(revision.getSelectedItemPosition())==2){
                customDialog = new Dialog(this);
                customDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                customDialog.setCancelable(false);
                customDialog.setContentView(R.layout.notificacion_01b);
                (customDialog.findViewById(R.id.si)).setOnClickListener(new View.OnClickListener() {

                    @Override
                    public void onClick(View view)
                    {
                        customDialog.dismiss();
                        if(act==1){

                            //new SyncCotizacionesUpdate(ActividadPrincipal.this,1).execute();
                            SincronizarCotiUpdate(0);
                        }else{
                            SincronizarCoti(0);
                            //new SyncCotizaciones(ActividadPrincipal.this,1).execute();
                        }

                        Intent i = new Intent(ActividadPrincipal.this,Menu_Principal.class);
                        startActivity(i);
                        finish();

                    }
                });
                customDialog.show();
            }else{
                if(act==1){
                    //new SyncCotizacionesUpdate(ActividadPrincipal.this,0).execute();
                    SincronizarCotiUpdate(0);
                }else{
                    SincronizarCoti(0);
                    //new SyncCotizaciones(ActividadPrincipal.this,0).execute();
                }

                Intent i = new Intent(ActividadPrincipal.this,Menu_Principal.class);
                startActivity(i);
                finish();
            }

        }


    }

    public void Cancelar() {
        customDialog = new Dialog(this);
        customDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        customDialog.setCancelable(true);
        customDialog.setContentView(R.layout.notificacion_01);
        TextView titulo = (TextView) customDialog.findViewById(R.id.mensaje);
        titulo.setText("¿Desea cancelar?\n" +
                "No se guardaran los datos");
        (customDialog.findViewById(R.id.si)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view)
            {
                customDialog.dismiss();
                Intent intent = new Intent(ActividadPrincipal.this,Menu_Principal.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
                finish();
            }
        });
        (customDialog.findViewById(R.id.no)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view)
            {
                customDialog.dismiss();

            }
        });

        assert customDialog != null;
        customDialog.show();

    }

    public void Accion(final int pos) {
        customDialog = new Dialog(this);
        customDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        customDialog.setCancelable(true);
        customDialog.setContentView(R.layout.notificacion_06);

        (customDialog.findViewById(R.id.si)).setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view)
            {

                ContentValues dato = productos.get(pos);
                Intent i = new Intent(getApplicationContext(), DetalleProductos.class);
                i.putExtra("posicion", pos);
                i.putExtra("estatus", 1);
                i.putExtra("flete", 1);
                i.putExtra("tipo",tipo);
                i.putExtra("EstatusEdición",EstatusEdición);
                i.putExtra("moneda",moneda.getSelectedItemPosition());
                i.putExtra(Tb_cotizacion_productos.cotizacion, sigNumeroCotizacion);
                i.putExtra(Tb_cotizacion_productos.producto, dato.get(Tb_cotizacion_productos.producto).toString());
                i.putExtra(Tb_cotizacion_productos.color, Integer.parseInt(dato.get(Tb_cotizacion_productos.color)!=null?dato.get(Tb_cotizacion_productos.color).toString():"0"));
                startActivity(i);
                customDialog.dismiss();

            }
        });
        (customDialog.findViewById(R.id.no)).setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view)
            {
                ContentValues dato = productos.get(pos);
                if(EstatusEdición==1){
                    ContentValues datos = new ContentValues();
                    datos.put(Tb_cotizacion_productos.cotizacion,dato.getAsString(Tb_cotizacion_productos.cotizacion));
                    datos.put(Tb_cotizacion_productos.producto,dato.getAsString(Tb_cotizacion_productos.producto));
                    datos.put(Tb_cotizacion_productos.producto_nombre,dato.getAsString(Tb_cotizacion_productos.producto_nombre));
                    datos.put(Tb_cotizacion_productos.cantidad,dato.getAsString(Tb_cotizacion_productos.cantidad));
                    datos.put(Tb_cotizacion_productos.subtotal,dato.getAsString(Tb_cotizacion_productos.subtotal));
                    datos.put(Tb_cotizacion_productos.igv,dato.getAsString(Tb_cotizacion_productos.igv));
                    datos.put(Tb_cotizacion_productos.precio,dato.getAsString(Tb_cotizacion_productos.precio));
                    datos.put(Tb_cotizacion_productos.total,dato.getAsString(Tb_cotizacion_productos.total));
                    datos.put(Tb_cotizacion_productos.cunio,dato.getAsString(Tb_cotizacion_productos.cunio));
                    datos.put(Tb_cotizacion_productos.produccion,dato.getAsString(Tb_cotizacion_productos.produccion));
                    datos.put(Tb_cotizacion_productos.requerido,dato.getAsString(Tb_cotizacion_productos.requerido));
                    datos.put(Tb_cotizacion_productos.idproduc,dato.getAsString(Tb_cotizacion_productos.idproduc));
                    datos.put(Tb_cotizacion_productos.action,"0");
                    productosEliminados.add(datos);
                }
                float tot = Float.parseFloat(dato.get(Tb_cotizacion_productos.total).toString());
                Log.d("total a borrar","Total: " + tot);
                Log.d("total actual","Total actual: " + ttl);
                int canti= Integer.parseInt(dato.get(Tb_cotizacion_productos.cantidad).toString());
                ctd -= canti;
                ttl -= tot;
                Log.d("total tras borrar","Total tras borrar: " + ttl);
                float nuevoTotal = ttl + (ttl * (tipo==2?0:porcentajePerceptor));
                productos.remove(pos);
                listaproduc.setAdapter(new adapter_producto(ActividadPrincipal.this,productos,moneda.getSelectedItemPosition()));
                cantidad.setText(""+ctd);

                Log.d("total actual + %","Total actual + %: " + ttl);
                ttl = productos.size()>0?ttl:0;
                total.setText(((moneda.getSelectedItemPosition()==0)?"US$ ":"S/. ") +" "+ convertidor.format(nuevoTotal));
                customDialog.dismiss();

            }
        });

        assert customDialog != null;
        customDialog.show();

    }

    int StatusCotiza(int i){
        int ii =0;
        switch (i){
            case 2:
                ii=2;//PENDIENTE
                break;
            case 1:
                ii=3;//APROBADA
                break;
            case 0:
                ii=5;//Ninguno
                break;

        }
        return ii;
    }

    public void ListadoClientes(){
        customDialog = new Dialog(this);
        customDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        customDialog.setCancelable(false);
        customDialog.setContentView(R.layout.notificacion_03_cargalista);
        clientes = control.GetFullClientes();
        TextView titulo = (TextView) customDialog.findViewById(R.id.titulo);
        titulo.setText("Listado de clientes");
        EditText busqueda = (EditText) customDialog.findViewById(R.id.buscar);
        final  RecyclerView lista = (RecyclerView) customDialog.findViewById(R.id.clientes);
        lista.setHasFixedSize(true);
        lista.setAdapter(adapter);

        busqueda.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence s, int i, int i1, int i2) {
                clientes = control.GetFullClientesBusqueda(s.toString());
                adapter = new ListClienteAdapter(ActividadPrincipal.this, clientes);
                lista.setAdapter(adapter);
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
        adapter = new ListClienteAdapter(this, clientes);
        adapter.setOnItemClickListener(new ListClienteAdapter.ClickListener() {
            @Override
            public void onItemClick(int position, View v) {
                try{
                    ContentValues dato = adapter.lista.get(position);
                    Log.i("CLIENTE", dato.toString());
                    razon.setText(dato.get(Tb_Clientes.razon_social).toString());
                    currentRuc = dato.get(Tb_Clientes.ruc).toString();
                    String mRuc = dato.get(Tb_Clientes.ruc).toString();
                    ruc.setText(mRuc);
                    correo.setText(dato.get(Tb_Clientes.correo).toString());
                    codigo = dato.get(Tb_Clientes.ruc).toString();
                    contacto.setText(dato.get(Tb_Clientes.contacto).toString());
                    tiponegocio = getNumberI(dato.get(Tb_Clientes.tiponegocio).toString(),1)-1;
                    tipo = getNumberI(dato.get(Tb_Clientes.tipoExtaNa).toString(),1);
                    porcentajePerceptor = getNumberF(dato.get(Tb_Clientes.por_perceptor).toString(),0);
                    int codiventa = Integer.parseInt(dato.get(Tb_Clientes.cond_venta).toString());
                    idPago = dato.get(Tb_Clientes.cond_pago).toString();
                    Log.e("CONDICIONDEPAGO",idPago);
                    Log.e("CONDICIONDEVENTA",String.valueOf(codiventa));
                    String cod_venta = codiventa==2?"Crédito":"Contado";
                    lyICOTE.setVisibility(tipo==2?View.VISIBLE:View.GONE);
                    condi_venta.setText(cod_venta);
                    if(condi_venta.getText().toString().compareTo("Crédito")==0){
                        String pagoName = control.GetCondicionPago(idPago);
                        condi_pago.setText(pagoName);
                    }else{
                        condi_pago.setText("Contado");
                    }
                    porcentajePerceptor= porcentajePerceptor>0?porcentajePerceptor/100:0;
                    tiponegocio = getNumberI(dato.get(Tb_Clientes.tiponegocio).toString(),1)-1;
                    correo.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {

                                final Dialog dialog = new Dialog(ActividadPrincipal.this);
                                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                                dialog.setCancelable(true);
                                dialog.setContentView(R.layout.dialog_cambiar_correo);
                                final EditText editCorreo = (EditText) dialog.findViewById(R.id.editdialogCorreo);
                                editCorreo.setText(correo.getText().toString().trim());
                                TextView accionCambiar = (TextView) dialog.findViewById(R.id.cambiar);
                                accionCambiar.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        control.updateCorreoCliente(ruc.getText().toString(), editCorreo.getText().toString().trim());
                                        correo.setText(editCorreo.getText().toString().trim());
                                        CambiarDatoClienteService(editCorreo.getText().toString().trim(), ruc.getText().toString());
                                        dialog.dismiss();
                                    }
                                });
                                dialog.show();


                        }
                    });
                    contacto.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {

                                final Dialog dialog = new Dialog(ActividadPrincipal.this);
                                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                                dialog.setCancelable(true);
                                dialog.setContentView(R.layout.dialog_cambiar_correo);
                                final EditText editCorreo = (EditText) dialog.findViewById(R.id.editdialogCorreo);
                                editCorreo.setText(contacto.getText().toString().trim());
                                TextView accionCambiar = (TextView) dialog.findViewById(R.id.cambiar);
                                accionCambiar.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        control.updateContactoCliente(ruc.getText().toString(), editCorreo.getText().toString().trim());
                                        contacto.setText(editCorreo.getText().toString().trim());
                                        CambiarDatoClienteService(editCorreo.getText().toString().trim(), ruc.getText().toString());
                                        dialog.dismiss();
                                    }
                                });
                                dialog.show();


                        }
                    });
                    requestContacts(mRuc);
                }catch (Exception e){
                    Toast.makeText(ActividadPrincipal.this,"El cliente que quiere escoger tiene errores de datos",Toast.LENGTH_SHORT).show();
                }



                customDialog.dismiss();
            }
        });
        lista.setLayoutManager(new LinearLayoutManager(this,LinearLayoutManager.VERTICAL,false));
        (customDialog.findViewById(R.id.no)).setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view)
            {
                customDialog.dismiss();
            }
        });

        assert customDialog != null;
        customDialog.show();
    }

    public void CambiarDatoClienteService(final String correocliente, final String ruccliente){
        final ProgressDialog dialog = new ProgressDialog(this);
        dialog.setTitle("Cargando...");
        dialog.show();



        RequestQueue queue = Volley.newRequestQueue(this);
        String url = Const.URL_V1 + "clientes";
        StringRequest postRequest = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>()
                {
                    @Override
                    public void onResponse(String response) {
                        // response
                        Log.d("Response", response);
                        try {
                            JSONObject obj = new JSONObject(response);
                            if(obj.getBoolean("success")){
                                control.updateEstadoCliente(ruccliente);
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }



                        dialog.dismiss();
                    }
                },
                new Response.ErrorListener()
                {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        // error
                        NetworkResponse response = error.networkResponse;
                        if (error instanceof ServerError && response != null) {
                            try {
                                String res = new String(response.data,
                                        HttpHeaderParser.parseCharset(response.headers, "utf-8"));
                                // Now you can use any deserializer to make sense of data
                                Log.i("ERROR1", res);
                                JSONObject obj = new JSONObject(res);
                                Log.i("ERROR2", obj.toString());
                            } catch (UnsupportedEncodingException e1) {
                                // Couldn't properly decode data to string
                                e1.printStackTrace();
                            } catch (JSONException e2) {
                                // returned data is not JSONObject?
                                e2.printStackTrace();
                            }
                        }
                        dialog.dismiss();
                    }
                }
        ) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String>  params = new HashMap<String, String>();
                params.put("x-api-key", pref.getString("key", ""));
                return params;
            }

            @Override
            protected Map<String, String> getParams()
            {
                String selectQuery = "SELECT " + Tb_Clientes.razon_social + "," + Tb_Clientes.ruc + "," + Tb_Clientes.contacto + "," + Tb_Clientes.code + "," +
                        Tb_Clientes.direccion + "," + Tb_Clientes.telefono_fijo + "," + Tb_Clientes.telefono_movil + "," + Tb_Clientes.perceptor
                        + "," + Tb_Clientes.retenedor + "," + Tb_Clientes.por_perceptor + "," + Tb_Clientes.calificacion
                        + "," + Tb_Clientes.cond_pago + "," + Tb_Clientes.nombre_comercial + "," + Tb_Clientes.correo + "," + Tb_Clientes.estado
                        + "," + Tb_Clientes.zona+ "," + Tb_Clientes.tiponegocio + "," + Tb_Clientes.tipoExtaNa +"," + Tb_Clientes.rubro +
                        "," + Tb_Clientes.sector +"," + Tb_Clientes.current_ruc +" FROM " + Tb_Clientes.nombreTabla + " WHERE " + Tb_Clientes.estado + "= 2 AND " + Tb_Clientes.ruc + "="+ruccliente;
                SQLiteDatabase database = control.getWritableDatabase();
                Cursor cursor = database.rawQuery(selectQuery, null);
                Map<String, String>  params = new HashMap<String, String>();
                if (cursor.moveToFirst()) {
                    do {

                        params.put("razon_social", cursor.getString(0).trim());
                        params.put("ruc", cursor.getString(1).trim());
                        params.put("contacto", cursor.getString(2).trim());
                        params.put("direccion", cursor.getString(3).trim());
                        params.put("telefono_fijo", cursor.getString(4).trim());
                        params.put("telefono_movil", cursor.getString(5).trim());
                        params.put("perceptor", cursor.getString(6).trim());
                        params.put("retenedor", cursor.getString(7).trim());
                        params.put("porcentaje", cursor.getString(8).trim());
                        params.put("calificacion", cursor.getString(9).trim());
                        params.put("cond_pago", cursor.getString(10).trim());
                        params.put("nombre_comercial", cursor.getString(11).trim());
                        params.put("correo", cursor.getString(12).trim());
                        params.put("zona", cursor.getString(14).trim());
                        params.put("tipo_cliente", cursor.getString(15).trim());
                        params.put("tipo", cursor.getString(16).trim());
                        params.put("rubro", cursor.getString(17).trim());
                        params.put("sector", cursor.getString(18).trim());
                        params.put("current_ruc", cursor.getString(19).trim());
                    } while (cursor.moveToNext());
                }
                cursor.close();


                Log.i("cambiar correo", params.toString());

                return params;
            }
        };
        postRequest.setRetryPolicy(new DefaultRetryPolicy(0, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        queue.add(postRequest);
    }

    int getNumberI(String d, int i){
        try{
            return Integer.parseInt(d);
        }catch (Exception e){
            return i;
        }
    }

    float getNumberF(String d,float i){
        try{
            return Float.parseFloat(d);
        }catch (Exception e){
            return i;
        }
    }

    public void ListadoCondicionPago(){
        customDialog = new Dialog(this);
        customDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        customDialog.setCancelable(false);
        customDialog.setContentView(R.layout.notificacion_03_cargalista);
        condiVenta = condiVentaOrigen;
        TextView titulo = (TextView) customDialog.findViewById(R.id.titulo);
        titulo.setText("Condicion de pago");
        EditText busqueda = (EditText) customDialog.findViewById(R.id.buscar);
        busqueda.setHint("Buscar condición de pago");
        final  RecyclerView lista1 = (RecyclerView) customDialog.findViewById(R.id.clientes);
        lista1.setHasFixedSize(true);

        lista1.setAdapter(adapterVentas);

        busqueda.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence s, int i, int i1, int i2) {
                condiVenta = control.GetFullVentaBusqueda(s.toString());
                adapterVentas = new CondicionVentaAdapter(ActividadPrincipal.this, condiVenta);
                lista1.setAdapter(adapterVentas);
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
        adapterVentas = new CondicionVentaAdapter(this, condiVenta);
        adapterVentas.setOnItemClickListener(new CondicionVentaAdapter.ClickListener() {
            @Override
            public void onItemClick(int position, View v) {
                ContentValues dato = adapterVentas.lista.get(position);
                idPago = dato.get("id").toString();
                condi_pago.setText(dato.get("nombre").toString());
                customDialog.dismiss();
            }
        });
        lista1.setLayoutManager(new LinearLayoutManager(this,LinearLayoutManager.VERTICAL,false));
        (customDialog.findViewById(R.id.no)).setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view)
            {
                customDialog.dismiss();
            }
        });

        assert customDialog != null;
        customDialog.show();
    }


    private class conversionMoneda extends AsyncTask<Void,Void,Integer> {

        int mnd;
        int ant_moneda;
        Context cont;
        float valor_soles = Inicio.conversorMonedas;
        ProgressDialog pd;
        public conversionMoneda(int _m,int _am,Context c){
            mnd = _m;
            ant_moneda = _am;
            try{
                pd =  new ProgressDialog(c);
                pd.setIndeterminate(true);
                pd.setMessage("Guardando cotización");
                pd.setCancelable(false);
                pd.show();
            }catch (Exception e){
                System.out.println("Error1: "+e.toString());
            }
        }


        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected Integer doInBackground(Void... voids) {
            Log.i("MONEDAS", mnd+" - "+ant_moneda);
            if(mnd != ant_moneda){
                cambioMoneda = true;
                convertido = 1;
                if(mnd == 1 && ant_moneda == 0) {
                    for(int i =0; i<productos.size();i++){
                        ContentValues n = productos.get(i);
                        float temp_subtotal,temp_igv,temp_precio,temp_total;
                        temp_precio = n.getAsFloat(Tb_cotizacion_productos.precio);
                        temp_subtotal = n.getAsFloat(Tb_cotizacion_productos.subtotal);
                        temp_igv = n.getAsFloat(Tb_cotizacion_productos.igv);
                        temp_total = n.getAsFloat(Tb_cotizacion_productos.total);

                        n.put(Tb_cotizacion_productos.precio,String.valueOf((temp_precio*valor_soles)));
                        n.put(Tb_cotizacion_productos.subtotal,String.valueOf((temp_subtotal*valor_soles)));
                        n.put(Tb_cotizacion_productos.igv,String.valueOf((temp_igv*valor_soles)));
                        n.put(Tb_cotizacion_productos.total,String.valueOf((temp_total*valor_soles)));
                        productos.remove(i);
                        productos.add(n);
                    }

                    ttl= (ttl)*valor_soles;

                }
                if(mnd == 0 && ant_moneda == 1) {
                    for(int i =0; i<productos.size();i++){
                        ContentValues n = productos.get(i);
                        float temp_subtotal,temp_igv,temp_precio,temp_total;
                        temp_precio = n.getAsFloat(Tb_cotizacion_productos.precio);
                        temp_subtotal = n.getAsFloat(Tb_cotizacion_productos.subtotal);
                        temp_igv = n.getAsFloat(Tb_cotizacion_productos.igv);
                        temp_total = n.getAsFloat(Tb_cotizacion_productos.total);

                        n.put(Tb_cotizacion_productos.precio,String.valueOf((temp_precio/valor_soles)));
                        n.put(Tb_cotizacion_productos.subtotal,String.valueOf((temp_subtotal/valor_soles)));
                        n.put(Tb_cotizacion_productos.igv,String.valueOf((temp_igv/valor_soles)));
                        n.put(Tb_cotizacion_productos.total,String.valueOf((temp_total/valor_soles)));
                        productos.remove(i);
                        productos.add(n);

                    }
                    ttl= (ttl)/valor_soles;
                }
            }else{
                cambioMoneda = false;
            }
            return 0;
        }

        @Override
        protected void onPostExecute(Integer integer) {
            super.onPostExecute(integer);
            pd.dismiss();
            listaproduc.setAdapter(new adapter_producto(ActividadPrincipal.this,productos,moneda.getSelectedItemPosition()));
            float tota = (ttl + (ttl*(tipo==2?0:porcentajePerceptor)));
            total.setText(((moneda.getSelectedItemPosition()==0)?"US$ ":"S/. ") +" "+ convertidor.format(tota));
            monedaAnterior = moneda.getSelectedItemPosition();
        }


    }

    private class GetContactos extends AsyncTask<Void, Void, Integer> {

        private BaseDatosSqlite control;
        private String ruc;
        private ArrayAdapter<String> adapter;

        GetContactos(BaseDatosSqlite control,String ruc){
            this.control = control;
            this.ruc = ruc;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected void onPostExecute(Integer integer) {
            super.onPostExecute(integer);
            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item); // The drop down view
            if (EstatusEdición == 1) {
                spinner.setOnItemSelectedListener(null);
                isFirstTimeLoadingSpinner = true;
            }
            spinner.setAdapter(adapter);
            spinner.setSelection(ListContactos.indexOf(contacto.getText().toString()));
            spinner.setOnItemSelectedListener(ActividadPrincipal.this);
        }

        @Override
        protected Integer doInBackground(Void... voids) {
            ListContactos = new ArrayList<>();
            ListContactosDestalle = control.getContactsByRUC(ruc);
            for (AllContacts contacts: ListContactosDestalle) {
                ListContactos.add(contacts.getContacto());
            }

            adapter = new ArrayAdapter<>(ActividadPrincipal.this, android.R.layout.simple_spinner_item, ListContactos);
            return 0;
        }
    }

    @Override
    public void onItemSelected(AdapterView<?> adapterView, View view, int position, long l) {
        if(!isFirstTimeLoadingSpinner) {
            Log.d("spinner", "-->" + position);
            actualizarInterfz(position);
        }else{
            isFirstTimeLoadingSpinner = false;
        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {

    }
}


