package com.felixcabrita.cotizacionesamconsultores;

/**
 * Creado por Felix Cabrita en 29/6/2017.
*/
import android.content.ContentValues;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.ArrayList;
import java.util.Locale;

import com.felixcabrita.cotizacionesamconsultores.models.Tb_Cotizaciones;

public class adapter_cotizacion extends BaseAdapter {

    private DecimalFormat convertidor = new DecimalFormat("#,##0.00", DecimalFormatSymbols.getInstance(Locale.ITALIAN));   // CONVERTIDOR FLOAT \\
    ArrayList<ContentValues> lista = new ArrayList<>();
    private Context propio;
    int moneda = 0;
    adapter_cotizacion(Context c, ArrayList<ContentValues> l){
        this.propio = c;
        this.lista = l;
        DecimalFormatSymbols dfs = new DecimalFormatSymbols();
        dfs.setDecimalSeparator('.');
        dfs.setGroupingSeparator(',');
        convertidor.setDecimalFormatSymbols(dfs);
    }

    @Override
    public int getCount() {
        return lista.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        TextView nombreCLiente;
        TextView nPedido;
        TextView total;
        TextView fecha;
        TextView hora;
        TextView item,estado;
        LayoutInflater inf = (LayoutInflater) propio.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        if (null == convertView) {
            convertView = inf.inflate(R.layout.adapter_cotizacion, parent, false);
        }
        nombreCLiente   = (TextView) convertView.findViewById(R.id.nombreclientepedido);
        nPedido         = (TextView) convertView.findViewById(R.id.nPedido);
        total           = (TextView) convertView.findViewById(R.id.totalPedido);
        fecha           = (TextView) convertView.findViewById(R.id.fechapedido);
        item            = (TextView) convertView.findViewById(R.id.item);
        estado            = (TextView) convertView.findViewById(R.id.estado);
        item.setText(lista.get(position).get(Tb_Cotizaciones.items).toString());
        //String[] parts = lista.get(position).get(Tb_Cotizaciones.fecha).toString().split(" ");
        String part1 = lista.get(position).get(Tb_Cotizaciones.fecha).toString(); // 004
        moneda = Integer.parseInt(lista.get(position).get(Tb_Cotizaciones.moneda).toString());
        nombreCLiente.setText(lista.get(position).get(Tb_Cotizaciones.nombre).toString());

        nPedido.setText((lista.get(position).get(Tb_Cotizaciones.codigo)!=null)?lista.get(position).get(Tb_Cotizaciones.codigo).toString():"No sincronizado");
        fecha.setText(part1);
        total.setText(((moneda==2)?"US$ ":"S/. ") +convertidor.format(Float.parseFloat(lista.get(position).get(Tb_Cotizaciones.monto_total).toString())));

        estado.setText(nPedido.getText().toString().compareTo("No stock disponible")==0?"No sincronizado":estadoCotizacion(Integer.parseInt(lista.get(position).get(Tb_Cotizaciones.estado).toString())));
        return convertView;
    }


    String estadoCotizacion(int i){
        String estado= " ";
        switch (i){
            case 3:
                estado = "Aprobado";
                break;
            case 4:
                estado = "Rechazado";
                break;
            case 1:
                estado = "Aprobado";
                break;
            case 0:
                estado = "Rechazado";
                break;
            case 2:
                estado = "Pendiente por revisión";
                break;
            case 5:
                estado = "Ninguno";
                break;
        }
        return estado;

    }
}

