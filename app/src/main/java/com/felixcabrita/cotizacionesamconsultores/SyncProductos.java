package com.felixcabrita.cotizacionesamconsultores;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.preference.PreferenceManager;

import com.felixcabrita.cotizacionesamconsultores.utils.Const;

import org.json.JSONArray;
import org.json.JSONException;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 * Creado por Felix Cabrita en 4/7/2017.
 */

class SyncProductos extends AsyncTask<Void,Void,Void> {


    boolean internet = true;
    ProgressDialog pd;
    private BaseDatosSqlite cnn;
    String urls = Const.URL_V1 + "productos";
    Context c;
    boolean error = false;

    private SharedPreferences pref ;
    private SharedPreferences.Editor editor;
    public SyncProductos(Context c) {
        cnn = new BaseDatosSqlite(c);
        pref = PreferenceManager.getDefaultSharedPreferences(c);
        this.c = c;
        editor = pref.edit();
            /*try{
                pd =  new ProgressDialog(c);
                pd.setIndeterminate(true);
                pd.setMessage("Iniciando seción");
                pd.setCancelable(false);
                pd.show();
            }catch (Exception e){
                System.out.println("Error1: "+e.toString());
            }*/

    }


    @Override
    protected void onPreExecute() {
        super.onPreExecute();

    }

    @Override
    protected Void doInBackground(Void... voids) {
        ConnectivityManager connMgr = (ConnectivityManager)
                c.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();
        if (networkInfo != null && networkInfo.isConnected()) {
            try {
                URL producto = new URL(urls);
                HttpURLConnection conProducto = (HttpURLConnection) producto.openConnection();
                //connection.setRequestProperty("Authorization", "Basic " + Base64.encodeToString((user + ":" + pass).getBytes(), Base64.NO_WRAP));
                if (conProducto.getResponseCode() == HttpURLConnection.HTTP_OK) {
                    InputStream inputStreamResponse = conProducto.getInputStream();
                    String linea;
                    StringBuilder respuestaCadena = new StringBuilder();
                    BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStreamResponse, "UTF-8"));
                    while ((linea = bufferedReader.readLine()) != null) {
                        respuestaCadena.append(linea);
                    }
                    // JSONObject datos = new JSONObject(respuestaCadena.toString());
                    try {
                        JSONArray jsonArray = new JSONArray(respuestaCadena.toString());// datos.getJSONArray("");
                        cnn.CapturaDatosProductosJSON(jsonArray);
                    } catch (JSONException e) {
                        error = true;
                        e.printStackTrace();
                    }
                    editor.putInt("syncP",0);
                    editor.apply();

                } else {
                    error = true;
                    System.out.println("Error2 " + conProducto.getResponseCode());
                }

            } catch (IOException e) {
                error = true;
                System.out.println("Error1: " + e.toString());
            }

            /*if(!error){
                cotizacion = control.GetFullClientes();
                nombre = new String[cotizacion.size()];
                for (int i = 0;i<nombre.length;i++){
                    nombre[i] = cotizacion.get(i).get(tb_cliente.razon_social);
                }
            }*/

        } else {
            internet = false;
        }
        return null;
    }


    @Override
    protected void onPostExecute(Void aVoid) {
        super.onPostExecute(aVoid);

    }


}