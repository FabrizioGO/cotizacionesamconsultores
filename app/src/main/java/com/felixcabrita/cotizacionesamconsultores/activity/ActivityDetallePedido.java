package com.felixcabrita.cotizacionesamconsultores.activity;

import android.app.ProgressDialog;
import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.felixcabrita.cotizacionesamconsultores.R;
import com.felixcabrita.cotizacionesamconsultores.net.RetrofitAdapter;
import com.felixcabrita.cotizacionesamconsultores.net.ServicesContact;
import com.felixcabrita.cotizacionesamconsultores.net.model.responses.DetallePedido;
import com.felixcabrita.cotizacionesamconsultores.utils.Const;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;

public class ActivityDetallePedido extends AppCompatActivity {

    private String ruc;
    private int pedido;
    ProgressDialog progressDoalog;
    ServicesContact api;
    Call<List<DetallePedido>> call;
    Toolbar toolbar;
    TextView text_numero_pedido;
    TextView text_ruc_detalle;
    TextView text_fecha_emi;
    TextView text_direccion_detalle;
    TextView text_nombre_cliente;
    TextView text_nro;
    TextView text_um;
    TextView text_cantidad;
    TextView text_codigo;
    TextView text_descripcion;
    TextView text_precio;
    TextView text_precio_unitario;
    TextView text_pre_lista;
    TextView text_peso;
    TextView text_ped_cor_anu;
    TextView text_monto_neto;
    TextView text_monto_igv;
    TextView text_percepcion;
    TextView text_monto_total;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detalle_pedido);

        if (getIntent()!=null) {
            pedido = getIntent().getIntExtra("pedido",0);
            ruc = getIntent().getStringExtra("ruc");
        }
        initUI();
        initProvidedNetwork();
    }
    private void initProvidedNetwork() {
        RetrofitAdapter.mBaseUrl = Const.URL_V1;
        api = RetrofitAdapter.getApiService();
        requestDetallePedidos();
    }

    public void initUI()
    {
        text_numero_pedido=(TextView) findViewById(R.id.text_numero_pedido);
        text_ruc_detalle=(TextView) findViewById(R.id.text_ruc_detalle);
        text_fecha_emi=(TextView) findViewById(R.id.text_fecha_emi);
        text_direccion_detalle=(TextView) findViewById(R.id.text_direccion_detalle);
        text_nombre_cliente=(TextView)findViewById(R.id.text_nombre_cliente);
        text_nro=(TextView)findViewById(R.id.text_nro);
        text_um=(TextView)findViewById(R.id.text_um);
        text_cantidad=(TextView)findViewById(R.id.text_cantidad);
        text_codigo=(TextView)findViewById(R.id.text_codigo);
        text_descripcion=(TextView)findViewById(R.id.text_descripcion);
        text_precio=(TextView)findViewById(R.id.text_precio);
        text_precio_unitario=(TextView)findViewById(R.id.text_precio_unitario);
        text_pre_lista=(TextView)findViewById(R.id.text_pre_lista);
        text_peso=(TextView)findViewById(R.id.text_peso);
        text_ped_cor_anu=(TextView)findViewById(R.id.text_ped_cor_anu);
        text_monto_neto=(TextView)findViewById(R.id.text_monto_neto);
        text_monto_igv=(TextView)findViewById(R.id.text_monto_igv);
        text_percepcion=(TextView)findViewById(R.id.text_percepcion);
        text_monto_total=(TextView)findViewById(R.id.text_monto_total);

        toolbar=(Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitleTextColor(Color.WHITE);
        toolbar.setNavigationIcon(R.drawable.ic_arrow_back);
        toolbar.setTitle("Detalle de pedido");
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                finish();
            }}
        );
    }


    private void requestDetallePedidos() {
        progressDoalog = new ProgressDialog(this);
        progressDoalog.setMessage("Cargando....");
        progressDoalog.setTitle("Detalle pedido...");
        progressDoalog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDoalog.show();
        call = api.detallePedidos(ruc,pedido);
        call.enqueue(new Callback<List<DetallePedido>>() {
            @Override
            public void onResponse(Call<List<DetallePedido>> call, retrofit2.Response<List<DetallePedido>> response) {
                progressDoalog.dismiss();
                switch (response.code()) {
                    case 200:
                        Log.d("Test", "Imprimiendo respuesta -->" + call.request().url());
                        if(response.isSuccessful() && !response.body().isEmpty())
                        {
                            DetallePedido pedido = response.body().get(0);
                            text_numero_pedido.setText(""+pedido.getNroPedido());
                            text_ruc_detalle.setText(""+pedido.getRuc());
                            text_fecha_emi.setText(""+pedido.getFECHAEMI());
                            text_direccion_detalle.setText(""+pedido.getDireccionCliente());
                            text_nombre_cliente.setText(""+pedido.getNombreCliente());
                            text_nro.setText(""+pedido.getNRO());
                            text_um.setText(""+pedido.getUM());
                            text_cantidad.setText(""+pedido.getCantidad());
                            text_codigo.setText(""+pedido.getCodigo());
                            text_descripcion.setText(""+pedido.getDesccripcion());
                            text_precio.setText("US$ "+pedido.getPrecio());
                            text_precio_unitario.setText("US$ "+pedido.getPreUnitario());
                            text_pre_lista.setText("US$ "+pedido.getPRELSTA());
                            text_peso.setText(""+pedido.getPESO());
                            text_ped_cor_anu.setText(""+pedido.getPedCorAnu());
                            text_monto_neto.setText("US$ "+pedido.getMontoNeto());
                            text_monto_igv.setText("US$ "+pedido.getMontoIgv());
                            text_percepcion.setText(""+pedido.getPercepcion());
                            text_monto_total.setText("US$ "+pedido.getMontoTotal());
                            Log.d("Test","-->isSuccessful()");
                            Log.d("Test", "pedido detalle -->" + call.request().url());
                        }
                        else
                        {
                            Log.d("Test","-->No_funciono");
                        }
                    break;
                    default:
                        progressDoalog.dismiss();
                        Log.d("Test", "onResponse: El codigo no es 200, ha ocurrido un error");
                }
            }
            @Override
            public void onFailure(Call<List<DetallePedido>> call, Throwable t) {
                progressDoalog.dismiss();
                Log.d("Test", "Throwable: " + t.getCause() + " call: " + call.toString());
            }
        });
    }
    @Override
    public void onBackPressed() {
    }
}
