package com.felixcabrita.cotizacionesamconsultores.activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.EditText;

import com.felixcabrita.cotizacionesamconsultores.BaseDatosSqlite;
import com.felixcabrita.cotizacionesamconsultores.R;
import com.felixcabrita.cotizacionesamconsultores.adapter.adapter_listPedidos;
import com.felixcabrita.cotizacionesamconsultores.net.ModelsPedidos;
import com.felixcabrita.cotizacionesamconsultores.net.RetrofitAdapter;
import com.felixcabrita.cotizacionesamconsultores.net.ServicesContact;
import com.felixcabrita.cotizacionesamconsultores.utils.Const;
import com.felixcabrita.cotizacionesamconsultores.utils.Util;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;

public class ActivityHistorialPedidos extends AppCompatActivity {

    private String currentRuc;
    private BaseDatosSqlite control;
    private Toolbar toolbar;
    private ProgressDialog progressDialog;
    private ServicesContact api;
    private Call<List<ModelsPedidos>> call;
    private RecyclerView recicler;
    private ArrayList<ModelsPedidos> ListPedidos = new ArrayList<>();
    private adapter_listPedidos adaptador;
    private EditText search;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_historial_pedidos);

        if (getIntent()!=null)
            currentRuc = getIntent().getStringExtra("ruc");

        control = new BaseDatosSqlite(this);
        search=(EditText)findViewById(R.id.search);
        recicler=(RecyclerView)findViewById(R.id.recicler_pedidos);
        LinearLayoutManager reci = new LinearLayoutManager(this);
        recicler.setLayoutManager(reci);
        recicler.setHasFixedSize(true);
        toolbar=(Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitleTextColor(Color.WHITE);
        toolbar.setNavigationIcon(R.drawable.ic_arrow_back);
        toolbar.setTitle("Historial de Pedidos");
        if (Util.isNetworkAvailable(this))
            initProvidedNetwork();
        else
            new GetPedidos(control,currentRuc).execute();
        addTextListener();
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {

                finish();
            }}
        );

    }

    public void detallePedido(int pedido)
    {
        Intent mainIntent = new Intent().setClass(ActivityHistorialPedidos.this, ActivityDetallePedido.class);
        mainIntent.putExtra("ruc",currentRuc);
        mainIntent.putExtra("pedido",pedido);
        startActivity(mainIntent);
        overridePendingTransition(R.anim.zoom_back_in,R.anim.zoom_back_out);
    }

    private void initProvidedNetwork() {
        RetrofitAdapter.mBaseUrl = Const.URL_V1;
        api = RetrofitAdapter.getApiService();
        requestPedidos();
    }
    private void requestPedidos() {
        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Cargando....");
        progressDialog.setTitle("Listando de pedidos...");
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDialog.show();

        //"20508565934"
        call = api.historialPedidos(currentRuc);
        call.enqueue(new Callback<List<ModelsPedidos>>() {

            @Override
            public void onResponse(Call<List<ModelsPedidos>> call, retrofit2.Response<List<ModelsPedidos>> response) {
                progressDialog.dismiss();
                if (response != null) {
                    switch (response.code()) {
                        case 200:
                            Log.d("Test", "Imprimiendo respuesta -->" + call.request().url());
                            if (response.isSuccessful()) {
                                Log.d("Test", "-->isSuccessful()");
                                Log.d("Test", "Imprimiendo respuesta -->" + call.request().url());
                            } else {
                                Log.d("Test", "-->No_funciono");
                            }
                            if (response.body() != null && !response.body().isEmpty()) {
                                //  progressDialog.dismiss();
                                for (ModelsPedidos pedido : response.body()) {
                                    ListPedidos.add(new ModelsPedidos(
                                            pedido.getPedido(),
                                            pedido.getFecha(),
                                            pedido.getTipoPedido(),
                                            pedido.getCliente(),
                                            pedido.getRuc(),
                                            pedido.getDireccion(),
                                            pedido.getMontoNeto(),
                                            pedido.getMontoIgv(),
                                            pedido.getMontoTotal(),
                                            pedido.getTipoMoneda(),
                                            pedido.getPercepcion()
                                    ));
                                    Log.d("Test", "Imprimiendo respuesta -->" + pedido.toString());
                                }
                                adaptador = new adapter_listPedidos(ListPedidos, ActivityHistorialPedidos.this);
                                recicler.setAdapter(adaptador);
                                new GuardarPedidos(control,ListPedidos,currentRuc).execute();
                                adaptador.notifyDataSetChanged();
                            } else {
                                Util.mostrarMensaje(ActivityHistorialPedidos.this, "El cliente no tiene datos en historial de pedidos", "OK").show();
                            }
                            break;
                        default:
                            Log.d("Test", "onResponse: El codigo no es 200, ha ocurrido un error");
                    }
                }
            }
            @Override
            public void onFailure(Call<List<ModelsPedidos>> call, Throwable t) {
                progressDialog.dismiss();
                Log.d("Test", "Throwable: " + t.getCause() + " call: " + call.toString());

            }
        });
    }
    public void addTextListener(){

        search.addTextChangedListener(new TextWatcher() {

            public void afterTextChanged(Editable s) {}

            public void beforeTextChanged(CharSequence s, int start, int count, int after) {}

            public void onTextChanged(CharSequence query, int start, int before, int count) {

                query = query.toString().toLowerCase();

                final ArrayList<ModelsPedidos> filteredList = new ArrayList<>();

                for (int i = 0; i < ListPedidos.size(); i++) {
                    ModelsPedidos a= new ModelsPedidos();
                    final String text = ListPedidos.get(i).getCliente().toLowerCase();
                    if (text.contains(query)) {
                        a.setCliente(ListPedidos.get(i).getCliente());
                        a.setDireccion(ListPedidos.get(i).getDireccion());
                        a.setFecha(ListPedidos.get(i).getFecha());
                        a.setMontoIgv(ListPedidos.get(i).getMontoIgv());
                        a.setMontoNeto(ListPedidos.get(i).getMontoNeto());
                        a.setMontoTotal(ListPedidos.get(i).getMontoTotal());
                        a.setPercepcion(ListPedidos.get(i).getPercepcion());
                        a.setRuc(ListPedidos.get(i).getRuc());
                        a.setTipoMoneda(ListPedidos.get(i).getTipoMoneda());
                        a.setTipoPedido(ListPedidos.get(i).getTipoPedido());
                        a.setPedido(ListPedidos.get(i).getPedido());
                        filteredList.add(a);
                    }
                }

                adaptador= new adapter_listPedidos(filteredList, ActivityHistorialPedidos.this);
                recicler.setAdapter(adaptador);
                adaptador.notifyDataSetChanged();

            }
        });}
    @Override
    public void onBackPressed() {
    }

    private static class GuardarPedidos extends AsyncTask<Void, Void, Integer> {

        private BaseDatosSqlite control;
        private ArrayList<ModelsPedidos> list;
        private String ruc;

        GuardarPedidos(BaseDatosSqlite control, ArrayList<ModelsPedidos> list, String ruc){
            this.control = control;
            this.list = list;
            this.ruc = ruc;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected void onPostExecute(Integer integer) {
            super.onPostExecute(integer);
        }

        @Override
        protected Integer doInBackground(Void... voids) {
            control.deletePedidosClientes(ruc);
            control.saveClientePedido(ruc,list);
            return 0;
        }
    }

    private class GetPedidos extends AsyncTask<Void, Void, Integer> {

        private BaseDatosSqlite control;
        private String ruc;

        GetPedidos(BaseDatosSqlite control,String ruc){
            this.control = control;
            this.ruc = ruc;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected void onPostExecute(Integer integer) {
            super.onPostExecute(integer);
            recicler.setAdapter(adaptador);
            adaptador.notifyDataSetChanged();
        }

        @Override
        protected Integer doInBackground(Void... voids) {
            ListPedidos = control.getPedidosByRUC(ruc);
            adaptador = new adapter_listPedidos(ListPedidos, ActivityHistorialPedidos.this);
            return 0;
        }
    }
}
