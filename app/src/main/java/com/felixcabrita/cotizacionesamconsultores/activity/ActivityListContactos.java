package com.felixcabrita.cotizacionesamconsultores.activity;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.felixcabrita.cotizacionesamconsultores.BaseDatosSqlite;
import com.felixcabrita.cotizacionesamconsultores.R;
import com.felixcabrita.cotizacionesamconsultores.models.GuardarContactos;
import com.felixcabrita.cotizacionesamconsultores.models.Tb_contactos;
import com.felixcabrita.cotizacionesamconsultores.net.model.request.Contact;
import com.felixcabrita.cotizacionesamconsultores.utils.Util;
import com.felixcabrita.cotizacionesamconsultores.adapter.adapter_listContactos;
import com.felixcabrita.cotizacionesamconsultores.net.RetrofitAdapter;
import com.felixcabrita.cotizacionesamconsultores.net.ServicesContact;
import com.felixcabrita.cotizacionesamconsultores.net.model.responses.AllContacts;
import com.felixcabrita.cotizacionesamconsultores.utils.Const;
import com.felixcabrita.cotizacionesamconsultores.utils.Preferences;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ActivityListContactos extends AppCompatActivity {

    private String clienteID;
    private BaseDatosSqlite control;
    FloatingActionButton floting;
    Toolbar toolbar;
    ServicesContact api;
    Call<List<AllContacts>> call;
    adapter_listContactos adaptador;
    RecyclerView recicler;
    ArrayList<AllContacts> ListContactos = new ArrayList<>();
    ProgressDialog progressDialog;
    public EditText search;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_contactos);
        toolbar=(Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitleTextColor(Color.WHITE);
        toolbar.setNavigationIcon(R.drawable.ic_arrow_back);
        toolbar.setTitle("Lista de contactos");
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Log.d("Test", "finish finish -->");
                finish();
            }}
        );
        setSupportActionBar(toolbar);

        if (getIntent() != null)
            clienteID = getIntent().getStringExtra("ruc");

        control = new BaseDatosSqlite(this);
        iniUI();
        click();

        addTextListener();
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (Util.isNetworkAvailable(this)) {
            new DatosSynco(this, clienteID).execute();
            initProvidedNetwork();
        }else
            new GetContactos(control,clienteID).execute();
    }

    private void initProvidedNetwork() {
        RetrofitAdapter.mBaseUrl = Const.URL_V1;
        api = RetrofitAdapter.getApiService();
        requestContacts();
    }

    private void requestContacts() {
        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Cargando....");
        progressDialog.setTitle("Listando contactos...");
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDialog.show();

        call = api.allContacts(clienteID);
        Log.e("KeyUser",Preferences.getkeyUser());
        call.enqueue(new Callback<List<AllContacts>>() {
            @Override
            public void onResponse(Call<List<AllContacts>> call, Response<List<AllContacts>> response) {
                progressDialog.dismiss();
                if (response != null && response.body() != null) {
                    Log.d("Test", "Response: " + response.code() + " call: " + call);
                    switch (response.code()) {
                        case 200:
                            if (!response.body().isEmpty()) {
                                ListContactos = new ArrayList<>();
                                Log.d("Test", "-->isEmpty()" + call.request().url());
                                for (AllContacts contact : response.body()) {
                                    ListContactos.add( new AllContacts(
                                            contact.getItem(),
                                            contact.getValue(),
                                            contact.getTipo(),
                                            contact.getDescripcion(),
                                            contact.getContacto(),
                                            contact.getDireccion(),
                                            contact.getCorreo(),
                                            contact.getTelefonoFijo(),
                                            contact.getTelefonoMovil(),
                                            contact.getZona(),
                                            contact.getZonaText(),
                                            contact.getVendedor(),
                                            contact.getVendedorText(),
                                            contact.getCondVenta(),
                                            contact.getCondVentaText(),
                                            contact.getCondPago(),
                                            contact.getCondPagoText()));
                                    Log.d("Test", "Imprimiendo respuesta -->" + contact.toString());
                                }
                                adaptador = new adapter_listContactos(ListContactos, ActivityListContactos.this);
                                recicler.setAdapter(adaptador);
                                new GuardarContactos(control,ListContactos,clienteID,1).execute();
                                adaptador.notifyDataSetChanged();
                            }else{
                                Util.mostrarMensaje(ActivityListContactos.this,"No se encontraron contactos para este cliente");
                            }
                            break;
                        default:
                            Log.d("Test", "onResponse: El codigo no es 200, ha ocurrido un error");
                    }
                }else{
                    progressDialog.dismiss();
                    Log.d("Test", "Response: " + "No hay contactos" + " call: " + call);
                }
            }
            @Override
            public void onFailure(Call<List<AllContacts>> call, Throwable t) {
                progressDialog.dismiss();
                Log.d("Test", "Throwable: " + t.getCause() + " call: " + call.toString());
            }
        });
    }

    public void iniUI()
    {
        search=(EditText)findViewById(R.id.search);
        recicler = (RecyclerView) findViewById(R.id.recicler);
        LinearLayoutManager reci = new LinearLayoutManager(this);
        recicler.setLayoutManager(reci);
        recicler.setHasFixedSize(true);

    }
    @Override
    public void onBackPressed() {
        finish();
    }

    public void editContacto()
    {
        Intent mainIntent = new Intent().setClass(ActivityListContactos.this, ActivityEditarContacto.class);
        mainIntent.putExtra("ruc",clienteID);
        startActivityForResult(mainIntent,0);
        overridePendingTransition(R.anim.zoom_back_in,R.anim.zoom_back_out);
    }
    public void click()
    {
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                onBackPressed();
            }}
        );
        floting =(FloatingActionButton) findViewById(R.id.floatingActionButton2);

        floting.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), ActivityCrearContactos.class);
                intent.putExtra("ruc",clienteID);
                startActivityForResult(intent,1);
            }
        });
    }

    public void addTextListener() {

        search.addTextChangedListener(new TextWatcher() {

            public void afterTextChanged(Editable s) {
            }

            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            public void onTextChanged(CharSequence query, int start, int before, int count) {

                query = query.toString().toLowerCase();

                final ArrayList<AllContacts> filteredList = new ArrayList<>();

                for (int i = 0; i < ListContactos.size(); i++) {
                    AllContacts a = new AllContacts();
                    final String text = ListContactos.get(i).getContacto().toLowerCase();
                    if (text.contains(query)) {
                        a.setItem(ListContactos.get(i).getItem());
                        a.setValue(ListContactos.get(i).getValue());
                        a.setTipo(ListContactos.get(i).getTipo());
                        a.setDescripcion(ListContactos.get(i).getDescripcion());
                        a.setContacto(ListContactos.get(i).getContacto());
                        a.setDireccion(ListContactos.get(i).getDireccion());
                        a.setCorreo(ListContactos.get(i).getCorreo());
                        a.setTelefonoFijo(ListContactos.get(i).getTelefonoFijo());
                        a.setTelefonoMovil(ListContactos.get(i).getTelefonoMovil());
                        a.setZona(ListContactos.get(i).getZona());
                        a.setZonaText(ListContactos.get(i).getZonaText());
                        a.setVendedor(ListContactos.get(i).getVendedor());
                        a.setVendedorText(ListContactos.get(i).getVendedorText());
                        a.setCondVenta(ListContactos.get(i).getCondVenta());
                        a.setCondVentaText(ListContactos.get(i).getCondVentaText());
                        a.setCondPago(ListContactos.get(i).getCondPago());
                        a.setCondPagoText(ListContactos.get(i).getCondPagoText());
                        filteredList.add(a);
                    }
                }

                adaptador = new adapter_listContactos(filteredList, ActivityListContactos.this);
                adaptador.notifyDataSetChanged();
                recicler.setAdapter(adaptador);
                adaptador.notifyDataSetChanged();

            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.actividad_principal, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.sync:
                new DatosSynco(ActivityListContactos.this,clienteID).execute();
                requestContacts();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private class GetContactos extends AsyncTask<Void, Void, Integer> {

        private BaseDatosSqlite control;
        private String ruc;

        GetContactos(BaseDatosSqlite control,String ruc){
            this.control = control;
            this.ruc = ruc;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected void onPostExecute(Integer integer) {
            super.onPostExecute(integer);
            recicler.setAdapter(adaptador);
            adaptador.notifyDataSetChanged();
        }

        @Override
        protected Integer doInBackground(Void... voids) {
            ListContactos = control.getContactsByRUC(ruc);
            adaptador = new adapter_listContactos(ListContactos, ActivityListContactos.this);
            //new DatosSynco(ActivityListContactos.this,ruc);
            return 0;
        }
    }

    private class DatosSynco extends AsyncTask<Void,Void,Integer> {

        boolean internet = true;
        Context cont;
        String ruc;
        String msj ="";
        String mensj = "";
        public DatosSynco(Context c, String ruc){
            cont = c;
            this.ruc = ruc;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            initProvidedNetwork();
            Log.e("DatosSynco","onPreExecute");
        }

        @Override
        protected Integer doInBackground(Void... voids) {
            Log.e("DatosSynco","onInBackgroundInicio");
            if (Util.isNetworkAvailable(ActivityListContactos.this)) {
                internet = true;
                if (control.CheckContactosSync()) {
                    try {
                        String selectQuery = "SELECT * FROM " + Tb_contactos.nombreTabla + " WHERE " + Tb_contactos.cliente + " = " + ruc + " AND " + Tb_contactos.estado + "= 0 OR " + Tb_contactos.estado + "= 2";
                        ArrayList<AllContacts> allContactsList = control.getContactsByRUC(ruc,selectQuery);

                        for (AllContacts contact : allContactsList) {
                            Log.e("Contact" + contact.getItem(),contact.toString());
                            Contact newContact = new Contact(clienteID,
                                    contact.getItem(),
                                    1,
                                    contact.getDescripcion(),
                                    contact.getContacto(),
                                    contact.getDireccion(),
                                    contact.getCorreo(),
                                    contact.getTelefonoFijo(),
                                    contact.getTelefonoMovil(),
                                    contact.getCondVenta(),
                                    contact.getCondPago()
                            );
                            createContact(newContact);
                        }
                        control.deleteContactoClientes(clienteID);
                    } catch (Exception e) {
                        System.out.println("Error-SyncContactos: " + e.toString());
                    }

                }
            }else{
                internet = false;
            }
            Log.e("DatosSynco","onBackGroundFinal");
            return 0;
        }


        @Override
        protected void onPostExecute(Integer integer) {
            super.onPostExecute(integer);
            if(!msj.isEmpty()){
                Toast.makeText(cont,mensj,Toast.LENGTH_LONG).show();
            }
            Log.e("DatosSynco", "onPostExecute");
        }

        public void createContact(Contact mContactNew) {
            Log.d("agg", "objeto-->" + mContactNew.toString());
            Call<String> contact = api.createContact(mContactNew);
            contact.enqueue(new Callback<String>() {
                @Override
                public void onResponse(Call<String> call, Response<String> response) {
                    Log.d("agg", "response: " + response);
                    if(response.isSuccessful())
                    {
                        Log.d("agg", "Values: " + "bien--->");
                        control.updateEstadoContacto(clienteID);
                        requestContacts();
                    }else
                    {
                        Log.d("agg", "Ha ocurrido un error");
                    }
                    switch (response.code()) {
                        case 200:
                            Log.d("agg", "Values: " + "--->");
                            break;
                        default:
                            Log.d("agg", "Ha ocurrido un error");
                    }
                }

                @Override
                public void onFailure(Call<String> call, Throwable t) {
                    Log.d("Test", "Caused: " + t);
                    Log.d("Test", "Caused: " + call.request().body().toString());

                }
            });
        }


        private void initProvidedNetwork() {
            RetrofitAdapter.mBaseUrl = Const.URL_V1;
            api = RetrofitAdapter.getApiService();
        }


    }

}
