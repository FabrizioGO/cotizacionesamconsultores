package com.felixcabrita.cotizacionesamconsultores.activity;

import android.app.ProgressDialog;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;

import com.felixcabrita.cotizacionesamconsultores.BaseDatosSqlite;
import com.felixcabrita.cotizacionesamconsultores.R;
import com.felixcabrita.cotizacionesamconsultores.adapter.adapter_listSituacion;
import com.felixcabrita.cotizacionesamconsultores.net.RetrofitAdapter;
import com.felixcabrita.cotizacionesamconsultores.net.ServicesContact;
import com.felixcabrita.cotizacionesamconsultores.net.model.responses.ModelsSituacion;
import com.felixcabrita.cotizacionesamconsultores.utils.Const;
import com.felixcabrita.cotizacionesamconsultores.utils.Preferences;
import com.felixcabrita.cotizacionesamconsultores.utils.Util;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ActivitySituacionActual extends AppCompatActivity {

    private BaseDatosSqlite control;
    private String currentRuc;
    Toolbar toolbar;
    ServicesContact api;
    ProgressDialog progressDialog;
    Call<List<ModelsSituacion>> call;
    ArrayList<ModelsSituacion> ListSituacion = new ArrayList<>();
    RecyclerView recicler;
    adapter_listSituacion adaptador;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_situacion_actual);
        toolbar=(Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitleTextColor(Color.WHITE);
        toolbar.setNavigationIcon(R.drawable.ic_arrow_back);
        toolbar.setTitle("Situación actual");
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                onBackPressed();
            }}
        );

        control = new BaseDatosSqlite(this);

        if (getIntent()!=null)
            currentRuc = getIntent().getStringExtra("ruc");
        InitUI();
        if (Util.isNetworkAvailable(this))
            initProvidedNetwork();
        else
            new GetSituacion(control,currentRuc).execute();
    }
    private void initProvidedNetwork() {
        RetrofitAdapter.mBaseUrl = Const.URL_V1;
        api = RetrofitAdapter.getApiService();
        requestSituacion();
    }
    public void InitUI()
    {
        recicler=(RecyclerView)findViewById(R.id.recicler);
        LinearLayoutManager reci = new LinearLayoutManager(this);
        recicler.setLayoutManager(reci);
        recicler.setHasFixedSize(true);
    }

    private void requestSituacion() {
        Log.e("KeyUser",String.valueOf(Preferences.getkeyUser()));
        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Cargando....");
        progressDialog.setTitle("Listando...");
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDialog.show();
        //"20508565934"
        call = api.situacion(currentRuc);
        call.enqueue(new Callback<List<ModelsSituacion>>() {

            @Override
            public void onResponse(Call<List<ModelsSituacion>> call, Response<List<ModelsSituacion>> response) {
                progressDialog.dismiss();
                Log.d("Test", "Response: " + response.code() + " call: " + call);
                switch (response.code()) {
                    case 200:
                        if(!response.body().isEmpty())
                        {
                            Log.d("Test","-->isEmptySituacion "+ call.request().url().toString());

                            for (ModelsSituacion situacion : response.body()) {
                                ListSituacion.add( new ModelsSituacion(
                                        situacion.getFechaDocmto(),
                                        situacion.getDocmto(),
                                        situacion.getVcSerie(),
                                        situacion.getVcNrodocumento(),
                                        situacion.getCODVTA(),
                                        situacion.getFechaVecnto(),
                                        situacion.getUbicacion(),
                                        situacion.getSituacion(),
                                        situacion.getMnd(),
                                        situacion.getTotalDeuda(),
                                        situacion.getAmortizado(),
                                        situacion.getSaldo(),
                                        situacion.getVENDEDOR(),
                                        situacion.getCLAVE(),
                                        situacion.getCCBCORDCOM(),
                                        situacion.getTD(),
                                        situacion.getDOCUMENTO(),
                                        situacion.getVCDescrip(),
                                        situacion.getVCRuc()));

                                Log.d("Test", "Imprimiendo respuesta -->" + situacion.toString());
                            }
                            adaptador= new adapter_listSituacion(ListSituacion);
                            recicler.setAdapter(adaptador);
                            new GuardarSituacion(control,ListSituacion,currentRuc).execute();
                            adaptador.notifyDataSetChanged();
                        }else{
                            Util.mostrarMensaje(ActivitySituacionActual.this,"El cliente no tiene datos en situación actual","Vover atras").show();
                        }
                        break;
                    default:
                        //adaptador.notifyDataSetChanged();
                        Log.d("Test", "onResponse: El codigo no es 200, ha ocurrido un error");
                }
            }
            @Override
            public void onFailure(Call<List<ModelsSituacion>> call, Throwable t) {
                progressDialog.dismiss();
                Log.d("Test", "Throwable: " + t.getCause() + " call: " + call.toString());
            }
        });
    }

    private static class GuardarSituacion extends AsyncTask<Void, Void, Integer> {

        private BaseDatosSqlite control;
        private ArrayList<ModelsSituacion> list;
        private String ruc;

        GuardarSituacion(BaseDatosSqlite control, ArrayList<ModelsSituacion> list, String ruc){
            this.control = control;
            this.list = list;
            this.ruc = ruc;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected void onPostExecute(Integer integer) {
            super.onPostExecute(integer);
        }

        @Override
        protected Integer doInBackground(Void... voids) {
            control.deleteSituacionClientes(ruc);
            control.saveSituacionCliente(ruc,list);
            return 0;
        }
    }

    private class GetSituacion extends AsyncTask<Void, Void, Integer> {

        private BaseDatosSqlite control;
        private String ruc;

        GetSituacion(BaseDatosSqlite control, String ruc){
            this.control = control;
            this.ruc = ruc;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected void onPostExecute(Integer integer) {
            super.onPostExecute(integer);
            recicler.setAdapter(adaptador);
            adaptador.notifyDataSetChanged();
        }

        @Override
        protected Integer doInBackground(Void... voids) {
            ListSituacion = control.getSituacionsCliente(ruc);
            adaptador = new adapter_listSituacion(ListSituacion);
            return 0;
        }
    }
}
