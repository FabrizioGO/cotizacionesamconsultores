package com.felixcabrita.cotizacionesamconsultores.activity;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.ContentValues;
import android.content.DialogInterface;
import android.content.res.Resources;
import android.graphics.Color;
import android.os.Handler;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import com.felixcabrita.cotizacionesamconsultores.BaseDatosSqlite;
import com.felixcabrita.cotizacionesamconsultores.CondicionVentaAdapter;
import com.felixcabrita.cotizacionesamconsultores.ListaClientes;
import com.felixcabrita.cotizacionesamconsultores.R;
import com.felixcabrita.cotizacionesamconsultores.net.RetrofitAdapter;
import com.felixcabrita.cotizacionesamconsultores.net.ServicesContact;
import com.felixcabrita.cotizacionesamconsultores.net.model.request.Contact;
import com.felixcabrita.cotizacionesamconsultores.net.model.responses.AllContacts;
import com.felixcabrita.cotizacionesamconsultores.utils.Const;
import com.felixcabrita.cotizacionesamconsultores.utils.Preferences;
import com.felixcabrita.cotizacionesamconsultores.utils.Util;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ActivityEditarContacto extends AppCompatActivity {

    private String clienteID;
    private ArrayList<ContentValues> condiVenta;
    private BaseDatosSqlite control;
    ServicesContact api;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.guardarContacto)
    TextView guardarContacto;
    @BindView(R.id.text_cancelar_contacto)
    TextView text_cancelar_contacto;
    @BindView(R.id.text_limpiarContacto)
    TextView text_limpiarContacto;
    @BindView(R.id.edit_descripcion)
    EditText edit_descripcion;
    @BindView(R.id.edit_direccion)
    EditText edit_direccion;
    @BindView(R.id.edit_nombre_contacto)
    EditText edit_nombre_contacto;
    @BindView(R.id.edit_correo)
    EditText edit_correo;
    @BindView(R.id.edit_telefono_fijo)
    EditText edit_telefono_fijo;
    @BindView(R.id.edit_movil)
    EditText edit_movil;
    @BindView(R.id.edit_cod_pago)
    TextView edit_cod_pago;
    //@BindView(R.id.edit_cod_venta)
    //EditText edit_cod_venta;
    @BindView(R.id.spinner_cod_venta)
    Spinner spinner_cond_venta;

    private String idVenta, idPago;
    private Dialog customDialog;
    private boolean isEditing;
    private CondicionVentaAdapter adapterVentas;

    ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        if (getIntent() != null)
            clienteID = getIntent().getStringExtra("ruc");

        control = new BaseDatosSqlite(this);
        initProvidedNetwork();
        initUI();
        setupSpinner();
        cargarDatosEditar();
        clickUI();
    }


    private void setupSpinner() {
        Resources resources = getResources();
        ArrayAdapter<String> pagoArrayAdapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_item, resources.getStringArray(R.array.venta));
        pagoArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item); // The drop down view
        spinner_cond_venta.setAdapter(pagoArrayAdapter);
        spinner_cond_venta.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int position, long l) {
                //
                idVenta = String.valueOf(position+1);
                if (position == 0){
                    edit_cod_pago.setText((String) adapterView.getItemAtPosition(position));
                    idPago = "18";
                    edit_cod_pago.setEnabled(false);
                }else{
                    if (!isEditing) {
                        edit_cod_pago.setText("");
                        isEditing = false;
                    }
                    edit_cod_pago.setEnabled(true);

                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        condiVenta = control.GetFullVenta();
        adapterVentas = new CondicionVentaAdapter(this, condiVenta);

        ArrayAdapter<String> ventaArrayAdapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_item, resources.getStringArray(R.array.venta));
        ventaArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item); // The drop down view
    }

    public void initUI()
    {

        toolbar=(Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitleTextColor(Color.WHITE);
        toolbar.setNavigationIcon(R.drawable.ic_arrow_back);
        toolbar.setTitle("Editar contacto");
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                finish();
            }}
        );


        edit_cod_pago.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ListadoCondicionVenta();
            }
        });
    }
    public void cargarDatosEditar()
    {
        isEditing = true;
        edit_descripcion.setText(Preferences.loadDireccionContacto("descripcion"));
        edit_nombre_contacto.setText(Preferences.loadDireccionContacto("nombre"));

        edit_direccion.setText(""+Preferences.loadDireccionLugarContacto("direccionLugar"));
        edit_correo.setText(""+Preferences.loadCorreoContacto("correo"));

        edit_telefono_fijo.setText(""+Preferences.loadNumeroFijoContacto("telefonoFijo"));
        edit_movil.setText(""+Preferences.loadNumeroFijoContacto("telefonoMovil"));

        //edit_cod_pago.setText(""+Preferences.loadCodPagoContacto("condicionPago"));
        idPago = String.valueOf(Preferences.loadCodPagoContacto("condicionPago"));
        //Log.e("condicionpago",String.valueOf(conPago));

        String condicionDePago = control.GetCondicionPago(idPago);
        Log.e("condicionpago",condicionDePago);
        edit_cod_pago.setEnabled(true);
        edit_cod_pago.setText(condicionDePago);

        idVenta = String.valueOf(Preferences.loadCodVenta("condicionVenta"));
        spinner_cond_venta.setSelection(idVenta.equals("2") ? 1 : 0);
        //int index = control.getCondicionVentaIndex(idCodVenta);



    }

    public void clickUI()
    {
        text_cancelar_contacto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        text_limpiarContacto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                limpiarCampos();
            }
        });

        guardarContacto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                boolean cancel = false;
                if(TextUtils.isEmpty(edit_nombre_contacto.getText().toString()))
                {
                    edit_nombre_contacto.setError("Falta nombre de contacto");
                    cancel = true;
                }
                if(TextUtils.isEmpty(edit_descripcion.getText().toString()))
                {
                    edit_descripcion.setError("Falta descripcion");
                    cancel = true;
                }
                if(TextUtils.isEmpty(edit_direccion.getText().toString()))
                {
                    edit_direccion.setError("Falta direccion");
                    cancel = true;
                }
                if(TextUtils.isEmpty(edit_correo.getText().toString()))
                {
                    edit_correo.setError("Falta correo electronico");
                    cancel = true;
                }
                if(TextUtils.isEmpty(edit_telefono_fijo.getText().toString()))
                {
                    edit_telefono_fijo.setError("Falta telefono fijo");
                    cancel = true;
                }
                if(TextUtils.isEmpty(edit_movil.getText().toString()))
                {
                    edit_movil.setError("Falta telefono movil");
                    cancel = true;
                }
                if(TextUtils.isEmpty(edit_cod_pago.getText().toString()))
                {
                    edit_cod_pago.setError("Falta cod pago");
                    cancel = true;
                }

                if (!cancel)
                {
                    int code=Preferences.loadCode("code");
                    Contact newContact = new Contact(clienteID,
                            code,
                            1,
                            ""+edit_descripcion.getText().toString(),
                            ""+edit_nombre_contacto.getText().toString(),
                            ""+edit_direccion.getText().toString(),
                            ""+edit_correo.getText().toString(),
                            ""+edit_telefono_fijo.getText().toString(),
                            ""+edit_movil.getText().toString(),
                            Integer.parseInt(idVenta),
                            Integer.parseInt(idPago)
                    );
                    Log.e("NewContact",newContact.toString());
                    if (Util.isNetworkAvailable(ActivityEditarContacto.this))
                        createContact(newContact);
                    else{
                        AllContacts contacts = new AllContacts(code, code, 1,
                                "" + edit_descripcion.getText().toString(),
                                "" + edit_nombre_contacto.getText().toString(),
                                "" + edit_direccion.getText().toString(),
                                "" + edit_correo.getText().toString(),
                                "" + edit_telefono_fijo.getText().toString(),
                                "" + edit_movil.getText().toString(),
                                0, "", 0, "",
                                Integer.parseInt(idVenta), spinner_cond_venta.getSelectedItem().toString(),
                                Integer.parseInt(idPago), edit_cod_pago.getText().toString()
                        );
                        control.updateClienteContact(clienteID, contacts, 2);
                        if (!Util.isNetworkAvailable(ActivityEditarContacto.this)) {
                            alertDialogo("Contacto registrado", "Operación exitosa!");
                            Handler handler = new Handler();
                            handler.postDelayed(new Runnable() {
                                @Override
                                public void run() {
                                    finish();
                                }
                            }, 3000);
                        }
                    }

                    limpiarCampos();

                }

            } });

    }
    @Override
    public void onBackPressed() {
    }

    public void ListadoCondicionVenta(){
        customDialog = new Dialog(this);
        customDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        customDialog.setCancelable(false);
        customDialog.setContentView(R.layout.notificacion_03_cargalista);
        TextView titulo = (TextView) customDialog.findViewById(R.id.titulo);
        titulo.setText("Condicion de venta");
        EditText busqueda = (EditText) customDialog.findViewById(R.id.buscar);
        busqueda.setHint("Buscar condición de venta");
        final RecyclerView lista1 = (RecyclerView) customDialog.findViewById(R.id.clientes);
        lista1.setHasFixedSize(true);

        lista1.setAdapter(adapterVentas);

        busqueda.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence s, int i, int i1, int i2) {
                condiVenta = control.GetFullVentaBusqueda(s.toString());
                adapterVentas = new CondicionVentaAdapter(ActivityEditarContacto.this, condiVenta);
                lista1.setAdapter(adapterVentas);
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
        adapterVentas = new CondicionVentaAdapter(this, condiVenta);
        adapterVentas.setOnItemClickListener(new CondicionVentaAdapter.ClickListener() {
            @Override
            public void onItemClick(int position, View v) {
                ContentValues dato = adapterVentas.lista.get(position);
                idPago = dato.get("id").toString();
                edit_cod_pago.setText(dato.get("nombre").toString());
                customDialog.dismiss();
            }
        });
        lista1.setLayoutManager(new LinearLayoutManager(this,LinearLayoutManager.VERTICAL,false));
        (customDialog.findViewById(R.id.no)).setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view)
            {
                customDialog.dismiss();
            }
        });

        assert customDialog != null;
        customDialog.show();
    }

    public void alertDialogo(String titulo, String confirmacion)
    {

        AlertDialog.Builder builder = new AlertDialog.Builder(this);

        builder.setMessage(titulo)
                .setTitle(confirmacion);

        final AlertDialog dialog = builder.create();

        dialog.show();

        // Hide after some seconds
        final Handler handler  = new Handler();
        final Runnable runnable = new Runnable() {
            @Override
            public void run() {
                if (dialog.isShowing()) {
                    dialog.dismiss();
                    finish();
                }
            }
        };

        dialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
                handler.removeCallbacks(runnable);
            }
        });

        handler.postDelayed(runnable, 1500);
    }

    public  void limpiarCampos()
    {
        edit_descripcion.setText("");
        edit_direccion.setText("");
        edit_nombre_contacto.setText("");
        edit_correo.setText("");
        edit_telefono_fijo.setText("");
        edit_movil.setText("");
        edit_cod_pago.setText("");

        edit_descripcion.setError(null);
        edit_direccion.setError(null);
        edit_nombre_contacto.setError(null);
        edit_correo.setError(null);
        edit_telefono_fijo.setError(null);
        edit_movil.setError(null);
        edit_cod_pago.setError(null);
    }

    public void createContact(Contact mContactNew) {
        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Cargando....");
        progressDialog.setTitle("Guardando datos...");
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDialog.show();

        Call<String> contact = api.createContact(mContactNew);
        contact.enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                progressDialog.dismiss();
                Log.d("Test", "response: " + response);
                if(response.isSuccessful())
                {
                    Log.d("Test", "Values: " + "bien--->");
                }else
                {
                    Log.d("Test", "Ha ocurrido un error");
                }
                switch (response.code()) {
                    case 200:
                        alertDialogo("Contacto editado","Operación exitosa!");
                        Log.d("Test", "Values: " + "--->");
                        break;
                    default:
                        Log.d("Test", "Ha ocurrido un error");
                }

            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
                Log.d("Test", "Caused: " + t);
                Log.d("Test", "Caused: " + call.request().body().toString());
                progressDialog.dismiss();
            }
        });
    }


    private void initProvidedNetwork() {
        RetrofitAdapter.mBaseUrl = Const.URL_V1;
        api = RetrofitAdapter.getApiService();
    }

}
