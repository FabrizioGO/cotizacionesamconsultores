package com.felixcabrita.cotizacionesamconsultores.net.model.responses;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Fabrizio Gagliardi on 10/4/2018.
 **/

public class Visita {

    private int id;
    private int cliente;
    private int vendedor;
    private int direccion;
    @SerializedName("fecha_inicio")
    private String fechaInicio;
    @SerializedName("fecha_fin")
    private String fechaFin;
    private String motivo;
    @SerializedName("longitud_inicio")
    private String longitudInicio;
    @SerializedName("latitud_inicio")
    private String latitudInicio;
    @SerializedName("longitud_fin")
    private String longitudFin;
    @SerializedName("latitud_fin")
    private String latitudFin;
    private int estado;
    @SerializedName("proxima_visita")
    private String proximaVisita;
    @SerializedName("razon_social")
    private String razonSocial;
    private String contacto;
    private String nombres;
    private String apellidos;

    public Visita(){

    }

    public Visita(int id, int cliente, int vendedor, int direccion,
                  String fechaInicio, String fechaFin, String motivo,
                  String longitudInicio, String latitudInicio, String longitudFin, String latitudFin,
                  int estado, String proximaVisita, String razonSocial, String contacto, String nombres, String apellidos){
        this.id = id;
        this.cliente = cliente;
        this.vendedor = vendedor;
        this.direccion = direccion;
        this.fechaInicio = fechaInicio;
        this.fechaFin = fechaFin;
        this.motivo = motivo;
        this.longitudInicio = longitudInicio;
        this.longitudFin = longitudFin;
        this.latitudInicio = latitudInicio;
        this.latitudFin = latitudFin;
        this.estado = estado;
        this.proximaVisita = proximaVisita;
        this.razonSocial = razonSocial;
        this.contacto = contacto;
        this.nombres = nombres;
        this.apellidos = apellidos;

    }

    public int getId() {
        return id;
    }

    public int getCliente() {
        return cliente;
    }

    public int getDireccion() {
        return direccion;
    }

    public int getEstado() {
        return estado;
    }

    public int getVendedor() {
        return vendedor;
    }

    public String getApellidos() {
        return apellidos;
    }

    public String getContacto() {
        return contacto;
    }

    public String getFechaFin() {
        return fechaFin;
    }

    public String getFechaInicio() {
        return fechaInicio;
    }

    public String getLatitudFin() {
        return latitudFin;
    }

    public String getLatitudInicio() {
        return latitudInicio;
    }

    public String getLongitudFin() {
        return longitudFin;
    }

    public String getLongitudInicio() {
        return longitudInicio;
    }

    public String getMotivo() {
        return motivo;
    }

    public String getNombres() {
        return nombres;
    }

    public String getProximaVisita() {
        return proximaVisita;
    }

    public String getRazonSocial() {
        return razonSocial;
    }
}
