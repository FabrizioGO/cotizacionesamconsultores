package com.felixcabrita.cotizacionesamconsultores.net.model.request;

/**
 * Created by eabp on 12/01/2018.
 */

public class Contact {


    private String ruc;

    private int code;

    private int tipo;

    private String descripcion;

    private String contacto;

    private String direccion;

    private String correo;

    private String telefono_fijo;

    private String telefono_movil;

    private int cond_venta;

    private int cond_pago;

    public Contact(String ruc, int code, int tipo, String descripcion, String contacto, String direccion, String correo, String telefono_fijo, String telefono_movil, int cond_venta, int cond_pago) {
        this.ruc = ruc;
        this.code = code;
        this.tipo = tipo;
        this.descripcion = descripcion;
        this.contacto = contacto;
        this.direccion = direccion;
        this.correo = correo;
        this.telefono_fijo = telefono_fijo;
        this.telefono_movil = telefono_movil;
        this.cond_venta = cond_venta;
        this.cond_pago = cond_pago;
    }

    public void setRuc(String ruc) {
        this.ruc = ruc;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public void setTipo(int tipo) {
        this.tipo = tipo;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public void setContacto(String contacto) {
        this.contacto = contacto;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public void setCorreo(String correo) {
        this.correo = correo;
    }

    public void setTelefono_fijo(String telefono_fijo) {
        this.telefono_fijo = telefono_fijo;
    }

    public void setTelefono_movil(String telefono_movil) {
        this.telefono_movil = telefono_movil;
    }

    public void setCond_venta(int cond_venta) {
        this.cond_venta = cond_venta;
    }

    public void setCond_pago(int cond_pago) {
        this.cond_pago = cond_pago;
    }

    public String getRuc() {
        return ruc;
    }

    public int getCode() {
        return code;
    }

    public int getTipo() {
        return tipo;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public String getContacto() {
        return contacto;
    }

    public String getDireccion() {
        return direccion;
    }

    public String getCorreo() {
        return correo;
    }

    public String getTelefono_fijo() {
        return telefono_fijo;
    }

    public String getTelefono_movil() {
        return telefono_movil;
    }

    public int getCond_venta() {
        return cond_venta;
    }

    public int getCond_pago() {
        return cond_pago;
    }

    @Override
    public String toString() {
        return "Contact{" +
                "ruc='" + ruc + '\'' +
                ", code=" + code +
                ", tipo=" + tipo +
                ", descripcion='" + descripcion + '\'' +
                ", contacto='" + contacto + '\'' +
                ", direccion='" + direccion + '\'' +
                ", correo='" + correo + '\'' +
                ", telefono_fijo='" + telefono_fijo + '\'' +
                ", telefono_movil='" + telefono_movil + '\'' +
                ", cond_venta=" + cond_venta +
                ", cond_pago=" + cond_pago +
                '}';
    }
}



