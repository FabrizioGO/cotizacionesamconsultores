package com.felixcabrita.cotizacionesamconsultores.net;

import com.felixcabrita.cotizacionesamconsultores.net.model.request.Contact;
import com.felixcabrita.cotizacionesamconsultores.net.model.responses.AllContacts;
import com.felixcabrita.cotizacionesamconsultores.net.model.responses.DetallePedido;
import com.felixcabrita.cotizacionesamconsultores.net.model.responses.ModelsSituacion;

import org.json.JSONObject;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;

/**
 * Created by eabp on 12/01/2018.
 **/

public interface ServicesContact {

    @GET("clientes/contactos/{ruc}")
    Call<List<AllContacts>> allContacts(@Path(value = "ruc") String ruc);

    @GET("clientes/situacion/{ruc}")
    Call<List<ModelsSituacion>> situacion(@Path(value = "ruc") String ruc);

    @POST("clientes/contactos/")
    Call<String> createContact(@Body Contact newContact);

    @GET("clientes/pedidos/{ruc}")
    Call<List<ModelsPedidos>> historialPedidos(@Path(value = "ruc") String ruc);

    @GET("clientes/detalle_pedido/{ruc}/{pedido}")
    Call<List<DetallePedido>> detallePedidos(@Path(value = "ruc") String ruc, @Path(value = "pedido") int pedido);
}
