package com.felixcabrita.cotizacionesamconsultores.net.model.responses;

/**
 * Created by eabp on 12/01/2018.
 */

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class AllContacts {

    @SerializedName("item")
    @Expose
    private int item;
    @SerializedName("value")
    @Expose
    private int value;
    @SerializedName("tipo")
    @Expose
    private int tipo;
    @SerializedName("descripcion")
    @Expose
    private String descripcion;
    @SerializedName("contacto")
    @Expose
    private String contacto;
    @SerializedName("direccion")
    @Expose
    private String direccion;
    @SerializedName("correo")
    @Expose
    private String correo;
    @SerializedName("telefono_fijo")
    @Expose
    private String telefonoFijo;
    @SerializedName("telefono_movil")
    @Expose
    private String telefonoMovil;
    @SerializedName("zona")
    @Expose
    private int zona;
    @SerializedName("zona_text")
    @Expose
    private String zonaText;
    @SerializedName("vendedor")
    @Expose
    private int vendedor;
    @SerializedName("vendedor_text")
    @Expose
    private String vendedorText;
    @SerializedName("cond_venta")
    @Expose
    private int condVenta;
    @SerializedName("cond_venta_text")
    @Expose
    private String condVentaText;
    @SerializedName("cond_pago")
    @Expose
    private int condPago;
    @SerializedName("cond_pago_text")
    @Expose
    private String condPagoText;

    public int getItem() {
        return item;
    }

    public void setItem(int item) {
        this.item = item;
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }

    public int getTipo() {
        return tipo;
    }

    public void setTipo(int tipo) {
        this.tipo = tipo;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getContacto() {
        return contacto;
    }

    public void setContacto(String contacto) {
        this.contacto = contacto;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public String getCorreo() {
        return correo;
    }

    public void setCorreo(String correo) {
        this.correo = correo;
    }

    public String getTelefonoFijo() {
        return telefonoFijo;
    }

    public void setTelefonoFijo(String telefonoFijo) {
        this.telefonoFijo = telefonoFijo;
    }

    public String getTelefonoMovil() {
        return telefonoMovil;
    }

    public void setTelefonoMovil(String telefonoMovil) {
        this.telefonoMovil = telefonoMovil;
    }

    public int getZona() {
        return zona;
    }

    public void setZona(int zona) {
        this.zona = zona;
    }

    public String getZonaText() {
        return zonaText;
    }

    public void setZonaText(String zonaText) {
        this.zonaText = zonaText;
    }

    public int getVendedor() {
        return vendedor;
    }

    public void setVendedor(int vendedor) {
        this.vendedor = vendedor;
    }

    public String getVendedorText() {
        return vendedorText;
    }

    public void setVendedorText(String vendedorText) {
        this.vendedorText = vendedorText;
    }

    public int getCondVenta() {
        return condVenta;
    }

    public void setCondVenta(int condVenta) {
        this.condVenta = condVenta;
    }

    public String getCondVentaText() {
        return condVentaText;
    }

    public void setCondVentaText(String condVentaText) {
        this.condVentaText = condVentaText;
    }

    public int getCondPago() {
        return condPago;
    }

    public void setCondPago(int condPago) {
        this.condPago = condPago;
    }

    public String getCondPagoText() {
        return condPagoText;
    }

    public void setCondPagoText(String condPagoText) {
        this.condPagoText = condPagoText;
    }

    public AllContacts()
    {

    }
    public AllContacts(int item, int value, int tipo, String descripcion, String contacto, String direccion, String correo, String telefonoFijo, String telefonoMovil, int zona, String zonaText, int vendedor, String vendedorText, int condVenta, String condVentaText, int condPago, String condPagoText) {
        this.item = item;
        this.value = value;
        this.tipo = tipo;
        this.descripcion = descripcion;
        this.contacto = contacto;
        this.direccion = direccion;
        this.correo = correo;
        this.telefonoFijo = telefonoFijo;
        this.telefonoMovil = telefonoMovil;
        this.zona = zona;
        this.zonaText = zonaText;
        this.vendedor = vendedor;
        this.vendedorText = vendedorText;
        this.condVenta = condVenta;
        this.condVentaText = condVentaText;
        this.condPago = condPago;
        this.condPagoText = condPagoText;
    }

    @Override
    public String toString() {
        return "AllContacts{" +
                "item=" + item +
                ", value=" + value +
                ", tipo=" + tipo +
                ", descripcion='" + descripcion + '\'' +
                ", contacto='" + contacto + '\'' +
                ", direccion='" + direccion + '\'' +
                ", correo='" + correo + '\'' +
                ", telefonoFijo='" + telefonoFijo + '\'' +
                ", telefonoMovil='" + telefonoMovil + '\'' +
                ", zona=" + zona +
                ", zonaText='" + zonaText + '\'' +
                ", vendedor=" + vendedor +
                ", vendedorText='" + vendedorText + '\'' +
                ", condVenta=" + condVenta +
                ", condVentaText='" + condVentaText + '\'' +
                ", condPago=" + condPago +
                ", condPagoText='" + condPagoText + '\'' +
                '}';
    }
}