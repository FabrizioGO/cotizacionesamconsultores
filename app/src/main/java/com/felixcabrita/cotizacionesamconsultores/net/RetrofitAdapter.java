package com.felixcabrita.cotizacionesamconsultores.net;

import com.felixcabrita.cotizacionesamconsultores.utils.Preferences;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import okhttp3.HttpUrl;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.converter.scalars.ScalarsConverterFactory;

/**
 * Created by eabp on 12/01/2018.
 */
public class RetrofitAdapter {
    public static String mBaseUrl;

    private static ServicesContact LOGIN_SERVICE;

    public static ServicesContact getApiService() {

        if (LOGIN_SERVICE == null) {
            Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl(mBaseUrl)
                    .addConverterFactory(ScalarsConverterFactory.create())
                    .addConverterFactory(GsonConverterFactory.create())
                    .client(createOkHttpClient())
                    .build();
            LOGIN_SERVICE = retrofit.create(ServicesContact.class);
        }

        return LOGIN_SERVICE;
    }

    private static OkHttpClient createOkHttpClient() {
        final OkHttpClient.Builder httpClient =
                new OkHttpClient.Builder()
                        .readTimeout(20, TimeUnit.SECONDS)
                        .connectTimeout(20, TimeUnit.SECONDS);
        httpClient.addInterceptor(new Interceptor() {
            @Override
            public Response intercept(Chain chain) throws IOException {
                final Request original = chain.request();
                final HttpUrl originalHttpUrl = original.url();

                final HttpUrl url = originalHttpUrl.newBuilder()
                        .build();

                // Request customization: add request headers
                final Request.Builder requestBuilder = original.newBuilder()
                        .addHeader("x-api-key", Preferences.getkeyUser())
                        .url(url);
                        //.addHeader("x-api-key", Preferences.getkeyUser());

                final Request request = requestBuilder.build();
                return chain.proceed(request);
            }
        });

        return httpClient.build();
    }
}
