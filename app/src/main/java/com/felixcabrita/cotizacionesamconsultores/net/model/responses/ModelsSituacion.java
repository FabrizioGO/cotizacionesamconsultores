package com.felixcabrita.cotizacionesamconsultores.net.model.responses;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Joseris on 16/01/2018.
 */

public class ModelsSituacion {

    @SerializedName("Fecha_Docmto")
    @Expose
    private String fechaDocmto;
    @SerializedName("Docmto")
    @Expose
    private String docmto;
    @SerializedName("vc_serie")
    @Expose
    private String vcSerie;
    @SerializedName("vc_nrodocumento")
    @Expose
    private String vcNrodocumento;
    @SerializedName("CODVTA")
    @Expose
    private String CODVTA;
    @SerializedName("Fecha_Vecnto")
    @Expose
    private String fechaVecnto;
    @SerializedName("Ubicacion")
    @Expose
    private String ubicacion;
    @SerializedName("Situacion")
    @Expose
    private String situacion;
    @SerializedName("Mnd")
    @Expose
    private String mnd;
    @SerializedName("Total_Deuda")
    @Expose
    private String totalDeuda;
    @SerializedName("Amortizado")
    @Expose
    private String amortizado;
    @SerializedName("Saldo")
    @Expose
    private String saldo;
    @SerializedName("VENDEDOR")
    @Expose
    private String vENDEDOR;
    @SerializedName("CLAVE")
    @Expose
    private String cLAVE;
    @SerializedName("CCB_CORDCOM")
    @Expose
    private String cCBCORDCOM;
    @SerializedName("TD")
    @Expose
    private String tD;
    @SerializedName("DOCUMENTO")
    @Expose
    private String dOCUMENTO;
    @SerializedName("VC_Descrip")
    @Expose
    private String vCDescrip;
    @SerializedName("VC_Ruc")
    @Expose
    private String vCRuc;

    public String getFechaDocmto() {
        return fechaDocmto;
    }

    public void setFechaDocmto(String fechaDocmto) {
        this.fechaDocmto = fechaDocmto;
    }

    public String getDocmto() {
        return docmto;
    }

    public void setDocmto(String docmto) {
        this.docmto = docmto;
    }

    public String getVcSerie() {
        return vcSerie;
    }

    public void setVcSerie(String vcSerie) {
        this.vcSerie = vcSerie;
    }

    public String getVcNrodocumento() {
        return vcNrodocumento;
    }

    public void setVcNrodocumento(String vcNrodocumento) {
        this.vcNrodocumento = vcNrodocumento;
    }

    public String getCODVTA() {
        return CODVTA;
    }

    public void setCODVTA(String cODVTA) {
        this.CODVTA = cODVTA;
    }

    public String getFechaVecnto() {
        return fechaVecnto;
    }

    public void setFechaVecnto(String fechaVecnto) {
        this.fechaVecnto = fechaVecnto;
    }

    public String getUbicacion() {
        return ubicacion;
    }

    public void setUbicacion(String ubicacion) {
        this.ubicacion = ubicacion;
    }

    public String getSituacion() {
        return situacion;
    }

    public void setSituacion(String situacion) {
        this.situacion = situacion;
    }

    public String getMnd() {
        return mnd;
    }

    public void setMnd(String mnd) {
        this.mnd = mnd;
    }

    public String getTotalDeuda() {
        return totalDeuda;
    }

    public void setTotalDeuda(String totalDeuda) {
        this.totalDeuda = totalDeuda;
    }

    public String getAmortizado() {
        return amortizado;
    }

    public void setAmortizado(String amortizado) {
        this.amortizado = amortizado;
    }

    public String getSaldo() {
        return saldo;
    }

    public void setSaldo(String saldo) {
        this.saldo = saldo;
    }

    public String getVENDEDOR() {
        return vENDEDOR;
    }

    public void setVENDEDOR(String vENDEDOR) {
        this.vENDEDOR = vENDEDOR;
    }

    public String getCLAVE() {
        return cLAVE;
    }

    public void setCLAVE(String cLAVE) {
        this.cLAVE = cLAVE;
    }

    public String getCCBCORDCOM() {
        return cCBCORDCOM;
    }

    public void setCCBCORDCOM(String cCBCORDCOM) {
        this.cCBCORDCOM = cCBCORDCOM;
    }

    public String getTD() {
        return tD;
    }

    public void setTD(String tD) {
        this.tD = tD;
    }

    public String getDOCUMENTO() {
        return dOCUMENTO;
    }

    public void setDOCUMENTO(String dOCUMENTO) {
        this.dOCUMENTO = dOCUMENTO;
    }

    public String getVCDescrip() {
        return vCDescrip;
    }

    public void setVCDescrip(String vCDescrip) {
        this.vCDescrip = vCDescrip;
    }

    public String getVCRuc() {
        return vCRuc;
    }

    public void setVCRuc(String vCRuc) {
        this.vCRuc = vCRuc;
    }

    public ModelsSituacion(String fechaDocmto, String docmto, String vcSerie, String vcNrodocumento, String CODVTA, String fechaVecnto, String ubicacion, String situacion, String mnd, String totalDeuda, String amortizado, String saldo, String vENDEDOR, String cLAVE, String cCBCORDCOM, String tD, String dOCUMENTO, String vCDescrip, String vCRuc) {
        this.fechaDocmto = fechaDocmto;
        this.docmto = docmto;
        this.vcSerie = vcSerie;
        this.vcNrodocumento = vcNrodocumento;
        this.CODVTA = CODVTA;
        this.fechaVecnto = fechaVecnto;
        this.ubicacion = ubicacion;
        this.situacion = situacion;
        this.mnd = mnd;
        this.totalDeuda = totalDeuda;
        this.amortizado = amortizado;
        this.saldo = saldo;
        this.vENDEDOR = vENDEDOR;
        this.cLAVE = cLAVE;
        this.cCBCORDCOM = cCBCORDCOM;
        this.tD = tD;
        this.dOCUMENTO = dOCUMENTO;
        this.vCDescrip = vCDescrip;
        this.vCRuc = vCRuc;
    }

    @Override
    public String toString() {
        return "ModelsSituacion{" +
                "fechaDocmto='" + fechaDocmto + '\'' +
                ", docmto='" + docmto + '\'' +
                ", vcSerie='" + vcSerie + '\'' +
                ", vcNrodocumento='" + vcNrodocumento + '\'' +
                ", CODVTA='" + CODVTA + '\'' +
                ", fechaVecnto='" + fechaVecnto + '\'' +
                ", ubicacion='" + ubicacion + '\'' +
                ", situacion='" + situacion + '\'' +
                ", mnd='" + mnd + '\'' +
                ", totalDeuda='" + totalDeuda + '\'' +
                ", amortizado='" + amortizado + '\'' +
                ", saldo='" + saldo + '\'' +
                ", vENDEDOR='" + vENDEDOR + '\'' +
                ", cLAVE='" + cLAVE + '\'' +
                ", cCBCORDCOM='" + cCBCORDCOM + '\'' +
                ", tD='" + tD + '\'' +
                ", dOCUMENTO='" + dOCUMENTO + '\'' +
                ", vCDescrip='" + vCDescrip + '\'' +
                ", vCRuc='" + vCRuc + '\'' +
                '}';
    }
}
