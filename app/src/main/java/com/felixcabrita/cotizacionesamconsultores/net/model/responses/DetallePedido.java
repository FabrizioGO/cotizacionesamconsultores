package com.felixcabrita.cotizacionesamconsultores.net.model.responses;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Joseris on 22/01/2018.
 */

public class DetallePedido {
    @SerializedName("Nro_Pedido")
    @Expose
    private Integer nroPedido;
    @SerializedName("Ruc")
    @Expose
    private String ruc;
    @SerializedName("FECHA_EMI")
    @Expose
    private String fECHAEMI;
    @SerializedName("Direccion_Cliente")
    @Expose
    private String direccionCliente;
    @SerializedName("Nombre_Cliente")
    @Expose
    private String nombreCliente;
    @SerializedName("NRO")
    @Expose
    private Integer nRO;
    @SerializedName("UM")
    @Expose
    private String uM;
    @SerializedName("Cantidad")
    @Expose
    private Integer cantidad;
    @SerializedName("Codigo")
    @Expose
    private String codigo;
    @SerializedName("Desccripcion")
    @Expose
    private String desccripcion;
    @SerializedName("Precio")
    @Expose
    private String precio;
    @SerializedName("Pre_Unitario")
    @Expose
    private String preUnitario;
    @SerializedName("PRELSTA")
    @Expose
    private String pRELSTA;
    @SerializedName("PESO")
    @Expose
    private Double pESO;
    @SerializedName("ped_cor_anu")
    @Expose
    private Integer pedCorAnu;
    @SerializedName("Monto_Neto")
    @Expose
    private String montoNeto;
    @SerializedName("Monto_Igv")
    @Expose
    private String montoIgv;
    @SerializedName("Percepcion")
    @Expose
    private Float percepcion;
    @SerializedName("Monto_Total")
    @Expose
    private String montoTotal;

    public Integer getNroPedido() {
        return nroPedido;
    }

    public void setNroPedido(Integer nroPedido) {
        this.nroPedido = nroPedido;
    }

    public String getRuc() {
        return ruc;
    }

    public void setRuc(String ruc) {
        this.ruc = ruc;
    }

    public String getFECHAEMI() {
        return fECHAEMI;
    }

    public void setFECHAEMI(String fECHAEMI) {
        this.fECHAEMI = fECHAEMI;
    }

    public String getDireccionCliente() {
        return direccionCliente;
    }

    public void setDireccionCliente(String direccionCliente) {
        this.direccionCliente = direccionCliente;
    }

    public String getNombreCliente() {
        return nombreCliente;
    }

    public void setNombreCliente(String nombreCliente) {
        this.nombreCliente = nombreCliente;
    }

    public Integer getNRO() {
        return nRO;
    }

    public void setNRO(Integer nRO) {
        this.nRO = nRO;
    }

    public String getUM() {
        return uM;
    }

    public void setUM(String uM) {
        this.uM = uM;
    }

    public Integer getCantidad() {
        return cantidad;
    }

    public void setCantidad(Integer cantidad) {
        this.cantidad = cantidad;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public String getDesccripcion() {
        return desccripcion;
    }

    public void setDesccripcion(String desccripcion) {
        this.desccripcion = desccripcion;
    }

    public String getPrecio() {
        return precio;
    }

    public void setPrecio(String precio) {
        this.precio = precio;
    }

    public String getPreUnitario() {
        return preUnitario;
    }

    public void setPreUnitario(String preUnitario) {
        this.preUnitario = preUnitario;
    }

    public String getPRELSTA() {
        return pRELSTA;
    }

    public void setPRELSTA(String pRELSTA) {
        this.pRELSTA = pRELSTA;
    }

    public Double getPESO() {
        return pESO;
    }

    public void setPESO(Double pESO) {
        this.pESO = pESO;
    }

    public Integer getPedCorAnu() {
        return pedCorAnu;
    }

    public void setPedCorAnu(Integer pedCorAnu) {
        this.pedCorAnu = pedCorAnu;
    }

    public String getMontoNeto() {
        return montoNeto;
    }

    public void setMontoNeto(String montoNeto) {
        this.montoNeto = montoNeto;
    }

    public String getMontoIgv() {
        return montoIgv;
    }

    public void setMontoIgv(String montoIgv) {
        this.montoIgv = montoIgv;
    }

    public Float getPercepcion() {
        return percepcion;
    }

    public void setPercepcion(Float percepcion) {
        this.percepcion = percepcion;
    }

    public String getMontoTotal() {
        return montoTotal;
    }

    public void setMontoTotal(String montoTotal) {
        this.montoTotal = montoTotal;
    }

    @Override
    public String toString() {
        return "DetallePedido{" +
                "nroPedido=" + nroPedido +
                ", ruc='" + ruc + '\'' +
                ", fECHAEMI='" + fECHAEMI + '\'' +
                ", direccionCliente='" + direccionCliente + '\'' +
                ", nombreCliente='" + nombreCliente + '\'' +
                ", nRO=" + nRO +
                ", uM='" + uM + '\'' +
                ", cantidad=" + cantidad +
                ", codigo='" + codigo + '\'' +
                ", desccripcion='" + desccripcion + '\'' +
                ", precio='" + precio + '\'' +
                ", preUnitario='" + preUnitario + '\'' +
                ", pRELSTA='" + pRELSTA + '\'' +
                ", pESO=" + pESO +
                ", pedCorAnu=" + pedCorAnu +
                ", montoNeto='" + montoNeto + '\'' +
                ", montoIgv='" + montoIgv + '\'' +
                ", percepcion=" + percepcion +
                ", montoTotal='" + montoTotal + '\'' +
                '}';
    }
}
