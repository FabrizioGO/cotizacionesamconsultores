package com.felixcabrita.cotizacionesamconsultores.net;

import com.felixcabrita.cotizacionesamconsultores.net.model.responses.ModelsSituacion;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Joseris Hostienda on 18/01/2018.
 */

public class ModelsPedidos {

    @SerializedName("pedido")
    @Expose
    private Integer pedido;
    @SerializedName("fecha")
    @Expose
    private String fecha;
    @SerializedName("tipo_pedido")
    @Expose
    private String tipoPedido;
    @SerializedName("tipo_pago")
    @Expose
    private String tipoPago;
    @SerializedName("cliente")
    @Expose
    private String cliente;
    @SerializedName("ruc")
    @Expose
    private String ruc;
    @SerializedName("direccion")
    @Expose
    private String direccion;
    @SerializedName("monto_neto")
    @Expose
    private String montoNeto;
    @SerializedName("monto_igv")
    @Expose
    private String montoIgv;
    @SerializedName("monto_total")
    @Expose
    private String montoTotal;
    @SerializedName("tipo_moneda")
    @Expose
    private String tipoMoneda;
    @SerializedName("percepcion")
    @Expose
    private Float percepcion;


    public ModelsPedidos()
    {

    }
    public ModelsPedidos(int pedido,String fecha,String tipoPedido,String cliente,String ruc,String direccion, String montoNeto,String montoIgv,String montoTotal, String tipoMoneda, float percepcion)
    {
        this.pedido=pedido;
        this.fecha=fecha;
        this.tipoPedido=tipoPedido;
        this.cliente=cliente;
        this.ruc=ruc;
        this.direccion=direccion;
        this.montoNeto=montoNeto;
        this.montoIgv=montoIgv;
        this.montoTotal=montoTotal;
        this.tipoMoneda=tipoMoneda;
        this.percepcion=percepcion;
    }
    @Override
    public String toString() {
        return "ModelsPedidos{" +
                " pedido=" + pedido +
                ", fecha=" + fecha +
                ", tipoPedido=" + tipoPedido +
                ", cliente='" + cliente + '\'' +
                ", ruc='" + ruc + '\'' +
                ", direccion='" + direccion + '\'' +
                ", montoNeto='" + montoNeto + '\'' +
                ", montoIgv='" + montoIgv + '\'' +
                ", montoTotal='" + montoTotal + '\'' +
                ", tipoMoneda=" + tipoMoneda +
                ", percepcion='" + percepcion + '\'' +
                '}';
    }
    public Integer getPedido() {
        return pedido;
    }

    public void setPedido(Integer pedido) {
        this.pedido = pedido;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public String getTipoPedido() {
        return tipoPedido;
    }

    public void setTipoPedido(String tipoPedido) {
        this.tipoPedido = tipoPedido;
    }

    public String getTipoPago() {
        return tipoPago;
    }

    public void setTipoPago(String tipoPago) {
        this.tipoPago = tipoPago;
    }

    public String getCliente() {
        return cliente;
    }

    public void setCliente(String cliente) {
        this.cliente = cliente;
    }

    public String getRuc() {
        return ruc;
    }

    public void setRuc(String ruc) {
        this.ruc = ruc;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public String getMontoNeto() {
        return montoNeto;
    }

    public void setMontoNeto(String montoNeto) {
        this.montoNeto = montoNeto;
    }

    public String getMontoIgv() {
        return montoIgv;
    }

    public void setMontoIgv(String montoIgv) {
        this.montoIgv = montoIgv;
    }

    public String getMontoTotal() {
        return montoTotal;
    }

    public void setMontoTotal(String montoTotal) {
        this.montoTotal = montoTotal;
    }

    public String getTipoMoneda() {
        return tipoMoneda;
    }

    public void setTipoMoneda(String tipoMoneda) {
        this.tipoMoneda = tipoMoneda;
    }

    public Float getPercepcion() {
        return percepcion;
    }

    public void setPercepcion(Float percepcion) {
        this.percepcion = percepcion;
    }


}
