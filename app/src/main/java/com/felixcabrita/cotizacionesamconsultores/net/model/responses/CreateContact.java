package com.felixcabrita.cotizacionesamconsultores.net.model.responses;

/**
 * Created by eabp on 12/01/2018.
*/

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class CreateContact {

    @SerializedName("success")
    @Expose
    private boolean success;

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

}
