package com.felixcabrita.cotizacionesamconsultores;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.AsyncTask;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.TextView;

import com.felixcabrita.cotizacionesamconsultores.utils.Const;
import com.felixcabrita.cotizacionesamconsultores.models.Tb_Clientes;
import com.felixcabrita.cotizacionesamconsultores.utils.Util;

import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

public class Menu_Principal extends AppCompatActivity implements View.OnClickListener {

    BaseDatosSqlite control;
    SharedPreferences pref;
    int Status = 1;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu__principal);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        pref = PreferenceManager.getDefaultSharedPreferences(this);
        Status = 1;
        control = new BaseDatosSqlite(this);
        new DatosSynco(this).execute();
    }

    private class DatosSynco extends AsyncTask<Void,Void,Integer> {

        boolean internet = true;
        Context cont;
        String urls = Const.URL_V1 + "clientes";
        public DatosSynco(Context c){
            try{
                cont = c;
            }catch (Exception e){
                System.out.println("Error1: "+e.toString());
            }

        }


        @Override
        protected void onPreExecute() {
            super.onPreExecute();

        }

        @Override
        protected Integer doInBackground(Void... voids) {
                if (Util.isNetworkAvailable(Menu_Principal.this) && Status == 1) {
                    internet = true;
                    if (control.CheckDataSync()) {
                        try {
                            String selectQuery = "SELECT " + Tb_Clientes.razon_social + "," + Tb_Clientes.ruc + "," + Tb_Clientes.contacto + "," +Tb_Clientes.code + "," +
                                    Tb_Clientes.direccion + "," + Tb_Clientes.telefono_fijo + "," + Tb_Clientes.telefono_movil + "," + Tb_Clientes.perceptor
                                    + "," + Tb_Clientes.retenedor + "," + Tb_Clientes.por_perceptor + "," + Tb_Clientes.calificacion
                                    + "," + Tb_Clientes.cond_pago + "," + Tb_Clientes.cond_venta + "," + Tb_Clientes.nombre_comercial + "," + Tb_Clientes.correo + "," + Tb_Clientes.estado
                                    + "," + Tb_Clientes.zona+ "," + Tb_Clientes.tiponegocio + "," + Tb_Clientes.sector + "," + Tb_Clientes.rubro + "," + Tb_Clientes.tipoExtaNa + " FROM " + Tb_Clientes.nombreTabla + " WHERE " + Tb_Clientes.estado + "= 0 OR " + Tb_Clientes.estado + "= 2";
                            SQLiteDatabase database = control.getWritableDatabase();
                            Cursor cursor = database.rawQuery(selectQuery, null);
                            if (cursor.moveToFirst()) {
                                do {
                                    try {
                                        HttpClient httpclient = new DefaultHttpClient();
                                        HttpPost httppost = new HttpPost(urls);
                                        httppost.setHeader("x-api-key", pref.getString("key", ""));
                                        List<NameValuePair> params = new ArrayList<>();
                                        params.add(new BasicNameValuePair("razon_social", cursor.getString(0).trim()));
                                        params.add(new BasicNameValuePair("ruc", cursor.getString(1).trim()));
                                        params.add(new BasicNameValuePair("contacto", cursor.getString(2).trim()));
                                        params.add(new BasicNameValuePair("code", cursor.getString(3).trim()));
                                        params.add(new BasicNameValuePair("direccion", cursor.getString(4).trim()));
                                        params.add(new BasicNameValuePair("telefono_fijo", cursor.getString(5).trim()));
                                        params.add(new BasicNameValuePair("telefono_movil", cursor.getString(6).trim()));
                                        params.add(new BasicNameValuePair("perceptor", cursor.getString(7)));
                                        params.add(new BasicNameValuePair("retenedor", cursor.getString(8)));
                                        params.add(new BasicNameValuePair("porcentaje", cursor.getString(9)));
                                        params.add(new BasicNameValuePair("calificacion", cursor.getString(10).trim()));
                                        params.add(new BasicNameValuePair("cond_pago", cursor.getString(11).trim()));
                                        params.add(new BasicNameValuePair("cond_venta", cursor.getString(12).trim()));
                                        params.add(new BasicNameValuePair("nombre_comercial", cursor.getString(13).trim()));
                                        params.add(new BasicNameValuePair("correo", cursor.getString(14).trim()));
                                        params.add(new BasicNameValuePair("zona", cursor.getString(16).trim()));
                                        params.add(new BasicNameValuePair("rubro", cursor.getString(19).trim()));
                                        params.add(new BasicNameValuePair("sector", cursor.getString(18).trim()));
                                        params.add(new BasicNameValuePair("tipo_cliente", cursor.getString(17).trim()));
                                        params.add(new BasicNameValuePair("tipo", cursor.getString(20).trim()));
                                        if (cursor.getString(13).compareTo("2") == 0) {
                                            params.add(new BasicNameValuePair("current_ruc", cursor.getString(1).trim()));
                                        } else {
                                            params.add(new BasicNameValuePair("current_ruc", ""));
                                        }

                                        httppost.setEntity(new UrlEncodedFormEntity(params));
                                        HttpResponse resp = httpclient.execute(httppost);
                                        if (HttpStatus.SC_ACCEPTED == resp.getStatusLine().getStatusCode() || HttpStatus.SC_OK == resp.getStatusLine().getStatusCode() ) {
                                            control.updateEstadoCliente(cursor.getString(1).trim());
                                        }
                                    } catch (Exception e) {
                                        System.out.println("Error Sync Subieda 0: " + e.toString());
                                    }

                                } while (cursor.moveToNext());
                            }
                            cursor.close();
                        } catch (Exception e) {
                            System.out.println("Error-GetFullClientes: " + e.toString());
                        }

                    }

                    try {
                        URL cliente = new URL(urls);
                        HttpURLConnection conCliente = (HttpURLConnection) cliente.openConnection();
                        conCliente.setRequestProperty("x-api-key", pref.getString("key", ""));
                        if (conCliente.getResponseCode() == HttpURLConnection.HTTP_OK) {
                            InputStream inputStreamResponse = conCliente.getInputStream();
                            String linea;
                            StringBuilder respuestaCadena = new StringBuilder();
                            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStreamResponse, "UTF-8"));
                            while ((linea = bufferedReader.readLine()) != null) {
                                respuestaCadena.append(linea);
                            }
                            // JSONObject datos = new JSONObject(respuestaCadena.toString());

                            JSONArray jsonArray = new JSONArray(respuestaCadena.toString());
                            if(jsonArray.length()>0) {
                                control.CapturaDatosClientesJSONV2(jsonArray);
                            }else{
                                control.BorradaGeneral(Tb_Clientes.nombreTabla,"");
                            }
                        }else {
                            control.BorradaGeneral(Tb_Clientes.nombreTabla, "");
                        }
                    } catch (IOException e) {
                        System.out.println("Error1: " + e.toString());
                    } catch (JSONException e) {
                        control.BorradaGeneral(Tb_Clientes.nombreTabla, "");
                        e.printStackTrace();
                    }
                }else{
                    internet = false;
                }
            return 0;
        }

    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.usuario:
                Intent i = new Intent(Menu_Principal.this, DatosUsuario.class);
                startActivity(i);
                Status = 0;
                finish();
                break;
            case R.id.clientes:
                Intent i1 = new Intent(Menu_Principal.this, ListaClientes.class);
                startActivity(i1);
                Status = 0;
                finish();
                break;
            case R.id.cerrar:
                Cerrar();
                break;
            case R.id.historial:
                Intent i3 = new Intent(Menu_Principal.this,Historial_cotizacion.class);
                i3.putExtra("volver","1");
                startActivity(i3);
                Status = 0;
                finish();
                break;
            case R.id.cotiza:
                Intent i11 = new Intent(Menu_Principal.this,ActividadPrincipal.class);
                i11.putExtra("edit",0);
                startActivity(i11);
                Status = 0;
                finish();
                break;
        }
    }

    @Override
    public void onBackPressed() {
        Cerrar();
    }

    Dialog customDialog;
    public void Cerrar() {
        customDialog = new Dialog(this);
        customDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        customDialog.setCancelable(false);
        customDialog.setContentView(R.layout.notificacion_01);
        ((TextView) customDialog.findViewById(R.id.si)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view)
            {
                customDialog.dismiss();
                Intent intent = new Intent(Menu_Principal.this,Inicio.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                Status = 0;
                startActivity(intent);
                finish();
            }
        });
        ((TextView) customDialog.findViewById(R.id.no)).setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view)
            {
                customDialog.dismiss();

            }
        });

        assert customDialog != null;
        customDialog.show();

    }


}
