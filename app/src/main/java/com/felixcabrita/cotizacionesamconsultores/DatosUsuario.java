package com.felixcabrita.cotizacionesamconsultores;

import android.app.Dialog;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.felixcabrita.cotizacionesamconsultores.utils.Const;
import com.felixcabrita.cotizacionesamconsultores.models.Tb_Usuarios;
import com.felixcabrita.cotizacionesamconsultores.utils.Util;

import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;

import java.util.ArrayList;
import java.util.List;

public class DatosUsuario extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener, View.OnClickListener {

    BaseDatosSqlite control;
    ContentValues usario;
    TextView nombre,apellido,movil,fijo,correo;
    EditText pw,repas;
    EditText tipo;
    CheckBox checkAprobar;
    String ppww="";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_datos_usuario);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);


        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();
        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        SharedPreferences pref = PreferenceManager.getDefaultSharedPreferences(this);
        View headerView = navigationView.getHeaderView(0);
        TextView tvHeaderName= (TextView) headerView.findViewById(R.id.usuario);
        tvHeaderName.setText(pref.getString("nombre","usuario"));
        checkAprobar = (CheckBox)findViewById(R.id.check_aprobar);
        checkAprobar.setChecked(pref.getInt("permiso_aprobar", 0)==1);
        tipo = (EditText)findViewById(R.id.tipo);
        nombre = (TextView)findViewById(R.id.nombres);
        apellido = (TextView)findViewById(R.id.apellidos);
        movil = (TextView)findViewById(R.id.movil);
        fijo = (TextView)findViewById(R.id.fijo);
        correo = (TextView)findViewById(R.id.correo);
        pw = (EditText) findViewById(R.id.pw);
        repas = (EditText) findViewById(R.id.repass);
        control = new BaseDatosSqlite(this);
        usario = control.GetUsuarios();
        nombre.setText(usario.get(Tb_Usuarios.tb_nombre).toString());
        movil.setText(usario.get(Tb_Usuarios.tb_telemovil).toString());
        fijo.setText(usario.get(Tb_Usuarios.tb_telefijo).toString());
        correo.setText(usario.get(Tb_Usuarios.tb_nombre).toString());
        apellido.setText(usario.get(Tb_Usuarios.tb_apellidos).toString());
        pw.setText(usario.get(Tb_Usuarios.tb_pw).toString());
        repas.setText(usario.get(Tb_Usuarios.tb_pw).toString());
        ppww = usario.get(Tb_Usuarios.tb_pw).toString();
        tipo.setText(Ttipo(Integer.parseInt(usario.get(Tb_Usuarios.tb_tipoUsuario).toString())));
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        }
    }


    String Ttipo(int i){
        String v = "Vendedor";
        switch (i){
            case 2:
                v = "Vendedor";
            break;
            case 1:
                v =  "Administrador";
            break;
            case 3:
                v =  "Supervisor";
            break;
        }
        return  v;
    }


    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        switch (id){
            case R.id.inicio:
                Intent i33 = new Intent(DatosUsuario.this, Menu_Principal.class);
                startActivity(i33);
                finish();
                break;
            case R.id.usuari:
                Intent i2 = new Intent(DatosUsuario.this,DatosUsuario.class);
                startActivity(i2);
                finish();
                break;
            case R.id.clientes:
                Intent i = new Intent(DatosUsuario.this,ListaClientes.class);
                startActivity(i);
                finish();
                break;
            case R.id.cotizar:
                Intent i1 = new Intent(DatosUsuario.this,ActividadPrincipal.class);
                i1.putExtra("edit",0);
                startActivity(i1);
                finish();
                break;
            case R.id.cerrar:
                Cerrar();
                break;
            case R.id.histo:
                Intent i3 = new Intent(DatosUsuario.this,Historial_cotizacion.class);
                i3.putExtra("volver","1");
                startActivity(i3);
                finish();
                break;
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    public void onClick(View view) {
        int id = view.getId();

        switch (id) {
            case R.id.guardar:
                if (!pw.getText().toString().trim().isEmpty() || !repas.getText().toString().trim().isEmpty()) {
                    if (validacion()) {
                        Guardar();
                    }
                } else {
                    Toast.makeText(DatosUsuario.this, "Deben llenarse los campos obligatorios(*)" +
                            "", Toast.LENGTH_SHORT).show();
                }
                break;
            case R.id.cancelar:
                Intent i = new Intent(DatosUsuario.this, Menu_Principal.class);
                startActivity(i);
                finish();
                break;
        }
    }
    boolean validacion(){
        return pw.getText().toString().trim().compareTo(repas.getText().toString().trim()) == 0;
    }
    Dialog customDialog;
    public void Cerrar() {
        customDialog = new Dialog(this);
        customDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        customDialog.setCancelable(false);
        customDialog.setContentView(R.layout.notificacion_01);
        (customDialog.findViewById(R.id.si)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view)
            {
                customDialog.dismiss();
                Intent intent = new Intent(DatosUsuario.this,Inicio.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
                finish();
            }
        });
        (customDialog.findViewById(R.id.no)).setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view)
            {
                customDialog.dismiss();

            }
        });

        assert customDialog != null;
        customDialog.show();

    }

    public void Cancelar() {
        customDialog = new Dialog(this);
        customDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        customDialog.setCancelable(false);
        customDialog.setContentView(R.layout.notificacion_01);
        TextView titulo = (TextView) customDialog.findViewById(R.id.mensaje);
        titulo.setText("¿Desea cancelar?\n" +
                "No se guardaran los datos");
        (customDialog.findViewById(R.id.si)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view)
            {
                customDialog.dismiss();
                Intent intent = new Intent(DatosUsuario.this,ActividadPrincipal.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
                finish();
            }
        });
        (customDialog.findViewById(R.id.no)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view)
            {
                customDialog.dismiss();

            }
        });

        assert customDialog != null;
        customDialog.show();

    }

    public void Guardar() {
        customDialog = new Dialog(this);
        customDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        customDialog.setCancelable(false);
        customDialog.setContentView(R.layout.notificacion_01);

        TextView titulo = (TextView) customDialog.findViewById(R.id.mensaje);
        titulo.setText("¿Desea guardar los datos del usuario?");
        TextView s = (TextView) customDialog.findViewById(R.id.si);
        s.setText("Si");
        TextView n = (TextView) customDialog.findViewById(R.id.no);
        n.setText("No");
        (customDialog.findViewById(R.id.si)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view)
            {
                control.Update(pw.getText().toString().trim());
                new DatosSynco(DatosUsuario.this).execute();
                Intent i = new Intent(DatosUsuario.this, Menu_Principal.class);
                startActivity(i);
                finish();
            }
        });
        (customDialog.findViewById(R.id.no)).setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view)
            {
                customDialog.dismiss();

            }
        });

        assert customDialog != null;
        customDialog.show();

    }


    private class DatosSynco extends AsyncTask<Void,Void,Integer> {


        Context cont;
        String urls = Const.URL_V1 + "usuarios";
        DatosSynco(Context c){
            try{
                cont = c;
            }catch (Exception e){
                System.out.println("Error1: "+e.toString());
            }
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

        }

        @Override
        protected Integer doInBackground(Void... voids) {
            if (Util.isNetworkAvailable(DatosUsuario.this)) {
                String selectQuery = "SELECT "+Tb_Usuarios.tb_codigo+","+Tb_Usuarios.tb_pw+" FROM " + Tb_Usuarios.tb_nombreTabla;
                SQLiteDatabase database = control.getWritableDatabase();
                Cursor cursor = database.rawQuery(selectQuery, null);
                try {
                    if (cursor.moveToFirst()) {
                        do {
                            HttpClient httpclient = new DefaultHttpClient();
                            HttpPost httppost = new HttpPost(urls);
                            List<NameValuePair> params = new ArrayList<>();
                            params.add(new BasicNameValuePair("id", cursor.getString(0)));
                            params.add(new BasicNameValuePair("contrasenia", cursor.getString(1)));
                            httppost.setEntity(new UrlEncodedFormEntity(params));
                            HttpResponse resp = httpclient.execute(httppost);
                            if (HttpStatus.SC_ACCEPTED == resp.getStatusLine().getStatusCode() || HttpStatus.SC_OK == resp.getStatusLine().getStatusCode() ) {
                                control.UpdateEstado();
                            }
                        } while (cursor.moveToNext());
                    }
                } catch (Exception e) {
                    System.out.println("Error-GetUsuarios: " + e.toString());
                }
                cursor.close();
            }
            return 0;
        }

        @Override
        protected void onPostExecute(Integer integer) {
            super.onPostExecute(integer);
        }

    }
}
