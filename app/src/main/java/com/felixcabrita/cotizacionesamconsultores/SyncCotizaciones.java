package com.felixcabrita.cotizacionesamconsultores;

import android.content.Context;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.preference.PreferenceManager;

import com.felixcabrita.cotizacionesamconsultores.utils.Const;
import com.felixcabrita.cotizacionesamconsultores.models.Tb_cotizacion_productos;
import com.felixcabrita.cotizacionesamconsultores.models.Tb_Cotizaciones;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * Creado por Felix Cabrita en 6/7/2017.
 */

class SyncCotizaciones extends AsyncTask<Void,Void,Integer> {

    boolean internet = true;

    String urls = Const.URL_V1 + "cotizaciones";
    private Context c;
    private SharedPreferences pref ;
    private BaseDatosSqlite cnn;
    private SharedPreferences.Editor editor;
    public SyncCotizaciones(Context c) {
        try{
            cnn = new BaseDatosSqlite(c);
            pref = PreferenceManager.getDefaultSharedPreferences(c);
            this.c = c;
            editor = pref.edit();
        }catch (Exception e){
            System.out.println("Error1: "+e.toString());
        }

    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        editor.putInt("syncC",0);
        editor.apply();
        editor.putInt("syncP",0);
        editor.apply();
        editor.putInt("syncCo",0);
        editor.apply();
    }

    @Override
    protected Integer doInBackground(Void... voids) {
            ConnectivityManager connMgr = (ConnectivityManager)
                    c.getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();
            if (networkInfo != null && networkInfo.isConnected()) {
                if (cnn.CheckDataSyncCotiza(0)) {

                    try {
                        String selectQuery = "SELECT id," + Tb_Cotizaciones.fecha + "," + Tb_Cotizaciones.fecha_registro +
                                "," + Tb_Cotizaciones.cliente + "," + Tb_Cotizaciones.vendedor + "," + Tb_Cotizaciones.cond_pago + "," +
                                Tb_Cotizaciones.cond_venta + "," + Tb_Cotizaciones.flete + "," + Tb_Cotizaciones.tiempo_validez +
                                "," + Tb_Cotizaciones.lugar_cotizacion + "," + Tb_Cotizaciones.moneda + "," + Tb_Cotizaciones.subtotal +
                                "," + Tb_Cotizaciones.monto_igv + "," + Tb_Cotizaciones.monto_total + "," + Tb_Cotizaciones.observaciones +
                                "," + Tb_Cotizaciones.por_revision + "," + Tb_Cotizaciones.items + "," + Tb_Cotizaciones.estadoSynco +
                                "," + Tb_Cotizaciones.estado + " FROM " + Tb_Cotizaciones.nombreTabla + " WHERE " + Tb_Cotizaciones.estadoSynco + " = 0";

                        SQLiteDatabase database = cnn.getWritableDatabase();
                        Cursor cursor = database.rawQuery(selectQuery, null);
                        if (cursor.moveToFirst()) {
                            do {
                                try {
                                    HttpClient httpclient = new DefaultHttpClient();
                                    HttpPost httppost = new HttpPost(urls);
                                    httppost.setHeader("x-api-key", pref.getString("key", ""));
                                    List<NameValuePair> params = new ArrayList<>();
                                    ///.add(new BasicNameValuePair("codigo", cursor.getString(0)));
                                    params.add(new BasicNameValuePair("fecha", cursor.getString(1)));
                                    params.add(new BasicNameValuePair("fecha_registro", cursor.getString(2)));
                                    params.add(new BasicNameValuePair("ruc", cursor.getString(3)));
                                    params.add(new BasicNameValuePair("vendedor", cursor.getString(4)));
                                    params.add(new BasicNameValuePair("cond_pago", cursor.getString(5)));
                                    params.add(new BasicNameValuePair("zona", cursor.getString(6)));
                                    params.add(new BasicNameValuePair("flete", cursor.getString(7)));
                                    params.add(new BasicNameValuePair("tiempo_validez", cursor.getString(8)));
                                    params.add(new BasicNameValuePair("lugar_cotizacion", cursor.getString(9)));
                                    params.add(new BasicNameValuePair("moneda", cursor.getString(10)));
                                    float igbb = Float.parseFloat(cursor.getString(12).trim()) * 0.18f;
                                    float sst = Float.parseFloat(cursor.getString(12).trim()) - igbb;
                                    params.add(new BasicNameValuePair("subtotal", String.valueOf(sst)));
                                    params.add(new BasicNameValuePair("monto_igv", String.valueOf(igbb)));
                                    params.add(new BasicNameValuePair("monto_total", cursor.getString(12)));
                                    params.add(new BasicNameValuePair("revision", cursor.getString(14)));
                                    params.add(new BasicNameValuePair("observaciones", cursor.getString(13)));
                                    String selectQuery1 = "SELECT " + Tb_cotizacion_productos.producto + "," + Tb_cotizacion_productos.cantidad + "," + Tb_cotizacion_productos.precio + "," +
                                            Tb_cotizacion_productos.produccion + "," + Tb_cotizacion_productos.cunio + "," + Tb_cotizacion_productos.subtotal + "," + Tb_cotizacion_productos.igv +
                                            "," + Tb_cotizacion_productos.total + " FROM " + Tb_cotizacion_productos.nombreTabla + " WHERE " + Tb_cotizacion_productos.cotizacion + " = " + cursor.getString(0);

                                    //
                                    Cursor cursor1 = database.rawQuery(selectQuery1, null);
                                    try {
                                        if (cursor1.moveToFirst()) {
                                            do {

                                                params.add(new BasicNameValuePair("productos[codigo][]", cursor1.getString(0)));
                                                params.add(new BasicNameValuePair("productos[cantidad][]", cursor1.getString(1)));
                                                params.add(new BasicNameValuePair("productos[precio][]", cursor1.getString(2)));
                                                params.add(new BasicNameValuePair("productos[para][]", cursor1.getString(3)));
                                                params.add(new BasicNameValuePair("productos[cunio][]", cursor1.getString(4)));
                                                params.add(new BasicNameValuePair("productos[subtotal][]", cursor1.getString(5)));
                                                params.add(new BasicNameValuePair("productos[igv][]", cursor1.getString(6)));
                                                params.add(new BasicNameValuePair("productos[total][]", cursor1.getString(7)));
                                            } while (cursor1.moveToNext());
                                        }
                                    } catch (Exception e) {
                                        System.out.println("Error-GetItemsCotizacion(): " + e.toString());
                                    }

                                    httppost.setEntity(new UrlEncodedFormEntity(params));
                                    HttpResponse resp = httpclient.execute(httppost);
                                    HttpEntity ent = resp.getEntity();/*y obtenemos una respuesta*/
                                    if (HttpStatus.SC_ACCEPTED == resp.getStatusLine().getStatusCode() || HttpStatus.SC_OK == resp.getStatusLine().getStatusCode()) {
                                        //cnn.updateEstadoCotiza(cursor.getString(0));
                                    }

                                    System.out.println("sube0: " + resp.getStatusLine().toString() + " " + EntityUtils.toString(ent) + " ) ");
                                } catch (Exception e) {
                                    System.out.println("Error Sync Subieda 0: " + e.toString());
                                }

                            } while (cursor.moveToNext());
                        }
                        cursor.close();
                    } catch (Exception e) {
                        System.out.println("Error-GetFullClientes: " + e.toString());
                    }

                }
            }else{
                internet = false;
            }
        return 0;
    }

    @Override
    protected void onPostExecute(Integer integer) {
        super.onPostExecute(integer);

    }


}